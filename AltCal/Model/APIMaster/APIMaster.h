//
//  APIMaster.h
//  AltCal
//
//  Created by Zaeem Khatib on 27/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "Reachability.h"

typedef void (^APICompletionHandler)(NSDictionary* responseDict);



@interface APIMaster : NSObject
{
    NSURLSessionDataTask *requestObject;
}

+(APIMaster*)getSharedInstance;
-(void)callAPIWithURL : (NSString*)url andPostDictionary : (NSDictionary*)parameters andBaseURL : (NSString*)baseURL WithCallBackResponse: (void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

-(void)callAPIWithNoParametrsAndURL : (NSString*)url andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

-(void) callAPIWithPUTMethodAndURL : (NSString*)url andPostDictionary : (NSDictionary*)parameters andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

-(void) callAPIWithDELETEMethodAndURL : (NSString*)url andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

-(void)cancelAllAPIs;

-(void)callAPIWithNoParametrsFoRGoogleAndURL : (NSString*)url withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

@end
