//
//  UIFunction.h
//  AltCal
//
//  Created by Zaeem Khatib on 26/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constants.h"
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import "Reachability.h"

@interface UIFunction : NSObject



+(UIButton*)createButton:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor image:(UIImage*)image title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor;

+(UILabel*)createLable:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor title:(NSString*)title font:(UIFont*)font titleColor:(UIColor*)titleColor;

+(UIImageView*)createUIImageView:(CGRect)frame backgroundColor:(UIColor*)backgroundColor image:(UIImage*)image isLogo:(BOOL)isLogo;

+(BOOL) validateEmail:(NSString *)checkString;

+(UIView*)createHeader:(CGRect)frame bckgroundColor:(UIColor*)backgroundColor;

+(UIView*)createUIViews : (CGRect)frame bckgroundColor:(UIColor*)backgroundColor;

+(UITextField*)createTextField : (CGRect)frame font:(UIFont*)font placeholder:(NSString*)placeholder;

+(UIActivityIndicatorView*)createActivityIndicatorView :(CGRect)frame;

+(NSString*) autoDetectCountryCode;

+(NSMutableArray*)func_GetAllCountries_Name;

+(NSMutableArray*)func_GetAllCountries_Code;

+ (BOOL)stringContainsEmoji:(NSString *)string;

+(NSAttributedString*)addSpacingInSring : (NSString*)string;

+ (CGFloat)targetSize:(CGFloat)targetSuperviewSize targetSubSize:(CGFloat)targetSubviewSize currentSupSize:(CGFloat)currentSizeOfSuperview;

+(void)func_Alert_InternetUnavailable;

+(void)func_CouldNotConnectWithServer;

+(NSMutableDictionary*)removeAllNULLValuesFromDictionary : (NSMutableDictionary*)dicitonaryGetAQuotation; // REMOVE ALL NULL VALUES

+(NSString*)convertDictionaryToJSON : (id) dictionary;

+(NSString*)getCurrentTimeInString;

+(NSString*)getCurrentDateInString;

@end
