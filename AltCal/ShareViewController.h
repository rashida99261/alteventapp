//
//  ShareViewController.h
//  AltCal
//
//  Created by Zaeem Khatib on 25/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import <Toast/UIView+Toast.h>
#import <Photos/Photos.h>
#import "APIMaster.h"
#import "DGActivityIndicatorView.h"
#import <SafariServices/SafariServices.h>
#import <Messages/Messages.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMessageComposeViewController.h>
#import "IDMPhotoBrowser.h"
#import <Social/Social.h>

@interface ShareViewController : UIViewController

@end
