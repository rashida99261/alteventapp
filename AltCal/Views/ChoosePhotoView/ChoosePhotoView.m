//
//  ChoosePhotoView.m
//  AltCal
//
//  Created by Zaeem Khatib on 26/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import "ChoosePhotoView.h"

@implementation ChoosePhotoView
{
    UIView *mainView;
    UIView *_tapableView;
}

@synthesize choosePhotoDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.backgroundColor =[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0.1];
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 300)];
        }
        else
        {
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 200)];
        }
        mainView.backgroundColor = [UIColor whiteColor];
        [self addSubview:mainView];
        
        
        [UIView animateWithDuration:.3f animations:^{
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                mainView.frame = CGRectMake(0, self.frame.size.height-300, self.frame.size.width, 300);
            }
            else
            {
                mainView.frame = CGRectMake(0, self.frame.size.height-200, self.frame.size.width, 200);
            }
            self.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0.4];
        } completion:^(BOOL finished) {
        }];
        
        
        _tapableView = [[UIView alloc]init];
        _tapableView.backgroundColor = [UIColor clearColor];
        _tapableView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-mainView.frame.size.height);
        UITapGestureRecognizer *tapToRemoveAlert = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeChoosePhotoView)];
        [_tapableView addGestureRecognizer:tapToRemoveAlert];
        [self addSubview:_tapableView];
        
        
        
        UIButton *btnCamera = [UIFunction createButton:CGRectMake(0, 0, mainView.frame.size.width, mainView.frame.size.height/4.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Take a new photo" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnCamera addTarget:self action:@selector(choosePhotoFromCameraButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnCamera];
        [self addDividerAtView : btnCamera];
        
        
        UIButton *btnGallery = [UIFunction createButton:CGRectMake(0, btnCamera.frame.size.height+btnCamera.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/4.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Choose from Gallery" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnGallery addTarget:self action:@selector(choosePhotoFromGalleryButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnGallery];
        [self addDividerAtView : btnGallery];
        
        UIButton *btnUpload = [UIFunction createButton:CGRectMake(0, btnGallery.frame.size.height+btnGallery.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/4.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Upload Document" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnUpload addTarget:self action:@selector(uploadDocumentButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnUpload];
        [self addDividerAtView : btnUpload];
        
        
        UIButton *btnCancel = [UIFunction createButton:CGRectMake(0, btnUpload.frame.size.height+btnUpload.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/4.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Cancel" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor redColor]];
        [btnCancel addTarget:self action:@selector(removeChoosePhotoView) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnCancel];
        [self addDividerAtView : btnCancel];
    }
    
    return self;
}

-(void)addDividerAtView : (UIView*)view
{
    UIView *viewDivider = [UIFunction createUIViews:CGRectMake(10, view.frame.size.height+view.frame.origin.y, mainView.frame.size.width-20, 1) bckgroundColor:[UIColor colorWithRed:230.0/255 green:230.0/255 blue:230.0/255 alpha:1.0]];
    [mainView addSubview:viewDivider];
}

#pragma mark
#pragma mark removeChoosePhotoView
-(void)removeChoosePhotoView
{
    [UIView animateWithDuration:.3f animations:^{
        mainView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height);
        self.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)choosePhotoFromCameraButtonPressed
{
    [self removeChoosePhotoView];
    [choosePhotoDelegate choosePhotoFromCameraButtonPressed];
}

-(void)choosePhotoFromGalleryButtonPressed
{
    [self removeChoosePhotoView];
    [choosePhotoDelegate choosePhotoFromGalleryButtonPressed];
}

-(void)uploadDocumentButtonPressed
{
    [self removeChoosePhotoView];
    [choosePhotoDelegate uploadDocumentButtonPressed];
}

@end
