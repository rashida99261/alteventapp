//
//  ChoosePhotoView.h
//  AltCal
//
//  Created by Zaeem Khatib on 26/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFunction.h"
#import "Constants.h"

@protocol choosePhotoMethod;

@interface ChoosePhotoView : UIView
{
    id <choosePhotoMethod> choosePhotoDelegate;
}
-(void) removeChoosePhotoView;

@property (retain) id<choosePhotoMethod> choosePhotoDelegate;
@end

@protocol choosePhotoMethod <NSObject>

- (void)choosePhotoFromCameraButtonPressed;
- (void)choosePhotoFromGalleryButtonPressed;
- (void)uploadDocumentButtonPressed;

@end
