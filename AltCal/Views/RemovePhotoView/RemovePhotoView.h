//
//  RemovePhotoView.h
//  AltCal
//
//  Created by Zaeem Khatib on 26/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFunction.h"
#import "Constants.h"

@protocol removePhotoMethod;

@interface RemovePhotoView : UIView
{
    id <removePhotoMethod> removePhotoDelegate;
}

-(void) removePhotoViewFunction;

@property (retain) id<removePhotoMethod> removePhotoDelegate;
@end

@protocol removePhotoMethod <NSObject>

- (void)viewPhotoButtonPressed;
- (void)removePhotoButtonPressed;

@end
