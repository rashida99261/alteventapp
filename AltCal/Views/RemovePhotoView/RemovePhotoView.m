//
//  RemovePhotoView.m
//  AltCal
//
//  Created by Zaeem Khatib on 26/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import "RemovePhotoView.h"

@implementation RemovePhotoView
{
    UIView *mainView;
    UIView *_tapableView;
}

@synthesize removePhotoDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.backgroundColor =[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0.1];
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 225)];
        }
        else
        {
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 150)];
        }
        mainView.backgroundColor = [UIColor whiteColor];
        [self addSubview:mainView];
        
        
        [UIView animateWithDuration:.3f animations:^{
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                mainView.frame = CGRectMake(0, self.frame.size.height-225, self.frame.size.width, 225);
            }
            else
            {
                mainView.frame = CGRectMake(0, self.frame.size.height-150, self.frame.size.width, 150);
            }
            self.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0.4];
        } completion:^(BOOL finished) {
        }];
        
        
        _tapableView = [[UIView alloc]init];
        _tapableView.backgroundColor = [UIColor clearColor];
        _tapableView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-mainView.frame.size.height);
        UITapGestureRecognizer *tapToRemoveAlert = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removePhotoViewFunction)];
        [_tapableView addGestureRecognizer:tapToRemoveAlert];
        [self addSubview:_tapableView];
        
        
        
        UIButton *btnViewPhoto = [UIFunction createButton:CGRectMake(0, 0, mainView.frame.size.width, mainView.frame.size.height/3.0) bckgroundColor:[UIColor clearColor] image:nil title:@"View Attachment(s)" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnViewPhoto addTarget:self action:@selector(viewPhotoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnViewPhoto];
        [self addDividerAtView : btnViewPhoto];
        
        
        UIButton *btnRemovePhoto = [UIFunction createButton:CGRectMake(0, btnViewPhoto.frame.size.height+btnViewPhoto.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/3.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Remove Attachment" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnRemovePhoto addTarget:self action:@selector(removePhotoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnRemovePhoto];
        [self addDividerAtView : btnRemovePhoto];
        
        
        UIButton *btnCancel = [UIFunction createButton:CGRectMake(0, btnRemovePhoto.frame.size.height+btnRemovePhoto.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/3.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Cancel" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor redColor]];
        [btnCancel addTarget:self action:@selector(removePhotoViewFunction) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnCancel];
        [self addDividerAtView : btnCancel];
    }
    
    return self;
}

-(void)addDividerAtView : (UIView*)view
{
    UIView *viewDivider = [UIFunction createUIViews:CGRectMake(10, view.frame.size.height+view.frame.origin.y, mainView.frame.size.width-20, 1) bckgroundColor:[UIColor colorWithRed:230.0/255 green:230.0/255 blue:230.0/255 alpha:1.0]];
    [mainView addSubview:viewDivider];
}

#pragma mark
#pragma mark removeChoosePhotoView
-(void)removePhotoViewFunction
{
    [UIView animateWithDuration:.3f animations:^{
        mainView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height);
        self.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)viewPhotoButtonPressed
{
    [self removePhotoViewFunction];
    [removePhotoDelegate viewPhotoButtonPressed];
}

-(void)removePhotoButtonPressed
{
    [self removePhotoViewFunction];
    [removePhotoDelegate removePhotoButtonPressed];
}



@end
