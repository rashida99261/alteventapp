//
//  MultiOptionView.m
//  AltCal
//
//  Created by Zaeem Khatib on 26/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import "MultiOptionView.h"

@implementation MultiOptionView
{
    UIView *mainView;
    UIView *_tapableView;
}

@synthesize MultiOptionViewDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.backgroundColor =[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0.1];
        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 450)];
        }
        else
        {
            mainView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 300)];
        }
        mainView.backgroundColor = [UIColor whiteColor];
        [self addSubview:mainView];
        
        
        [UIView animateWithDuration:.3f animations:^{
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                mainView.frame = CGRectMake(0, self.frame.size.height-450, self.frame.size.width, 450);
            }
            else
            {
                mainView.frame = CGRectMake(0, self.frame.size.height-300, self.frame.size.width, 300);
            }
            self.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0.4];
        } completion:^(BOOL finished) {
        }];
        
        
        _tapableView = [[UIView alloc]init];
        _tapableView.backgroundColor = [UIColor clearColor];
        _tapableView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-mainView.frame.size.height);
        UITapGestureRecognizer *tapToRemoveAlert = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeMultiOptionViewFunction)];
        [_tapableView addGestureRecognizer:tapToRemoveAlert];
        [self addSubview:_tapableView];
        
        
        
        UIButton *btnCamera = [UIFunction createButton:CGRectMake(0, 0, mainView.frame.size.width, mainView.frame.size.height/6.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Take a new photo" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnCamera addTarget:self action:@selector(choosePhotoFromCameraButtonPressedFromMultiOptionView) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnCamera];
        [self addDividerAtView : btnCamera];
        
        
        UIButton *btnGallery = [UIFunction createButton:CGRectMake(0, btnCamera.frame.size.height+btnCamera.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/6.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Choose from Gallery" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnGallery addTarget:self action:@selector(choosePhotoFromGalleryButtonPressedFromMultiOptionView) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnGallery];
        [self addDividerAtView : btnGallery];
        
        UIButton *btnUpload = [UIFunction createButton:CGRectMake(0, btnGallery.frame.size.height+btnGallery.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/6.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Upload Document" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnUpload addTarget:self action:@selector(uploadDocumentButtonPressedFromMultiOptionView) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnUpload];
        [self addDividerAtView : btnUpload];
        
        
        UIButton *btnViewPhoto = [UIFunction createButton:CGRectMake(0, btnUpload.frame.size.height+btnUpload.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/6.0) bckgroundColor:[UIColor clearColor] image:nil title:@"View Attachment(s)" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnViewPhoto addTarget:self action:@selector(viewPhotoButtonPressedFromMultiOptionView) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnViewPhoto];
        [self addDividerAtView : btnViewPhoto];
        
        
        UIButton *btnRemovePhoto = [UIFunction createButton:CGRectMake(0, btnViewPhoto.frame.size.height+btnViewPhoto.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/6.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Remove Attachment" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor blackColor]];
        [btnRemovePhoto addTarget:self action:@selector(removePhotoButtonPressedFromMultiOptionView) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnRemovePhoto];
        [self addDividerAtView : btnRemovePhoto];
        
        
        UIButton *btnCancel = [UIFunction createButton:CGRectMake(0, btnRemovePhoto.frame.size.height+btnRemovePhoto.frame.origin.y, mainView.frame.size.width, mainView.frame.size.height/6.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Cancel" font:[UIFont fontWithName:fontRegular size:[UIFunction targetSize:TargetSize.height targetSubSize:16.0 currentSupSize:SCREEN_HEIGHT]] titleColor:[UIColor redColor]];
        [btnCancel addTarget:self action:@selector(removeMultiOptionViewFunction) forControlEvents:UIControlEventTouchUpInside];
        [mainView addSubview:btnCancel];
        [self addDividerAtView : btnCancel];
    }
    
    return self;
}

-(void)addDividerAtView : (UIView*)view
{
    UIView *viewDivider = [UIFunction createUIViews:CGRectMake(10, view.frame.size.height+view.frame.origin.y, mainView.frame.size.width-20, 1) bckgroundColor:[UIColor colorWithRed:230.0/255 green:230.0/255 blue:230.0/255 alpha:1.0]];
    [mainView addSubview:viewDivider];
}

- (void)choosePhotoFromCameraButtonPressedFromMultiOptionView
{
    [self removeMultiOptionViewFunction];
    [MultiOptionViewDelegate choosePhotoFromCameraButtonPressedFromMultiOptionView];
}
- (void)choosePhotoFromGalleryButtonPressedFromMultiOptionView
{
    [self removeMultiOptionViewFunction];
    [MultiOptionViewDelegate choosePhotoFromGalleryButtonPressedFromMultiOptionView];
}
- (void)uploadDocumentButtonPressedFromMultiOptionView
{
    [self removeMultiOptionViewFunction];
    [MultiOptionViewDelegate uploadDocumentButtonPressedFromMultiOptionView];
}
- (void)viewPhotoButtonPressedFromMultiOptionView
{
    [self removeMultiOptionViewFunction];
    [MultiOptionViewDelegate viewPhotoButtonPressedFromMultiOptionView];
}
- (void)removePhotoButtonPressedFromMultiOptionView
{
    [self removeMultiOptionViewFunction];
    [MultiOptionViewDelegate removePhotoButtonPressedFromMultiOptionView];
}

#pragma mark
#pragma mark
-(void)removeMultiOptionViewFunction
{
    [UIView animateWithDuration:.3f animations:^{
        mainView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height);
        self.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0];
        
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}




@end
