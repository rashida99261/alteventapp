//
//  MultiOptionView.h
//  AltCal
//
//  Created by Zaeem Khatib on 26/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIFunction.h"
#import "Constants.h"

@protocol MultiOptionViewMethod;

@interface MultiOptionView : UIView
{
    id <MultiOptionViewMethod> MultiOptionViewDelegate;
}
-(void) removeMultiOptionViewFunction;
@property (retain) id<MultiOptionViewMethod> MultiOptionViewDelegate;

@end


@protocol MultiOptionViewMethod <NSObject>

- (void)choosePhotoFromCameraButtonPressedFromMultiOptionView;
- (void)choosePhotoFromGalleryButtonPressedFromMultiOptionView;
- (void)uploadDocumentButtonPressedFromMultiOptionView;
- (void)viewPhotoButtonPressedFromMultiOptionView;
- (void)removePhotoButtonPressedFromMultiOptionView;

@end
