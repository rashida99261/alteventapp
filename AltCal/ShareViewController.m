//
//  ShareViewController.m
//  AltCal
//
//  Created by Zaeem Khatib on 25/10/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import "ShareViewController.h"
#import "UIFunction.h"
#import "MultiOptionView.h"
#import "ChoosePhotoView.h"
#import "RemovePhotoView.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "Constants.h"
#import <WebKit/WebKit.h>


@interface ShareViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, choosePhotoMethod, UINavigationControllerDelegate, UIImagePickerControllerDelegate, removePhotoMethod, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate, UISearchBarDelegate, UIDocumentMenuDelegate, UIDocumentPickerDelegate,WKNavigationDelegate , UIPickerViewDataSource,UIPickerViewDelegate, MultiOptionViewMethod, MFMessageComposeViewControllerDelegate>

@property (nonatomic, strong) UICollectionView *collectionViewForImages;

@end


@implementation ShareViewController
{
    MultiOptionView *viewMultipleOptions;
    UILabel *lblEventsCount;
    UILabel *lblNotificationsCount;
    float widthOfCollectionView;
    ChoosePhotoView *viewToChoosePhoto;
    NSMutableDictionary *dictionaryCreateEvent;
    NSInteger indexOfImage;
    RemovePhotoView *viewFullSizeOrRemovePhoto;
    UITableView *tableViewCreateEvent;
    UIView *backGroundViewForPicker;
    GMSPlacesClient *_placesClient;
    
    UITableView *tableViewSearch;
    NSMutableArray *arraySearch;
    UIView *viewForGoogleSearchAPI;
    UIButton *btnAddAttachment;
    UIView *viewForWebView;
    UIView *viewForCountryCodePicker;
    NSArray *arrayAllCodes;
    NSArray *arrayAllCountries;
    
    NSString *tempEventStartTime;
    NSString *tempEventEndTime;
    DGActivityIndicatorView *activityIndicatorView;
    
}

#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)fetchUserLocation
{
    
}

-(void)createDictionaryForEvent
{
    NSArray *arrayImages = [[NSArray alloc]initWithObjects:@"",@"",@"",@"",@"", nil];
    dictionaryCreateEvent = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                             @"",@"event_city",
                             @"",@"event_topic",
                             @"",@"event_name",
                             @"",@"event_date",
                             @"",@"event_start_time",
                             @"",@"event_end_time",
                             @"",@"event_address_first",
                             @"",@"event_address_second",
                             @"",@"event_zip_code",
                             @"",@"event_phone_number",
                             @"",@"event_website",
                             @"",@"event_description",
                             arrayImages, @"event_images",
                             @"", @"latitude",
                             @"", @"longitude",
                             @"", @"user_country_code",
                             nil];
    
    if ([UIFunction autoDetectCountryCode].length>0)
    {
        [dictionaryCreateEvent setObject:[NSString stringWithFormat:@"+%@",[UIFunction autoDetectCountryCode]] forKey:@"user_country_code"];
    }
    else
    {
        [dictionaryCreateEvent setObject:@"+1" forKey:@"user_country_code"];
    }
    
    NSLog(@"____ %@",dictionaryCreateEvent );
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    [GMSPlacesClient provideAPIKey:kiOSKeyForGoogleAPI];
    NSLog(@"     ");
    self.view.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    widthOfCollectionView = 0.0;
    [self performSelector:@selector(fetchUserLocation) withObject:nil afterDelay:1.0];
    [self createDictionaryForEvent];
    _placesClient = [[GMSPlacesClient alloc] init];
    arrayAllCodes = [[UIFunction func_GetAllCountries_Code]mutableCopy];
    arrayAllCountries = [[UIFunction func_GetAllCountries_Name]mutableCopy];
    
    // LOGO
    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake(self.view.frame.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [self.view addSubview:imgViewLogo];
    
    UIButton *btnBack = [UIButton buttonWithType: UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44, 44);
    btnBack.backgroundColor = [UIColor clearColor];
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    // ADD EVENT
    UIView *viewAddEvent = [UIFunction createUIViews:CGRectMake(0, 80, self.view.frame.size.width, 30) bckgroundColor:[UIColor clearColor]];
    [self.view addSubview:viewAddEvent];
    
    UILabel *lblAddEvent = [UIFunction createLable:CGRectMake(viewAddEvent.frame.size.width/2.0-65.0, 5, 130, viewAddEvent.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:@"ADD EVENT" font:[UIFont fontWithName:fontLight size:20.0] titleColor:[UIColor whiteColor]];
    lblAddEvent.textAlignment = NSTextAlignmentCenter;
    [viewAddEvent addSubview:lblAddEvent];
    
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-15, viewAddEvent.frame.size.height/2.0-5.0, 10.0, 10.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-35, viewAddEvent.frame.size.height/2.0-3.5, 7.0, 7.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-50, viewAddEvent.frame.size.height/2.0-2.5, 5.0, 5.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width, viewAddEvent.frame.size.height/2.0-5.0, 10.0, 10.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width+20, viewAddEvent.frame.size.height/2.0-3.5, 7.0, 7.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width+40, viewAddEvent.frame.size.height/2.0-2.5, 5.0, 5.0) atView:viewAddEvent];
    
    
    // COLLECTION VIEW
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    self.collectionViewForImages = [[UICollectionView alloc] initWithFrame:CGRectMake(10, self.view.frame.size.height-110, self.view.frame.size.width-20, 100) collectionViewLayout:layout];
    [self.collectionViewForImages setDataSource:self];
    [self.collectionViewForImages setDelegate:self];
    [self.collectionViewForImages registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [self.collectionViewForImages setBackgroundColor:[UIColor clearColor]];
    self.collectionViewForImages.scrollEnabled = YES;
    self.collectionViewForImages.tag = 69802140;
    self.collectionViewForImages.pagingEnabled = YES;
    self.collectionViewForImages.showsHorizontalScrollIndicator = NO;
    self.collectionViewForImages.bounces = NO;
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self.collectionViewForImages setCollectionViewLayout:layout];
    [self.view addSubview:self.collectionViewForImages];
    widthOfCollectionView = self.collectionViewForImages.frame.size.width; // get widh of collection view
    
    
    // ****** View Create Event ********
    UIView *viewCreateEventBackGround = [UIFunction createUIViews:CGRectMake(self.collectionViewForImages.frame.origin.x, viewAddEvent.frame.size.height+viewAddEvent.frame.origin.y+60, self.collectionViewForImages.frame.size.width, self.collectionViewForImages.frame.origin.y-viewAddEvent.frame.size.height-viewAddEvent.frame.origin.y-60) bckgroundColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
    viewCreateEventBackGround.layer.cornerRadius = 5.0;
    [self.view addSubview:viewCreateEventBackGround];
    
    
    // ****** Add Attachment ********
    btnAddAttachment = [UIButton buttonWithType: UIButtonTypeCustom];
    btnAddAttachment.frame = CGRectMake(viewCreateEventBackGround.frame.size.width/2.0-35.0, viewCreateEventBackGround.frame.origin.y-45.0, 90.0, 90.0);
    btnAddAttachment.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    [btnAddAttachment setImage:[UIImage imageNamed:@"upload_image"] forState:UIControlStateNormal];
    [btnAddAttachment addTarget:self action:@selector(addAttachmentButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    btnAddAttachment.layer.cornerRadius = btnAddAttachment.frame.size.height/2.0;
    btnAddAttachment.clipsToBounds = true;
    btnAddAttachment.contentMode =  UIViewContentModeScaleAspectFill;
    [self.view addSubview:btnAddAttachment];
    
    [self createCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x-3, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0), 4.0, 4.0 ) atView:self.view];
    [self createCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)-8, 5.0, 5.0 ) atView:self.view];
    [self createCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x+4, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)-16, 5.5, 5.5 ) atView:self.view];
    
    
    [self createBlackCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x+btnAddAttachment.frame.size.width, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)+25, 4.0, 4.0 ) atView:self.view];
    [self createBlackCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x+btnAddAttachment.frame.size.width-2, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)+33, 5.0, 5.0 ) atView:self.view];
    [self createBlackCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x+btnAddAttachment.frame.size.width-7, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)+41, 5.5, 5.5 ) atView:self.view];
    
    
    
    
    
    // ****** SAVE ********
    UIButton *btnSave = [UIFunction createButton:CGRectMake(viewCreateEventBackGround.frame.size.width/2.0-45.0, viewCreateEventBackGround.frame.size.height-30, 90, 30) bckgroundColor:[UIColor clearColor] image: nil title:@"SAVE" font: [UIFont fontWithName:fontRegular size:14.0] titleColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255 alpha:1.0]];
    [btnSave addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [viewCreateEventBackGround addSubview:btnSave];
    
    
    // ****** Event City and Topic ********
    NSString *event_city = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_city"]];
    NSString *event_topic = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_topic"]];
    
    UITextField *txtFieldCity = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, 80, 30)];
    txtFieldCity.placeholder = @"City";
    txtFieldCity.tag = 74300;
    txtFieldCity.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    txtFieldCity.font = [UIFont fontWithName:fontLight size:14];
    txtFieldCity.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    txtFieldCity.delegate = self;
    txtFieldCity.text = event_city;
    txtFieldCity.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtFieldCity.textAlignment = NSTextAlignmentCenter;
    txtFieldCity.returnKeyType = UIReturnKeyNext;
    txtFieldCity.keyboardType = UIKeyboardTypeDefault;
    txtFieldCity.autocapitalizationType = UITextAutocapitalizationTypeWords;
    txtFieldCity.autocorrectionType = UITextAutocorrectionTypeNo;
    [viewCreateEventBackGround addSubview:txtFieldCity];
    txtFieldCity.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"City" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
    
    UITextField *txtFieldTopic = [[UITextField alloc]initWithFrame:CGRectMake(viewCreateEventBackGround.frame.size.width-90, 5, 80, 30)];
    txtFieldTopic.placeholder = @"Topic";
    txtFieldTopic.tag = 74301;
    txtFieldTopic.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    txtFieldTopic.font = [UIFont fontWithName:fontLight size:14];
    txtFieldTopic.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    txtFieldTopic.delegate = self;
    txtFieldTopic.text = event_topic;
    txtFieldTopic.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtFieldTopic.textAlignment = NSTextAlignmentCenter;
    txtFieldTopic.returnKeyType = UIReturnKeyNext;
    txtFieldTopic.keyboardType = UIKeyboardTypeDefault;
    txtFieldTopic.autocapitalizationType = UITextAutocapitalizationTypeWords;
    txtFieldTopic.autocorrectionType = UITextAutocorrectionTypeNo;
    [viewCreateEventBackGround addSubview:txtFieldTopic];
    txtFieldTopic.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Topic" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
    
    
    // Table View
    [tableViewCreateEvent removeFromSuperview];
    tableViewCreateEvent = nil;
    tableViewCreateEvent = [self createTableView:CGRectMake(5, txtFieldCity.frame.size.height+txtFieldCity.frame.origin.y+10, viewCreateEventBackGround.frame.size.width-10, btnSave.frame.origin.y-txtFieldCity.frame.size.height-txtFieldCity.frame.origin.y-10) backgroundColor:[UIColor clearColor]];
    [viewCreateEventBackGround addSubview:tableViewCreateEvent];
    tableViewCreateEvent.tag = 457548;
    tableViewCreateEvent.contentSize = [tableViewCreateEvent sizeThatFits:CGSizeMake(CGRectGetWidth(tableViewCreateEvent.bounds), CGFLOAT_MAX)];
    [tableViewCreateEvent setContentInset:UIEdgeInsetsMake(0, 0, 130, 0)];
    [self checkLogin];
    [self fetchImages];
}


#pragma mark
#pragma mark Create Table
-(UITableView*) createTableView : (CGRect)frame backgroundColor:(UIColor*)backgroundColor
{
    UITableView *_tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    _tableView.dataSource=self;
    _tableView.delegate = self;
    _tableView.backgroundColor=backgroundColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.alwaysBounceVertical = NO;
    [_tableView setAllowsSelection:YES];
    return _tableView;
}

#pragma mark
#pragma mark Alert Function
- (void) showAlertWithMessage : (NSString*)message
{
    [self.view makeToast:message duration:2.0 position:CSToastPositionBottom];
}


#pragma mark
#pragma mark AlertController Function
- (void) showAlertControllerWithMessage : (NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Event Saved" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"View Event" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *current_date = [UIFunction getCurrentDateInString];
        NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
       // NSArray *arrayMyEvents = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:true];
        
       // EventDetailsViewController *controller = [[EventDetailsViewController alloc]init];
        //controller.event_id = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:0] valueForKey:@"event_id"]];
        //[self.navigationController pushViewController:controller animated:true];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Create Event" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:true completion:nil];
}

-(void)createCircleViewWithFrame : (CGRect)frame atView:(UIView*)baseView
{
    UIView *view = [UIFunction createUIViews:frame bckgroundColor:[UIColor whiteColor]];
    view.layer.cornerRadius = view.frame.size.height/2.0;
    view.layer.masksToBounds = true;
    view.clipsToBounds = true;
    [baseView addSubview:view];
}

-(void)createBlackCircleViewWithFrame : (CGRect)frame atView:(UIView*)baseView
{
    UIView *view = [UIFunction createUIViews:frame bckgroundColor:[UIColor blackColor]];
    view.layer.cornerRadius = view.frame.size.height/2.0;
    view.layer.masksToBounds = true;
    view.clipsToBounds = true;
    [baseView addSubview:view];
}


#pragma mark
#pragma mark Done/Save Button Pressed
-(void)doneButtonPressed
{
    NSLog(@"done button pressed %@",dictionaryCreateEvent);
    [self removePickerView];
    [self dismissKeyboard];
    
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:kAppGroup];
    
    NSString *event_name = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_name"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *user_email = [NSString stringWithFormat:@"%@",[[sharedDefaults valueForKey:kLoginData] valueForKey:@"user_email"]];
    [dictionaryCreateEvent setObject:user_email forKey:@"user_email"];
    
    
    NSString *user_id = [NSString stringWithFormat:@"%@",[[sharedDefaults valueForKey:kLoginData] valueForKey:@"user_id"]];
    [dictionaryCreateEvent setObject:user_id forKey:@"user_id"];
    
    if (event_name.length == 0)
    {
        [self showAlertWithMessage:@"Please enter event name."];
    }
    else
    {
        BOOL isUserLoggedIn = [self checkLogin];
        if (!isUserLoggedIn) {
            
            return;
        }
        
        NSMutableArray *arrPosts = [sharedDefaults objectForKey:kSharedPostArray];
        if (arrPosts == nil) {
            
            NSMutableArray *posts = [[NSMutableArray alloc] init];
            [posts addObject:dictionaryCreateEvent];
            [sharedDefaults setObject:posts forKey:kSharedPostArray];
            [sharedDefaults synchronize];
        }
        else {
         
            NSMutableArray *arrNewPosts = [arrPosts mutableCopy];
            [arrNewPosts addObject:dictionaryCreateEvent];
            [sharedDefaults setObject:arrNewPosts forKey:kSharedPostArray];
            [sharedDefaults synchronize];
        }
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Event Saved" message:@"Event created successfully." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [[self extensionContext] completeRequestReturningItems:nil completionHandler:nil];
        }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:true completion:nil];
    }
}



#pragma mark
#pragma mark UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView.tag == 69802140)
    {
        return 1;
    }
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == 69802140)
    {
        return 5;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 69802140)
    {
        return CGSizeMake(widthOfCollectionView/5.0, widthOfCollectionView/5.0);
    }
    return CGSizeMake(0, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 69802140)
    {
        UICollectionViewCell *cell = nil;
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
        cell.backgroundColor=[UIColor clearColor];
        
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
        
        @try
        {
            UICollectionViewLayoutAttributes * theAttributes = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
            CGRect frame = [collectionView convertRect:theAttributes.frame toView:[collectionView superview]];
            
            
            /* if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
             {
             NSString *imageName = [[dictionaryCreateEvent valueForKey:@"event_images"]objectAtIndex:0];
             if (imageName.length > 0)
             {
             NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:0]];
             imageName = [imageName stringByReplacingOccurrencesOfString:@"TOP" withString:@""];
             NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
             NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
             BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
             if (fileExists == true)
             {
             NSString *pathExtension = [filePath pathExtension];
             if (pathExtension.length > 0)
             {
             if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
             {
             [btnAddAttachment setImage:[UIImage imageWithContentsOfFile:filePath] forState:UIControlStateNormal];
             }
             else
             {
             [btnAddAttachment setImage:[UIImage imageNamed:@"attachment"] forState:UIControlStateNormal];
             }
             }
             }
             }
             else
             {
             [btnAddAttachment setImage:[UIImage imageNamed:@"upload_image"] forState:UIControlStateNormal];
             }
             }*/
            
            
            
            if (indexPath.row == 0)
            {
                float width = frame.size.width-35.0;
                
                UIView *view = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:view];
                
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(view.frame.size.width-width-5, 17.5, width , frame.size.height-35.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:0]];
                    NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
                    NSString *documentsDirectory = [documentsURL path];
                    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 1)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(12.5, 12.5, frame.size.width-25.0, frame.size.height-25.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:1]];
                    NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
                    NSString *documentsDirectory = [documentsURL path];
                    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 2)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(5.0, 5.0, frame.size.width-10.0, frame.size.height-10.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:2]];
                    NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
                    NSString *documentsDirectory = [documentsURL path];
                    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 3)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(12.5, 12.5, frame.size.width-25.0, frame.size.height-25.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:3]];
                    NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
                    NSString *documentsDirectory = [documentsURL path];
                    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 4)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(5, 17.5, frame.size.width-35.0, frame.size.height-35.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:4]];
                    NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
                    NSString *documentsDirectory = [documentsURL path];
                    NSString* filePath = [documentsDirectory stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception is 3 %@",exception.description);
        }
        
        cell.userInteractionEnabled = true;
        return cell;
    }
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    indexOfImage = indexPath.row;
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    
    if ([[arrayImages objectAtIndex:indexOfImage] isEqualToString:@""])
    {
        // [self addEventImages];
    }
    else
    {
        // option to view image or remove image
        [self viewOrRemovePhotoPressed];
    }
}

#pragma mark
#pragma mark //******************** Choose Photo Delegates *************************//
-(void)addEventImages
{
    [self.view endEditing:YES];
    
    [viewToChoosePhoto removeFromSuperview];
    viewToChoosePhoto = nil;
    viewToChoosePhoto = [[ChoosePhotoView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    viewToChoosePhoto.choosePhotoDelegate=self;
    [self.view addSubview:viewToChoosePhoto];
}

-(void)removeChoosePhotoView
{
    [UIView animateWithDuration:.3f animations:^{
        viewToChoosePhoto.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        [viewToChoosePhoto removeFromSuperview];
        viewToChoosePhoto = nil;
    }];
}

-(void)choosePhotoFromCameraButtonPressed
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        // open camera
        [self performSelector:@selector(showCamera) withObject:nil afterDelay:0.3];
    }
    else
    {
        // show alert that device has no camera
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Device has no camera" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *settingsAction = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action)
                                         {
                                         }];
        
        [alertController addAction:settingsAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)uploadDocumentButtonPressed
{
    NSLog(@"upload document");
    UIDocumentMenuViewController *importMenu =
    [[UIDocumentMenuViewController alloc] initWithDocumentTypes:[self UTIs] inMode:UIDocumentPickerModeImport];
    importMenu.delegate = self;
    [self presentViewController:importMenu animated:YES completion:nil];
}

-(void)choosePhotoFromGalleryButtonPressed
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        
        NSLog(@"status is %zd",status);
        
        switch (status) {
            case PHAuthorizationStatusAuthorized:
            {
                NSLog(@"PHAuthorizationStatusAuthorized");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openGallery];
                });
            }
                break;
                
            case PHAuthorizationStatusRestricted || status == PHAuthorizationStatusDenied:
            {
                NSLog(@"PHAuthorizationStatusRestricted");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    UIAlertAction *cancelAction = [UIAlertAction
                                                   actionWithTitle:@"OK"
                                                   style:UIAlertActionStyleDestructive
                                                   handler:^(UIAlertAction *action)
                                                   { }];
                    
                    [alertController addAction:cancelAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                });
            }
                
                break;
                
            case PHAuthorizationStatusDenied:
            {
                NSLog(@"PHAuthorizationStatusDenied");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    UIAlertAction *cancelAction = [UIAlertAction
                                                   actionWithTitle:@"OK"
                                                   style:UIAlertActionStyleDestructive
                                                   handler:^(UIAlertAction *action)
                                                   { }];
                    
                    [alertController addAction:cancelAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                });
                
            }
                
                break;
                
            case PHAuthorizationStatusNotDetermined:
            {
                NSLog(@"PHAuthorizationStatusNotDetermined");
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
                 {
                     if(granted)
                     {
                         NSLog(@"Granted access");
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self openGallery];
                         });
                     }
                     else
                     {
                         NSLog(@"Not granted access");
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
                             
                             
                             UIAlertAction *cancelAction = [UIAlertAction
                                                            actionWithTitle:@"OK"
                                                            style:UIAlertActionStyleDestructive
                                                            handler:^(UIAlertAction *action)
                                                            { }];
                             
                            [alertController addAction:cancelAction];
                             [self presentViewController:alertController animated:YES completion:nil];
                             
                         });                     }
                 }];
            }
                
                break;
            default:
                break;
        }
    }];
}


-(void)showCamera
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (status == AVAuthorizationStatusAuthorized)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openCamera];
        });
    }
    else if (status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted )
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction *action)
                                       { }];
        
        
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (status == AVAuthorizationStatusNotDetermined)
    {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access");
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self openCamera];
                 });
             }
             else
             {
                 NSLog(@"Not granted access");
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
                 
                 
                 UIAlertAction *cancelAction = [UIAlertAction
                                                actionWithTitle:@"OK"
                                                style:UIAlertActionStyleDestructive
                                                handler:^(UIAlertAction *action)
                                                { }];
                 
                 [alertController addAction:cancelAction];
                 [self presentViewController:alertController animated:YES completion:nil];
             }
         }];
    }
}


-(void)openCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate  = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:Nil];
}

-(void)openGallery
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate  = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark IMAGE PICKER DELEGATES

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormatter.dateFormat = @"YYYY_MM_dd_hh_mm_ss.SSS";
    NSString *imageName = [NSString stringWithFormat:@"%@.jpg",[dateFormatter stringFromDate:todayDate]];
    
    NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
    NSString *documentsDirectory = [documentsURL path];//[paths objectAtIndex:0];
    NSString *pathOfImage = [documentsDirectory stringByAppendingPathComponent:imageName];  // IT IS THE PATH OF CHOOSEN IMAGE
    NSData *imageData= UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage"],0.0);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imageData= UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerOriginalImage"],0.0);
    }
    [imageData writeToFile:pathOfImage atomically:YES];
    
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    
    if (arrayImages.count < 6)
    {
        [arrayImages replaceObjectAtIndex:indexOfImage withObject:imageName];
    }
    else
    {
        [self showAlertWithMessage:@"You can add maximum 5 attachments."];
    }
    
    [dictionaryCreateEvent setValue:arrayImages forKey:@"event_images"];
    NSLog(@"_____ %@",dictionaryCreateEvent);
    [self.collectionViewForImages reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark
#pragma mark //******************** View or Remove Photo Delegates *************************//
-(void)viewOrRemovePhotoPressed
{
    [self.view endEditing:YES];
    
    [viewFullSizeOrRemovePhoto removeFromSuperview];
    viewFullSizeOrRemovePhoto = nil;
    viewFullSizeOrRemovePhoto = [[RemovePhotoView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    viewFullSizeOrRemovePhoto.removePhotoDelegate=self;
    [self.view addSubview:viewFullSizeOrRemovePhoto];
}

-(void)removePhotoViewFunction
{
    [UIView animateWithDuration:.3f animations:^{
        viewFullSizeOrRemovePhoto.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        [viewFullSizeOrRemovePhoto removeFromSuperview];
        viewFullSizeOrRemovePhoto = nil;
    }];
}

-(void)viewPhotoButtonPressed
{
    NSLog(@"viewPhotoButtonPressed");
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    
    if ([[arrayImages objectAtIndex:indexOfImage]length]>0 && (![[arrayImages objectAtIndex:indexOfImage]hasSuffix:@".jpg"] || [[arrayImages objectAtIndex:indexOfImage]hasSuffix:@".png"])) // attachment is not an image
    {
        NSString *attachmentName = [arrayImages objectAtIndex:indexOfImage];
        NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
        NSString* documentsPath = [documentsURL path];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:attachmentName];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        if (fileExists == true)
        {
            NSLog(@"filePath : %@", filePath);
            NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
            [self loadAttachmentInWebView:fileURL];
        }
    }
    else
    {
        NSArray *filtered = [arrayImages filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"length > 0"]];
        NSMutableArray *photosURL = [[NSMutableArray alloc]init];
        for (int counter = 0; counter < filtered.count; counter++)
        {
            NSString *imageName = [NSString stringWithFormat:@"%@",[filtered objectAtIndex:counter]];
            if ([imageName containsString:@"TOP"])
            {
                imageName = [imageName stringByReplacingOccurrencesOfString:@"TOP" withString:@""];
            }
            
            NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
            NSString* documentsPath = [documentsURL path];
            NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
            if (fileExists == true && ([imageName hasSuffix:@".jpg"] || [imageName hasSuffix:@".png"]))
            {
                [photosURL addObject:filePath];
            }
        }
        
        if (photosURL.count == 0)
        {
            return;
        }
        
        NSArray *photos = [IDMPhoto photosWithFilePaths:photosURL];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:self.view];
        browser.displayActionButton = NO;
        [browser setInitialPageIndex:0];
        browser.displayArrowButton = NO;
        browser.displayCounterLabel = YES;
        browser.usePopAnimation = NO;
        browser.disableVerticalSwipe = YES;
        [self presentViewController:browser animated:YES completion:nil];
    }
}



-(void)removePhotoButtonPressed
{
    NSLog(@"removePhotoButtonPressed");
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    [arrayImages replaceObjectAtIndex:indexOfImage withObject:@""];
    [dictionaryCreateEvent setObject:arrayImages forKey:@"event_images"];
    [self.collectionViewForImages reloadData];
}

#pragma mark
#pragma mark Table View Data Source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 457548)
    {
        return 9;
    }
    else if (tableView.tag == 67800) //search table
    {
        return arraySearch.count;
    }
    return 0;
}




- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 457548)
    {
        if (indexPath.row == 0) //city + topic OLD
        {
            return 0;
        }
        else if (indexPath.row == 1) // event name
        {
            return [UIImage imageNamed:@"event_icon"].size.height+10;
        }
        else if (indexPath.row == 2) // event date and time
        {
            return 2*([UIImage imageNamed:@"date_icon"].size.height+10);
        }
        else if (indexPath.row == 3) // event address1
        {
            return [UIImage imageNamed:@"view_map"].size.height+10;
        }
        else if (indexPath.row == 4) // event address2
        {
            return [UIImage imageNamed:@"view_map"].size.height+10;
        }
        else if (indexPath.row == 5) // event zipcode
        {
            return [UIImage imageNamed:@"view_map"].size.height+10;
        }
        else if (indexPath.row == 6) // event phone number
        {
            return [UIImage imageNamed:@"call_icon"].size.height+10;
        }
        else if (indexPath.row == 7) // event website
        {
            return [UIImage imageNamed:@"website_icon"].size.height+10;
        }
        else if (indexPath.row == 8) //description
        {
            return 150;
        }
    }
    
    else if (tableView.tag == 67800) //search table
    {
        return 55;
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else
    {
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
    }
    
    CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
    
    if (tableView.tag == 457548)
    {
        @try
        {
            
            if (indexPath.row == 0) //city + topic
            {
            }
            
            else if (indexPath.row == 1) //event name
            {
                NSString *event_name = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_name"]];
                
                UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBase];
                
                UIImageView *imgViewAddress = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"event_icon"].size.height/2.0, [UIImage imageNamed:@"event_icon"].size.width, [UIImage imageNamed:@"event_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"event_icon"] isLogo:NO];
                [viewBase addSubview:imgViewAddress];
                
                UITextField *txtFieldEventName = [[UITextField alloc]initWithFrame:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10)];
                txtFieldEventName.placeholder = @"Event Name";
                txtFieldEventName.tag = 74302;
                txtFieldEventName.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
                txtFieldEventName.font = [UIFont fontWithName:fontLight size:14];
                txtFieldEventName.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
                txtFieldEventName.delegate = self;
                txtFieldEventName.text = event_name;
                txtFieldEventName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtFieldEventName.textAlignment = NSTextAlignmentLeft;
                txtFieldEventName.returnKeyType = UIReturnKeyNext;
                txtFieldEventName.keyboardType = UIKeyboardTypeDefault;
                txtFieldEventName.autocapitalizationType = UITextAutocapitalizationTypeWords;
                txtFieldEventName.autocorrectionType = UITextAutocorrectionTypeNo;
                [viewBase addSubview:txtFieldEventName];
                txtFieldEventName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
                txtFieldEventName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Event Name" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
            }
            
            else if (indexPath.row == 2) // event date and time
            {
                NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_date"]];
                NSString *event_start_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_start_time"]];
                NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_end_time"]];
                
                if ([event_date isEqualToString:@""] || [event_date containsString:@"(null)"] || event_date == nil)
                {
                    event_date = @"";
                }
                
                if (event_date.length == 0)
                {
                    event_date = @"00-00-00";
                }
                
                if ([event_start_time isEqualToString:@""] || [event_start_time containsString:@"(null)"] || event_start_time == nil)
                {
                    event_start_time = @"";
                }
                
                if (event_start_time.length == 0)
                {
                    event_start_time = @"00:00 am";
                }
                
                if ([event_end_time isEqualToString:@""] || [event_end_time containsString:@"(null)"] || event_end_time == nil)
                {
                    event_end_time = @"";
                }
                
                if (event_end_time.length == 0)
                {
                    event_end_time = @"00:00 pm";
                }
                
                UIView *viewBaseForDate = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height/2.0) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBaseForDate];
                
                UIView *viewDate = [UIFunction createUIViews:CGRectMake(0, 5, viewBaseForDate.frame.size.width, viewBaseForDate.frame.size.height-10) bckgroundColor:[UIColor clearColor]];
                [viewBaseForDate addSubview:viewDate];
                
                UIImageView *imgViewDate = [UIFunction createUIImageView:CGRectMake(5, viewDate.frame.size.height/2.0-[UIImage imageNamed:@"date_icon"].size.height/2.0, [UIImage imageNamed:@"date_icon"].size.width, [UIImage imageNamed:@"date_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"date_icon"] isLogo:NO];
                [viewDate addSubview:imgViewDate];
                
                
                UIButton *btnEventDate = [UIFunction createButton:CGRectMake(imgViewDate.frame.size.width+imgViewDate.frame.origin.x+15, 0, viewDate.frame.size.width-imgViewDate.frame.size.width-imgViewDate.frame.origin.x-15, viewDate.frame.size.height) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] image:nil title:event_date font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
                btnEventDate.titleLabel.textAlignment = NSTextAlignmentLeft;
                btnEventDate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                [btnEventDate setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
                [btnEventDate addTarget:self action:@selector(selectEventDate) forControlEvents:UIControlEventTouchUpInside];
                [viewDate addSubview:btnEventDate];
                
                
                // ************* Time ********************* //
                UIView *viewBaseForTime = [UIFunction createUIViews:CGRectMake(0, viewBaseForDate.frame.size.height+viewBaseForDate.frame.origin.y, viewBaseForDate.frame.size.width, viewBaseForDate.frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBaseForTime];
                
                UIView *viewTime = [UIFunction createUIViews:CGRectMake(0, 5, viewBaseForTime.frame.size.width, viewBaseForTime.frame.size.height-10) bckgroundColor:[UIColor clearColor]];
                [viewBaseForTime addSubview:viewTime];
                
                
                UIImageView *imgViewTime = [UIFunction createUIImageView:CGRectMake(5, viewTime.frame.size.height/2.0-[UIImage imageNamed:@"time_icon"].size.height/2.0, [UIImage imageNamed:@"time_icon"].size.width, [UIImage imageNamed:@"time_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"time_icon"] isLogo:NO];
                [viewTime addSubview:imgViewTime];
                
                
                UIButton *btnEventStartTime = [UIFunction createButton:CGRectMake(imgViewTime.frame.size.width+imgViewTime.frame.origin.x+15, 0, viewTime.frame.size.width/2.0-25, viewTime.frame.size.height) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] image:nil title:event_start_time font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
                [btnEventStartTime addTarget:self action:@selector(selectEventStartTime) forControlEvents:UIControlEventTouchUpInside];
                btnEventStartTime.titleLabel.textAlignment = NSTextAlignmentLeft;
                btnEventStartTime.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                [btnEventStartTime setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
                [viewTime addSubview:btnEventStartTime];
                
                UILabel *lblHiphen = [UIFunction createLable:CGRectMake(btnEventStartTime.frame.origin.x+btnEventStartTime.frame.size.width, 0, 10, viewTime.frame.size.height) bckgroundColor:[UIColor clearColor] title:@"-" font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
                lblHiphen.textAlignment = NSTextAlignmentCenter;
                [viewTime addSubview:lblHiphen];
                
                
                UIButton *btnEventEndTime = [UIFunction createButton:CGRectMake(lblHiphen.frame.size.width+lblHiphen.frame.origin.x, 0, btnEventStartTime.frame.size.width, viewTime.frame.size.height) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] image:nil title:event_end_time font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
                [btnEventEndTime addTarget:self action:@selector(selectEventEndTime) forControlEvents:UIControlEventTouchUpInside];
                btnEventEndTime.titleLabel.textAlignment = NSTextAlignmentLeft;
                btnEventEndTime.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                [btnEventEndTime setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
                [viewTime addSubview:btnEventEndTime];
                
            }
            
            else if (indexPath.row == 3) // event address 1
            {
                NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_first"]];
                
                UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBase];
                
                UIImageView *imgViewAddress = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"view_map"].size.height/2.0, [UIImage imageNamed:@"view_map"].size.width, [UIImage imageNamed:@"view_map"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"view_map"] isLogo:NO];
                
                [imgViewAddress setUserInteractionEnabled:YES];
                UITapGestureRecognizer *taplocation = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(openMapScreen)];
                [imgViewAddress addGestureRecognizer:taplocation];
                
                [viewBase addSubview:imgViewAddress];
                
                
                UIButton *btnSelectAddress = [UIFunction createButton:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] image:nil title:nil font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
                btnSelectAddress.tag = 505050;
                btnSelectAddress.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
                btnSelectAddress.contentEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
                [btnSelectAddress addTarget:self action:@selector(selectEventAddress:) forControlEvents:UIControlEventTouchUpInside];
                [viewBase addSubview:btnSelectAddress];
                
                if (event_address_first.length == 0)
                {
                    [btnSelectAddress setTitle:@"Address" forState:UIControlStateNormal];
                }
                else
                {
                    [btnSelectAddress setTitle:event_address_first forState:UIControlStateNormal];
                }
            }
            
            else if (indexPath.row == 4) // event address 2
            {
                NSString *event_address_second = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_second"]];
                
                UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBase];
                
                UIImageView *imgViewAddress = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"view_map"].size.height/2.0, [UIImage imageNamed:@"view_map"].size.width, [UIImage imageNamed:@"view_map"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"view_map"] isLogo:NO];
                imgViewAddress.hidden = true;
                [viewBase addSubview:imgViewAddress];
                
                
                UITextField *txtFieldEventAddress = [[UITextField alloc]initWithFrame:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10)];
                txtFieldEventAddress.placeholder = @"Address";
                txtFieldEventAddress.tag = 74304;
                txtFieldEventAddress.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
                txtFieldEventAddress.font = [UIFont fontWithName:fontLight size:14];
                txtFieldEventAddress.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
                txtFieldEventAddress.delegate = self;
                txtFieldEventAddress.text = event_address_second;
                txtFieldEventAddress.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtFieldEventAddress.textAlignment = NSTextAlignmentLeft;
                txtFieldEventAddress.returnKeyType = UIReturnKeyNext;
                txtFieldEventAddress.keyboardType = UIKeyboardTypeDefault;
                txtFieldEventAddress.autocapitalizationType = UITextAutocapitalizationTypeWords;
                txtFieldEventAddress.autocorrectionType = UITextAutocorrectionTypeNo;
                [viewBase addSubview:txtFieldEventAddress];
                txtFieldEventAddress.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
                txtFieldEventAddress.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Address" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
            }
            
            else if (indexPath.row == 5) // zipcode
            {
                NSString *event_zip_code = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_zip_code"]];
                
                UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBase];
                
                UIImageView *imgViewAddress = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"view_map"].size.height/2.0, [UIImage imageNamed:@"view_map"].size.width, [UIImage imageNamed:@"view_map"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"view_map"] isLogo:NO];
                imgViewAddress.hidden = true;
                [viewBase addSubview:imgViewAddress];
                
                
                UITextField *txtFieldEventZipCode = [[UITextField alloc]initWithFrame:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10)];
                txtFieldEventZipCode.placeholder = @"Zipcode";
                txtFieldEventZipCode.tag = 74305;
                txtFieldEventZipCode.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
                txtFieldEventZipCode.font = [UIFont fontWithName:fontLight size:14];
                txtFieldEventZipCode.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
                txtFieldEventZipCode.delegate = self;
                txtFieldEventZipCode.text = event_zip_code;
                txtFieldEventZipCode.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtFieldEventZipCode.textAlignment = NSTextAlignmentLeft;
                txtFieldEventZipCode.returnKeyType = UIReturnKeyNext;
                txtFieldEventZipCode.keyboardType = UIKeyboardTypeDefault;
                txtFieldEventZipCode.autocapitalizationType = UITextAutocapitalizationTypeWords;
                txtFieldEventZipCode.autocorrectionType = UITextAutocorrectionTypeNo;
                [viewBase addSubview:txtFieldEventZipCode];
                txtFieldEventZipCode.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
                txtFieldEventZipCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Zipcode" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
            }
            
            else if (indexPath.row == 6) //phone number
            {
                NSString *event_phone_number = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_phone_number"]];
                NSString *user_country_code = [NSString stringWithFormat:@" %@",[dictionaryCreateEvent valueForKey:@"user_country_code"]];
                
                UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBase];
                
                UIImageView *imgViewPhone = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"call_icon"].size.height/2.0, [UIImage imageNamed:@"call_icon"].size.width, [UIImage imageNamed:@"call_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"call_icon"] isLogo:NO];
                [viewBase addSubview:imgViewPhone];
                
                UILabel *lblCountryCode = [UIFunction createLable:CGRectMake(imgViewPhone.frame.size.width+imgViewPhone.frame.origin.x+15, 5, 50, viewBase.frame.size.height-10) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] title:user_country_code font:[UIFont fontWithName:fontLight size:14.0] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
                lblCountryCode.userInteractionEnabled = true;
                lblCountryCode.textAlignment = NSTextAlignmentLeft;
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(func_SelectCountryCode)];
                [tap setNumberOfTouchesRequired:1];
                [lblCountryCode addGestureRecognizer:tap];
                [viewBase addSubview:lblCountryCode];
                
                
                UITextField *txtFieldEventPhoneNumber = [[UITextField alloc]initWithFrame:CGRectMake(lblCountryCode.frame.size.width+lblCountryCode.frame.origin.x, 5, viewBase.frame.size.width-lblCountryCode.frame.size.width-lblCountryCode.frame.origin.x, viewBase.frame.size.height-10)];
                txtFieldEventPhoneNumber.placeholder = @"Phone";
                txtFieldEventPhoneNumber.tag = 74306;
                txtFieldEventPhoneNumber.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
                txtFieldEventPhoneNumber.font = [UIFont fontWithName:fontLight size:14];
                txtFieldEventPhoneNumber.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
                txtFieldEventPhoneNumber.delegate = self;
                txtFieldEventPhoneNumber.text = event_phone_number;
                txtFieldEventPhoneNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtFieldEventPhoneNumber.textAlignment = NSTextAlignmentLeft;
                txtFieldEventPhoneNumber.returnKeyType = UIReturnKeyNext;
                txtFieldEventPhoneNumber.keyboardType = UIKeyboardTypeNumberPad;
                txtFieldEventPhoneNumber.autocapitalizationType = UITextAutocapitalizationTypeWords;
                txtFieldEventPhoneNumber.autocorrectionType = UITextAutocorrectionTypeNo;
                [viewBase addSubview:txtFieldEventPhoneNumber];
                txtFieldEventPhoneNumber.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
                txtFieldEventPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Phone" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
                
                UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
                [toolBar setBarStyle:UIBarStyleDefault];
                UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
                
                UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(dismissKeyboard)];
                toolBar.items = @[flex, barButtonDone];
                barButtonDone.tintColor = [UIColor blackColor];
                txtFieldEventPhoneNumber.inputAccessoryView = toolBar;
            }
            
            else if (indexPath.row == 7) //website
            {
                NSString *event_website = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_website"]];
                
                UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBase];
                
                UIImageView *imgViewWebsite = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"website_icon"].size.height/2.0, [UIImage imageNamed:@"website_icon"].size.width, [UIImage imageNamed:@"website_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"website_icon"] isLogo:NO];
                [viewBase addSubview:imgViewWebsite];
                
                UITextField *txtFieldEventWebsite = [[UITextField alloc]initWithFrame:CGRectMake(imgViewWebsite.frame.size.width+imgViewWebsite.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewWebsite.frame.size.width-imgViewWebsite.frame.origin.x-15, viewBase.frame.size.height-10)];
                txtFieldEventWebsite.placeholder = @"Event Website";
                txtFieldEventWebsite.tag = 74307;
                txtFieldEventWebsite.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
                txtFieldEventWebsite.font = [UIFont fontWithName:fontLight size:14];
                txtFieldEventWebsite.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
                txtFieldEventWebsite.delegate = self;
                txtFieldEventWebsite.text = event_website;
                txtFieldEventWebsite.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtFieldEventWebsite.textAlignment = NSTextAlignmentLeft;
                txtFieldEventWebsite.returnKeyType = UIReturnKeyNext;
                txtFieldEventWebsite.keyboardType = UIKeyboardTypeDefault;
                txtFieldEventWebsite.autocapitalizationType = UITextAutocapitalizationTypeNone;
                txtFieldEventWebsite.autocorrectionType = UITextAutocorrectionTypeNo;
                [viewBase addSubview:txtFieldEventWebsite];
                txtFieldEventWebsite.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
                txtFieldEventWebsite.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Event Website" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
            }
            
            else if (indexPath.row == 8) //description
            {
                NSString *event_description = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_description"]];
                
                UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:viewBase];
                
                UIImageView *imgViewDescription = [UIFunction createUIImageView:CGRectMake(5, 10, [UIImage imageNamed:@"description_icon"].size.width, [UIImage imageNamed:@"description_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"description_icon"] isLogo:NO];
                [viewBase addSubview:imgViewDescription];
                
                UIView *viewDescription = [UIFunction createUIViews:CGRectMake(imgViewDescription.frame.size.width+imgViewDescription.frame.origin.x+15, 5, viewBase.frame.size.width/2.0-imgViewDescription.frame.size.width-imgViewDescription.frame.origin.x, viewBase.frame.size.height-10) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
                viewDescription.userInteractionEnabled = true;
                [viewBase addSubview:viewDescription];
                
                
                UITextView *txtViewDescription = [[UITextView alloc] init ];
                txtViewDescription.frame = CGRectMake(5, 5, viewDescription.frame.size.width-10, viewDescription.frame.size.height-10);
                [txtViewDescription setFont:[UIFont fontWithName:fontLight size:14]];
                txtViewDescription.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
                txtViewDescription.returnKeyType = UIReturnKeyDone;
                txtViewDescription.userInteractionEnabled = true;
                txtViewDescription.autocapitalizationType = UITextAutocapitalizationTypeSentences;
                txtViewDescription.autocorrectionType = UITextAutocorrectionTypeNo;
                txtViewDescription.delegate = self;
                txtViewDescription.tag = 21000;
                txtViewDescription.backgroundColor = [UIColor clearColor];
                [viewDescription addSubview:txtViewDescription];
                
                if (event_description.length == 0)
                {
                    txtViewDescription.text = @"Description";
                }
                else
                {
                    txtViewDescription.text = event_description;
                }
            }
        }
        @catch (NSException *exception)
        {
            NSLog(@"Expression is %@",exception.description);
        }
    }
    
    else if (tableView.tag == 67800) //search table
    {
        NSString *address1 = [NSString stringWithFormat:@"%@",[[arraySearch objectAtIndex:indexPath.row] valueForKey:@"address_first"]];
        NSString *address2 = [NSString stringWithFormat:@"%@",[[arraySearch objectAtIndex:indexPath.row] valueForKey:@"address_second"]];
        
        UIView *viewDivider = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, 1) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
        [cell.contentView addSubview:viewDivider];
        
        
        UILabel *lblAddress1 = [UIFunction createLable:CGRectMake(10, 5, frame.size.width-20, 25) bckgroundColor:[UIColor clearColor] title:address1 font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
        [cell.contentView addSubview:lblAddress1];
        
        UILabel *lblAddress2 = [UIFunction createLable:CGRectMake(lblAddress1.frame.origin.x, lblAddress1.frame.size.height+lblAddress1.frame.origin.y, lblAddress1.frame.size.width, 20) bckgroundColor:[UIColor clearColor] title:address2 font:[UIFont fontWithName:fontLight size:12] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
        [cell.contentView addSubview:lblAddress2];
        
        
        if (arraySearch.count-1 == indexPath.row)
        {
            UIView *viewDividerBottom = [UIFunction createUIViews:CGRectMake(0, frame.size.height-1, frame.size.width, 1) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
            [cell.contentView addSubview:viewDividerBottom];
        }
    }
    
    cell.userInteractionEnabled = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:true];
    [self removePickerView];
    
    if (tableView.tag == 67800) //search table
    {
        NSString *place_id = [NSString stringWithFormat:@"%@",[[arraySearch objectAtIndex:indexPath.row] valueForKey:@"place_id"]];
        NSString *primaryAddress = [NSString stringWithFormat:@"%@",[[arraySearch objectAtIndex:indexPath.row] valueForKey:@"address_first"]];
        NSString *secondaryAddress = [NSString stringWithFormat:@"%@",[[arraySearch objectAtIndex:indexPath.row] valueForKey:@"address_second"]];
        [self func_APICallToGetLatitudeAndLongitudeFromGoogleWithPlaceId:place_id primaryAddress:primaryAddress secondaryAddress:secondaryAddress];
    }
}

#pragma mark
#pragma mark Select Event Start Date
-(void)selectEventDate
{
    NSLog(@"select start date of event");
    
    [self.view endEditing:true];
    [self dismissKeyboard];
    
    @try
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [backGroundViewForPicker removeFromSuperview];
        backGroundViewForPicker = nil;
        backGroundViewForPicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor colorWithRed:214.0/255 green:240.0/255 blue:252.0/255 alpha:1.0]];
        [self.view addSubview:backGroundViewForPicker];
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setBarStyle:UIBarStyleDefault];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(removePickerView)];
        toolBar.items = @[flex, barButtonDone];
        barButtonDone.tintColor = [UIColor blackColor];
        [backGroundViewForPicker addSubview:toolBar];
        
        UIDatePicker *datePicker  = [[UIDatePicker alloc]init];
        [datePicker setFrame:CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, self.view.frame.size.width, 216.0)];
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.tag = 9542400;
        [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"US"]];
        datePicker.backgroundColor = [UIColor clearColor];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        [datePicker setMinimumDate:[NSDate date]];
        [backGroundViewForPicker addSubview:datePicker];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [backGroundViewForPicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self updateDateField : datePicker];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is 4 %@",exception.description);
    }
}

-(void)updateDateField : (UIDatePicker*)datePicker
{
    if (datePicker.tag == 9542400) // Event Start Date
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        [formatter setDateFormat:@"MM-dd-yyyy"];
        NSString *stringFromDate = [formatter stringFromDate:datePicker.date];
        [dictionaryCreateEvent setValue:stringFromDate forKey:@"event_date"];
        [tableViewCreateEvent reloadData];
    }
    else if (datePicker.tag == 545454) // Event Start Time
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        [formatter setDateFormat:@"hh:mm a"];
        NSString *stringFromDate = [formatter stringFromDate:datePicker.date];
        tempEventStartTime = stringFromDate;
    }
    else if (datePicker.tag == 454545) // Event End Time
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        [formatter setDateFormat:@"hh:mm a"];
        NSString *stringFromDate = [formatter stringFromDate:datePicker.date];
        tempEventEndTime = stringFromDate;
        
        //        [dictionaryCreateEvent setValue:stringFromDate forKey:@"event_end_time"];
        //        [tableViewCreateEvent reloadData];
    }
}

#pragma mark
#pragma mark Remove Date Picker For Event Start Time and End Time
-(void)removeDatePickerForEventStartTime
{
    @try
    {
        NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_date"]];
        NSString *event_start_time = tempEventStartTime;
        NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_end_time"]];
        
        if ([event_date containsString:@"("] || [event_date containsString:@"null"] || event_date == nil)
        {
            event_date = @"";
        }
        
        if ([event_start_time containsString:@"("] || [event_start_time containsString:@"null"] || event_start_time == nil)
        {
            event_start_time = @"";
        }
        
        if ([event_end_time containsString:@"("] || [event_end_time containsString:@"null"] || event_end_time == nil)
        {
            event_end_time = @"";
        }
        
        
        if (event_date.length == 0)
        {
            [self showAlertWithMessage:@"Please select start date."];
        }
        else if (event_start_time.length == 0)
        {
            [self showAlertWithMessage:@"Please select start time."];
        }
        else
        {
            NSString *newDateInString = [NSString stringWithFormat:@"%@ %@",event_date, event_start_time];
            BOOL isSelectedDateAllowd = [self isEndDateIsSmallerThanCurrent:newDateInString];
            
            if (isSelectedDateAllowd == false)
            {
                [self showAlertWithMessage:@"Start time must be greater than current time."];
                
                tempEventStartTime = @"";
                [dictionaryCreateEvent setValue:tempEventStartTime forKey:@"event_start_time"];
                [tableViewCreateEvent reloadData];
            }
            else
            {
                [dictionaryCreateEvent setValue:tempEventStartTime forKey:@"event_start_time"];
                [tableViewCreateEvent reloadData];
                tempEventStartTime = @"";
            }
        }
        [self removePickerView];
    }
    @catch (NSException *exception)
    {
        
    }
}

-(void)removeDatePickerForEventEndTime
{
    @try
    {
        NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_date"]];
        NSString *event_start_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_start_time"]];
        NSString *event_end_time = tempEventEndTime;
        
        
        if ([event_date containsString:@"("] || [event_date containsString:@"null"] || event_date == nil)
        {
            event_date = @"";
        }
        
        if ([event_start_time containsString:@"("] || [event_start_time containsString:@"null"] || event_start_time == nil)
        {
            event_start_time = @"";
        }
        
        if ([event_end_time containsString:@"("] || [event_end_time containsString:@"null"] || event_end_time == nil)
        {
            event_end_time = @"";
        }
        
        
        if (event_date.length == 0)
        {
            [self showAlertWithMessage:@"Please select start date."];
        }
        else if (event_start_time.length == 0)
        {
            [self showAlertWithMessage:@"Please select start time."];
        }
        else if (event_end_time.length == 0)
        {
            [self showAlertWithMessage:@"Please select end time."];
        }
        
        else
        {
            NSString *newDateForEventStart = [NSString stringWithFormat:@"%@ %@",event_date, event_start_time];
            NSString *newDateForEventEnd = [NSString stringWithFormat:@"%@ %@",event_date, event_end_time];
            
            BOOL isSelectedDateAllowd = [self isEndDateIsSmallerThanEventStartDate:newDateForEventStart : newDateForEventEnd];
            
            if (isSelectedDateAllowd == false)
            {
                [self showAlertWithMessage:@"End time must be greater than start time."];
                
                tempEventEndTime = @"";
                [dictionaryCreateEvent setValue:tempEventEndTime forKey:@"event_end_time"];
                [tableViewCreateEvent reloadData];
            }
            else
            {
                [dictionaryCreateEvent setValue:tempEventEndTime forKey:@"event_end_time"];
                [tableViewCreateEvent reloadData];
                tempEventEndTime = @"";
            }
        }
        [self removePickerView];
    }
    @catch (NSException *exception)
    {
        
    }
}


-(void)selectEventEndTime
{
    NSLog(@"select end time of event");
    
    [self.view endEditing:true];
    [self dismissKeyboard];
    
    @try
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [backGroundViewForPicker removeFromSuperview];
        backGroundViewForPicker = nil;
        backGroundViewForPicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor colorWithRed:214.0/255 green:240.0/255 blue:252.0/255 alpha:1.0]];
        [self.view addSubview:backGroundViewForPicker];
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setBarStyle:UIBarStyleDefault];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(removeDatePickerForEventEndTime)];
        toolBar.items = @[flex, barButtonDone];
        barButtonDone.tintColor = [UIColor blackColor];
        [backGroundViewForPicker addSubview:toolBar];
        
        UIDatePicker *datePicker  = [[UIDatePicker alloc]init];
        [datePicker setFrame:CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, self.view.frame.size.width, 216.0)];
        datePicker.datePickerMode = UIDatePickerModeTime;
        datePicker.tag = 454545;
        [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"US"]];
        datePicker.backgroundColor = [UIColor clearColor];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        //[datePicker setMinimumDate:[NSDate date]];
        [backGroundViewForPicker addSubview:datePicker];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [backGroundViewForPicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self updateDateField : datePicker];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is 5 %@",exception.description);
    }
}

-(void)selectEventStartTime
{
    NSLog(@"select event start time");
    
    [self.view endEditing:true];
    [self dismissKeyboard];
    
    @try
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [backGroundViewForPicker removeFromSuperview];
        backGroundViewForPicker = nil;
        backGroundViewForPicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor colorWithRed:214.0/255 green:240.0/255 blue:252.0/255 alpha:1.0]];
        [self.view addSubview:backGroundViewForPicker];
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setBarStyle:UIBarStyleDefault];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(removeDatePickerForEventStartTime)];
        toolBar.items = @[flex, barButtonDone];
        barButtonDone.tintColor = [UIColor blackColor];
        [backGroundViewForPicker addSubview:toolBar];
        
        UIDatePicker *datePicker  = [[UIDatePicker alloc]init];
        [datePicker setFrame:CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, self.view.frame.size.width, 216.0)];
        datePicker.datePickerMode = UIDatePickerModeTime;
        datePicker.tag = 545454;
        [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"US"]];
        datePicker.backgroundColor = [UIColor clearColor];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        //[datePicker setMinimumDate:[NSDate date]];
        [backGroundViewForPicker addSubview:datePicker];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [backGroundViewForPicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self updateDateField : datePicker];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is 1 %@",exception.description);
    }
}


-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [tableViewCreateEvent scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

-(void)removePickerView
{
    [UIView animateWithDuration:.2f animations:^{
        
        [backGroundViewForPicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];
        [viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];
        
        
    } completion:^(BOOL finished) {
        
        [backGroundViewForPicker removeFromSuperview];
        backGroundViewForPicker = nil;
        [viewForCountryCodePicker removeFromSuperview];
        viewForCountryCodePicker = nil;
    }];
}


#pragma mark
#pragma mark Text View Delegates
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    if (textView.tag == 21000)
    {
        NSString * stringToRange = [textView.text substringWithRange:NSMakeRange(0,range.location)];
        stringToRange = [stringToRange stringByAppendingString:text];
        NSArray *wordArray       = [stringToRange componentsSeparatedByString:@" "];
        NSString * wordTyped     = [wordArray componentsJoinedByString:@" "];
        [dictionaryCreateEvent setValue:wordTyped forKey:@"event_description"];
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self removePickerView];
    [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:8 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    
    if(textView.tag == 21000)
    {
        
        NSString *_strNotes = [dictionaryCreateEvent valueForKey:@"event_description"];
        
        if (_strNotes.length==0)
        {
            textView.text = @"";
        }
        else
        {
            textView.text = _strNotes;
        }
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.tag == 21000)
    {
        NSString *_strNotes = [dictionaryCreateEvent valueForKey:@"event_description"];
        
        if (_strNotes.length==0)
        {
            textView.text = @"Description";
        }
        else
        {
            textView.text = _strNotes;
        }
    }
}


#pragma mark
#pragma mark TextField Delegate
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [tableViewCreateEvent scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField.tag == 74300) // city
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_city"];
    }
    else if (textField.tag == 74301) // topic
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_topic"];
    }
    else if (textField.tag == 74302) // name
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_name"];
    }
    else if (textField.tag == 74303) // address1
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_address_first"];
    }
    else if (textField.tag == 74304) // address2
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_address_second"];
    }
    else if (textField.tag == 74305) // zip code
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_zip_code"];
    }
    else if (textField.tag == 74306) // phone number
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_phone_number"];
    }
    else if (textField.tag == 74307) // website
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_website"];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 74300) // city
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74301];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74301) // topic
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74302];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74302) // name
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74303];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74303) // address1
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74304];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74304) // address2
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74305];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74305) // zip code
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74306];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74306) // phone number
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74307];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74307) // website
    {
        UITextView *_txtView = (UITextView*)[self.view viewWithTag:21000];
        [_txtView becomeFirstResponder];
    }
    
    
    [textField resignFirstResponder];
    return NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self removePickerView];
    
    if (textField.tag == 74300) // city
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74301) // topic
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74302) // name
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74303) // address1
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74304) // address2
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74305) // zip code
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74306) // phone number
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74307) // website
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if (textField.tag == 74300) // city
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "] invertedSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        
        if (![string isEqualToString:filtered])
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength<=30)
            {
                [dictionaryCreateEvent setObject:text forKey:@"event_city"];
            }
            return (newLength >= 31) ? NO : YES;
        }
        else if (range.length == 1)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setObject:text forKey:@"event_city"];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    else if (textField.tag == 74301) // topic
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setValue:text forKey:@"event_topic"];
        }
        return (newLength >= 31) ? NO : YES;
    }
    
    else if (textField.tag == 74302) // name
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 "] invertedSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        
        if (![string isEqualToString:filtered])
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength<=30)
            {
                [dictionaryCreateEvent setObject:text forKey:@"event_name"];
            }
            return (newLength >= 31) ? NO : YES;
        }
        else if (range.length == 1)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setObject:text forKey:@"event_name"];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    else if (textField.tag == 74303) // address1
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setValue:text forKey:@"event_address_first"];
        }
        return (newLength >= 31) ? NO : YES;
    }
    
    else if (textField.tag == 74304) // address2
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setValue:text forKey:@"event_address_second"];
        }
        return (newLength >= 31) ? NO : YES;
    }
    
    else if (textField.tag == 74305) // zip code
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 10)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setValue:text forKey:@"event_zip_code"];
        }
        return (newLength >= 11) ? NO : YES;
    }
    
    else if (textField.tag == 74306) // phone number
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        
        NSString *_strPhoneNumber = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=15)
        {
            [dictionaryCreateEvent setValue:_strPhoneNumber forKey:@"event_phone_number"];
        }
        return (newLength >= 16) ? NO : YES;
    }
    
    else if (textField.tag == 74307) // website
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setValue:text forKey:@"event_website"];
        }
        return (newLength >= 31) ? NO : YES;
    }
    
    return YES;
}

#pragma mark
#pragma maark ----------- Select Address ---------------
-(void)selectEventAddress : (UIButton*)sender
{
    arraySearch = [[NSMutableArray alloc]init];
    
    
    [self removePickerView];
    [viewForGoogleSearchAPI removeFromSuperview];
    viewForGoogleSearchAPI = nil;
    viewForGoogleSearchAPI = [UIFunction createUIViews:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) bckgroundColor:[UIColor blackColor]];
    viewForGoogleSearchAPI.alpha = 0.0;
    [self.view addSubview:viewForGoogleSearchAPI];
    
    UISearchBar *txtFieldEventAddress = [[UISearchBar alloc] initWithFrame:CGRectMake(20, 250, viewForGoogleSearchAPI.frame.size.width-40, sender.frame.size.height)];
    txtFieldEventAddress.delegate = self;
    txtFieldEventAddress.showsCancelButton = YES;
    txtFieldEventAddress.tintColor = [UIColor blackColor];
    txtFieldEventAddress.layer.cornerRadius = 5.0;
    txtFieldEventAddress.clipsToBounds = YES;
    txtFieldEventAddress.backgroundColor = [UIColor whiteColor];
    //txtFieldEventAddress.barTintColor = [UIColor whiteColor];
    //txtFieldEventAddress.searchBarStyle = UISearchBarStyleMinimal;
    txtFieldEventAddress.userInteractionEnabled = YES;
    txtFieldEventAddress.placeholder = @"Look up Places";
    [viewForGoogleSearchAPI addSubview:txtFieldEventAddress];
    
    
    [UIView animateWithDuration:0.3 animations:^{
        
        viewForGoogleSearchAPI.alpha = 1.0;
        txtFieldEventAddress.frame = CGRectMake(txtFieldEventAddress.frame.origin.x, 30, txtFieldEventAddress.frame.size.width, txtFieldEventAddress.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        [txtFieldEventAddress becomeFirstResponder];
        
        [tableViewSearch removeFromSuperview];
        tableViewSearch = nil;
        tableViewSearch = [self createTableView:CGRectMake(10, txtFieldEventAddress.frame.size.height+txtFieldEventAddress.frame.origin.y+10, viewForGoogleSearchAPI.frame.size.width-20, viewForGoogleSearchAPI.frame.size.height-txtFieldEventAddress.frame.size.height-txtFieldEventAddress.frame.origin.y-20) backgroundColor:[UIColor clearColor]];
        [viewForGoogleSearchAPI addSubview:tableViewSearch];
        tableViewSearch.tag = 67800;
        
    }];
    
}

-(void)removeSearchView
{
    [UIView animateWithDuration:0.3 animations:^{
        viewForGoogleSearchAPI.alpha = 0.0;
    } completion:^(BOOL finished) {
        [viewForGoogleSearchAPI removeFromSuperview];
        viewForGoogleSearchAPI = nil;
        [tableViewSearch removeFromSuperview];
        tableViewSearch = nil;
    }];
}

-(void)openMapScreen {
    
    NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_first"]];
    event_address_first = [event_address_first stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    NSString *event_address_second = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_second"]];
    event_address_second = [event_address_second stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    if (([event_address_first length] > 0) || ([event_address_second length] > 0)) {
        
        NSString* url = [NSString stringWithFormat: @"http://maps.google.com/?q=%@,%@", event_address_first, event_address_second];
        NSURL *urlToOpen = [NSURL URLWithString:url];
        SFSafariViewController *controller = [[SFSafariViewController alloc]initWithURL:urlToOpen];
        [self presentViewController:controller animated:YES completion:nil];
    }
}


#pragma mark
#pragma mark Search Delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
    
    for (UIView *view in searchBar1.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                NSLog(@"enableCancelButton");
                return;
            }
        }
    }
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar
{
    [self.view endEditing:YES];
    
    for (UIView *view in aSearchBar.subviews)
    {
        for (id subview in view.subviews)
        {
            if ( [subview isKindOfClass:[UIButton class]] )
            {
                [subview setEnabled:YES];
                NSLog(@"enableCancelButton");
                return;
            }
        }
    }
}


-(void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    @try
    {
        NSString *trimmedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (trimmedString.length>0)
        {
            GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
            //filter.type = kGMSPlacesAutocompleteTypeFilterAddress;
            
            [_placesClient autocompleteQuery:trimmedString
                                      bounds:nil
                                      filter:filter
                                    callback:^(NSArray *results, NSError *error)
             {
                 if (error != nil)
                 {
                     NSLog(@"Autocomplete error %@", [error localizedDescription]);
                     return;
                 }
                 
                 [arraySearch removeAllObjects];
                 
                 for (GMSAutocompletePrediction* result in results)
                 {
                     NSDictionary *exploreDict = [[NSDictionary alloc]initWithObjectsAndKeys:
                                                  result.attributedPrimaryText.string,@"address_first",
                                                  result.attributedSecondaryText.string,@"address_second",
                                                  result.placeID,@"place_id",
                                                  nil];
                     [arraySearch addObject:exploreDict];
                 }
                 NSLog(@"arraySearch is %@",arraySearch);
                 [tableViewSearch reloadData];
             }];
        }
        else
        {
            [arraySearch removeAllObjects];
            [tableViewSearch reloadData];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}



- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"searchBarCancelButtonClicked");
    [self removeSearchView];
}

-(void) func_APICallToGetLatitudeAndLongitudeFromGoogleWithPlaceId : (NSString*)place_id primaryAddress: (NSString*)primaryAddress secondaryAddress:(NSString*)secondaryAddress
{
    @try
    {
        Reachability *reach = [Reachability reachabilityForInternetConnection];
        NetworkStatus netStatus = [reach currentReachabilityStatus];
        
        if (netStatus == NotReachable)
        {
            [self showAlertWithMessage:@"Internet Unavailable"];
        }
        else
        {
            //show loading indicator
            [self showActivityIndicatorWithColor:[UIColor blackColor]];
            NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=%@",place_id,kiOSKeyForGoogleAPI];
            [[APIMaster getSharedInstance]callAPIWithNoParametrsFoRGoogleAndURL:url withCallBackResponse:^(NSURLSessionDataTask *operation, id responseObject, NSError *error) {
                
                if (error)
                {
                    //remove loading indicator
                    [self hideActivityIndicator];
                    
                    @try
                    {
                        NSData *errorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey];
                        if (!errorData)
                        {
                            [UIFunction func_CouldNotConnectWithServer];
                        }
                        else
                        {
                            NSDictionary *serializedData = [NSJSONSerialization JSONObjectWithData: errorData options:kNilOptions error:nil];
                            NSLog(@"error is %@ and error code is %zd and serializedData is %@",error.localizedDescription, error.code, serializedData);
                            
                            NSString *message;
                            if ([serializedData valueForKey:@"UserMessage"])
                            {
                                message = [NSString stringWithFormat:@"%@ : %@",[serializedData valueForKey:@"Code"],[serializedData valueForKey:@"UserMessage"]];
                            }
                            else
                            {
                                message = [NSString stringWithFormat:@"%@",serializedData];
                            }
                        }
                    }
                    @catch (NSException *exception)
                    {
                        NSLog(@"Exception is %@", exception.description);
                    }
                }
                else
                {
                    //remove loading indicator
                    [self hideActivityIndicator];
                    
                    NSDictionary* json = (NSDictionary* ) responseObject;
                    NSLog(@"json is %@",json);
                    
                    NSString *strLatitude = [NSString stringWithFormat:@"%@",[[[[json objectForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"]valueForKey:@"lat"]];
                    NSString *strLongitude = [NSString stringWithFormat:@"%@",[[[[json objectForKey:@"result"] valueForKey:@"geometry"] valueForKey:@"location"]valueForKey:@"lng"]];
                    
                    NSLog(@"strLatitude ___ %@",strLatitude);
                    NSLog(@"strLongitude ___ %@",strLongitude);
                    
                    NSString *zipCode = @"";
                    
                    NSArray *arrAddressComponents = [[json objectForKey:@"result"] objectForKey:@"address_components"];
                    for (NSDictionary *dict in arrAddressComponents) {
                        
                        NSString *type = [[dict valueForKey:@"types"] objectAtIndex:0];
                        if ([type isEqualToString:@"postal_code"]) {
                            
                            NSString *postalCode = [dict valueForKey:@"long_name"];
                            zipCode = postalCode;
                        }
                    }
                    
                    if (strLatitude.length == 0 || [strLatitude containsString:@"(null)"] || strLongitude.length == 0 || [strLongitude containsString:@"(null)"])
                    {
                        [self showAlertWithMessage:@"Unable to get location. Please try with some other value."];
                    }
                    else
                    {
                        [dictionaryCreateEvent setValue:primaryAddress forKey:@"event_address_first"];
                        [dictionaryCreateEvent setValue:secondaryAddress forKey:@"event_address_second"];
                        [dictionaryCreateEvent setValue:strLatitude forKey:@"latitude"];
                        [dictionaryCreateEvent setValue:strLongitude forKey:@"longitude"];
                        [dictionaryCreateEvent setValue:zipCode forKey:@"event_zip_code"];
                        [tableViewCreateEvent reloadData];
                        [self removeSearchView];
                    }
                }
            }];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

#pragma mark
#pragma mark Add Attachment
-(void)addAttachmentButtonPressed
{
    NSLog(@"addAttachmentButtonPressed");
    
    [self.view endEditing:YES];
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    BOOL isAddImage = false;
    
    for (int counter = 0; counter < arrayImages.count; counter++)
    {
        if ([[arrayImages objectAtIndex:counter]length] == 0)
        {
            indexOfImage = counter;
            isAddImage = true;
            break;
        }
    }
    
    if (isAddImage == true)
    {
        [self addEventImages];
    }
    else
    {
        [self showAlertWithMessage:@"You can add maximum 5 attachments."];
    }
}

#pragma mark
#pragma mark Buttons Actions
-(void)readMoreButtonPressed
{
    NSLog(@"read More Button Pressed");
}




-(void)inviteGuestsButtonPressed
{
    NSLog(@"invite guests Button Pressed");
    [self messagesButtonPressed];
}
-(void)messagesButtonPressed
{
    NSLog(@"messages Button Pressed");
    
    if(![MFMessageComposeViewController canSendText])
    {
        [self showAlertWithMessage:@"Your device doesn't support SMS!"];
        return;
    }
    
    //NSArray *recipents = @[@"12345678", @"72345524"];
    //NSString *message = [NSString stringWithFormat:@"Just sent the %@ file to your email. Please check!", file];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    //[messageController setRecipients:recipents];
    [messageController setBody:nil];
    [self presentViewController:messageController animated:YES completion:nil];
}
-(void)ticketsButtonPressed
{
    NSLog(@"tickets Button Pressed");
}

#pragma Message Composer Delegates
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [self showAlertWithMessage:@"Failed to send SMS!"];
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark
#pragma mark Document Menu Delegates
-(void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker
{
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    NSLog(@"url is %@",url);
    NSFileCoordinator *coordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
    NSError *error = nil;
    [coordinator coordinateReadingItemAtURL:url options:0 error:&error byAccessor:^(NSURL *newURL) {
        NSData *data = [NSData dataWithContentsOfURL:newURL];
        
        if (data.length == 0)
        {
            [self showAlertWithMessage:@"This document can not be uploaded."];
            return;
        }
        
        NSString *pathExtension = [url pathExtension];
        NSDate *todayDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        dateFormatter.dateFormat = @"YYYY_MM_dd_hh_mm_ss.SSS";
        NSString *fileName = [NSString stringWithFormat:@"%@.%@",[dateFormatter stringFromDate:todayDate],pathExtension];
        
        NSURL *documentDirectory = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
        NSString *documentsDirectory = [documentDirectory path];
        NSString *pathOfDocument = [documentsDirectory stringByAppendingPathComponent:fileName];
        [data writeToFile:pathOfDocument atomically:YES];
        
        
        NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
        arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
        
        if (arrayImages.count < 6)
        {
            [arrayImages replaceObjectAtIndex:indexOfImage withObject:fileName];
        }
        else
        {
            [self showAlertWithMessage:@"You can add maximum 5 attachments."];
        }
        
        [dictionaryCreateEvent setValue:arrayImages forKey:@"event_images"];
        NSLog(@"_____ %@",dictionaryCreateEvent);
        [self.collectionViewForImages reloadData];
    }];
    
    if (error)
    {
        // Do something else
    }
}

-(NSArray*)UTIs
{
    return @[ @"public.content"];
}

#pragma mark
#pragma mark Open Web View For Attachments
-(void)loadAttachmentInWebView : (NSURL*)attachment
{
    [viewForWebView removeFromSuperview];
    viewForWebView = nil;
    viewForWebView = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height) bckgroundColor:[UIColor blackColor]];
    [self.view addSubview:viewForWebView];
    
    [UIView animateWithDuration:0.3 animations:^{
        viewForWebView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
        UILabel *label = [UIFunction createLable:CGRectMake(0, 20, viewForWebView.frame.size.width, 44) bckgroundColor:[UIColor clearColor] title:@"Attachment" font:[UIFont fontWithName:fontRegular size:14.0] titleColor:[UIColor whiteColor]];
        label.textAlignment = NSTextAlignmentCenter;
        [viewForWebView addSubview:label];
        
        //        UIButton *btnCancel = [UIFunction createButton:CGRectMake(0, 20, [UIImage imageNamed:@"back_white"].size.width, [UIImage imageNamed:@"back_white"].size.height) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"back_white"] title:nil font:nil titleColor:nil];
        //        [btnCancel addTarget:self action:@selector(removeWebView) forControlEvents:UIControlEventTouchUpInside];
        //        [viewForWebView addSubview:btnCancel];
        
        UIButton *btnCancel = [UIButton buttonWithType: UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(0, 20, 44, 44);
        btnCancel.backgroundColor = [UIColor clearColor];
        [btnCancel setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(removeWebView) forControlEvents:UIControlEventTouchUpInside];
        [viewForWebView addSubview:btnCancel];
        
        
        
        WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width , self.view.frame.size.height-64)];
        NSURLRequest *request = [NSURLRequest requestWithURL:attachment];
        [webView loadRequest:request];
        webView.navigationDelegate = self;
        //webView.scalesPageToFit = true;
        [viewForWebView addSubview:webView];
    }];
}

-(void)removeWebView
{
    [UIView animateWithDuration:0.3 animations:^{
        viewForWebView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [viewForWebView removeFromSuperview];
        viewForWebView = nil;
    }];
}


-(void)backButtonPressed
{
    [[self extensionContext] completeRequestReturningItems:nil completionHandler:nil];
}


#pragma mark
#pragma mark func_SelectCountryCode
-(void)func_SelectCountryCode
{
    NSLog(@"func_SelectCountryCode");
    
    [self removeAllPickers];
    [self.view endEditing:YES];
    [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    viewForCountryCodePicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor colorWithRed:214.0/255 green:240.0/255 blue:252.0/255 alpha:1.0]];
    [self.view addSubview:viewForCountryCodePicker];
    
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(doneButtonPressedInPickerView:)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor blackColor];
    [viewForCountryCodePicker addSubview:toolBar];
    
    
    
    
    UIPickerView *pickerCountryCode =  [[UIPickerView alloc]init];
    pickerCountryCode.frame = CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, viewForCountryCodePicker.frame.size.width, 216.0);
    pickerCountryCode.delegate = self;
    pickerCountryCode.dataSource = self;
    pickerCountryCode.showsSelectionIndicator = YES;
    pickerCountryCode.backgroundColor = [UIColor clearColor];
    pickerCountryCode.tag = 480040;
    [viewForCountryCodePicker addSubview:pickerCountryCode];
    
    
    [UIView animateWithDuration:.2f animations:^{
        
        [viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
        
    } completion:^(BOOL finished) {
        
        [dictionaryCreateEvent setObject:[arrayAllCodes objectAtIndex:0] forKey:@"user_country_code"];
        [tableViewCreateEvent reloadData];
    }];
}

#pragma mark
#pragma mark Remove All Picker Views
-(void)doneButtonPressedInPickerView :(id)sender
{
    [self. view endEditing:YES];
    
    @try
    {
        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self removeAllPickers];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void)removeAllPickers
{
    [viewForCountryCodePicker removeFromSuperview];
    viewForCountryCodePicker = nil;
}

#pragma mark
#pragma mark Picker View DataSource And Delegates Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        return arrayAllCodes.count;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        return 1;
    }
    return 0;
}

- (UIView*)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (thePickerView.tag == 480040) // select user country and c ountry code
    {
        UIView* viewForPicker = [[UIView alloc]init];
        viewForPicker.frame = CGRectMake(0, 0, self.view.frame.size.width, 45);
        viewForPicker.backgroundColor = [UIColor clearColor];
        
        
        UILabel *countryName = [[UILabel alloc]init];
        countryName.frame = CGRectMake(10, 5, 240, 40);
        countryName.textAlignment = NSTextAlignmentLeft;
        countryName.backgroundColor = [UIColor clearColor];
        countryName.text = [arrayAllCountries objectAtIndex:row];
        countryName.font = [UIFont fontWithName:fontRegular size:16];
        [viewForPicker addSubview:countryName];
        
        UILabel *countryCode = [[UILabel alloc]init];
        countryCode.frame = CGRectMake(self.view.frame.size.width-90, 5, 70, 40);
        countryCode.textAlignment = NSTextAlignmentRight;
        countryCode.font = [UIFont fontWithName:fontRegular size:16];
        countryCode.backgroundColor = [UIColor clearColor];
        countryCode.text = [arrayAllCodes objectAtIndex:row];
        [viewForPicker addSubview:countryCode];
        
        return viewForPicker;
    }
    
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 480040) // select user country and country code
    {
        [dictionaryCreateEvent setObject:[arrayAllCodes objectAtIndex:row] forKey:@"user_country_code"];
        [tableViewCreateEvent reloadData];
    }
}

#pragma mark
#pragma mark ***************** Multi Options View Delegate *****************
-(void)showMultipleOptionsToAddRemoveAttachment
{
    [self.view endEditing:YES];
    
    [viewMultipleOptions removeFromSuperview];
    viewMultipleOptions = nil;
    viewMultipleOptions = [[MultiOptionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    viewMultipleOptions.MultiOptionViewDelegate=self;
    [self.view addSubview:viewMultipleOptions];
    
}
-(void) removeMultiOptionViewFunction
{
    [UIView animateWithDuration:0.3f animations:^{
        viewMultipleOptions.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [viewMultipleOptions removeFromSuperview];
        viewMultipleOptions = nil;
    }];
}

- (void)choosePhotoFromCameraButtonPressedFromMultiOptionView
{
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    BOOL isImageAdded = false;
    
    for (int counter = 0; counter < arrayImages.count; counter++)
    {
        NSString *imageName = [arrayImages objectAtIndex:counter];
        if (imageName.length == 0)
        {
            indexOfImage = counter;
            isImageAdded = true;
            break;
        }
    }
    
    if (isImageAdded == true)
    {
        isImageAdded = false;
        [self choosePhotoFromCameraButtonPressed];
    }
}
- (void)choosePhotoFromGalleryButtonPressedFromMultiOptionView
{
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    BOOL isImageAdded = false;
    
    for (int counter = 0; counter < arrayImages.count; counter++)
    {
        NSString *imageName = [arrayImages objectAtIndex:counter];
        if (imageName.length == 0)
        {
            indexOfImage = counter;
            isImageAdded = true;
            break;
        }
    }
    
    if (isImageAdded == true)
    {
        isImageAdded = false;
        [self choosePhotoFromGalleryButtonPressed];
    }
}
- (void)uploadDocumentButtonPressedFromMultiOptionView
{
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    BOOL isImageAdded = false;
    
    for (int counter = 0; counter < arrayImages.count; counter++)
    {
        NSString *imageName = [arrayImages objectAtIndex:counter];
        if (imageName.length == 0)
        {
            indexOfImage = counter;
            isImageAdded = true;
            break;
        }
    }
    
    if (isImageAdded == true)
    {
        isImageAdded = false;
        [self uploadDocumentButtonPressed];
    }
}

- (void)viewPhotoButtonPressedFromMultiOptionView
{
    [self viewPhotoButtonPressed];
}

- (void)removePhotoButtonPressedFromMultiOptionView
{
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    [arrayImages replaceObjectAtIndex:0 withObject:@""];
    [dictionaryCreateEvent setObject:arrayImages forKey:@"event_images"];
    [self.collectionViewForImages reloadData];
}

#pragma mark
#pragma mark Compare Two Dates
- (BOOL)isEndDateIsSmallerThanCurrent:(NSString *)dateOfBirth
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *enddate = [dateFormatter dateFromString:dateOfBirth];
    NSDate *currentdate = [self GetLocalDate];
    
    
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return NO;
    else
        return YES;
}

- (NSDate *) GetLocalDate
{
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    theCalendar.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    dayComponent = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSDate *localDate = [theCalendar dateFromComponents:dayComponent];
    
    return localDate;
}


-(BOOL)isEndDateIsSmallerThanEventStartDate : (NSString*)eventStartDate : (NSString*)eventEndDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *startDate = [dateFormatter dateFromString:eventStartDate];
    NSDate *endDate = [dateFormatter dateFromString:eventEndDate];
    
    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:startDate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return NO;
    else
        return YES;
    
}


#pragma mark
#pragma mark -------- Activity Indicator --------------
-(void)showActivityIndicatorWithColor : (UIColor*)color
{
    if (!activityIndicatorView)
    {
        activityIndicatorView = [[DGActivityIndicatorView alloc]initWithType:DGActivityIndicatorAnimationTypeBallSpinFadeLoader tintColor:color];
        activityIndicatorView.frame = CGRectMake(self.view.frame.size.width/2.0-32.0, 150.0, 64, 68.5714);
        [self.view addSubview:activityIndicatorView];
    }
    
    [activityIndicatorView startAnimating];
}
-(void)hideActivityIndicator
{
    [activityIndicatorView stopAnimating];
}

-(void)fetchImages {
    
    NSString *UTTypeImage = (__bridge NSString *)kUTTypeImage;
    
    for (NSExtensionItem *item in self.extensionContext.inputItems) {
        
        [item.attachments enumerateObjectsUsingBlock:^(NSItemProvider * _Nonnull itemProvider, NSUInteger idx, BOOL * _Nonnull stop) {
            
            if ([itemProvider hasItemConformingToTypeIdentifier:UTTypeImage]) {
                
                [itemProvider loadItemForTypeIdentifier:UTTypeImage options:nil completionHandler:^(id<NSSecureCoding> _Nullable itemProviderItem, NSError * _Null_unspecified error) {
                     
                         
                         NSData *imageData;
                         
                         if ([(NSObject *)itemProviderItem isKindOfClass:[NSData class]])
                         {
                             imageData = (NSData*)itemProviderItem;
                         }
                         else if ([(NSObject *)itemProviderItem isKindOfClass:[NSURL class]])
                         {
                             NSURL *imageURL = (NSURL*)itemProviderItem;
                             imageData = [NSData dataWithContentsOfURL:imageURL];
                         }
                         else if ([(NSObject *)itemProviderItem isKindOfClass:[UIImage class]])
                         {
                             // An application can share directly an UIImage.
                             // The most common case is screenshot sharing without saving to file.
                             // As screenshot using PNG format when they are saved to file we also use PNG format when saving UIImage to NSData.
                             UIImage *image = (UIImage*)itemProviderItem;
                             imageData = UIImagePNGRepresentation(image);
                         }
                         
                         if (imageData)
                         {
                             [self saveImage:imageData atIndex:idx];
                         }
                         else
                         {
                             NSLog(@"[ShareExtensionManager] sendContentToRoom: failed to loadItemForTypeIdentifier. Error: %@", error);
                         }
                 }];
            }
        }];
    }
}

-(void)saveImage: (NSData *)imageData atIndex:(NSUInteger)index {
    
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormatter.dateFormat = @"YYYY_MM_dd_hh_mm_ss.SSS";
    NSString *imageName = [NSString stringWithFormat:@"%@.jpg",[dateFormatter stringFromDate:todayDate]];
    
    NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
    NSString *documentsDirectory = [documentsURL path];//[paths objectAtIndex:0];
    NSString *pathOfImage = [documentsDirectory stringByAppendingPathComponent:imageName];  // IT IS THE PATH OF CHOOSEN IMAGE
    
    UIImage *image = [[UIImage alloc] initWithData:imageData];
    NSData *data = UIImageJPEGRepresentation(image, 0);
    [data writeToFile:pathOfImage atomically:YES];
    
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    
    if (arrayImages.count < 6)
    {
        [arrayImages replaceObjectAtIndex:index withObject:imageName];
    }
    else
    {
        [self showAlertWithMessage:@"You can add maximum 5 attachments."];
    }
    
    [dictionaryCreateEvent setValue:arrayImages forKey:@"event_images"];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.collectionViewForImages reloadData];
    });
}

- (BOOL)checkLogin {
    
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:kAppGroup];
    NSDictionary *loginData = [sharedDefaults valueForKey:kLoginData];
    if (loginData == nil) {
        
        [self showLoginError];
        return NO;
    }
    
    return YES;
}

- (void)showLoginError {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Login" message:@"Please open AltCal app and login to create an event." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [[self extensionContext] completeRequestReturningItems:nil completionHandler:nil];
    }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:true completion:nil];
}

@end
