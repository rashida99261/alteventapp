//
//  ChoosePhotoView.h
//      
//
//  Created by   on 29/01/2016.
//  Copyright © 2016. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol choosePhotoMethod;

@interface ChoosePhotoView : UIView
{
    id <choosePhotoMethod> choosePhotoDelegate;
}
-(void) removeChoosePhotoView;

@property (retain) id<choosePhotoMethod> choosePhotoDelegate;
@end

@protocol choosePhotoMethod <NSObject>

- (void)choosePhotoFromCameraButtonPressed;
- (void)choosePhotoFromGalleryButtonPressed;
- (void)uploadDocumentButtonPressed;

@end

