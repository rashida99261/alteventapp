//
//  SideMenu.h
//     
//
//  Created by   on 05/10/2015.
//  Copyright © 2015. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol sideMenuProtocol;
@interface SideMenu : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    id <sideMenuProtocol> sideMenuDelegate;
}

-(void) func_SideMenu_RemoveSideMenu;
-(void) func_SideMenu_reloadData;

@property (retain) id<sideMenuProtocol> sideMenuDelegate;

@property (atomic, weak) IBOutlet UIView *mainView;
@property (atomic, weak) IBOutlet UIButton *tapableView;
@property (atomic, weak) IBOutlet UITableView *tblMenu;
@property (atomic, weak) IBOutlet UILabel *lblVersion;
@property (atomic, weak) IBOutlet UILabel *lblUSerName;

@end


@protocol sideMenuProtocol <NSObject>

- (void)func_SideMenu_YourDay;
- (void)func_SideMenu_CreateEvent;
- (void)func_SideMenu_MyEvents;
- (void)func_SideMenu_PastEvents;
- (void)func_SideMenu_Editevents;
- (void)func_SideMenu_ChangePassword;
- (void)func_SideMenu_Logout;


@end

