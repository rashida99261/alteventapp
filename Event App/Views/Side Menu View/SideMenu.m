//
//  SideMenu.m
//     
//
//  Created by   on 05/10/2015.
//  Copyright © 2015. All rights reserved.
//

#import "SideMenu.h"



@implementation SideMenu
{
    UITableView *_tableViewSideMenu;
    NSMutableArray *_arrayTableForNames;
    NSMutableArray *_arrayTableForImages;
}


@synthesize sideMenuDelegate;


- (void)viewDidLoad
{
    
    self.view.backgroundColor = [UIColor clearColor];
    [self.view setOpaque:true];
    
    _arrayTableForNames = [[NSMutableArray alloc]initWithObjects:
                                  @"Your Day",
                                  @"Add Event",
                                  @"Your Events",
                                  @"Past Events",
                                  @"Events To Edit",
                                  @"Change Password",
                                  @"Logout",
                                  nil];

           
           _arrayTableForImages = [[NSMutableArray alloc]initWithObjects:
                                  @"your_day",
                                  @"create_event",
                                  @"my_events",
                                   @"my_events",
                                   @"my_events",
                                  @"change_password",
                                  @"",
                                  nil];

    
     NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
         _lblVersion.text = [NSString stringWithFormat:@"  Version %@",version];
         _lblVersion.font = [UIFont fontWithName:fontLight size:12.0];
         _lblVersion.textColor = [UIColor colorWithRed:115.0/255 green:115.0/255 blue:115.0/255 alpha:1.0];

             NSMutableDictionary *user_data = [[NSMutableDictionary alloc]init];
             user_data = [[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData]mutableCopy];
             NSString *name = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_name"]];
            _lblUSerName.text = name;
            _lblUSerName.textColor = [UIColor colorWithRed:68.0/255 green:68.0/255 blue:68.0/255 alpha:1.0];

    _tblMenu.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

         [_tblMenu reloadData];
           

}

- (void)viewWillAppear:(BOOL)animated
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [_tapableView addTarget:self action:@selector(func_SideMenu_RemoveSideMenu) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark
#pragma mark Table View DataSource and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrayTableForNames.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    else
    {
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
    }
    
    CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
    
    @try
    {
        
       
            cell.backgroundColor = [UIColor clearColor];

            UIView *_viewDivider = [UIFunction createUIViews:CGRectMake(0, frame.size.height-1, frame.size.width, 0.6) bckgroundColor:[UIColor colorWithRed:115.0/255 green:115.0/255 blue:115.0/255 alpha:1.0]];
            [cell.contentView addSubview:_viewDivider];

            UIImageView *imageView = [UIFunction createUIImageView:CGRectMake(10, frame.size.height/2.0-[UIImage imageNamed:[_arrayTableForImages objectAtIndex:indexPath.row]].size.height/2.0, [UIImage imageNamed:[_arrayTableForImages objectAtIndex:indexPath.row]].size.width, [UIImage imageNamed:[_arrayTableForImages objectAtIndex:indexPath.row]].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:[_arrayTableForImages objectAtIndex:indexPath.row]] isLogo:NO];
            [cell.contentView addSubview:imageView];
            
            UILabel *lblText = [UIFunction createLable:CGRectMake(imageView.frame.size.width+imageView.frame.origin.x+10, 5, frame.size.width-imageView.frame.size.width-imageView.frame.origin.x-20, frame.size.height-10) bckgroundColor:[UIColor clearColor] title:[NSString stringWithFormat:@"%@",[_arrayTableForNames objectAtIndex:indexPath.row]] font:[UIFont fontWithName:fontLight size:14.0] titleColor:[UIColor colorWithRed:115.0/255 green:115.0/255 blue:115.0/255 alpha:1.0]];
            [cell.contentView addSubview:lblText];
            
            
            if (indexPath.row == 0) // your day means today events
            {
                NSString *current_date = [UIFunction getCurrentDateInString];
                NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
                NSArray *array = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:false];
                UILabel *lblEventsCount = [UIFunction createLable:CGRectMake(frame.size.width-50, frame.size.height/2.0-20, 40, 40) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] title:[NSString stringWithFormat:@"%zd",array.count] font:[UIFont fontWithName:fontLight size:14.0] titleColor:[UIColor colorWithRed:115.0/255 green:115.0/255 blue:115.0/255 alpha:1.0]];
                lblEventsCount.layer.cornerRadius = lblEventsCount.frame.size.height/2.0;
                lblEventsCount.textAlignment = NSTextAlignmentCenter;
                lblEventsCount.clipsToBounds = true;
                [cell.contentView addSubview:lblEventsCount];
            }
            else if (indexPath.row == 2) // my events
            {
                NSString *current_date = [UIFunction getCurrentDateInString];
                NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
                NSArray *arrayMyEvents = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:true];
                NSArray *filteredEvents = [self removePastEvents:arrayMyEvents];
                
                UILabel *lblEventsCount = [UIFunction createLable:CGRectMake(frame.size.width-50, frame.size.height/2.0-20, 40, 40) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] title:[NSString stringWithFormat:@"%zd",filteredEvents.count] font:[UIFont fontWithName:fontLight size:14.0] titleColor:[UIColor colorWithRed:115.0/255 green:115.0/255 blue:115.0/255 alpha:1.0]];
                lblEventsCount.layer.cornerRadius = lblEventsCount.frame.size.height/2.0;
                lblEventsCount.textAlignment = NSTextAlignmentCenter;
                lblEventsCount.clipsToBounds = true;
                [cell.contentView addSubview:lblEventsCount];
            }
            else if (indexPath.row == 3) // past events
            {
                NSString *current_date = [UIFunction getCurrentDateInString];
                NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
                NSArray *arrayMyEvents = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:true];
                NSArray *pastEvents = [self filterPastEvents:arrayMyEvents];
                
                UILabel *lblEventsCount = [UIFunction createLable:CGRectMake(frame.size.width-50, frame.size.height/2.0-20, 40, 40) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] title:[NSString stringWithFormat:@"%zd",pastEvents.count] font:[UIFont fontWithName:fontLight size:14.0] titleColor:[UIColor colorWithRed:115.0/255 green:115.0/255 blue:115.0/255 alpha:1.0]];
                lblEventsCount.layer.cornerRadius = lblEventsCount.frame.size.height/2.0;
                lblEventsCount.textAlignment = NSTextAlignmentCenter;
                lblEventsCount.clipsToBounds = true;
                [cell.contentView addSubview:lblEventsCount];
            }
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self func_SideMenu_RemoveSideMenu];

    if (indexPath.row == 0) //your day means today's event
    {
        [self func_SideMenu_YourDay];
    }
    else if (indexPath.row == 1) // add event
    {
        [self func_SideMenu_CreateEvent];
    }
    else if (indexPath.row == 2) // my events
    {
        [self func_SideMenu_MyEvents];
    }
    else if (indexPath.row == 3)
    {
        [self func_SideMenu_PastEvents];
    }
    else if (indexPath.row == 4) // edit event
    {
        [self func_SideMenu_Editevents];
    }
    else if (indexPath.row == 5) // change password
    {
        [self func_SideMenu_ChangePassword];
    }
    else if (indexPath.row == 6) // logout
    {
        [self func_SideMenu_Logout];
    }
}

- (NSArray *)filterPastEvents: (NSArray *)events
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSMutableArray *pastEvents = [[NSMutableArray alloc] init];
    NSDate *today = [[NSDate alloc] init];
    NSString *todayString = [dateFormatter stringFromDate:today];
    NSDate *formattedTodayDate = [dateFormatter dateFromString:todayString];
    [events enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *event_date = [NSString stringWithFormat:@"%@",[obj valueForKey:@"event_date"]];
        
        if ([event_date length] > 0) {
            
            NSDate *eventDate = [dateFormatter dateFromString:event_date];
            if (eventDate != nil) {
                
                if ([formattedTodayDate compare:eventDate] == NSOrderedDescending) {
                    
                    [pastEvents addObject:obj];
                }
            }
        }
    }];
    
    return pastEvents;
}

- (NSArray *)removePastEvents: (NSArray *)events
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSMutableArray *filteredEvents = [[NSMutableArray alloc] init];
    filteredEvents = [events mutableCopy];
    NSDate *today = [[NSDate alloc] init];
    NSString *todayString = [dateFormatter stringFromDate:today];
    NSDate *formattedTodayDate = [dateFormatter dateFromString:todayString];
    [events enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *event_date = [NSString stringWithFormat:@"%@",[obj valueForKey:@"event_date"]];
        
        if ([event_date length] > 0) {
            
            NSDate *eventDate = [dateFormatter dateFromString:event_date];
            if (eventDate != nil) {
                
                if ([formattedTodayDate compare:eventDate] == NSOrderedDescending) {
                    
                    [filteredEvents removeObjectAtIndex:idx];
                }
            }
        }
    }];
    
    return filteredEvents;
}

#pragma mark
#pragma mark ************************************************************ Side Menu Delegates ************************************************************

-(void)func_SideMenu_RemoveSideMenu
{
    
    [UIView animateWithDuration:.3f animations:^{
        //self->_mainView.frame = CGRectMake(-self.frame.size.width, 0, self.frame.size.width, self.frame.size.height);
       // self.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:0];

    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
        [self dismissViewControllerAnimated:true completion:nil];
        
        CATransition *transition = [[CATransition alloc] init];
           transition.duration = 0.5;
           transition.type = kCATransitionPush;
           transition.subtype = kCATransitionFromRight;
           [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
           [self.view.window.layer addAnimation:transition forKey:kCATransition];
           [self dismissViewControllerAnimated:true completion:nil];
    }];
}

- (void)func_SideMenu_YourDay
{
    [sideMenuDelegate func_SideMenu_YourDay];
}
- (void)func_SideMenu_CreateEvent
{
    [sideMenuDelegate func_SideMenu_CreateEvent];
}

- (void)func_SideMenu_MyEvents
{
    [sideMenuDelegate func_SideMenu_MyEvents];
}

- (void)func_SideMenu_PastEvents
{
    [sideMenuDelegate func_SideMenu_PastEvents];
}

- (void)func_SideMenu_Editevents
{
    [sideMenuDelegate func_SideMenu_Editevents];
}


- (void)func_SideMenu_ChangePassword
{
    [sideMenuDelegate func_SideMenu_ChangePassword];
}

- (void)func_SideMenu_Logout
{
    [sideMenuDelegate func_SideMenu_Logout];
}

- (void)func_SideMenu_reloadData
{
    if (_tableViewSideMenu != nil) {
        
        [_tableViewSideMenu reloadData];
    }
}





@end
