//
//  MultiOptionView.h
//  Event App
//
//  Created by 01/05/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MultiOptionViewMethod;

@interface MultiOptionView : UIView
{
    id <MultiOptionViewMethod> MultiOptionViewDelegate;
}
-(void) removeMultiOptionViewFunction;
@property (retain) id<MultiOptionViewMethod> MultiOptionViewDelegate;

@end


@protocol MultiOptionViewMethod <NSObject>

- (void)choosePhotoFromCameraButtonPressedFromMultiOptionView;
- (void)choosePhotoFromGalleryButtonPressedFromMultiOptionView;
- (void)uploadDocumentButtonPressedFromMultiOptionView;
- (void)viewPhotoButtonPressedFromMultiOptionView;
- (void)removePhotoButtonPressedFromMultiOptionView;

@end
