//
//  yourDayTblCell.h
//  Event App
//
//  Created by Reinforce on 14/11/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface yourDayTblCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *imgClock;
@property (nonatomic, weak) IBOutlet UIButton *imgBlack;
@property (nonatomic, weak) IBOutlet UILabel *lblEventName;
@property (nonatomic, weak) IBOutlet UILabel *lblEventDate;
@property (nonatomic, weak) IBOutlet UIButton *btnNext;




@end

NS_ASSUME_NONNULL_END
