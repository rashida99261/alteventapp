//
//  editLaterTblCell.h
//  Event App
//
//  Created by Reinforce on 02/12/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface editLaterTblCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblEventName;
@property (nonatomic, weak) IBOutlet UILabel *lblEventDateTime;

@end

NS_ASSUME_NONNULL_END
