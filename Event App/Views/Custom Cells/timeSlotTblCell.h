//
//  timeSlotTblCell.h
//  Event App
//
//  Created by Reinforce on 21/11/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface timeSlotTblCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *btnStartDate;
@property (nonatomic, weak) IBOutlet UIButton *btnEndDate;
@property (nonatomic, weak) IBOutlet UIButton *btnPlus;
@property (nonatomic, weak) IBOutlet UIButton *btnMinus;
@property (nonatomic, weak) IBOutlet UIButton *btnPopup;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *leftIconwidth;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *rightIconwidth;

@end

NS_ASSUME_NONNULL_END
