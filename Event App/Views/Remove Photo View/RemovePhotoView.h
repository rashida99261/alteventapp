//
//  RemovePhotoView.h
//  Event App
//
//  Created by   on 20/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol removePhotoMethod;

@interface RemovePhotoView : UIView
{
    id <removePhotoMethod> removePhotoDelegate;
}

-(void) removePhotoViewFunction;

@property (retain) id<removePhotoMethod> removePhotoDelegate;
@end

@protocol removePhotoMethod <NSObject>

- (void)viewPhotoButtonPressed;
- (void)removePhotoButtonPressed;

@end


