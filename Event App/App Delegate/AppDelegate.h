//
//  AppDelegate.h
//  Event App
//
//  Created by   on 17/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
#import <CoreLocation/CoreLocation.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;
@property(strong,nonatomic)CLLocationManager *locationManager;
@property(strong,nonatomic)CLLocation *currentLocation;

-(void)functionToHandleInvalidAccessToken;
-(void)showActivityIndicatorWithColor : (UIColor*)color;
-(void)hideActivityIndicator;


#pragma mark
#pragma mark **************** AccessLocation and Its Delegates ****************
-(void)func_AccessLocation;

@end

AppDelegate *appDelegate(void);
