//
//  AppDelegate.m
//  Event App
//
//  Created by   on 17/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Appirater.h>
#import "Harpy.h"
#import "LandingViewController.h"
@import GoogleMaps;

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

@interface AppDelegate () <CLLocationManagerDelegate>

@end

@implementation AppDelegate
{
    BOOL locationFetched;
    DGActivityIndicatorView *activityIndicatorView;
}

@synthesize locationManager;
@synthesize currentLocation;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    ViewController *_viewVC = [[ViewController alloc]init];
    _navController = [[UINavigationController alloc]initWithRootViewController:_viewVC];
    _navController.navigationBarHidden = YES;
    _navController.navigationBar.tintColor = [UIColor whiteColor];
    self.window.rootViewController = _navController;
    self.window.backgroundColor = [UIColor whiteColor];
    self.navController.interactivePopGestureRecognizer.enabled = YES;
    self.navController.interactivePopGestureRecognizer.delegate =  nil;
    [self.window makeKeyAndVisible];
    
    
    if(launchOptions != nil)
    {
        NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (userInfo != nil)
        {
            NSLog(@"userInfo is %@",userInfo);
        }
    }

    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    NSString *PathDB = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"EventApp.sqlite"]];
    NSLog(@"DBPath: %@",PathDB);
    
    
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"address_from_map"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"place_from_map"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_address1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_address2"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_city"];
      [[NSUserDefaults standardUserDefaults] synchronize];
      
      [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_state"];
      [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_zipcode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
      
    
    
                 
                    
                 
                    
 
    
    /*if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0"))
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)])
        {
            // iOS 8 Notifications
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }*/

    [UIApplication sharedApplication].statusBarHidden = NO;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [UIApplication sharedApplication].networkActivityIndicatorVisible=FALSE;
    
    
    [GMSServices provideAPIKey:@"AIzaSyA3qbDqbrc2kM9uFuBjHum_RWZJIiMO3ko"];  //iOS Key kiOSKeyForGoogleAPI
    [GMSPlacesClient provideAPIKey:@"AIzaSyA3qbDqbrc2kM9uFuBjHum_RWZJIiMO3ko"];
    
    [[DataManager getSharedInstance]createDataBaseFunction];
    [self createDefaultUser];
    
    
    return YES;
}

#pragma mark
#pragma mark didReceiveRemoteNotification
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    UIApplicationState state = [application applicationState];
    if (state != UIApplicationStateActive )
    {
        // *********************************  APP IS IN BACKGROUND STATE ***************************************
        
        NSLog(@"userInfo is %@",userInfo);
    }
    else
    {
        // *********************************  APP IS IN ACTIVE STATE ***************************************
        
        NSLog(@"userInfo is %@",userInfo);
    }
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [self syncExtensionData];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}



- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options {
    NSLog(@"application openURL options is called");
    
    NSString *sourceApplication = [options objectForKey:@"UIApplicationOpenURLOptionsSourceApplicationKey"];
    NSString *urlScheme = [url scheme];
    NSString *urlQuery = [url query];
    
    
    NSLog(@"valueaaa %@",urlQuery);
    
//    // Check URL scheme and caller
//    if ([urlScheme isEqualToString:@"https"] &&
//        [sourceApplication isEqualToString:@"maps.google.com"]) {
//        // Pass it via notification
//        NSLog(@"Value: %@", urlQuery);
//        NSDictionary *data = [NSDictionary dictionaryWithObject:urlQuery forKey:@"key"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"SafariCallback" object:self userInfo:data];
//        
//        return YES;
//    }
    return YES;
}

#pragma mark
#pragma mark **************** Device Token ****************
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"])
    {
    }
    else if ([identifier isEqualToString:@"answerAction"])
    {
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString* newToken = [deviceToken description];
    newToken = [newToken stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    newToken = [newToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    if(![[NSUserDefaults standardUserDefaults] objectForKey:kDeviceToken])
    {
        [[NSUserDefaults standardUserDefaults] setValue:newToken forKey:kDeviceToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"DEVICE TOKEN IS %@",[[NSUserDefaults standardUserDefaults] valueForKey:kDeviceToken] );
    }
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    NSLog(@"Error in registration. Error: %@", err);
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kDeviceToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark
#pragma mark functionToHandleInvalidAccessToken
-(void)functionToHandleInvalidAccessToken
{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    NSString *token ;
    token = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kDeviceToken]];
    
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:kDeviceToken];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"EventApp.sqlite"];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
//        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
//        NSLog(@"DATABASE REMOVED");
    }
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[APIMaster getSharedInstance]cancelAllAPIs];
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:kAppGroup];
    [sharedDefaults removeObjectForKey:kLoginData];
    [sharedDefaults synchronize];
    
    
    
    ViewController *_viewVC = [[ViewController alloc]init];
    _navController = [[UINavigationController alloc]initWithRootViewController:_viewVC];
    _navController.navigationBarHidden = YES;
    _navController.navigationBar.tintColor = [UIColor whiteColor];
    self.window.rootViewController = _navController;
    self.window.backgroundColor = [UIColor whiteColor];
    self.navController.interactivePopGestureRecognizer.enabled = YES;
    self.navController.interactivePopGestureRecognizer.delegate =  nil;
    [self.window makeKeyAndVisible];
}


#pragma mark
#pragma mark **************** AccessLocation and Its Delegates ****************
-(void)func_AccessLocation
{
    locationManager = nil;
    locationManager = [[CLLocationManager alloc]init];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [locationManager requestWhenInUseAuthorization];
    }
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSString *currentLatitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString *currentLongitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    if (currentLatitude.length==0)
    {
        currentLatitude = @"";
    }
    
    if (currentLongitude.length==0)
    {
        currentLongitude = @"";
    }
    
    if (locationFetched == false)
    {
        locationFetched = true;
        NSString *loginUserEmail = [[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData]valueForKey:@"user_email"];
        NSString *currentTime = [UIFunction getCurrentTimeInString];
        [[DataManager getSharedInstance]InsertUserLocationTable:currentLatitude :currentLongitude :loginUserEmail : currentTime];
    }
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status)
    {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0 && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusAuthorizedWhenInUse)
            {
                [locationManager requestWhenInUseAuthorization];
            }
            else
            {
                [locationManager startUpdatingLocation];
            }
        }
            break;
        default:
        {
            [locationManager startUpdatingLocation];
        }
            break;
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([error code] == kCLErrorDenied)
    {
        //you had denied
        NSLog(@"YOU HAVE DENIED PERMISSIONS. Error while getting core location : %@",[error localizedFailureReason]);
    }
}

#pragma mark
#pragma mark -------- Activity Indicator --------------
-(void)showActivityIndicatorWithColor : (UIColor*)color
{
    if (!activityIndicatorView)
    {
        activityIndicatorView = [[DGActivityIndicatorView alloc]initWithType:DGActivityIndicatorAnimationTypeBallSpinFadeLoader tintColor:color];
        activityIndicatorView.frame = CGRectMake(self.window.frame.size.width/2.0-32.0, 150.0, 64, 68.5714);
        [self.window addSubview:activityIndicatorView];
    }
    
    [activityIndicatorView startAnimating];
}
-(void)hideActivityIndicator
{
    [activityIndicatorView stopAnimating];
}




AppDelegate *appDelegate(void){
    
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

- (void)syncExtensionData {
    
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:kAppGroup];
    NSMutableArray *arrPosts = [sharedDefaults objectForKey:kSharedPostArray];
    if (arrPosts != nil) {
      
        for (NSDictionary *dict in arrPosts) {
            
            NSString *event_city = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_city"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_topic = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_topic"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_name = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_name"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_date = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_date"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_start_time = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_start_time"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_end_time = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_end_time"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_place = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_place"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_address_first = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_address_first"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_address_second = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_address_second"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_state = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_state"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_zip_code = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_zip_code"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_phone_number = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_phone_number"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_website = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_website"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *event_notesShare = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_NotesShare"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
             NSString *event_notesNotShare = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"event_NotesNoShare"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSArray *event_images = [dict valueForKey:@"event_images"];
            
            NSString *user_email = [NSString stringWithFormat:@"%@",[dict valueForKey:@"user_email"]];
            
            NSString *latitude = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"latitude"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *longitude = [[NSString stringWithFormat:@"%@",[dict valueForKey:@"longitude"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            NSString *user_id = [NSString stringWithFormat:@"%@",[dict valueForKey:@"user_id"]];
            
            NSString *user_country_code = [NSString stringWithFormat:@"%@",[dict valueForKey:@"user_country_code"]];
            NSString *place_id = [NSString stringWithFormat:@"%@",[dict valueForKey:@"place_id"]];
            NSString *event_save_later = [NSString stringWithFormat:@"%@",[dict valueForKey:@"event_save_later"]];
            
            
            
            NSArray *updatedImages = [self saveExtensionImages:event_images];
            
            [[DataManager getSharedInstance]Insert_EventTableWithCity:event_city topic:event_topic event_name:event_name event_date:event_date event_start_time:event_start_time event_end_time:event_end_time event_place:event_place event_address_first:event_address_first event_address_second:event_address_second event_state:event_state event_zip_code:event_zip_code event_phone_number:event_phone_number event_website:event_website event_notesShare:event_notesShare event_notesNotShare:event_notesNotShare event_images:updatedImages user_email:user_email latitude:latitude longitude:longitude : user_id user_country_code: user_country_code user_place_id:place_id event_save_later:event_save_later];
        }
        
        [sharedDefaults removeObjectForKey:kSharedPostArray];
        [sharedDefaults synchronize];
        
        UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
        if ([topController isKindOfClass:[UINavigationController class]]) {
            
          //  UINavigationController *navigationVC = (UINavigationController *)topController;
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
             LandingViewController *landingVC=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
            [landingVC setNotificationAndEventsBadge];
            [self.window.rootViewController.navigationController pushViewController:landingVC animated:YES];
          //  LandingViewController * = (LandingViewController *)navigationVC.visibleViewController;
        }
    }
}

- (NSArray *)saveExtensionImages: (NSArray *)arrayImages {
    
    NSMutableArray *arrayNewImages = [[NSMutableArray alloc] initWithObjects:@"",@"",@"",@"",@"", nil];
    [arrayImages enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *imageName = [NSString stringWithFormat:@"%@", obj];
        NSURL *documentsURL = [[NSFileManager defaultManager] containerURLForSecurityApplicationGroupIdentifier:kAppGroup];
        NSString *documentsDirectory = [documentsURL path];
        NSString* filePath = [documentsDirectory stringByAppendingPathComponent:imageName];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        if (fileExists == true)
        {
            NSString *pathExtension = [filePath pathExtension];
            if (pathExtension .length > 0)
            {
                if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                {
                    UIImage *image = [UIImage imageWithContentsOfFile:filePath];
                    NSString *imageName = [self storeShareExtensionImage:image];
                    [arrayNewImages replaceObjectAtIndex:idx withObject:imageName];
                    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
                }
                else
                {
                    UIImage *image = [UIImage imageNamed:@"attachment"];
                    NSString *imageName = [self storeShareExtensionImage:image];
                    [arrayNewImages replaceObjectAtIndex:idx withObject:imageName];
                    [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
                }
            }
        }
    }];
    
    return arrayNewImages;
}

- (NSString *)storeShareExtensionImage: (UIImage *)image {
    
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormatter.dateFormat = @"YYYY_MM_dd_hh_mm_ss.SSS";
    NSString *imageName = [NSString stringWithFormat:@"%@.jpg",[dateFormatter stringFromDate:todayDate]];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathOfImage = [documentsDirectory stringByAppendingPathComponent:imageName];  // IT IS THE PATH OF CHOOSEN IMAGE
    NSData *imageData= UIImageJPEGRepresentation(image,0.0);
    [imageData writeToFile:pathOfImage atomically:YES];
    return imageName;
}

- (void)createDefaultUser {
    
    NSString *isDefaultUserStored = [[NSUserDefaults standardUserDefaults] valueForKey:kIsDefaultUserStored];
    
    if (isDefaultUserStored != nil)
    {
        return;
    }
    
    NSString *name = @"John Doe";
    NSString *email = @"altcalapp@gmail.com";
    NSString *password = @"aA12345";
    NSString *mobile_number = @"8888888888";
    NSString *user_country_code = @"+1";
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *currntDate =  [dateFormatter stringFromDate:[NSDate date]];
    
    
    
    NSLog(@"Register default user into database");
    NSMutableDictionary *dictionaryLastUser = [[[DataManager getSharedInstance]Select_Last_UserFrom_DataBaseTable]mutableCopy];
    NSInteger last_user_id = [[dictionaryLastUser valueForKey:@"user_id"]integerValue];
    
    last_user_id++;
    NSString *new_id = [NSString stringWithFormat:@"%zd",last_user_id];
    BOOL isInsertionSuccess = [[DataManager getSharedInstance]Insert_UserDataBaseTable:new_id :name :email :password :mobile_number :@"0" :user_country_code :currntDate :currntDate :@"" :@"" :@""];
                               
    if (isInsertionSuccess == YES)
    {
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:kIsDefaultUserStored];
    }
}

@end
