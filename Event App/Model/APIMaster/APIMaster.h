//
//  APIMaster.h
//     
//
//  Created by   on 09/09/2015.
//  Copyright (c) 2015   . All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "Reachability.h"

typedef void (^APICompletionHandler)(NSDictionary* responseDict);



@interface APIMaster : NSObject
{
    NSURLSessionDataTask *requestObject;
}

+(APIMaster*)getSharedInstance;
-(void)callAPIWithURL : (NSString*)url andPostDictionary : (NSDictionary*)parameters andBaseURL : (NSString*)baseURL WithCallBackResponse: (void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

-(void)callAPIWithNoParametrsAndURL : (NSString*)url andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

-(void) callAPIWithPUTMethodAndURL : (NSString*)url andPostDictionary : (NSDictionary*)parameters andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

-(void) callAPIWithDELETEMethodAndURL : (NSString*)url andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

-(void)cancelAllAPIs;

-(void)callAPIWithNoParametrsFoRGoogleAndURL : (NSString*)url withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock;

@end






