////
//  APIMaster.m
//     
//
//  Created by   on 09/09/2015.
//  Copyright (c) 2015   . All rights reserved.
//

#import "APIMaster.h"

static APIMaster *sharedInstance = nil;


@implementation APIMaster

+(APIMaster*)getSharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[super allocWithZone:NULL]init];
    }
    return sharedInstance;
}

-(void)callAPIWithURL : (NSString*)url andPostDictionary : (NSDictionary*)parameters andBaseURL : (NSString*)baseURL WithCallBackResponse: (void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock
{
    @try
    {
        url = [NSString stringWithFormat:@"%@%@",baseURL,url];
        

        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
        [manager.operationQueue cancelAllOperations];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
        [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        [manager.requestSerializer setTimeoutInterval:300];        
        requestObject = [manager POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress)
        {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject)
        {
            responseBlock(task, responseObject, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"failure is %@",error.localizedDescription);

            if([error.localizedDescription rangeOfString:@"cancelled"].location == NSNotFound)
            responseBlock(task, nil, error);
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void)callAPIWithNoParametrsAndURL : (NSString*)url andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock
{
    @try
    {
        url = [NSString stringWithFormat:@"%@%@",baseURL,url];
        url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
        [manager.operationQueue cancelAllOperations];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
        [manager.requestSerializer setTimeoutInterval:300];
        
        requestObject = [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            responseBlock(task, responseObject, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if([error.localizedDescription rangeOfString:@"cancelled"].location == NSNotFound)
                responseBlock(task, nil, error);
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void) callAPIWithPUTMethodAndURL : (NSString*)url andPostDictionary : (NSDictionary*)parameters andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock
{
    @try
    {
        url = [NSString stringWithFormat:@"%@%@",baseURL,url];
        url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
        [manager.operationQueue cancelAllOperations];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
        [manager.requestSerializer setTimeoutInterval:300];

        requestObject = [manager PUT:url parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
             responseBlock(task, responseObject, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if([error.localizedDescription rangeOfString:@"cancelled"].location == NSNotFound)
                responseBlock(task, nil, error);
        }];
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void) callAPIWithDELETEMethodAndURL : (NSString*)url andBaseURL:(NSString*)baseURL withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock
{
    @try
    {
        url = [NSString stringWithFormat:@"%@%@",baseURL,url];
        url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
        [manager.operationQueue cancelAllOperations];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
        [manager.requestSerializer setTimeoutInterval:300];
        

        requestObject = [manager DELETE:url parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            responseBlock(task, responseObject, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if([error.localizedDescription rangeOfString:@"cancelled"].location == NSNotFound)
                responseBlock(task, nil, error);
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void)cancelAllAPIs
{
    if (requestObject.state == NSURLSessionTaskStateRunning || requestObject.state == NSURLSessionTaskStateSuspended || requestObject.state == NSURLSessionTaskStateCanceling || requestObject.state == NSURLSessionTaskStateCompleted)
    {
        [requestObject cancel];
        requestObject = nil;
    }
}

-(void)callAPIWithNoParametrsFoRGoogleAndURL : (NSString*)url withCallBackResponse:(void(^)(NSURLSessionDataTask *operation, id responseObject, NSError *error))responseBlock
{
    @try
    {
        url = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]init];
        [manager.operationQueue cancelAllOperations];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"application/json"];
        [manager.requestSerializer setTimeoutInterval:300];
        
        requestObject = [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            responseBlock(task, responseObject, nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            if([error.localizedDescription rangeOfString:@"cancelled"].location == NSNotFound)
                responseBlock(task, nil, error);
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}


@end
