//
//  DataManager.h
//       
//
//  Created by   on 4/22/15.
//  Copyright (c) 2015  . All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject
{
    NSString *databasePath;
}

+(DataManager*)getSharedInstance;
-(BOOL) createDataBaseFunction;

#pragma mark ---------------------------------------------
#pragma mark Insert User Database Table
#pragma mark ---------------------------------------------
-(BOOL)Insert_UserDataBaseTable :(NSString*)user_id :(NSString*)user_name : (NSString*)user_email : (NSString*)user_password : (NSString*)user_mobile_number : (NSString*)isPhoneNumberVerified : (NSString*)user_country_code : (NSString*)user_sign_up_date : (NSString*)user_login_date : (NSString*)user_city : (NSString*)user_latitude : (NSString*)user_longitude ;
-(BOOL) Update_UserDataBaseTable :(NSString*)user_id :(NSString*)user_name : (NSString*)user_email : (NSString*)user_password : (NSString*)user_mobile_number : (NSString*)isPhoneNumberVerified : (NSString*)user_country_code : (NSString*)user_sign_up_date : (NSString*)user_login_date : (NSString*)user_city : (NSString*)user_latitude : (NSString*)user_longitude;
-(NSMutableDictionary*) Select_UserDataBaseTable : (NSString*)user_email;
-(NSMutableDictionary*) Select_Last_UserFrom_DataBaseTable;
-(NSMutableDictionary*) Select_UserDataBaseTable : (NSString*)mobile_number andCountryCode: (NSString*)countryCode;


#pragma mark ---------------------------------------------
#pragma mark Insert Remember User Database Table
#pragma mark ---------------------------------------------
-(BOOL)Insert_RememberMeTable : (NSString*)user_email : (NSString*)user_password : (NSString*)user_login_date;
-(BOOL) Update_RememberMeTable : (NSString*)user_email : (NSString*)user_password : (NSString*)user_login_date;
-(NSMutableDictionary*) Select_RememberUserTable;


#pragma mark ---------------------------------------------
#pragma mark Insert User Location Table
#pragma mark ---------------------------------------------
-(BOOL)InsertUserLocationTable :(NSString*)latitude : (NSString*)longitude : (NSString*)user_email : (NSString*)currentTime;
-(NSMutableArray*) Select_UserLocationTable : (NSString*)user_email;


#pragma mark ---------------------------------------------
#pragma mark Insert Event Database Table
#pragma mark ---------------------------------------------

-(BOOL)Insert_EventTableWithCity :(NSString*)event_city topic : (NSString*)event_topic event_name :(NSString*)event_name event_date:(NSString*)event_date event_start_time:(NSString*)event_start_time event_end_time:(NSString*)event_end_time event_place:(NSString*)event_place event_address_first:(NSString*)event_address_first event_address_second:(NSString*)event_address_second event_state:(NSString*)event_state event_zip_code:(NSString*)event_zip_code event_phone_number:(NSString*)event_phone_number event_website:(NSString*)event_website event_notesShare:(NSString*)event_notesShare event_notesNotShare:(NSString*)event_notesNotShare event_images:(NSArray*)event_images user_email:(NSString*)user_email latitude:(NSString*)latitude longitude:(NSString*)longitude : (NSString*)user_id user_country_code : (NSString*)user_country_code user_place_id : (NSString*)place_id event_save_later : (NSString*)event_save_later;



-(BOOL)Update_EventTableWithCity :(NSString*)event_city topic : (NSString*)event_topic event_name :(NSString*)event_name event_date:(NSString*)event_date event_start_time:(NSString*)event_start_time event_end_time:(NSString*)event_end_time event_place:(NSString*)event_place event_address_first:(NSString*)event_address_first event_address_second:(NSString*)event_address_second event_state:(NSString*)event_state event_zip_code:(NSString*)event_zip_code event_phone_number:(NSString*)event_phone_number event_website:(NSString*)event_website event_notesShare:(NSString*)event_notesShare event_notesNotShare:(NSString*)event_notesNotShare event_images:(NSArray*)event_images event_id: (NSString*)event_id user_email:(NSString*)user_email latitude:(NSString*)latitude longitude:(NSString*)longitude : (NSString*)user_id user_country_code : (NSString*)user_country_code user_place_id : (NSString*)place_id event_save_later : (NSString*)event_save_later;

-(NSMutableArray*) Select_MyEventsWithEmail : (NSString*)user_email andDate: (NSString*)event_date forMyEvents : (BOOL)isMyEvents;

-(NSMutableArray*) Select_MyEventsWithSaveLater : (NSString*)user_email;
-(NSMutableDictionary*) selectEventWithId : (NSString*)event_id;
-(BOOL) deleteEventWithId : (NSString*)event_id;




@end










