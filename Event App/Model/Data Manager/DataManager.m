//
//  DataManager.m
//       
//
//  Created by   on 4/22/15.
//  Copyright (c) 2015  . All rights reserved.
//

#import "DataManager.h"
#import <sqlite3.h>


static DataManager *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;


@implementation DataManager


+(DataManager*)getSharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDataBaseFunction];
    }
    return sharedInstance;
}

#pragma mark
#pragma mark CreateDataBase
-(BOOL)createDataBaseFunction
{
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"EventApp.sqlite"]];
    
    BOOL isSuccess = YES;
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        char *errMsg;
        const char *sql_stmt =
        "CREATE TABLE IF NOT EXISTS 'aU-user-signup' (user_id TEXT, user_name TEXT, user_email TEXT PRIMARY KEY, user_password TEXT, user_mobile_number TEXT, isPhoneNumberVerified TEXT, user_country_code TEXT, user_sign_up_date TEXT, user_login_date TEXT, user_city TEXT, user_latitude TEXT, user_longitude TEXT)";
        
        const char *sql_stmt1 =
        "CREATE TABLE IF NOT EXISTS 'aF-user-login' (user_id TEXT PRIMARY KEY, user_email TEXT, user_password TEXT, user_login_date TEXT)";
        
        const char *sql_stmt2 =
        "CREATE TABLE IF NOT EXISTS 'aU-user-location' (latitude TEXT, longitude TEXT, user_email TEXT, currentTime TEXT)";

        const char *sql_stmt3 = 
        "CREATE TABLE IF NOT EXISTS 'aU-events' (event_id INTEGER PRIMARY KEY AUTOINCREMENT, event_city TEXT, event_topic TEXT , event_name TEXT, event_date TEXT, event_start_time TEXT, event_end_time TEXT, event_place TEXT,event_address_first TEXT, event_address_second TEXT, event_state TEXT, event_zip_code TEXT, event_phone_number TEXT, event_website TEXT,  event_NotesShare TEXT,event_NotesNoShare TEXT, event_images TEXT, user_email TEXT, latitude TEXT, longitude TEXT, user_id TEXT, user_country_code TEXT, place_id TEXT, event_save_later TEXT)";
        
       

        
        if ( (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) || (sqlite3_exec(database, sql_stmt1, NULL, NULL, &errMsg) != SQLITE_OK) || (sqlite3_exec(database, sql_stmt2, NULL, NULL, &errMsg) != SQLITE_OK)  || (sqlite3_exec(database, sql_stmt3, NULL, NULL, &errMsg) != SQLITE_OK))
        {
            isSuccess = NO;
            NSLog(@"Failed to create table: %s",errMsg);
        }
        sqlite3_close(database);
        return  isSuccess;
    }
    else
    {
        isSuccess = NO;
        NSLog(@"Failed to open/create database");
    }
    return isSuccess;
}


#pragma mark ---------------------------------------------
#pragma mark Insert User Database Table
#pragma mark ---------------------------------------------

-(BOOL)Insert_UserDataBaseTable :(NSString*)user_id :(NSString*)user_name : (NSString*)user_email : (NSString*)user_password : (NSString*)user_mobile_number : (NSString*)isPhoneNumberVerified :(NSString*)user_country_code : (NSString*)user_sign_up_date : (NSString*)user_login_date : (NSString*)user_city : (NSString*)user_latitude : (NSString*)user_longitude
{
    user_id                 =   [user_id stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_name               =   [user_name stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_email              =   [user_email stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_password           =   [user_password stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_mobile_number      =   [user_mobile_number stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    isPhoneNumberVerified   =   [isPhoneNumberVerified stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_country_code       =   [user_country_code stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_sign_up_date       =   [user_sign_up_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_login_date         =   [user_login_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_city               =   [user_city stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_latitude           =   [user_latitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_longitude          =   [user_longitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    

    
  //  INTEGER PRIMARY KEY AUTOINCREMENT
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertStatement = [NSString stringWithFormat: @"INSERT INTO 'aU-user-signup'(user_id, user_name, user_email, user_password, user_mobile_number, isPhoneNumberVerified, user_country_code, user_sign_up_date, user_login_date, user_city, user_latitude, user_longitude) VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",user_id, user_name, user_email, user_password, user_mobile_number, isPhoneNumberVerified, user_country_code, user_sign_up_date, user_login_date, user_city, user_latitude, user_longitude];
        
        const char *insert_stmt = [insertStatement UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Insertion Successful in user database table.");
            sqlite3_reset(statement);
            return true;
        }
        else
        {
            NSLog(@"Insertion Failure in user database table : %s",sqlite3_errmsg(database));
            sqlite3_reset(statement);
            return false;
        }
    }
    return false;
}


-(BOOL) Update_UserDataBaseTable :(NSString*)user_id :(NSString*)user_name : (NSString*)user_email : (NSString*)user_password : (NSString*)user_mobile_number : (NSString*)isPhoneNumberVerified : (NSString*)user_country_code : (NSString*)user_sign_up_date : (NSString*)user_login_date : (NSString*)user_city : (NSString*)user_latitude : (NSString*)user_longitude
{
    user_id                 =   [user_id stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_name               =   [user_name stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_email              =   [user_email stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_password           =   [user_password stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_mobile_number      =   [user_mobile_number stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    isPhoneNumberVerified   =   [isPhoneNumberVerified stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_country_code       =   [user_country_code stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_sign_up_date       =   [user_sign_up_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_login_date         =   [user_login_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_city               =   [user_city stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_latitude           =   [user_latitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_longitude          =   [user_longitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    

    
    BOOL isSuccess = NO;
    sqlite3_stmt *updateStmt = nil;
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
//        NSString *query = [NSString stringWithFormat: @"UPDATE 'aU-user-signup'  SET user_id = '%@' , user_name = '%@', user_email = '%@', user_password = '%@', user_mobile_number = '%@', isPhoneNumberVerified = '%@', user_country_code = '%@'  WHERE user_email = '%@'  ",user_id, user_name,  user_email,  user_password, user_mobile_number, isPhoneNumberVerified, user_country_code, user_email];
        
        NSString *query = [NSString stringWithFormat: @"UPDATE 'aU-user-signup'  SET user_id = '%@' , user_name = '%@', user_email = '%@', user_password = '%@', user_mobile_number = '%@', isPhoneNumberVerified = '%@', user_country_code = '%@' , user_sign_up_date = '%@', user_login_date = '%@', user_city = '%@', user_latitude = '%@', user_longitude = '%@' WHERE user_email = '%@'  ",user_id, user_name,  user_email,  user_password, user_mobile_number, isPhoneNumberVerified, user_country_code, user_sign_up_date, user_login_date, user_city, user_latitude, user_longitude, user_email];
        
        const char *sql =  [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sql, -1, &updateStmt, NULL)==SQLITE_OK)
        {
            sqlite3_bind_text(updateStmt, 0, [user_id UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 1, [user_name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 2, [user_email UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 3, [user_password UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 4, [user_mobile_number UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 5, [isPhoneNumberVerified UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 6, [user_country_code UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_text(updateStmt, 7, [user_sign_up_date UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 8, [user_login_date UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 9, [user_city UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 10,[user_latitude UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 11,[user_longitude UTF8String], -1, SQLITE_TRANSIENT);
        }
    }
    
    if(SQLITE_DONE != sqlite3_step(updateStmt))
    {
        NSLog(@"Error while updating user table. %s", sqlite3_errmsg(database));
        isSuccess = NO;
    }
    else
    {
        isSuccess = YES;
    }
    sqlite3_reset(statement);
    return isSuccess;
}


-(NSMutableDictionary*) Select_UserDataBaseTable : (NSString*)user_email
{
    NSMutableDictionary *dictionaryUserData = [[NSMutableDictionary alloc]init];
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aU-user-signup' where user_email = '%@' ORDER BY user_email DESC LIMIT 1",user_email];
        
        const char *query_stmt1 = [querySQL1 UTF8String];
        if (sqlite3_prepare_v2(database,query_stmt1, -1, &statement, NULL) == SQLITE_OK )
        {
            int j=1;
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *user_name = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSString *user_email = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSString *user_password = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                NSString *user_mobile_number = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)];
                NSString *isPhoneNumberVerified = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)];
                NSString *user_id = [[NSString alloc]init];
                char *tmp1 = (char *)sqlite3_column_text(statement, 0);
                if (tmp1 == NULL)
                    user_id = nil;
                else
                    user_id = [[NSString alloc] initWithUTF8String:tmp1];

                NSString *user_country_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)];
                
                NSString *user_sign_up_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)];
                NSString *user_login_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)];
                NSString *user_city = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 9)];
                NSString *user_latitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 10)];
                NSString *user_longitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 11)];

                
                user_id                 =   [user_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_name               =   [user_name stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_email              =   [user_email stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_password           =   [user_password stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_mobile_number      =   [user_mobile_number stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                isPhoneNumberVerified   =   [isPhoneNumberVerified stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_country_code       =   [user_country_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_sign_up_date       =   [user_sign_up_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_login_date         =   [user_login_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_city               =   [user_city stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_latitude           =   [user_latitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_longitude          =   [user_longitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                
                [dictionaryUserData setValue:user_id forKey:@"user_id"];
                [dictionaryUserData setValue:user_name forKey:@"user_name"];
                [dictionaryUserData setValue:user_email forKey:@"user_email"];
                [dictionaryUserData setValue:user_password forKey:@"user_password"];
                [dictionaryUserData setValue:user_mobile_number forKey:@"user_mobile_number"];
                [dictionaryUserData setValue:isPhoneNumberVerified forKey:@"isPhoneNumberVerified"];
                [dictionaryUserData setValue:user_country_code forKey:@"user_country_code"];
                [dictionaryUserData setValue:user_sign_up_date forKey:@"user_sign_up_date"];
                [dictionaryUserData setValue:user_login_date forKey:@"user_login_date"];
                [dictionaryUserData setValue:user_city forKey:@"user_city"];
                [dictionaryUserData setValue:user_latitude forKey:@"user_latitude"];
                [dictionaryUserData setValue:user_longitude forKey:@"user_longitude"];

                j++;
            }
        }
        sqlite3_reset(statement);
    }
    
    return dictionaryUserData;
}

-(NSMutableDictionary*) Select_Last_UserFrom_DataBaseTable
{
    NSMutableDictionary *dictionaryUserData = [[NSMutableDictionary alloc]init];
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aU-user-signup' ORDER BY user_id DESC LIMIT 1"];
        
        const char *query_stmt1 = [querySQL1 UTF8String];
        if (sqlite3_prepare_v2(database,query_stmt1, -1, &statement, NULL) == SQLITE_OK )
        {
            int j=1;
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *user_name = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSString *user_email = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSString *user_password = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                NSString *user_mobile_number = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)];
                NSString *isPhoneNumberVerified = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)];
                NSString *user_id = [[NSString alloc]init];
                char *tmp1 = (char *)sqlite3_column_text(statement, 0);
                if (tmp1 == NULL)
                    user_id = nil;
                else
                    user_id = [[NSString alloc] initWithUTF8String:tmp1];
                
                NSString *user_country_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)];
                
                NSString *user_sign_up_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)];
                NSString *user_login_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)];
                NSString *user_city = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 9)];
                NSString *user_latitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 10)];
                NSString *user_longitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 11)];
                
                
                user_id                 =   [user_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_name               =   [user_name stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_email              =   [user_email stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_password           =   [user_password stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_mobile_number      =   [user_mobile_number stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                isPhoneNumberVerified   =   [isPhoneNumberVerified stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_country_code       =   [user_country_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_sign_up_date       =   [user_sign_up_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_login_date         =   [user_login_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_city               =   [user_city stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_latitude           =   [user_latitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_longitude          =   [user_longitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];

                [dictionaryUserData setValue:user_id forKey:@"user_id"];
                [dictionaryUserData setValue:user_name forKey:@"user_name"];
                [dictionaryUserData setValue:user_email forKey:@"user_email"];
                [dictionaryUserData setValue:user_password forKey:@"user_password"];
                [dictionaryUserData setValue:user_mobile_number forKey:@"user_mobile_number"];
                [dictionaryUserData setValue:isPhoneNumberVerified forKey:@"isPhoneNumberVerified"];
                [dictionaryUserData setValue:user_country_code forKey:@"user_country_code"];
                [dictionaryUserData setValue:user_sign_up_date forKey:@"user_sign_up_date"];
                [dictionaryUserData setValue:user_login_date forKey:@"user_login_date"];
                [dictionaryUserData setValue:user_city forKey:@"user_city"];
                [dictionaryUserData setValue:user_latitude forKey:@"user_latitude"];
                [dictionaryUserData setValue:user_longitude forKey:@"user_longitude"];
                
                j++;
            }
        }
        sqlite3_reset(statement);
    }
    
    return dictionaryUserData;
}


#pragma mark ---------------------------------------------
#pragma mark Insert Remember User Database Table
#pragma mark ---------------------------------------------

-(BOOL)Insert_RememberMeTable : (NSString*)user_email : (NSString*)user_password : (NSString*)user_login_date
{
    user_email              =   [user_email stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_password           =   [user_password stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_login_date         =   [user_login_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    

    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertStatement = [NSString stringWithFormat: @"INSERT INTO 'aF-user-login'(user_id, user_email, user_password, user_login_date) VALUES ('%@', '%@', '%@', '%@')",@"1", user_email, user_password, user_login_date];
        
        const char *insert_stmt = [insertStatement UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Insertion Successful in user remember me table.");
            sqlite3_reset(statement);
            return true;
        }
        else
        {
            NSLog(@"Insertion Failure in user remember me table : %s",sqlite3_errmsg(database));
            sqlite3_reset(statement);
            return false;
        }
    }
    return false;
}

-(BOOL) Update_RememberMeTable : (NSString*)user_email : (NSString*)user_password : (NSString*)user_login_date
{
    user_email              =   [user_email stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_password           =   [user_password stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_login_date         =   [user_login_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    
    BOOL isSuccess = NO;
    sqlite3_stmt *updateStmt = nil;
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *query = [NSString stringWithFormat: @"UPDATE 'aF-user-login'  SET user_id = '%@', user_email = '%@', user_password = '%@' , user_login_date = '%@' WHERE user_id = '%@'  ", @"1", user_email,  user_password, user_login_date ,@"1"];
        
        const char *sql =  [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sql, -1, &updateStmt, NULL)==SQLITE_OK)
        {
            sqlite3_bind_text(updateStmt, 0, [@"1" UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 1, [user_email UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 2, [user_password UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 3, [user_login_date UTF8String], -1, SQLITE_TRANSIENT);
        }
    }
    
    if(SQLITE_DONE != sqlite3_step(updateStmt))
    {
        NSLog(@"Error while updating user table. %s", sqlite3_errmsg(database));
        isSuccess = NO;
    }
    else
    {
        isSuccess = YES;
    }
    sqlite3_reset(statement);
    return isSuccess;
}

-(NSMutableDictionary*) Select_RememberUserTable
{
    NSMutableDictionary *dictionaryUserData = [[NSMutableDictionary alloc]init];
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aF-user-login' where user_id = '%@' ORDER BY user_id DESC LIMIT 1",@"1"];
        
        const char *query_stmt1 = [querySQL1 UTF8String];
        if (sqlite3_prepare_v2(database,query_stmt1, -1, &statement, NULL) == SQLITE_OK )
          
        sqlite3_reset(statement);
    }
    
    return dictionaryUserData;
}

#pragma mark ---------------------------------------------
#pragma mark Insert User Location Table
#pragma mark ---------------------------------------------

-(BOOL)InsertUserLocationTable :(NSString*)latitude : (NSString*)longitude : (NSString*)user_email : (NSString*)currentTime
{
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertStatement = [NSString stringWithFormat: @"INSERT INTO 'aU-user-location'(latitude, longitude, user_email, currentTime) VALUES ('%@', '%@', '%@', '%@')",latitude, longitude, user_email, currentTime];
        
        const char *insert_stmt = [insertStatement UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Insertion Successful in user location database table.");
            sqlite3_reset(statement);
            return true;
        }
        else
        {
            NSLog(@"Insertion Failure in user location table : %s",sqlite3_errmsg(database));
            sqlite3_reset(statement);
            return false;
        }
    }
    return false;
}


-(NSMutableArray*) Select_UserLocationTable : (NSString*)user_email
{
    NSMutableArray *arrayUserLocation = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aU-user-location' where user_email = '%@'",user_email];
        
        const char *query_stmt1 = [querySQL1 UTF8String];
        if (sqlite3_prepare_v2(database,query_stmt1, -1, &statement, NULL) == SQLITE_OK )
        {
            int j=1;
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *latitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                NSString *longitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSString *user_email = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSString *currentTime = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];

                
                NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:
                                            latitude,@"latitude",
                                            longitude,@"longitude",
                                            user_email,@"user_email",
                                            currentTime,@"currentTime",
                                            nil];
                [arrayUserLocation addObject:dictionary];
                j++;
            }
        }
        sqlite3_reset(statement);
    }
    
    return arrayUserLocation;
}


#pragma mark ---------------------------------------------
#pragma mark Insert Event Database Table
#pragma mark ---------------------------------------------

-(BOOL)Insert_EventTableWithCity :(NSString*)event_city topic : (NSString*)event_topic event_name :(NSString*)event_name event_date:(NSString*)event_date event_start_time:(NSString*)event_start_time event_end_time:(NSString*)event_end_time event_place:(NSString*)event_place event_address_first:(NSString*)event_address_first event_address_second:(NSString*)event_address_second event_state:(NSString*)event_state event_zip_code:(NSString*)event_zip_code event_phone_number:(NSString*)event_phone_number event_website:(NSString*)event_website event_notesShare:(NSString*)event_notesShare event_notesNotShare:(NSString*)event_notesNotShare event_images:(NSArray*)event_images user_email:(NSString*)user_email latitude:(NSString*)latitude longitude:(NSString*)longitude : (NSString*)user_id user_country_code : (NSString*)user_country_code user_place_id : (NSString*)place_id event_save_later : (NSString*)event_save_later
{
    NSData * Data = [NSJSONSerialization dataWithJSONObject:event_images options:kNilOptions error:nil];
    NSString *imagesString = [[NSString alloc]initWithData:Data encoding:NSUTF8StringEncoding];

    event_city                          =       [event_city stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_topic                         =       [event_topic stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_name                          =       [event_name stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_date                          =       [event_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_start_time                    =       [event_start_time stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_end_time                      =       [event_end_time stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_place                         =       [event_place stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_address_first                 =       [event_address_first stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_address_second                =       [event_address_second stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_state                         =       [event_state stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_zip_code                      =       [event_zip_code stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_phone_number                  =       [event_phone_number stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_website                       =       [event_website stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_notesShare                   =        [event_notesShare stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_notesNotShare                   =      [event_notesNotShare stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    imagesString                        =       [imagesString stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_email                          =       [user_email stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    latitude                            =       [latitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    longitude                           =       [longitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_id                             =       [user_id stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_country_code                   =       [user_country_code stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    place_id                            =       [place_id stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_save_later                            =       [event_save_later stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    
     // ,
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *insertStatement = [NSString stringWithFormat: @"INSERT INTO 'aU-events'(event_city, event_topic, event_name, event_date, event_start_time, event_end_time, event_place, event_address_first, event_address_second, event_state, event_zip_code, event_phone_number, event_website, event_NotesShare, event_NotesNoShare ,event_images, user_email, latitude, longitude, user_id, user_country_code, place_id, event_save_later) VALUES ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@')",event_city, event_topic, event_name, event_date, event_start_time,event_end_time,event_place ,event_address_first,event_address_second,event_state, event_zip_code, event_phone_number, event_website, event_notesShare,event_notesNotShare,imagesString, user_email, latitude, longitude, user_id, user_country_code,place_id,event_save_later];
        
        const char *insert_stmt = [insertStatement UTF8String];
        sqlite3_prepare_v2(database, insert_stmt,-1, &statement, NULL);
        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"Insertion Successful in event database table.");
            sqlite3_reset(statement);
            return true;
        }
        else
        {
            NSLog(@"Insertion Failure in event database table : %s",sqlite3_errmsg(database));
            sqlite3_reset(statement);
            return false;
        }
    }
    return false;
}


-(BOOL)Update_EventTableWithCity :(NSString*)event_city topic : (NSString*)event_topic event_name :(NSString*)event_name event_date:(NSString*)event_date event_start_time:(NSString*)event_start_time event_end_time:(NSString*)event_end_time event_place:(NSString*)event_place event_address_first:(NSString*)event_address_first event_address_second:(NSString*)event_address_second event_state:(NSString*)event_state event_zip_code:(NSString*)event_zip_code event_phone_number:(NSString*)event_phone_number event_website:(NSString*)event_website event_notesShare:(NSString*)event_notesShare event_notesNotShare:(NSString*)event_notesNotShare event_images:(NSArray*)event_images event_id: (NSString*)event_id user_email:(NSString*)user_email latitude:(NSString*)latitude longitude:(NSString*)longitude : (NSString*)user_id user_country_code : (NSString*)user_country_code user_place_id : (NSString*)place_id event_save_later : (NSString*)event_save_later
{
    NSData * Data = [NSJSONSerialization dataWithJSONObject:event_images options:kNilOptions error:nil];
    NSString *imagesString = [[NSString alloc]initWithData:Data encoding:NSUTF8StringEncoding];
    
    event_city                          =       [event_city stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_topic                         =       [event_topic stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_name                          =       [event_name stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_date                          =       [event_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_start_time                    =       [event_start_time stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_end_time                      =       [event_end_time stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_place                         =       [event_place stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_address_first                 =       [event_address_first stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_address_second                =       [event_address_second stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_state                         =       [event_state stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_zip_code                      =       [event_zip_code stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_phone_number                  =       [event_phone_number stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_website                       =       [event_website stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_notesShare                    =       [event_notesShare stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_notesNotShare                 =       [event_notesNotShare stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    imagesString                        =       [imagesString stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_email                          =       [user_email stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    latitude                            =       [latitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    longitude                           =       [longitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_id                             =       [user_id stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    user_country_code                   =       [user_country_code stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    place_id                            =       [place_id stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
    event_save_later                    =       [event_save_later stringByReplacingOccurrencesOfString:@"'" withString:@"`"];

    
    
    BOOL isSuccess = NO;
    sqlite3_stmt *updateStmt = nil;
    const char *dbpath = [databasePath UTF8String];
    if(sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *query = [NSString stringWithFormat: @"UPDATE 'aU-events'  SET event_city = '%@', event_topic = '%@', event_name = '%@', event_date = '%@', event_start_time = '%@', event_end_time = '%@',event_place = '%@', event_address_first = '%@', event_address_second = '%@',event_state = '%@' ,event_zip_code = '%@', event_phone_number = '%@', event_website = '%@', event_NotesShare = '%@', event_NotesNoShare = '%@', event_images = '%@', user_email = '%@', latitude = '%@', longitude = '%@', user_id = '%@' , user_country_code = '%@' , place_id = '%@' , event_save_later = '%@' WHERE event_id = '%@'  ",event_city,  event_topic,  event_name, event_date, event_start_time, event_end_time,event_place ,event_address_first, event_address_second, event_state, event_zip_code, event_phone_number, event_website, event_notesShare,event_notesNotShare, imagesString, user_email, latitude, longitude, user_id, user_country_code, place_id,event_save_later, event_id];
        
        const char *sql =  [query UTF8String];
        
        if(sqlite3_prepare_v2(database, sql, -1, &updateStmt, NULL)==SQLITE_OK)
        {
            sqlite3_bind_text(updateStmt, 1, [event_city UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 2, [event_topic UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 3, [event_name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 4, [event_date UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 5, [event_start_time UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 6, [event_end_time UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 7, [event_place UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 8, [event_address_first UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 9, [event_address_second UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 10, [event_state UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 11, [event_zip_code UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 12, [event_phone_number UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 13, [event_website UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 14, [event_notesShare UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 15, [event_notesNotShare UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 16, [imagesString UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 17, [user_email UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 18, [latitude UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 19, [longitude UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 20, [user_id UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 21, [place_id UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 22, [user_country_code UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(updateStmt, 23, [event_save_later UTF8String], -1, SQLITE_TRANSIENT);
           
        }
        
    }
    
    if(SQLITE_DONE != sqlite3_step(updateStmt))
    {
        NSLog(@"Error while updating event table. %s", sqlite3_errmsg(database));
        isSuccess = NO;
    }
    else
    {
        isSuccess = YES;
    }
    sqlite3_reset(statement);
    return isSuccess;
}


-(NSMutableArray*) Select_MyEventsWithEmail : (NSString*)user_email andDate: (NSString*)event_date forMyEvents : (BOOL)isMyEvents
{
    NSMutableArray *arrayEvents = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL1 = @"";
        
        if (isMyEvents == true) // this is for my events
        {
            querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aU-events' where user_email = '%@' ORDER BY event_id DESC",user_email];
        }
        else // this is for todays events of login user
        {
            querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aU-events' where event_date = '%@' AND user_email = '%@' ORDER BY event_id DESC",event_date, user_email];
        }
        
        const char *query_stmt1 = [querySQL1 UTF8String];
        if (sqlite3_prepare_v2(database,query_stmt1, -1, &statement, NULL) == SQLITE_OK )
        {
            int j=1;
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *event_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                NSString *event_city = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSString *event_topic = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSString *event_name = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                NSString *event_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)];
                NSString *event_start_time = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)];
                NSString *event_end_time = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)];
                NSString *event_place = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)];;
                NSString *event_address_first = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)];
                NSString *event_address_second = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 9)];
                NSString *event_state = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 10)];
                NSString *event_zip_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 11)];
                NSString *event_phone_number = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 12)];
                NSString *event_website = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 13)];
                NSString *event_notesShare = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 14)];
                NSString *event_notesNotShare = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 15)];
                NSString *event_images = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 16)];
                NSString *user_email = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 17)];
                NSString *latitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 18)];
                NSString *longitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 19)];
                NSString *user_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 20)];
                NSString *user_country_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 22)];
                NSString *place_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 21)];
                NSString *event_save_later = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 23)];

                
                event_city                          =       [event_city stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_topic                         =       [event_topic stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_name                          =       [event_name stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_date                          =       [event_date stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_start_time                    =       [event_start_time stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_end_time                      =       [event_end_time stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_place                         =       [event_place stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_address_first                 =       [event_address_first stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_address_second                =       [event_address_second stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_state                         =       [event_state stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_zip_code                      =       [event_zip_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_phone_number                  =       [event_phone_number stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_website                       =       [event_website stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_notesShare                    =       [event_notesShare stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_notesNotShare                 =       [event_notesNotShare stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_images                        =       [event_images stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_email                          =       [user_email stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                latitude                            =       [latitude stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                longitude                           =       [longitude stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_id                             =       [user_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_country_code                   =       [user_country_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                place_id                            =       [place_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_save_later                    =       [event_save_later stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                
                NSData *imagesData = [event_images dataUsingEncoding:NSUTF8StringEncoding];
                NSArray *imagesArray = [[NSArray alloc]init];
                if (imagesData.length>0)
                {
                    imagesArray = [NSJSONSerialization JSONObjectWithData:imagesData options:0 error:nil];
                }
                
                NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:
                                            event_id,@"event_id",
                                            event_city,@"event_city",
                                            event_topic,@"event_topic",
                                            event_name,@"event_name",
                                            event_date,@"event_date",
                                            event_start_time,@"event_start_time",
                                            event_end_time,@"event_end_time",
                                            event_place,@"event_place",
                                            event_address_first,@"event_address_first",
                                            event_address_second,@"event_address_second",
                                            event_state,@"event_state",
                                            event_zip_code,@"event_zip_code",
                                            event_phone_number,@"event_phone_number",
                                            event_website,@"event_website",
                                            event_notesShare,@"event_NotesShare",
                                            event_notesNotShare,@"event_NotesNoShare",
                                            imagesArray,@"event_images",
                                            user_email,@"user_email",
                                            latitude,@"latitude",
                                            longitude,@"longitude",
                                            user_id,@"user_id",
                                            user_country_code,@"user_country_code",
                                            place_id,@"place_id",
                                            event_save_later,@"event_save_later",
                                            nil];
                
                [arrayEvents addObject:dictionary];
                j++;
            }
        }
        sqlite3_reset(statement);
    }
    
    return arrayEvents;
}

-(NSMutableArray*) Select_MyEventsWithSaveLater : (NSString*)user_email
{
    NSMutableArray *arrayEvents = [[NSMutableArray alloc]init];
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        
      
        NSString *querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aU-events' where user_email = '%@' ORDER BY event_id DESC", user_email];
        
       
        
        const char *query_stmt1 = [querySQL1 UTF8String];
        if (sqlite3_prepare_v2(database,query_stmt1, -1, &statement, NULL) == SQLITE_OK )
        {
            int j=1;
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *event_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                NSString *event_city = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSString *event_topic = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSString *event_name = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                NSString *event_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)];
                NSString *event_start_time = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)];
                NSString *event_end_time = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)];
                NSString *event_place = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)];;
                NSString *event_address_first = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)];
                NSString *event_address_second = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 9)];
                NSString *event_state = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 10)];
                NSString *event_zip_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 11)];
                NSString *event_phone_number = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 12)];
                NSString *event_website = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 13)];
                NSString *event_notesShare = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 14)];
                NSString *event_notesNotShare = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 15)];
                NSString *event_images = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 16)];
                NSString *user_email = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 17)];
                NSString *latitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 18)];
                NSString *longitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 19)];
                NSString *user_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 20)];
                NSString *user_country_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 22)];
                NSString *place_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 21)];
                NSString *event_save_later = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 23)];

                
                event_city                          =       [event_city stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_topic                         =       [event_topic stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_name                          =       [event_name stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_date                          =       [event_date stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_start_time                    =       [event_start_time stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_end_time                      =       [event_end_time stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_place                         =       [event_place stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_address_first                 =       [event_address_first stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_address_second                =       [event_address_second stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_state                         =       [event_state stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_zip_code                      =       [event_zip_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_phone_number                  =       [event_phone_number stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_website                       =       [event_website stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_notesShare                    =       [event_notesShare stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_notesNotShare                 =       [event_notesNotShare stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_images                        =       [event_images stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_email                          =       [user_email stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                latitude                            =       [latitude stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                longitude                           =       [longitude stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_id                             =       [user_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_country_code                   =       [user_country_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                place_id                            =       [place_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_save_later                    =       [event_save_later stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                
                NSData *imagesData = [event_images dataUsingEncoding:NSUTF8StringEncoding];
                NSArray *imagesArray = [[NSArray alloc]init];
                if (imagesData.length>0)
                {
                    imagesArray = [NSJSONSerialization JSONObjectWithData:imagesData options:0 error:nil];
                }
                
                NSDictionary *dictionary = [[NSDictionary alloc]initWithObjectsAndKeys:
                                            event_id,@"event_id",
                                            event_city,@"event_city",
                                            event_topic,@"event_topic",
                                            event_name,@"event_name",
                                            event_date,@"event_date",
                                            event_start_time,@"event_start_time",
                                            event_end_time,@"event_end_time",
                                            event_place,@"event_place",
                                            event_address_first,@"event_address_first",
                                            event_address_second,@"event_address_second",
                                            event_state,@"event_state",
                                            event_zip_code,@"event_zip_code",
                                            event_phone_number,@"event_phone_number",
                                            event_website,@"event_website",
                                            event_notesShare,@"event_NotesShare",
                                            event_notesNotShare,@"event_NotesNoShare",
                                            imagesArray,@"event_images",
                                            user_email,@"user_email",
                                            latitude,@"latitude",
                                            longitude,@"longitude",
                                            user_id,@"user_id",
                                            user_country_code,@"user_country_code",
                                            place_id,@"place_id",
                                            event_save_later,@"event_save_later",
                                            nil];
                
                [arrayEvents addObject:dictionary];
                j++;
            }
        }
        sqlite3_reset(statement);
    }
    
    return arrayEvents;
}


-(NSMutableDictionary*) selectEventWithId : (NSString*)event_id
{
    NSMutableDictionary *eventDetail = [[NSMutableDictionary alloc]init];
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aU-events' where event_id = '%@' ORDER BY event_id DESC",event_id];
        const char *query_stmt1 = [querySQL1 UTF8String];
        if (sqlite3_prepare_v2(database,query_stmt1, -1, &statement, NULL) == SQLITE_OK )
        {
            int j=1;
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *event_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 0)];
                NSString *event_city = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSString *event_topic = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSString *event_name = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                NSString *event_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)];
                NSString *event_start_time = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)];
                NSString *event_end_time = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)];
                NSString *event_place = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)];;
                NSString *event_address_first = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)];
                NSString *event_address_second = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 9)];
                NSString *event_state = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 10)];
                NSString *event_zip_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 11)];
                NSString *event_phone_number = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 12)];
                NSString *event_website = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 13)];
                NSString *event_notesShare = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 14)];
                NSString *event_notesNotShare = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 15)];
                NSString *event_images = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 16)];
                NSString *user_email = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 17)];
                NSString *latitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 18)];
                NSString *longitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 19)];
                NSString *user_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 20)];
                NSString *user_country_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 21)];
                 NSString *place_id = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 22)];
                 NSString *event_save_later = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 23)];

                
                event_city                          =       [event_city stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_topic                         =       [event_topic stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_name                          =       [event_name stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_date                          =       [event_date stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_start_time                    =       [event_start_time stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_end_time                      =       [event_end_time stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_place                         =       [event_place stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_address_first                 =       [event_address_first stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_address_second                =       [event_address_second stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_state                         =       [event_state stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_zip_code                      =       [event_zip_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_phone_number                  =       [event_phone_number stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_website                       =       [event_website stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_notesShare                    =       [event_notesShare stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_notesNotShare                 =       [event_notesNotShare stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_images                        =       [event_images stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_email                          =       [user_email stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                latitude                            =       [latitude stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                longitude                           =       [longitude stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_id                             =       [user_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_country_code                   =       [user_country_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                place_id                            =       [place_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                event_save_later                    =       [event_save_later stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                

                
                
                NSData *imagesData = [event_images dataUsingEncoding:NSUTF8StringEncoding];
                NSArray *imagesArray = [[NSArray alloc]init];
                if (imagesData.length>0)
                {
                    imagesArray = [NSJSONSerialization JSONObjectWithData:imagesData options:0 error:nil];
                }
                
                eventDetail = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                                            event_id,@"event_id",
                                            event_city,@"event_city",
                                            event_topic,@"event_topic",
                                            event_name,@"event_name",
                                            event_date,@"event_date",
                                            event_start_time,@"event_start_time",
                                            event_end_time,@"event_end_time",
                                            event_place,@"event_place",
                                            event_address_first,@"event_address_first",
                                            event_address_second,@"event_address_second",
                                            event_state,@"event_state",
                                            event_zip_code,@"event_zip_code",
                                            event_phone_number,@"event_phone_number",
                                            event_website,@"event_website",
                                            event_notesShare,@"event_NotesShare",
                                            event_notesNotShare,@"event_NotesNoShare",
                                            imagesArray,@"event_images",
                                            user_email,@"user_email",
                                            latitude,@"latitude",
                                            longitude,@"longitude",
                                            user_id,@"user_id",
                                            user_country_code,@"user_country_code",
                                            place_id,@"place_id",
                                            event_save_later,@"event_save_later",
                                            nil];
                j++;
            }
        }
        sqlite3_reset(statement);
    }
    
    return eventDetail;
}




-(BOOL) deleteEventWithId : (NSString*)event_id
{
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL1 = [NSString stringWithFormat: @"DELETE FROM 'aU-events' where event_id = '%@'", event_id];
        const char *query_stmt1 = [querySQL1 UTF8String];
        sqlite3_prepare_v2(database, query_stmt1,-1, &statement, NULL);

        if (sqlite3_step(statement) == SQLITE_DONE)
        {
            NSLog(@"deletion Successful");
            sqlite3_reset(statement);
            return true;
        }
        else
        {
            NSLog(@"Error while deleting event from event table. %s", sqlite3_errmsg(database));
            sqlite3_reset(statement);
            return false;
        }
    }
    
    sqlite3_reset(statement);
    return false;
}


-(NSMutableDictionary*) Select_UserDataBaseTable : (NSString*)mobile_number andCountryCode: (NSString*)countryCode
{
    NSMutableDictionary *dictionaryUserData = [[NSMutableDictionary alloc]init];
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL1 = [NSString stringWithFormat:@"SELECT * FROM 'aU-user-signup' where user_mobile_number = '%@' and user_country_code = '%@' ORDER BY user_mobile_number DESC LIMIT 1",mobile_number, countryCode];
        
        const char *query_stmt1 = [querySQL1 UTF8String];
        if (sqlite3_prepare_v2(database,query_stmt1, -1, &statement, NULL) == SQLITE_OK )
        {
            int j=1;
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                NSString *user_name = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 1)];
                NSString *user_email = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 2)];
                NSString *user_password = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 3)];
                NSString *user_mobile_number = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 4)];
                NSString *isPhoneNumberVerified = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 5)];
                NSString *user_id = [[NSString alloc]init];
                char *tmp1 = (char *)sqlite3_column_text(statement, 0);
                if (tmp1 == NULL)
                user_id = nil;
                else
                user_id = [[NSString alloc] initWithUTF8String:tmp1];
                
                NSString *user_country_code = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 6)];
                
                NSString *user_sign_up_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 7)];
                NSString *user_login_date = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 8)];
                NSString *user_city = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 9)];
                NSString *user_latitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 10)];
                NSString *user_longitude = [[NSString alloc]initWithUTF8String:(const char *) sqlite3_column_text(statement, 11)];
                
                
                user_name               =   [user_name stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_email              =   [user_email stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_password           =   [user_password stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_mobile_number      =   [user_mobile_number stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                isPhoneNumberVerified   =   [isPhoneNumberVerified stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_id                 =   [user_id stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_country_code       =   [user_country_code stringByReplacingOccurrencesOfString:@"`" withString:@"'"];
                user_sign_up_date       =   [user_sign_up_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_login_date         =   [user_login_date stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_city               =   [user_city stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_latitude           =   [user_latitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];
                user_longitude          =   [user_longitude stringByReplacingOccurrencesOfString:@"'" withString:@"`"];

                
                
                [dictionaryUserData setValue:user_name forKey:@"user_name"];
                [dictionaryUserData setValue:user_email forKey:@"user_email"];
                [dictionaryUserData setValue:user_password forKey:@"user_password"];
                [dictionaryUserData setValue:user_mobile_number forKey:@"user_mobile_number"];
                [dictionaryUserData setValue:isPhoneNumberVerified forKey:@"isPhoneNumberVerified"];
                [dictionaryUserData setValue:user_id forKey:@"user_id"];
                [dictionaryUserData setValue:user_country_code forKey:@"user_country_code"];
                [dictionaryUserData setValue:user_sign_up_date forKey:@"user_sign_up_date"];
                [dictionaryUserData setValue:user_login_date forKey:@"user_login_date"];
                [dictionaryUserData setValue:user_city forKey:@"user_city"];
                [dictionaryUserData setValue:user_latitude forKey:@"user_latitude"];
                [dictionaryUserData setValue:user_longitude forKey:@"user_longitude"];
                
                
                j++;
            }
        }
        sqlite3_reset(statement);
    }
    
    return dictionaryUserData;
}




@end
