

#pragma mark
#pragma mark //*********** Fonts ***************//
// ************************ Fonts ***************************** //
#define fontBold                                                        @"Gotham-Book"
#define fontLight                                                       @"GothamLight"
#define fontRegular                                                     @"GothamMedium"


#pragma mark
#pragma mark //*********** General ***************//
//***************** General ******************* //
#define TargetSize                                                      CGSizeMake(320, 568)
#define kDeviceToken                                                    @"device_token"
#define SCREEN_WIDTH                                                    ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT                                                   ([[UIScreen mainScreen] bounds].size.height)
#define kLoginData                                                      @"Login Data"
#define kiOSKeyForGoogleAPI                                             @"AIzaSyArwKrA09o8AqmTtYGzBPozfTGVoaB3tq0"//@"AIzaSyAg2eERS8HIC4wxx6TkH3THvVwzUs4ZeiY"
#define kAppGroup                                             @"group.altcal.eventsapp"
#define kSharedPostArray                                             @"SharedPostArray"
#define kIsDefaultUserStored                                             @"IsDefaultUserStored"
#define kIsShareExtensionSliderShown                                             @"IsShareExtensionSliderShown"
#define kSinchKey                                                       @"3f7c16d4-3df8-4691-84b9-6127d6d98d1e"

//OLD => 9a51a347-6d3b-4f0a-85d4-00596e5973ef


//AIzaSyBv7CImI7BPEiyAt9NAY35IXbC_5rk9BJg


