//
//  ChangePasswordViewController.m
//  Event App
//
//  Created by on 11/05/2017.
//  Copyright © 2017. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "GetMobilePasswordViewController.h"

@interface ChangePasswordViewController ()  <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@end

@implementation ChangePasswordViewController
{
    NSMutableDictionary *dictionaryChangePassword;
    UITableView *tableChangePassword;
    NSInteger incorrectAttemptsCount;
}

#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    dictionaryChangePassword = [[NSMutableDictionary alloc]init];
    [dictionaryChangePassword setValue:@"" forKey:@"old_password"];
    [dictionaryChangePassword setValue:@"" forKey:@"new_password"];
    [dictionaryChangePassword setValue:@"" forKey:@"re-enter_password"];

    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake([UIScreen mainScreen].bounds.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [self.view addSubview:imgViewLogo];
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44,  44);
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    
    UILabel *lblHeader = [UIFunction createLable:CGRectMake(btnBack.frame.size.width+btnBack.frame.origin.x, 80, self.view.frame.size.width-2*(btnBack.frame.size.width+btnBack.frame.origin.x), 44) bckgroundColor:[UIColor clearColor] title:@"Change Password" font:[UIFont fontWithName:fontRegular size:18.0] titleColor:[UIColor whiteColor]];
    lblHeader.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblHeader];
    
    UIView *blackHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, lblHeader.frame.origin.y + lblHeader.frame.size.height + 10)];
    [blackHeader setBackgroundColor:[UIColor blackColor]];
    [self.view insertSubview:blackHeader atIndex:0];
    
    
    [tableChangePassword removeFromSuperview];
    tableChangePassword = nil;
    tableChangePassword = [self createTableView:CGRectMake(0, blackHeader.frame.size.height+blackHeader.frame.origin.y+10, self.view.frame.size.width, self.view.frame.size.height-blackHeader.frame.size.height-blackHeader.frame.origin.y-10) backgroundColor:[UIColor clearColor]];
    [self.view addSubview:tableChangePassword];
    tableChangePassword.tag = 665922;
    incorrectAttemptsCount = 0;

}


#pragma mark
#pragma mark dismissViewController
-(void)backButtonPressed
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark Create Table
-(UITableView*) createTableView : (CGRect)frame backgroundColor:(UIColor*)backgroundColor
{
    UITableView *_tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    _tableView.dataSource=self;
    _tableView.delegate = self;
    _tableView.backgroundColor=backgroundColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.alwaysBounceVertical = NO;
    [_tableView setAllowsSelection:YES];
    return _tableView;
}

#pragma mark
#pragma mark Table View Data Source and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else
    {
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
    }
    CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
    
    @try
    {
        if (indexPath.row == 0) //old password
        {
            NSString *old_password = [NSString stringWithFormat:@"%@",[dictionaryChangePassword valueForKey:@"old_password"]];
            
            UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, frame.size.width-20, frame.size.height-10)];
            txtField.placeholder = @"Old Password";
            txtField.tag = 74300;
            txtField.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
            txtField.font = [UIFont fontWithName:fontLight size:14];
            txtField.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
            txtField.delegate = self;
            txtField.text = old_password;
            txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            txtField.textAlignment = NSTextAlignmentLeft;
            txtField.returnKeyType = UIReturnKeyNext;
            txtField.keyboardType = UIKeyboardTypeDefault;
            txtField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            txtField.autocorrectionType = UITextAutocorrectionTypeNo;
            [cell.contentView addSubview:txtField];
            txtField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
            txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Old Password" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
        }
        else if (indexPath.row == 1) //new password
        {
            NSString *new_password = [NSString stringWithFormat:@"%@",[dictionaryChangePassword valueForKey:@"new_password"]];
            
            UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, frame.size.width-20, frame.size.height-10)];
            txtField.placeholder = @"New Password";
            txtField.tag = 74301;
            txtField.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
            txtField.font = [UIFont fontWithName:fontLight size:14];
            txtField.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
            txtField.delegate = self;
            txtField.text = new_password;
            txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            txtField.textAlignment = NSTextAlignmentLeft;
            txtField.returnKeyType = UIReturnKeyNext;
            txtField.keyboardType = UIKeyboardTypeDefault;
            txtField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            txtField.autocorrectionType = UITextAutocorrectionTypeNo;
            [cell.contentView addSubview:txtField];
            txtField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
            txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
        }
        else if (indexPath.row == 2) // re-enter new password
        {
            NSString *reEnterPassword = [NSString stringWithFormat:@"%@",[dictionaryChangePassword valueForKey:@"re-enter_password"]];
            
            UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, frame.size.width-20, frame.size.height-10)];
            txtField.placeholder = @"Re-enter Password";
            txtField.tag = 74302;
            txtField.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
            txtField.font = [UIFont fontWithName:fontLight size:14];
            txtField.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
            txtField.delegate = self;
            txtField.text = reEnterPassword;
            txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            txtField.textAlignment = NSTextAlignmentLeft;
            txtField.returnKeyType = UIReturnKeyDone;
            txtField.keyboardType = UIKeyboardTypeDefault;
            txtField.autocapitalizationType = UITextAutocapitalizationTypeWords;
            txtField.autocorrectionType = UITextAutocorrectionTypeNo;
            [cell.contentView addSubview:txtField];
            txtField.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
            txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Re-enter Password" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
        }
        else if (indexPath.row == 3) // done button
        {
            UIButton *btnDone = [UIFunction createButton:CGRectMake(frame.size.width/2.0-25.0, frame.size.height/2.0-20.0, 50.0, 40.0) bckgroundColor:[UIColor clearColor] image:nil title:@"Done" font:[UIFont fontWithName:fontRegular size:16.0] titleColor:[UIColor blackColor]];
            [btnDone addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:btnDone];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.userInteractionEnabled = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)doneButtonPressed
{
    NSLog(@"doneButtonPressed");
    [self.view endEditing:true];
    
    NSString *old_password = [NSString stringWithFormat:@"%@",[dictionaryChangePassword valueForKey:@"old_password"]];
    NSString *new_password = [NSString stringWithFormat:@"%@",[dictionaryChangePassword valueForKey:@"new_password"]];
    NSString *reEnterPassword = [NSString stringWithFormat:@"%@",[dictionaryChangePassword valueForKey:@"re-enter_password"]];
    
    if (old_password.length < 6)
    {
        [self showAlertWithMessage:@"Old Password must be atleast 6 digits long."];
    }
    else if ([self isPasswordValid:old_password] == false)
    {
        incorrectAttemptsCount = incorrectAttemptsCount + 1;
        if (incorrectAttemptsCount > 1)
        {
            [self askForMobileNumberRecovery];
        }
        
        else
        {
            
            [self showAlertWithMessage:@"Old Password is incorrect."];
        }
    }
    else if (new_password.length < 6)
    {
        [self showAlertWithMessage:@"New Password must be atleast 6 digits long."];
    }
    else if (![new_password isEqualToString:reEnterPassword])
    {
        [self showAlertWithMessage:@"New Password and Re-entered password must be same."];
    }
    else if ([self isPasswordValid:new_password] == false)
    {
        [self showAlertWithMessage:@"Please use combination of lowercase, uppercase, and numbers."];
    }
    else
    {
        NSLog(@"check password");
        
        NSMutableDictionary *user_data = [[NSMutableDictionary alloc]init];
        user_data = [[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData]mutableCopy];
        NSLog(@"check password %@",user_data);
        NSString *oldPasswordSetByUser = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_password"]];
        
        if (![oldPasswordSetByUser isEqualToString:old_password])
        {
            [self showAlertWithMessage:@"Old Password is incorrect."];
        }
        else
        {
            [user_data setValue:new_password forKey:@"user_password"];
            NSString *isPhoneNumberVerified = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"isPhoneNumberVerified"]];
            NSString *user_country_code = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_country_code"]];
            NSString *user_email = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_email"]];
            NSString *user_id = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_id"]];
            NSString *user_mobile_number = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_mobile_number"]];
            NSString *user_name = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_name"]];
            
            NSString *user_sign_up_date = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_sign_up_date"]];
            NSString *user_login_date = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_login_date"]];
            NSString *user_city = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_city"]];
            NSString *user_latitude = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_latitude"]];
            NSString *user_longitude = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_longitude"]];
            
            
            
            
            BOOL isUpdateSuccess = [[DataManager getSharedInstance]Update_UserDataBaseTable:user_name :user_email :new_password :user_mobile_number :isPhoneNumberVerified :user_id : user_country_code :user_sign_up_date :user_login_date :user_city :user_latitude :user_longitude];
                                    
            if (isUpdateSuccess == YES)
            {
                [self showSuccerssAlertController];
                [[NSUserDefaults standardUserDefaults]setObject:user_data forKey:kLoginData];
                [[NSUserDefaults standardUserDefaults]synchronize];
            }
            else
            {
                [self showAlertWithMessage:@"Unable to update data. Please try again later."];
            }
        }
    }
}

#pragma mark
#pragma mark AlertController Function
- (void) showSuccerssAlertController
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"AltCal" message:@"Password Changed Successfully." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:true];
    }];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:true completion:nil];
}


#pragma mark
#pragma mark TextField Delegate
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [tableChangePassword scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField.tag == 74300) // old password
    {
        [dictionaryChangePassword setValue:@"" forKey:@"old_password"];
    }
    else if (textField.tag == 74301) // new_password
    {
        [dictionaryChangePassword setValue:@"" forKey:@"new_password"];
    }
    else if (textField.tag == 74302) // re-enter_password
    {
        [dictionaryChangePassword setValue:@"" forKey:@"re-enter_password"];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 74300) // old password
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74301];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74301) // new_password
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74302];
        [_txtField becomeFirstResponder];
    }

    
    [textField resignFirstResponder];
    return NO;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField.tag == 74300) // old password
    {
        [tableChangePassword scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74301) // new_password
    {
        [tableChangePassword scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74302) // re-enter_password
    {
        [tableChangePassword scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if (textField.tag == 74300) // old password
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=50)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryChangePassword setValue:text forKey:@"old_password"];
        }
        return (newLength >= 51) ? NO : YES;
    }
    
    else if (textField.tag == 74301) // new_password
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=50)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryChangePassword setValue:text forKey:@"new_password"];
        }
        return (newLength >= 51) ? NO : YES;
    }
    
    else if (textField.tag == 74302) // re-enter_password
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=50)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryChangePassword setValue:text forKey:@"re-enter_password"];
        }
        return (newLength >= 51) ? NO : YES;
    }
    
    
    return YES;
}


#pragma mark
#pragma mark Alert Function
- (void) showAlertWithMessage : (NSString*)message
{
    [self.view makeToast:message duration:2.0 position:CSToastPositionCenter];
}


-(BOOL) isPasswordValid:(NSString *)pwd
{
    NSCharacterSet *upperCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
    
    if ( [pwd length]<6 )
        return NO;  // too long or too short
    NSRange rang;
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if ( !rang.length )
        return NO;  // no letter
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    if ( !rang.length )
        return NO;  // no number;
    rang = [pwd rangeOfCharacterFromSet:upperCaseChars];
    if ( !rang.length )
        return NO;  // no uppercase letter;
    rang = [pwd rangeOfCharacterFromSet:lowerCaseChars];
    if ( !rang.length )
        return NO;  // no lowerCase Chars;
    return YES;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)askForMobileNumberRecovery
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Your Password\nDoesn’t Match Our Records\n\nEnter Your Saved Mobile Phone\nto Access Your Account and Change Your Password" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *sendAction = [UIAlertAction
                                 actionWithTitle:@"OK"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     GetMobilePasswordViewController *controller = [[GetMobilePasswordViewController alloc] init];
                                     controller.isFromForgotPassword = NO;
                                     [self.navigationController pushViewController:controller animated:YES];
                                 }];
    
    [alertController addAction:sendAction];
    [self presentViewController:alertController animated:YES completion:nil];
}


@end
