//
//  GetMobilePasswordViewController.m
//  Event App
//
//  Created by Zaeem Khatib on 15/11/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import "GetMobilePasswordViewController.h"

@interface GetMobilePasswordViewController ()<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@end

@implementation GetMobilePasswordViewController
{
    UIView *viewForCountryCodePicker;
    NSArray *arrayAllCodes;
    NSArray *arrayAllCountries;
    UILabel *lblCountryCode;
    UITextField *txtField;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    arrayAllCodes = [[UIFunction func_GetAllCountries_Code]mutableCopy];
    arrayAllCountries = [[UIFunction func_GetAllCountries_Name]mutableCopy];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    
    UIButton *btnBack = [UIButton buttonWithType: UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44, 44);
    btnBack.backgroundColor = [UIColor clearColor];
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    // LOGO
    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake([UIScreen mainScreen].bounds.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [self.view addSubview:imgViewLogo];
    
    UILabel *lblEnterMobile = [UIFunction createLable:CGRectMake(10, 100, self.view.frame.size.width - 20, 60) bckgroundColor:[UIColor clearColor] title:@"Enter your mobile number to recover your password." font:[UIFont fontWithName:fontBold size:16.0] titleColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
    lblEnterMobile.numberOfLines = 2;
    lblEnterMobile.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblEnterMobile];
    
    lblCountryCode = [UIFunction createLable:CGRectMake(20, lblEnterMobile.frame.origin.y + lblEnterMobile.frame.size.height + 10, 50, 30) bckgroundColor:[UIColor clearColor] title:@"+1" font:[UIFont fontWithName:fontRegular size:14.0] titleColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
    lblCountryCode.userInteractionEnabled = true;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(func_SelectCountryCode)];
    [tap setNumberOfTouchesRequired:1];
    [lblCountryCode addGestureRecognizer:tap];
    [self.view addSubview:lblCountryCode];
    
    
    txtField = [[UITextField alloc]initWithFrame:CGRectMake(lblCountryCode.frame.size.width+lblCountryCode.frame.origin.x, lblCountryCode.frame.origin.y, screenWidth-20-lblCountryCode.frame.size.width-lblCountryCode.frame.origin.x, 30)];
    txtField.placeholder = @"Mobile Phone";
    txtField.tag = 74104;
    txtField.font = [UIFont fontWithName:fontRegular size:16];
    txtField.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
    txtField.delegate = self;
    txtField.text = @"";
    txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField.textAlignment = NSTextAlignmentLeft;
    txtField.returnKeyType = UIReturnKeyNext;
    txtField.keyboardType = UIKeyboardTypeNumberPad;
    txtField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    txtField.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.view addSubview:txtField];
    txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]}];
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(dismissKeyboard)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor blackColor];
    txtField.inputAccessoryView = toolBar;
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(10, lblCountryCode.frame.origin.y+lblCountryCode.frame.size.height, screenWidth - 20, 1)];
    [separator setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:separator];
    
    UIButton *btnSubmit = [UIFunction createButton:CGRectMake(lblEnterMobile.frame.origin.x+20, separator.frame.origin.y+separator.frame.size.height+20, 120, 30) bckgroundColor:[UIColor whiteColor] image: nil title:@"RECOVER" font: [UIFont fontWithName:fontRegular size:16.0] titleColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255 alpha:1.0]];
    [btnSubmit addTarget:self action:@selector(submitButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    btnSubmit.layer.cornerRadius = 5.0;
    btnSubmit.center = CGPointMake(screenWidth/2, btnSubmit.center.y);
    [self.view addSubview:btnSubmit];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (newLength<=15)
    {
       
    }
    return (newLength >= 16) ? NO : YES;
}

-(void)backButtonPressed
{
    [self dismissKeyboard];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

-(void)submitButtonPressed
{
    [self dismissKeyboard];
    [self fetchPassword];
}

#pragma mark
#pragma mark func_SelectCountryCode
-(void)func_SelectCountryCode
{
    NSLog(@"func_SelectCountryCode");
    
    [self removeAllPickers];
    [self.view endEditing:YES];
    
    viewForCountryCodePicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor colorWithRed:214.0/255 green:240.0/255 blue:252.0/255 alpha:1.0]];
    [self.view addSubview:viewForCountryCodePicker];
    
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(doneButtonPressedInPickerView:)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor blackColor];
    [viewForCountryCodePicker addSubview:toolBar];
    
    
    
    
    UIPickerView *pickerCountryCode =  [[UIPickerView alloc]init];
    pickerCountryCode.frame = CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, viewForCountryCodePicker.frame.size.width, 216.0);
    pickerCountryCode.delegate = self;
    pickerCountryCode.dataSource = self;
    pickerCountryCode.showsSelectionIndicator = YES;
    pickerCountryCode.backgroundColor = [UIColor clearColor];
    pickerCountryCode.tag = 480040;
    [viewForCountryCodePicker addSubview:pickerCountryCode];
    
    
    [UIView animateWithDuration:.2f animations:^{
        
        [self->viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
        
    } completion:^(BOOL finished) {
        
        
        
    }];
}

#pragma mark
#pragma mark Remove All Picker Views
-(void)doneButtonPressedInPickerView :(id)sender
{
    [self. view endEditing:YES];
    
    @try
    {
        
        [UIView animateWithDuration:.2f animations:^{
            
            [self->viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self removeAllPickers];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void)removeAllPickers
{
    [viewForCountryCodePicker removeFromSuperview];
    viewForCountryCodePicker = nil;
}

#pragma mark
#pragma mark Picker View DataSource And Delegates Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        return arrayAllCodes.count;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        return 1;
    }
    return 0;
}

- (UIView*)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (thePickerView.tag == 480040) // select user country and c ountry code
    {
        UIView* viewForPicker = [[UIView alloc]init];
        viewForPicker.frame = CGRectMake(0, 0, self.view.frame.size.width, 45);
        viewForPicker.backgroundColor = [UIColor clearColor];
        
        
        UILabel *countryName = [[UILabel alloc]init];
        countryName.frame = CGRectMake(10, 5, 240, 40);
        countryName.textAlignment = NSTextAlignmentLeft;
        countryName.backgroundColor = [UIColor clearColor];
        countryName.text = [arrayAllCountries objectAtIndex:row];
        countryName.font = [UIFont fontWithName:fontRegular size:16];
        [viewForPicker addSubview:countryName];
        
        UILabel *countryCode = [[UILabel alloc]init];
        countryCode.frame = CGRectMake(self.view.frame.size.width-90, 5, 70, 40);
        countryCode.textAlignment = NSTextAlignmentRight;
        countryCode.font = [UIFont fontWithName:fontRegular size:16];
        countryCode.backgroundColor = [UIColor clearColor];
        countryCode.text = [arrayAllCodes objectAtIndex:row];
        [viewForPicker addSubview:countryCode];
        
        return viewForPicker;
    }
    
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        [lblCountryCode setText:[arrayAllCodes objectAtIndex:row]];
    }
}

- (void)fetchPassword

{
    NSString *mobileNumber = [txtField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *countryCode = [lblCountryCode.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (countryCode.length == 0)
    {
        [self showAlertWithMessage:@"Please select your country code."];
        return;
    }
    
    if (mobileNumber.length == 0)
    {
        [self showAlertWithMessage:@"Please enter your mobile number."];
        return;
    }
    
    // firstly check, if the email id is already exists or not, if exists, show success alert else ask user to sign up.
    NSMutableDictionary *dicionaryUserData = [[NSMutableDictionary alloc]init];
    dicionaryUserData = [[DataManager getSharedInstance] Select_UserDataBaseTable:mobileNumber andCountryCode:countryCode];
    if ([dicionaryUserData valueForKey:@"user_email"] != nil)
    {
        NSString *user_stored_password = [NSString stringWithFormat:@"%@",[dicionaryUserData valueForKey:@"user_password"]];
        NSString *user_stored_email = [NSString stringWithFormat:@"%@",[dicionaryUserData valueForKey:@"user_email"]];
        
        if (self.isFromForgotPassword == YES)
        {
         
            [self showPassword:[NSString stringWithFormat:@"Your Email is %@\n\nYour password is\n%@", user_stored_email, user_stored_password]];
        }
        
        else
        {
            [self showPassword:[NSString stringWithFormat:@"Your password is\n%@", user_stored_password]];
        }
    }
    else
    {
        [self showAlertWithMessage:@"We don’t have any AltCal users with that mobile phone number."];
    }
}

#pragma mark
#pragma mark Alert Function
- (void) showAlertWithMessage : (NSString*)message
{
    [self.view makeToast:message duration:2.0 position:CSToastPositionBottom];
}

- (void)showPassword: (NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    NSString *actionTitle = @"OK";
    if (self.isFromForgotPassword == YES)
    {
        actionTitle = @"LOGIN NOW";
    }
    
    UIAlertAction *okAction = [UIAlertAction
                                 actionWithTitle:actionTitle
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction *action)
                                 {
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
