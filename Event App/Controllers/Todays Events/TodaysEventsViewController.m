//
//  TodaysEventsViewController.m
//  Event App
//
//  Created by   on 20/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import "TodaysEventsViewController.h"
#import "EventDetailsViewController.h"
#import "LandingViewController.h"
#import "yourDayTblCell.h"

@interface TodaysEventsViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation TodaysEventsViewController
{
    
    NSMutableArray *arrayTodaysEvents;
    UISearchBar *searchBar;
}

@synthesize isTodayEvent;

#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self getAllEventsForToday];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

-(void)getAllEventsForToday
{
    NSString *current_date = [UIFunction getCurrentDateInString];
    NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
    arrayTodaysEvents = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:false];
    NSLog(@"______ %@",arrayTodaysEvents);
    [self sortEventsArray];
    
    if (arrayTodaysEvents.count == 0)
    {
        [_lblNoEvents setHidden:false];
        [_btnCreateEvent setHidden:false];
        [_tableTodaysEvents setHidden:true];
        [_btnCreateEvent addTarget:self action:@selector(createEventButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    else
    {
        
        [_lblNoEvents setHidden:true];
        [_btnCreateEvent setHidden:true];
        [_tableTodaysEvents setHidden:false];
        [_tableTodaysEvents reloadData];
    }
    
    
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"     ");
    arrayTodaysEvents = [[NSMutableArray alloc]init];
    
    _lblHeader.font = [UIFont fontWithName:fontBold size:16.0];
    if (isTodayEvent == true)
    {
        _lblHeader.text = @"Today's Events";
    }
    else
    {
        _lblHeader.text = @"Your day";
    }
    
    _lblHeaderDate.text = [UIFunction getCurrentDateInString];
    
     _lblNotificationsCount.font = [UIFont fontWithName:fontRegular size:10.0];
     _lblNotificationsCount.textAlignment = NSTextAlignmentCenter;
     _lblNotificationsCount.layer.cornerRadius = 10.5;
     _lblNotificationsCount.layer.borderColor = [UIColor whiteColor].CGColor;
     _lblNotificationsCount.layer.borderWidth = 2.0;
     _lblNotificationsCount.layer.masksToBounds = true;
    
    _tableTodaysEvents.tag = 457548;

}

#pragma mark
#pragma mark dismissViewController
-(IBAction)backButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createEventButtonPressed
{
    [self.view endEditing:YES];
   // LandingViewController *controller = [[LandingViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    LandingViewController *controller=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

//#pragma mark
//#pragma mark Create Table
//-(UITableView*) createTableView : (CGRect)frame backgroundColor:(UIColor*)backgroundColor
//{
//    UITableView *_tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
//    _tableView.dataSource=self;
//    _tableView.delegate = self;
//    _tableView.backgroundColor=backgroundColor;
//    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    _tableView.showsVerticalScrollIndicator = NO;
//    _tableView.alwaysBounceVertical = NO;
//    [_tableView setAllowsSelection:YES];
//    return _tableView;
//}

#pragma mark
#pragma mark Table View Data Source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayTodaysEvents.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 85;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"yourDayTblCell";
    yourDayTblCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    NSString *event_name = [NSString stringWithFormat:@"%@",[[arrayTodaysEvents objectAtIndex:indexPath.row] valueForKey:@"event_name"]];
           
    cell.lblEventName.text = event_name;
    
    
    cell.imgBlack.layer.cornerRadius = 27.5;
    cell.imgBlack.layer.borderColor = [UIColor yellowColor].CGColor;
    cell.imgBlack.layer.borderWidth = 2.0;
    cell.imgBlack.layer.masksToBounds = true;
    
           
    NSString *event_start_time = [NSString stringWithFormat:@"%@",[[arrayTodaysEvents objectAtIndex:indexPath.row] valueForKey:@"event_start_time"]];
           
    if (event_start_time.length > 0) {
        event_start_time = [NSString stringWithFormat:@"%@, ", event_start_time];
            
    }
    NSString *event_date = [NSString stringWithFormat:@"%@",[[arrayTodaysEvents objectAtIndex:indexPath.row] valueForKey:@"event_date"]];
    event_date = [self changeDateFormatter:event_date];
    NSString *event_date_time = [NSString stringWithFormat:@"%@%@",event_start_time, event_date];
    cell.lblEventDate.text = event_date_time;
           
            
        return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (arrayTodaysEvents.count > 0)
    {
        EventDetailsViewController *controller = [[EventDetailsViewController alloc]init];
        controller.event_id = [NSString stringWithFormat:@"%@",[[arrayTodaysEvents objectAtIndex:indexPath.row] valueForKey:@"event_id"]];
        [self.navigationController pushViewController:controller animated:true];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        BOOL isDelete = [[DataManager getSharedInstance]deleteEventWithId:[NSString stringWithFormat:@"%@",[[arrayTodaysEvents objectAtIndex:indexPath.row] valueForKey:@"event_id"]]];
        if (isDelete == true)
        {
            [self getAllEventsForToday];
        }
    }
}


-(NSString*)changeDateFormatter : (NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    dateFormatter.dateFormat = @"MMM dd, yyyy";
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    NSString *convertedate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    return convertedate;
}

-(void)sortEventsArray {
    
    if(arrayTodaysEvents.count > 0){
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM-dd-yyyy hh:mm a"];
//        for (int i = 0; i <= arrayTodaysEvents.count - 1 ; i++)
//        {
//            NSDictionary *dict = [arrayTodaysEvents objectAtIndex:i];
//            NSString *strDate = [dict valueForKey:@"event_date"];
//            if([strDate length ]== 0){
//                [arrNames addObject:dict];
//            }
//            else{
//                [arrDate addObject:dict];
//            }
//        }
        
        NSArray *sortedDateArray = [arrayTodaysEvents sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
            NSString *strDate = [obj1 valueForKey:@"event_date"];
            NSString *strDate2 = [obj2 valueForKey:@"event_date"];
            
            NSString *strTime1 = [obj1 valueForKey:@"event_start_time"];
            NSString *strTime2 = [obj2 valueForKey:@"event_start_time"];
            
            NSString *strWholeDate = [NSString stringWithFormat:@"%@,%@", strDate,strTime1];
            NSString *strWholeDate2 = [NSString stringWithFormat:@"%@,%@", strDate2,strTime2];
            if([strDate isEqualToString:strDate2]){
                NSComparisonResult result = [strWholeDate caseInsensitiveCompare:strWholeDate2];
                return result == NSOrderedDescending;
                
            }
            NSComparisonResult result = [strWholeDate caseInsensitiveCompare:strWholeDate2];
            return result == NSOrderedDescending;
        }];
        
//        arrDate = [sortedDateArray mutableCopy];
//        NSArray *sortedNameArray = [arrNames sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
//
//            NSString *event_name1 = [obj1 valueForKey:@"event_name"];
//            NSString *event_name2 = [obj2 valueForKey:@"event_name"];
//            NSComparisonResult result = [event_name1 caseInsensitiveCompare:event_name2];
//            return result == NSOrderedDescending;
//        }];
//
        //arrNames = [sortedNameArray mutableCopy];
       // NSArray *newArray=[arrDate arrayByAddingObjectsFromArray:arrNames];
        arrayTodaysEvents = [sortedDateArray mutableCopy];
        NSLog(@"main arr-- %@",arrayTodaysEvents);
    }
//    NSArray *sortedArray = [arrayTodaysEvents sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
//
//        NSString *strDate = [obj1 valueForKey:@"event_date"];
//        NSString *strTime = [obj1 valueForKey:@"event_start_time"];
//        NSString *strTime2 = [obj2 valueForKey:@"event_start_time"];
//        NSString *strDate2 = [obj2 valueForKey:@"event_date"];
//        NSString *eventName = [obj1 valueForKey:@"event_name"];
//        NSString *eventName2 = [obj2 valueForKey:@"event_name"];
//
//
//
//
//
//        if ([strTime length] == 0 && [strTime2 length] > 0) {
//
//            return YES;
//        }
//
//        else if ([strTime2 length] == 0 && [strTime length] > 0) {
//
//            return NO;
//        }
//
//        else if ([strTime length] == 0 && [strTime2 length] == 0) {
//
//            NSComparisonResult result = [eventName caseInsensitiveCompare:eventName2];
//            return result == NSOrderedDescending;
//        }
//
//        NSString *strWholeDate = [NSString stringWithFormat:@"%@ %@", strDate, strTime];
//        NSString *strWholeDate2 = [NSString stringWithFormat:@"%@ %@", strDate2, strTime2];
//        NSDate *date = [df dateFromString:strWholeDate];
//        NSDate *date2 = [df dateFromString:strWholeDate2];
//        return [date compare:date2];
//    }];
//
//    arrayTodaysEvents = [sortedArray mutableCopy];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
