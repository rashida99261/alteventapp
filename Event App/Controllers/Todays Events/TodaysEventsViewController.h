//
//  TodaysEventsViewController.h
//  Event App
//
//  Created by   on 20/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodaysEventsViewController : UIViewController
@property (atomic,assign) BOOL isTodayEvent;

@property (nonatomic,weak) IBOutlet UILabel *lblHeader;
@property (nonatomic,weak) IBOutlet UILabel *lblHeaderDate;
@property (nonatomic,weak) IBOutlet UILabel *lblNotificationsCount;

@property (nonatomic,weak) IBOutlet UITableView *tableTodaysEvents;

@property (nonatomic,weak) IBOutlet UILabel *lblNoEvents;
@property (nonatomic,weak) IBOutlet UIButton *btnCreateEvent;
@end
