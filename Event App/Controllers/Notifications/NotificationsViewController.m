//
//  NotificationsViewController.m
//  Event App
//
//  Created by   on 20/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import "NotificationsViewController.h"

@interface NotificationsViewController ()

@end

@implementation NotificationsViewController


#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}
#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"     ");
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake([UIScreen mainScreen].bounds.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [self.view addSubview:imgViewLogo];
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44,  44);
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    
    UILabel *lblHeader = [UIFunction createLable:CGRectMake(btnBack.frame.size.width+btnBack.frame.origin.x, 80, self.view.frame.size.width-2*(btnBack.frame.size.width+btnBack.frame.origin.x), 44) bckgroundColor:[UIColor clearColor] title:@"Notifications" font:[UIFont fontWithName:fontRegular size:18.0] titleColor:[UIColor whiteColor]];
    lblHeader.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblHeader];
    
    UIView *blackHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, lblHeader.frame.origin.y + lblHeader.frame.size.height + 10)];
    [blackHeader setBackgroundColor:[UIColor blackColor]];
    [self.view insertSubview:blackHeader atIndex:0];
    
    UILabel *lblNoNotifications = [UIFunction createLable:CGRectMake(10, self.view.frame.size.height/2.0-20.0, self.view.frame.size.width-20, 40) bckgroundColor:[UIColor clearColor] title:@"No Notifications yet!" font:[UIFont fontWithName:fontLight size:18.0] titleColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];
    lblNoNotifications.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblNoNotifications];
}

#pragma mark
#pragma mark dismissViewController
-(void)backButtonPressed
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
