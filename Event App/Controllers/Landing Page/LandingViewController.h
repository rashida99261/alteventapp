//
//  LandingViewController.h
//  Event App
//
//  Created by   on 17/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>
//@class invitePopUpView;

@interface LandingViewController : UIViewController

@property (atomic, assign) BOOL isEditEvent;
@property (atomic, assign) BOOL isFromLogin;



@property (nonatomic, retain) NSString *edit_event_id;

@property (nonatomic, assign) NSString *strAddressFirstField;

@property (weak, nonatomic) IBOutlet UIScrollView *eventScrollView;

@property (weak, nonatomic) IBOutlet UIView *navBarView;
@property (weak, nonatomic) IBOutlet UIView *viewBottomCollc;
@property (weak, nonatomic) IBOutlet UILabel *lblEvent;
@property (weak, nonatomic) IBOutlet UIView *viewCreateEventBackGround;
@property (weak, nonatomic) IBOutlet UIButton *btnAddAttachment;


@property (weak, nonatomic) IBOutlet UILabel *lblCircle1;
@property (weak, nonatomic) IBOutlet UILabel *lblCircle2;
@property (weak, nonatomic) IBOutlet UILabel *lblCircle3;
@property (weak, nonatomic) IBOutlet UILabel *lblCircle4;
@property (weak, nonatomic) IBOutlet UILabel *lblCircle5;
@property (weak, nonatomic) IBOutlet UILabel *lblCircle6;


@property (weak, nonatomic) IBOutlet UITextField *txtFieldCity;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldTopic;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEventName;
@property (weak, nonatomic) IBOutlet UIButton *btnEventDate;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldPlaceName;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchLocation;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddress1;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddress2;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldaddCity;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldaddState;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldaddPostalCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEventPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEventWebsite;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryCode;

@property (weak, nonatomic) IBOutlet UITextView *txtViewNotesshare;
@property (weak, nonatomic) IBOutlet UITextView *txtViewNotes_Noshare;

@property (weak, nonatomic) IBOutlet UIButton *btnTicket;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnInvite;

@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hgtNotesShare;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hgtNotes_No_Share;

@property (weak, nonatomic) IBOutlet UIButton *btnCreateEvent;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveLater;

@property (weak, nonatomic) IBOutlet UITableView *tblTimeSlot;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblHeight;


//Timeslot Name Pop up---
@property (weak, nonatomic) IBOutlet UIView *biewMainPopUp;
@property (weak, nonatomic) IBOutlet UIButton *btnCross;
@property (weak, nonatomic) IBOutlet UIView *viewInner;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIButton *btnSaveNAme;
@property (weak, nonatomic) IBOutlet UILabel *lblTitlePopup;


@property (weak, nonatomic) IBOutlet UITableView *tblAddresSearch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblAddressHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tblBottomAdrss;



- (void)setNotificationAndEventsBadge;

@end
