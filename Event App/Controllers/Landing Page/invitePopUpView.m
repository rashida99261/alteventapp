//
//  invitePopUpView.m
//  Event App
//
//  Created by Reinforce on 24/10/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import "invitePopUpView.h"
#import <Contacts/Contacts.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface invitePopUpView ()<UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>

@end

@implementation invitePopUpView
{
   
    NSMutableArray *arrayEmail;
    NSArray *arrayFilterEmail;
    
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    
    arrayEmail = [[NSMutableArray alloc] init];
    arrayFilterEmail = [[NSArray alloc] init];
    UIButton *btnBack = [UIButton buttonWithType: UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44, 44);
    btnBack.backgroundColor = [UIColor clearColor];
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    // LOGO
    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake([UIScreen mainScreen].bounds.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [self.view addSubview:imgViewLogo];
    
    _lblTitle.font = [UIFont fontWithName:fontBold size:16.0];
    _lblTo.font = [UIFont fontWithName:fontLight size:14.0];
    _lblMsg.font = [UIFont fontWithName:fontLight size:14.0];
    _lblInclude.font = [UIFont fontWithName:fontLight size:14.0];
    _lblsubject.font = [UIFont fontWithName:fontLight size:14.0];
    
    _lblName.font = [UIFont fontWithName:fontLight size:14.0];
    _lblDateTime.font = [UIFont fontWithName:fontLight size:14.0];
    _lblAddress.font = [UIFont fontWithName:fontLight size:14.0];
    _lblwebsite.font = [UIFont fontWithName:fontLight size:14.0];
    
    
    _txtMessage.font = [UIFont fontWithName:fontLight size:13.0];
    
    
    
    
    
    [_btnSave setBackgroundColor:[UIColor whiteColor]];
    [_btnSave setTitleColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
    _btnSave.layer.cornerRadius = 5.0;
    
    
    
    
    NSString *event_date = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_date"]];
    NSString *event_start_time = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_start_time"]];
    if (event_start_time.length == 0)
    {
        event_start_time = @"";
    }
    if ([event_date isEqualToString:@""] || [event_date containsString:@"(null)"] || event_date == nil)
    {
        //[_lblDate setHidden:TRUE];
        event_date = @"";
    }
    else{
       // [_lblDate setHidden:FALSE];
        //event_date = [NSString stringWithFormat:@"%@",event_date];
        
        if (event_start_time.length > 0) {
            event_start_time = [NSString stringWithFormat:@"%@", event_start_time];
        }
        event_date = [self changeDateFormatter:event_date];
    }
    
    
    
    NSString *event_name = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_name"]];
    if ([event_name isEqualToString:@""] || [event_name containsString:@"(null)"] || event_name == nil)
    {
       // [_lblName setHidden:TRUE];
        event_name = @"";
    }
    else{
       // [_lblName setHidden:FALSE];
        event_name = [NSString stringWithFormat:@"%@",event_name];
    }
    
    NSString *event_city = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_city"]];
    NSString *event_place = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_place"]];
    NSString *event_address_first = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_address_first"]];
    NSString *event_address_second = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_address_second"]];
    NSString *event_state = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_state"]];
    NSString *event_zip_code = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_zip_code"]];
    
    
    NSString *str;
    
    
    if([event_city isEqualToString:@""] && [event_place isEqualToString:@""] && [event_address_first isEqualToString:@""] && [event_address_second isEqualToString:@""] && [event_state isEqualToString:@""] && [event_zip_code isEqualToString:@""])
    {
        //[_lblAddress setHidden:TRUE];
        str=@"";
    }
    else{
        NSString *address = [NSString stringWithFormat:@"%@ %@ %@ %@,%@ %@",event_place,event_address_first,event_address_second,event_city,event_state,event_zip_code];
        // [_lblAddress setHidden:FALSE];
        str = [NSString stringWithFormat:@"%@",address];
        
    }
    
    NSString *event_website = [NSString stringWithFormat:@"%@",[_dictPassDataFrom valueForKey:@"event_website"]];
    if ([event_website isEqualToString:@""] || [event_website containsString:@"(null)"] || event_website == nil)
    {
        // [_lblName setHidden:TRUE];
        event_website = @"";
    }
    else{
        // [_lblName setHidden:FALSE];
        event_website = [NSString stringWithFormat:@"%@",event_website];
    }
    
    
    
    
//    NSString *sttt = [NSString stringWithFormat:@"%@\n\n%@\n\n%@\n\n%@\n\n%@",event_name,event_date,time,str,event_website];
//
//    if ([sttt length] > 0) {
//        sttt = [sttt substringToIndex:[sttt length] - 1];
//    }
//
//    NSLog(@"stttsttt--%@",sttt);
//
    //_lblAllEventDetails.text = sttt;
    
    _lblName.text = event_name;
    _lblDateTime.text = [NSString stringWithFormat:@"%@ %@",event_date,event_start_time];
    _lblAddress.text = str;
    _lblwebsite.text = event_website;
    
    _pickerCountryCode.delegate = self;
    _pickerCountryCode.dataSource = self;
    _pickerCountryCode.backgroundColor = [UIColor clearColor];
    _pickerCountryCode.tag = 80786;
    
    _viewForCountryCodePicker.hidden = YES;
    [self contactScan];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
}

-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(NSString*)changeDateFormatter : (NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    dateFormatter.dateFormat = @"MM/dd/yy";
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    NSString *convertedate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    return convertedate;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self contains[c] %@", string];
    arrayFilterEmail = [arrayEmail filteredArrayUsingPredicate:predicate];
    NSLog(@"HERE %@",arrayFilterEmail);
    
    if(arrayFilterEmail.count == 0){
        //[self removeAllPickers];
       // [textField becomeFirstResponder];
         self->_viewForCountryCodePicker.hidden = YES;
    }
    else{
            //[self openPickerView];
        [UIView animateWithDuration:.2f animations:^{
           // [textField resignFirstResponder];
            self->_viewForCountryCodePicker.hidden = NO;
            [self->_pickerCountryCode reloadData];
            
       
        } completion:^(BOOL finished) {}];
        
    }
    return  YES;
}

- (void) contactScan
{
    if ([CNContactStore class]) {
        //ios9 or later
        CNEntityType entityType = CNEntityTypeContacts;
        if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined)
        {
            CNContactStore * contactStore = [[CNContactStore alloc] init];
            [contactStore requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if(granted){
                    [self getAllContact];
                }
            }];
        }
        else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized)
        {
            [self getAllContact];
        }
    }
}

-(void)getAllContact
{
    if([CNContactStore class])
    {
        //iOS 9 or later
        NSError* contactError;
        CNContactStore* addressBook = [[CNContactStore alloc]init];
        [addressBook containersMatchingPredicate:[CNContainer predicateForContainersWithIdentifiers: @[addressBook.defaultContainerIdentifier]] error:&contactError];
        NSArray * keysToFetch =@[CNContactEmailAddressesKey, CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey, CNContactPostalAddressesKey];
        CNContactFetchRequest * request = [[CNContactFetchRequest alloc]initWithKeysToFetch:keysToFetch];
        BOOL success = [addressBook enumerateContactsWithFetchRequest:request error:&contactError usingBlock:^(CNContact * __nonnull contact, BOOL * __nonnull stop){
            [self parseContactWithContact:contact];
            
            
        }];
    }
}

- (void)parseContactWithContact :(CNContact* )contact
{
  //  NSString * firstName =  contact.givenName;
  //  NSString * lastName =  contact.familyName;
  //  NSString * phone = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
    NSArray * emailArr = [contact.emailAddresses valueForKey:@"value"];
    NSString *email = [emailArr objectAtIndex:0];
    NSLog(@"email--%@",email);
    [arrayEmail addObject:email];
    
}



-(void)dismissKeyboard {
    [_txtEmail resignFirstResponder];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}



#pragma mark
#pragma mark Remove All Picker Views
-(IBAction)doneButtonPressedInPickerView :(id)sender
{
    [self. view endEditing:YES];
    @try
    {
        [UIView animateWithDuration:.2f animations:^{
            //[self->viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];
        } completion:^(BOOL finished) {
            self->_viewForCountryCodePicker.hidden = YES;
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}



#pragma mark
#pragma mark Table View DataSource And Delegates Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayFilterEmail.count;
}

//- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
//{
//
//}
//
//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
//{
//        return 1;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [arrayFilterEmail objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:fontRegular size:15];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _txtEmail.text = [arrayFilterEmail objectAtIndex:indexPath.row];
}


#pragma Mark - Send button
#pragma MArk - Email page open

- (IBAction) okButtonAction : (id) sender {
    
    if (_txtEmail.text.length == 0)
    {
        [self showAlertWithMessage:@"Please enter email address."];
    }
    else{
        if([MFMailComposeViewController canSendMail]) {
            MFMailComposeViewController *mailCont = [[MFMailComposeViewController alloc] init];
            mailCont.mailComposeDelegate = self;        // Required to invoke mailComposeController when send
            [mailCont setSubject:_lblsubject.text];
            [mailCont setToRecipients:[NSArray arrayWithObject:_txtEmail.text]];
            [mailCont setMessageBody:_txtMessage.text isHTML:NO];
            [self presentViewController:mailCont animated:YES completion:nil];
        }
        else{
            [self showAlertWithMessage:@"Your device doesn't support email."];
        }
    }
    
   
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    //handle any error
    switch (result) {
        case MFMailComposeResultSent:
            [self showAlertWithMessage:@"Invitation send successfully!"];
            _txtEmail.text = @"";
            _txtMessage.text = @"";
            break;
            
        case MFMailComposeResultSaved:
            NSLog(@"Email saved");
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"Email cancelled");
            break;
        case MFMailComposeResultFailed:
            [self showAlertWithMessage:@"Email failed"];
            break;
        default:
            NSLog(@"Error occured during email creation");
            break;
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark
#pragma mark Alert Function
- (void) showAlertWithMessage : (NSString*)message
{
    [self.view makeToast:message duration:2.0 position:CSToastPositionBottom];
}

- (IBAction) buttonPressed:(id)sender {
    if ([sender isSelected]) {
        [sender setSelected: NO];
    } else {
        [sender setSelected: YES];
    }
}

@end
