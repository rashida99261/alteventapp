

#import "LandingViewController.h"
#import "SideMenu.h"
#import "TodaysEventsViewController.h"
#import "NotificationsViewController.h"
#import <IDMPhotoBrowser.h>
#import "ChoosePhotoView.h"
#import <Photos/Photos.h>
#import "RemovePhotoView.h"
#import "MyEventsViewController.h"
#import "EventDetailsViewController.h"
#import "MultiOptionView.h"
#import "ChangePasswordViewController.h"
#import <SafariServices/SafariServices.h>
#import "PastEventsViewController.h"
#import <GooglePlaces/GooglePlaces.h>
#import <WebKit/WKWebViewConfiguration.h>
#import "MapkitViewController.h"
#import "invitePopUpView.h"
#import <WebKit/WebKit.h>
#import "timeSlotTblCell.h"
#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import "EditLaterViewController.h"


@interface LandingViewController () <sideMenuProtocol, UICollectionViewDelegate, UICollectionViewDataSource, choosePhotoMethod, UINavigationControllerDelegate, UIImagePickerControllerDelegate, removePhotoMethod, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UISearchBarDelegate, MFMessageComposeViewControllerDelegate, UIDocumentMenuDelegate, UIDocumentPickerDelegate, WKNavigationDelegate, UIPickerViewDataSource,UIPickerViewDelegate, MultiOptionViewMethod, IDMPhotoBrowserDelegate, UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) UICollectionView *collectionViewForImages;

@end


@implementation LandingViewController
{
    MultiOptionView *viewMultipleOptions;
    UILabel *lblEventsCount;
    UILabel *lblNotificationsCount;
    float widthOfCollectionView;
    ChoosePhotoView *viewToChoosePhoto;
    NSMutableDictionary *dictionaryCreateEvent;
    NSInteger indexOfImage;
    RemovePhotoView *viewFullSizeOrRemovePhoto;
    UIView *backGroundViewForPicker;
    GMSPlacesClient *_placesClient;

    NSArray *arraySearch;
    UIView *viewForGoogleSearchAPI;
    
    UIView *viewForWebView;
    UIView *viewForCountryCodePicker;
    NSArray *arrayAllCodes;
    NSArray *arrayAllCountries;
    
    NSString *tempEventStartTime;
    NSString *tempEventEndTime;
    NSString *placeAddress;
    int index;
    
    SPGooglePlacesAutocompleteQuery *searchQuery;
    
    
    
    
    NSMutableArray *arrAddRow;
    NSMutableDictionary *arrTimeStartSlotSet;
    
     NSMutableDictionary *dictNameSlot;
    
    int btnIndex;
    int btnPopupTag;

}

@synthesize isEditEvent;
@synthesize edit_event_id;

#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self setNotificationAndEventsBadge];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
    
    [self setUpView];
    
    
    _viewInner.layer.borderColor = [[UIColor colorWithRed:254/255.0 green:226/255.0 blue:39/255.0 alpha:1.0] CGColor];    //systemyellow
    _viewInner.layer.borderWidth = 1.5;
    
    _lblTitlePopup.textColor = [UIColor colorWithRed:254/255.0 green:226/255.0 blue:39/255.0 alpha:1.0];
    
    _txtName.layer.borderColor = [[UIColor whiteColor] CGColor];
    _txtName.layer.borderWidth = 1.0;
    
    [_biewMainPopUp setHidden:TRUE];
    
    _tblAddressHeight.constant = 0;
    _tblBottomAdrss.constant = 0;
    
    [_txtFieldPlaceName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self->_tblAddresSearch.delegate = self;
    self->_tblAddresSearch.dataSource = self;
    self->_tblAddresSearch.allowsSelection = true;
    
    if (_isFromLogin == YES)
       {
          [self performSelector:@selector(checkIfImageStored) withObject:nil afterDelay:0.3];
           _isFromLogin = NO;
       }
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
   
}

-(void)viewWillLayoutSubviews
{
    [super updateViewConstraints];
    _tblHeight.constant = _tblTimeSlot.contentSize.height;
}

-(void)fetchUserLocation
{
    [appDelegate() func_AccessLocation];
}

-(void)createDictionaryForEvent
{
    index = 0;
    NSString *str = [NSString stringWithFormat:@"%d",index];
    arrAddRow = [[NSMutableArray alloc]initWithObjects:str, nil];
    
    arrTimeStartSlotSet = [[NSMutableDictionary alloc]init];
    dictNameSlot = [[NSMutableDictionary alloc]init];
    
    NSArray *arrayImages = [[NSArray alloc]initWithObjects:@"",@"",@"",@"",@"", nil];
    dictionaryCreateEvent = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                             @"",@"event_city",
                             @"",@"event_topic",
                             @"",@"event_name",
                             @"",@"event_date",
                             @"",@"event_start_time",
                             @"",@"event_end_time",
                             @"",@"event_place",
                             @"",@"event_address_first",
                             @"",@"event_address_second",
                             @"",@"event_state",
                             @"",@"event_zip_code",
                             @"",@"event_phone_number",
                             @"",@"event_website",
                             @"",@"event_NotesShare",
                             @"",@"event_NotesNoShare",
                             arrayImages, @"event_images",
                             @"", @"latitude",
                             @"", @"longitude",
                             @"", @"user_country_code",
                             @"", @"place_id",
                             @"", @"event_save_later",
                             nil];
    
    if ([UIFunction autoDetectCountryCode].length>0)
    {
        [dictionaryCreateEvent setObject:[NSString stringWithFormat:@"+%@",[UIFunction autoDetectCountryCode]] forKey:@"user_country_code"];
    }
    else
    {
        [dictionaryCreateEvent setObject:@"+1" forKey:@"user_country_code"];
    }
    
    if (isEditEvent == true)
    {
        dictionaryCreateEvent = [[[DataManager getSharedInstance]selectEventWithId:edit_event_id]mutableCopy];
       // [tableViewCreateEvent reloadData];
        
        NSLog(@"dict--%@",dictionaryCreateEvent);
        
        
        
        
        [self setUpView];
    }
    
    NSLog(@"____ %@",dictionaryCreateEvent );
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"     ");
    self.view.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    widthOfCollectionView = 0.0;
    
   
    [self createDictionaryForEvent];
    _placesClient = [[GMSPlacesClient alloc] init];
    arrayAllCodes = [[UIFunction func_GetAllCountries_Code]mutableCopy];
    arrayAllCountries = [[UIFunction func_GetAllCountries_Name]mutableCopy];
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(safariCallback:) name:@"SafariCallback" object:nil];
    
    // LOGO
    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake(self.view.frame.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 10, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [_navBarView addSubview:imgViewLogo];

    
    // MENU
    UIButton *btnMenu = [UIButton buttonWithType: UIButtonTypeCustom];
    btnMenu.frame = CGRectMake(0, 10, 44, 44);
    btnMenu.backgroundColor = [UIColor clearColor];
    [btnMenu setImage:[UIImage imageNamed:@"menu_white"] forState:UIControlStateNormal];
    [btnMenu addTarget:self action:@selector(menuButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [_navBarView addSubview:btnMenu];

    // TODAY's EVENTS
    UIButton *btnTodaysEvents = [UIButton buttonWithType: UIButtonTypeCustom];
    btnTodaysEvents.frame = CGRectMake(self.view.frame.size.width-30-[UIImage imageNamed:@"todays_events"].size.width, 10, 44, 44);
    btnTodaysEvents.backgroundColor = [UIColor clearColor];
    [btnTodaysEvents setContentMode:UIViewContentModeCenter];
    [btnTodaysEvents setImage:[UIImage imageNamed:@"todays_events"] forState:UIControlStateNormal];
    [btnTodaysEvents addTarget:self action:@selector(todaysEventsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [_navBarView addSubview:btnTodaysEvents];
    
    
//    lblEventsCount = [UIFunction createLable:CGRectMake(btnTodaysEvents.frame.size.width-20, btnTodaysEvents.frame.size.height-20, 20, 20) bckgroundColor:[UIColor clearColor] title:nil font:[UIFont fontWithName:fontRegular size:10.0] titleColor:[UIColor whiteColor]];
//    lblEventsCount.textAlignment = NSTextAlignmentCenter;
//    lblEventsCount.layer.cornerRadius = lblEventsCount.frame.size.height/2.0;
//    lblEventsCount.layer.borderColor = [UIColor whiteColor].CGColor;
//    lblEventsCount.layer.borderWidth = 2.0;
//    lblEventsCount.layer.masksToBounds = true;
//    lblEventsCount.userInteractionEnabled = true;
//    [btnTodaysEvents addSubview:lblEventsCount];

    //NOTIFICATIONS
    UIButton *btnNotifications = [UIButton buttonWithType: UIButtonTypeCustom];
    btnNotifications.frame = CGRectMake(btnTodaysEvents.frame.origin.x-44, btnMenu.frame.origin.y, 44, 44);
    btnNotifications.backgroundColor = [UIColor clearColor];
    [btnNotifications setContentMode:UIViewContentModeCenter];
    [btnNotifications setImage:[UIImage imageNamed:@"notifications"] forState:UIControlStateNormal];
    [btnNotifications addTarget:self action:@selector(notificationsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [_navBarView addSubview:btnNotifications];

    
    lblNotificationsCount = [UIFunction createLable:CGRectMake(btnNotifications.frame.size.width-20, btnNotifications.frame.size.height-20, 20, 20) bckgroundColor:[UIColor clearColor] title:nil font:[UIFont fontWithName:fontRegular size:10.0] titleColor:[UIColor whiteColor]];
    lblNotificationsCount.textAlignment = NSTextAlignmentCenter;
    lblNotificationsCount.layer.cornerRadius = lblNotificationsCount.frame.size.height/2.0;
    lblNotificationsCount.layer.borderColor = [UIColor whiteColor].CGColor;
    lblNotificationsCount.layer.borderWidth = 2.0;
    lblNotificationsCount.layer.masksToBounds = true;
    lblNotificationsCount.userInteractionEnabled = true;
    [btnNotifications addSubview:lblNotificationsCount];
    

    // ADD EVENT
    
    _lblCircle1.layer.cornerRadius = 5.0;
    _lblCircle1.layer.masksToBounds = true;
    
    _lblCircle4.layer.cornerRadius = 5.0;
    _lblCircle4.layer.masksToBounds = true;
    
    _lblCircle2.layer.cornerRadius = 3.5;
    _lblCircle2.layer.masksToBounds = true;
    
    _lblCircle5.layer.cornerRadius = 3.5;
    _lblCircle5.layer.masksToBounds = true;
    
    _lblCircle3.layer.cornerRadius = 2.5;
    _lblCircle3.layer.masksToBounds = true;
    
    _lblCircle6.layer.cornerRadius = 2.5;
    _lblCircle6.layer.masksToBounds = true;
    
    
   /* UILabel *lblAddEvent = [UIFunction createLable:CGRectMake(_viewAddEvent.frame.size.width/2.0-65.0, 8.5, 130, _viewAddEvent.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:@"ADD EVENT" font:[UIFont fontWithName:fontLight size:20.0] titleColor:[UIColor whiteColor]];
    lblAddEvent.textAlignment = NSTextAlignmentCenter;
    [_viewAddEvent addSubview:lblAddEvent];
    
    
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-15, _viewAddEvent.frame.size.height/2.0-4.0, 10.0, 10.0) atView:_viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-35, _viewAddEvent.frame.size.height/2.0-2.5, 7.0, 7.0) atView:_viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-50, _viewAddEvent.frame.size.height/2.0-1.5, 5.0, 5.0) atView:_viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width+5, _viewAddEvent.frame.size.height/2.0-4.0, 10.0, 10.0) atView:_viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width+25, _viewAddEvent.frame.size.height/2.0-2.5, 7.0, 7.0) atView:_viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width+45, _viewAddEvent.frame.size.height/2.0-1.5, 5.0, 5.0) atView:_viewAddEvent];*/
    
//
//    // COLLECTION VIEW
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    self.collectionViewForImages = [[UICollectionView alloc] initWithFrame:CGRectMake(10, 0, _viewBottomCollc.frame.size.width-20, 100) collectionViewLayout:layout];
    [self.collectionViewForImages setDataSource:self];
    [self.collectionViewForImages setDelegate:self];
    [self.collectionViewForImages registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [self.collectionViewForImages setBackgroundColor:[UIColor clearColor]];
    self.collectionViewForImages.scrollEnabled = YES;
    self.collectionViewForImages.tag = 69802140;
    self.collectionViewForImages.pagingEnabled = YES;
    self.collectionViewForImages.showsHorizontalScrollIndicator = NO;
    self.collectionViewForImages.bounces = NO;
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self.collectionViewForImages setCollectionViewLayout:layout];
    [_viewBottomCollc addSubview:self.collectionViewForImages];
    widthOfCollectionView = self.collectionViewForImages.frame.size.width; // get widh of collection view
//
//
//    // ****** View Create Event ********
//    UIView *viewCreateEventBackGround = [UIFunction createUIViews:CGRectMake(self.collectionViewForImages.frame.origin.x, viewaddEvent.frame.size.height+viewaddEvent.frame.origin.y+60, self.collectionViewForImages.frame.size.width, self.collectionViewForImages.frame.origin.y-viewaddEvent.frame.size.height-viewaddEvent.frame.origin.y-60) bckgroundColor:;
    _viewCreateEventBackGround.layer.cornerRadius = 5.0;
    _viewCreateEventBackGround.backgroundColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
//    [self.view addSubview:viewCreateEventBackGround];
//
//
//    // ****** Add Attachment ********
    
    _btnAddAttachment.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    [_btnAddAttachment addTarget:self action:@selector(addAttachmentButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    _btnAddAttachment.layer.cornerRadius = _btnAddAttachment.frame.size.height/2.0;
    _btnAddAttachment.clipsToBounds = true;
    _btnAddAttachment.contentMode =  UIViewContentModeScaleAspectFill;

    [self createCircleViewWithFrame:CGRectMake(_btnAddAttachment.frame.origin.x-3, _btnAddAttachment.frame.size.height+(_btnAddAttachment.frame.origin.y/2.0), 4.0, 4.0 ) atView:self.view];
    [self createCircleViewWithFrame:CGRectMake(_btnAddAttachment.frame.origin.x, _btnAddAttachment.frame.size.height+(_btnAddAttachment.frame.origin.y/2.0)-8, 5.0, 5.0 ) atView:self.view];
    [self createCircleViewWithFrame:CGRectMake(_btnAddAttachment.frame.origin.x+4, _btnAddAttachment.frame.size.height+(_btnAddAttachment.frame.origin.y/2.0)-16, 5.5, 5.5 ) atView:self.view];


    [self createBlackCircleViewWithFrame:CGRectMake(_btnAddAttachment.frame.origin.x+_btnAddAttachment.frame.size.width, _btnAddAttachment.frame.size.height+(_btnAddAttachment.frame.origin.y/2.0)+25, 4.0, 4.0 ) atView:self.view];
    [self createBlackCircleViewWithFrame:CGRectMake(_btnAddAttachment.frame.origin.x+_btnAddAttachment.frame.size.width-2, _btnAddAttachment.frame.size.height+(_btnAddAttachment.frame.origin.y/2.0)+33, 5.0, 5.0 ) atView:self.view];
    [self createBlackCircleViewWithFrame:CGRectMake(_btnAddAttachment.frame.origin.x+_btnAddAttachment.frame.size.width-7, _btnAddAttachment.frame.size.height+(_btnAddAttachment.frame.origin.y/2.0)+41, 5.5, 5.5 ) atView:self.view];

    
    // ****** SAVE ********
    
    [_btnSave setBackgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
    [_btnSave addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];


  
    

    
    // Table View
//    [tableViewCreateEvent removeFromSuperview];
//    tableViewCreateEvent = nil;
//    tableViewCreateEvent = [self createTableView:CGRectMake(5, txtFieldTopic.frame.size.height+txtFieldTopic.frame.origin.y+10, viewCreateEventBackGround.frame.size.width-10, btnSave.frame.origin.y-txtFieldTopic.frame.size.height-txtFieldTopic.frame.origin.y-10) backgroundColor:[UIColor clearColor]];
//    [viewCreateEventBackGround addSubview:tableViewCreateEvent];
//    tableViewCreateEvent.tag = 457548;
//    tableViewCreateEvent.contentSize = [tableViewCreateEvent sizeThatFits:CGSizeMake(CGRectGetWidth(tableViewCreateEvent.bounds), CGFLOAT_MAX)];
//    [tableViewCreateEvent setContentInset:UIEdgeInsetsMake(0, 0, 130, 0)];
//
//   // tableViewCreateEvent.estimatedRowHeight = 38;
//  //  tableViewCreateEvent.rowHeight = UITableViewAutomaticDimension;
//
    if (isEditEvent == true)
    {
        btnMenu.hidden = true;
        btnTodaysEvents.hidden = false;
        lblEventsCount.hidden = false;
        btnNotifications.hidden = false;
        lblNotificationsCount.hidden = false;
        _lblEvent.text = @"EDIT EVENT";

        UIButton *btnBack = [UIButton buttonWithType: UIButtonTypeCustom];
        btnBack.frame = CGRectMake(0, 20, 44, 44);
        btnBack.backgroundColor = [UIColor clearColor];
        [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
        [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:btnBack];
    }
}


//-(void)getAddressFromLatLong:(double)latitude and: (double)longitude
//{
//    CLGeocoder *ceo = [[CLGeocoder alloc]init];
//
//
//    NSLog(@"placemark %f",latitude);
//    NSLog(@"placemark %f",longitude);
//
//    CLLocation *loc = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
//
//    [ceo reverseGeocodeLocation: loc completionHandler:
//     ^(NSArray *placemarks, NSError *error) {
//         CLPlacemark *placemark = [placemarks objectAtIndex:0];
//         NSLog(@"placemark %@",placemark);
//         //String to hold address
//         NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//
//        NSLog(@"%@",locatedAt);
//
//
//         NSLog(@"addressDictionary %@", placemark.addressDictionary);
//
//        self->_txtFieldAddress1.text =  [placemark.addressDictionary objectForKey:@"SubLocality"];
//
//
//        self->_txtFieldaddCity.text = [placemark.addressDictionary objectForKey:@"City"];
//        self->_txtFieldaddPostalCode.text = [placemark.addressDictionary objectForKey:@"ZIP"];
//         self->_txtFieldaddState.text = [placemark.addressDictionary objectForKey:@"State"];
//        self->_txtFieldAddress2.text =  [placemark.addressDictionary objectForKey:@"Street"];
//
//        [self->dictionaryCreateEvent setValue:self->_txtFieldAddress1.text forKey:@"event_address_first"];
//        [self->dictionaryCreateEvent setValue:self->_txtFieldaddState.text forKey:@"event_state"];
//        [self->dictionaryCreateEvent setValue:self->_txtFieldaddPostalCode.text forKey:@"event_zip_code"];
//        [self->dictionaryCreateEvent setValue:self->_txtFieldAddress2.text forKey:@"event_address_second"];
//        [self->dictionaryCreateEvent setValue:self->_txtFieldaddCity.text forKey:@"event_city"];
//
//
////         NSLog(@"placemark %@",placemark.region);
////         NSLog(@"placemark %@",placemark.country);  // Give Country Name
////         NSLog(@"placemark %@",placemark.locality); // Extract the city name
////         NSLog(@"location %@",placemark.name);
////         NSLog(@"location %@",placemark.ocean);
////         NSLog(@"location %@",placemark.postalCode);
////         NSLog(@"location %@",placemark.subLocality);
////
////         NSLog(@"location %@",placemark.location);
////         //Print the location to console
////         NSLog(@"I am currently at %@",locatedAt);
//
//
//
//
//     }];
//}


-(void) setUpView
{
    
    // ****** Event City and Topic ********
    
    //
    
    UIImage *image = [[UIImage imageNamed:@"create_event"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [_btnCreateEvent setImage:image forState:UIControlStateNormal];
    _btnCreateEvent.tintColor = [UIColor whiteColor];
    
    NSString *event_city = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_city"]];
    _txtFieldCity.placeholder = @"City";
    _txtFieldCity.tag = 1;
    _txtFieldCity.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    _txtFieldCity.font = [UIFont fontWithName:fontLight size:14];
    _txtFieldCity.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    _txtFieldCity.delegate = self;
    _txtFieldCity.text = event_city;
    _txtFieldCity.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _txtFieldCity.textAlignment = NSTextAlignmentCenter;
    _txtFieldCity.returnKeyType = UIReturnKeyNext;
    _txtFieldCity.keyboardType = UIKeyboardTypeDefault;
    _txtFieldCity.autocapitalizationType = UITextAutocapitalizationTypeWords;
    _txtFieldCity.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtFieldCity.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _txtFieldCity.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"City" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
    //
    NSString *event_topic = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_topic"]];
    _txtFieldTopic.placeholder = @"Topic";
    _txtFieldTopic.tag = 2;
    _txtFieldTopic.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    _txtFieldTopic.font = [UIFont fontWithName:fontLight size:14];
    _txtFieldTopic.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    _txtFieldTopic.delegate = self;
    _txtFieldTopic.text = event_topic;
    _txtFieldTopic.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    _txtFieldTopic.textAlignment = NSTextAlignmentCenter;
    _txtFieldTopic.returnKeyType = UIReturnKeyNext;
    _txtFieldTopic.keyboardType = UIKeyboardTypeDefault;
    _txtFieldTopic.autocapitalizationType = UITextAutocapitalizationTypeWords;
    _txtFieldTopic.autocorrectionType = UITextAutocorrectionTypeNo;
    _txtFieldTopic.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    _txtFieldTopic.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Topic" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
    
    
        NSString *event_name = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_name"]];
        _txtFieldEventName.placeholder = @"Event Name";
       _txtFieldEventName.tag = 3;
        _txtFieldEventName.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _txtFieldEventName.font = [UIFont fontWithName:fontLight size:14];
        _txtFieldEventName.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtFieldEventName.delegate = self;
        _txtFieldEventName.text = event_name;
        _txtFieldEventName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _txtFieldEventName.textAlignment = NSTextAlignmentLeft;
        _txtFieldEventName.returnKeyType = UIReturnKeyNext;
        _txtFieldEventName.keyboardType = UIKeyboardTypeDefault;
        _txtFieldEventName.autocapitalizationType = UITextAutocapitalizationTypeWords;
        _txtFieldEventName.autocorrectionType = UITextAutocorrectionTypeNo;
        _txtFieldEventName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        _txtFieldEventName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Event Name" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
    
    
   
        NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_date"]];
        if ([event_date isEqualToString:@""] || [event_date containsString:@"(null)"] || event_date == nil)
           {
               event_date = @"";
           }
           
           if (event_date.length == 0)
           {
               event_date = @"00-00-00";
           }
       
        _btnEventDate.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        [_btnEventDate setTitleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
        [_btnEventDate setTitle:event_date forState:UIControlStateNormal];
        _btnEventDate.titleLabel.textAlignment = NSTextAlignmentLeft;
        _btnEventDate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_btnEventDate setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
        [_btnEventDate addTarget:self action:@selector(selectEventDate) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    
        
        // ************* Time ********************* //
    

        NSString *event_place_name = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_place"]];
        _txtFieldPlaceName.placeholder = @"Place Name";
        _txtFieldPlaceName.tag = 4;
        _txtFieldPlaceName.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _txtFieldPlaceName.font = [UIFont fontWithName:fontLight size:14];
        _txtFieldPlaceName.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtFieldPlaceName.delegate = self;

    
    
        NSString *strPlace = [[NSUserDefaults standardUserDefaults] valueForKey:@"place_from_map"];
        if ([strPlace length] > 0){
            event_place_name = strPlace;
            [dictionaryCreateEvent setValue:event_place_name forKey:@"event_place"];
        }
    
        _txtFieldPlaceName.text = event_place_name;
        _txtFieldPlaceName.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _txtFieldPlaceName.textAlignment = NSTextAlignmentLeft;
        _txtFieldPlaceName.returnKeyType = UIReturnKeyNext;
        _txtFieldPlaceName.keyboardType = UIKeyboardTypeDefault;
        _txtFieldPlaceName.autocapitalizationType = UITextAutocapitalizationTypeWords;
        _txtFieldPlaceName.autocorrectionType = UITextAutocorrectionTypeNo;

        _txtFieldPlaceName.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        _txtFieldPlaceName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Place Name" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
    
    
    
        [_btnSearchLocation addTarget:self action:@selector(openMapScreen) forControlEvents:UIControlEventTouchUpInside];


        NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_first"]];
        NSString *map_address1 = [[NSUserDefaults standardUserDefaults] valueForKey:@"map_address1"];
        if ([map_address1 length] > 0){
                event_address_first = map_address1;
                [dictionaryCreateEvent setValue:event_address_first forKey:@"event_address_first"];
        }
        _txtFieldAddress1.placeholder = @"Address";
        _txtFieldAddress1.text = event_address_first;
        _txtFieldAddress1.tag = 5;
        _txtFieldAddress1.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _txtFieldAddress1.font = [UIFont fontWithName:fontLight size:14];
        _txtFieldAddress1.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtFieldAddress1.delegate = self;
        _txtFieldAddress1.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _txtFieldAddress1.textAlignment = NSTextAlignmentLeft;
        _txtFieldAddress1.returnKeyType = UIReturnKeyNext;
        _txtFieldAddress1.keyboardType = UIKeyboardTypeDefault;
        _txtFieldAddress1.autocapitalizationType = UITextAutocapitalizationTypeWords;
        _txtFieldAddress1.autocorrectionType = UITextAutocorrectionTypeNo;

        _txtFieldAddress1.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        _txtFieldAddress1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Address" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];

    
    
        NSString *event_address_second = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_second"]];
        NSString *map_address2 = [[NSUserDefaults standardUserDefaults] valueForKey:@"map_address2"];
        if ([map_address2 length] > 0){
          event_address_second = map_address2;
          [dictionaryCreateEvent setValue:event_address_second forKey:@"event_address_second"];
        }
         
        _txtFieldAddress2.placeholder = @"Floor,Suite,Unit";
        _txtFieldAddress2.tag = 6;
        _txtFieldAddress2.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _txtFieldAddress2.font = [UIFont fontWithName:fontLight size:14];
        _txtFieldAddress2.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtFieldAddress2.delegate = self;
        _txtFieldAddress2.text = event_address_second;
        _txtFieldAddress2.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _txtFieldAddress2.textAlignment = NSTextAlignmentLeft;
        _txtFieldAddress2.returnKeyType = UIReturnKeyNext;
        _txtFieldAddress2.keyboardType = UIKeyboardTypeDefault;
        _txtFieldAddress2.autocapitalizationType = UITextAutocapitalizationTypeWords;
        _txtFieldAddress2.autocorrectionType = UITextAutocorrectionTypeNo;
        _txtFieldAddress2.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        _txtFieldAddress2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Floor,Suite,Unit" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];

    
        NSString *event_addcity = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_city"]];
        NSString *map_city = [[NSUserDefaults standardUserDefaults] valueForKey:@"map_city"];
        if ([map_city length] > 0){
          self->_txtFieldCity.text = map_city;
          event_addcity = map_city;
          [dictionaryCreateEvent setValue:event_addcity forKey:@"event_city"];
        }
          _txtFieldaddCity.placeholder = @"City";
          _txtFieldaddCity.tag = 7;
          _txtFieldaddCity.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
          _txtFieldaddCity.font = [UIFont fontWithName:fontLight size:14];
          _txtFieldaddCity.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
          _txtFieldaddCity.delegate = self;
          _txtFieldaddCity.text = event_addcity;
          _txtFieldaddCity.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
          _txtFieldaddCity.textAlignment = NSTextAlignmentLeft;
          _txtFieldaddCity.returnKeyType = UIReturnKeyNext;
          _txtFieldaddCity.keyboardType = UIKeyboardTypeDefault;
          _txtFieldaddCity.autocapitalizationType = UITextAutocapitalizationTypeWords;
          _txtFieldaddCity.autocorrectionType = UITextAutocorrectionTypeNo;
          _txtFieldaddCity.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
          _txtFieldaddCity.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"City" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];

    
    
        NSString *event_state = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_state"]];
        NSString *map_state = [[NSUserDefaults standardUserDefaults] valueForKey:@"map_state"];
        if ([map_state length] > 0){
          event_state = map_state;
          [dictionaryCreateEvent setValue:map_state forKey:@"event_state"];
        }
            _txtFieldaddState.placeholder = @"State";
                   _txtFieldaddState.tag = 8;
                   _txtFieldaddState.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
                   _txtFieldaddState.font = [UIFont fontWithName:fontLight size:14];
                   _txtFieldaddState.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
                   _txtFieldaddState.delegate = self;
                   _txtFieldaddState.text = event_state;
                   _txtFieldaddState.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                   _txtFieldaddState.textAlignment = NSTextAlignmentLeft;
                   _txtFieldaddState.returnKeyType = UIReturnKeyNext;
                   _txtFieldaddState.keyboardType = UIKeyboardTypeDefault;
                   _txtFieldaddState.autocapitalizationType = UITextAutocapitalizationTypeWords;
                   _txtFieldaddState.autocorrectionType = UITextAutocorrectionTypeNo;
                   _txtFieldaddState.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
                   _txtFieldaddState.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"State" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
    
    
        NSString *event_zip_code = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_zip_code"]];
        NSString *postal_code = [[NSUserDefaults standardUserDefaults] valueForKey:@"map_zipcode"];
        if ([postal_code length] > 0){
            event_zip_code = postal_code;
            [dictionaryCreateEvent setValue:postal_code forKey:@"event_zip_code"];
        }
    
        _txtFieldaddPostalCode.placeholder = @"Zipcode";
        _txtFieldaddPostalCode.tag = 9;
        _txtFieldaddPostalCode.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _txtFieldaddPostalCode.font = [UIFont fontWithName:fontLight size:14];
        _txtFieldaddPostalCode.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtFieldaddPostalCode.delegate = self;
        _txtFieldaddPostalCode.text = event_zip_code;
        _txtFieldaddPostalCode.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _txtFieldaddPostalCode.textAlignment = NSTextAlignmentLeft;
        _txtFieldaddPostalCode.returnKeyType = UIReturnKeyNext;
        _txtFieldaddPostalCode.keyboardType = UIKeyboardTypeDefault;
        _txtFieldaddPostalCode.autocapitalizationType = UITextAutocapitalizationTypeWords;
        _txtFieldaddPostalCode.autocorrectionType = UITextAutocorrectionTypeNo;
        _txtFieldaddPostalCode.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        _txtFieldaddPostalCode.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Zipcode" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];
    
    
    
            NSString *placeId = [[NSUserDefaults standardUserDefaults] valueForKey:@"placeId"];
            if ([placeId length] > 0){
                    [dictionaryCreateEvent setValue:placeId forKey:@"place_id"];
            }
            else{
                [dictionaryCreateEvent setValue:@"" forKey:@"place_id"];
            }
//        double latitude = [[NSUserDefaults standardUserDefaults] doubleForKey:@"map_location_lat"];
//        double longitude = [[NSUserDefaults standardUserDefaults] doubleForKey:@"map_location_long"];
//
//         NSNumber *myLatD = [NSNumber numberWithDouble:latitude];
//         NSNumber *myLongD = [NSNumber numberWithDouble:longitude];
//         NSString *strLat = [myLatD stringValue];
//         NSString *strLong = [myLongD stringValue];
//          if([strLat isEqualToString:@"0"] && [strLong isEqualToString:@"0"])
//         {
//
//         }
//          else{
//              [self getAddressFromLatLong:latitude and:(double)longitude];
//          }

    

        NSString *event_phone_number = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_phone_number"]];
        NSString *user_country_code = [NSString stringWithFormat:@" %@",[dictionaryCreateEvent valueForKey:@"user_country_code"]];

        _lblCountryCode.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _lblCountryCode.text = user_country_code;
        _lblCountryCode.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _lblCountryCode.font = [UIFont fontWithName:fontLight size:14.0];
        _lblCountryCode.userInteractionEnabled = true;
        _lblCountryCode.textAlignment = NSTextAlignmentLeft;

        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(func_SelectCountryCode)];
        [tap setNumberOfTouchesRequired:1];
        tap.delegate = self;
        [_lblCountryCode addGestureRecognizer:tap];


        _txtFieldEventPhoneNumber.placeholder = @"Phone";
        _txtFieldEventPhoneNumber.tag = 10;
        _txtFieldEventPhoneNumber.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _txtFieldEventPhoneNumber.font = [UIFont fontWithName:fontLight size:14];
        _txtFieldEventPhoneNumber.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtFieldEventPhoneNumber.delegate = self;
        _txtFieldEventPhoneNumber.text = event_phone_number;
        _txtFieldEventPhoneNumber.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _txtFieldEventPhoneNumber.textAlignment = NSTextAlignmentLeft;
        _txtFieldEventPhoneNumber.returnKeyType = UIReturnKeyNext;
        _txtFieldEventPhoneNumber.keyboardType = UIKeyboardTypeNumberPad;
        _txtFieldEventPhoneNumber.autocapitalizationType = UITextAutocapitalizationTypeWords;
        _txtFieldEventPhoneNumber.autocorrectionType = UITextAutocorrectionTypeNo;
        _txtFieldEventPhoneNumber.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        _txtFieldEventPhoneNumber.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Phone" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];

        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setBarStyle:UIBarStyleDefault];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];

        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(dismissKeyboard)];
        toolBar.items = @[flex, barButtonDone];
        barButtonDone.tintColor = [UIColor blackColor];
        _txtFieldEventPhoneNumber.inputAccessoryView = toolBar;
    
    
        NSString *event_website = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_website"]];



        _txtFieldEventWebsite.placeholder = @"Event Website";
        _txtFieldEventWebsite.tag = 11;
        _txtFieldEventWebsite.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _txtFieldEventWebsite.font = [UIFont fontWithName:fontLight size:14];
        _txtFieldEventWebsite.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtFieldEventWebsite.delegate = self;
        _txtFieldEventWebsite.text = event_website;
        _txtFieldEventWebsite.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _txtFieldEventWebsite.textAlignment = NSTextAlignmentLeft;
        _txtFieldEventWebsite.returnKeyType = UIReturnKeyNext;
        _txtFieldEventWebsite.keyboardType = UIKeyboardTypeDefault;
        _txtFieldEventWebsite.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _txtFieldEventWebsite.autocorrectionType = UITextAutocorrectionTypeNo;
        _txtFieldEventWebsite.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
        _txtFieldEventWebsite.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Event Website" attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]}];

    
        NSString *event_NotesShare = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_NotesShare"]];


        [_txtViewNotesshare setFont:[UIFont fontWithName:fontLight size:14]];
        _txtViewNotesshare.tag = 12;
        _txtViewNotesshare.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        _txtViewNotesshare.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtViewNotesshare.returnKeyType = UIReturnKeyDone;
        _txtViewNotesshare.userInteractionEnabled = true;
        _txtViewNotesshare.scrollEnabled = NO;
        _txtViewNotesshare.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        _txtViewNotesshare.autocorrectionType = UITextAutocorrectionTypeNo;
        _txtViewNotesshare.delegate = self;
        _txtViewNotesshare.tag = 21000;

        if (event_NotesShare.length == 0)
        {
            _txtViewNotesshare.text = @"Create a note or describe your event";
            _hgtNotesShare.constant = 30;
        }
        else
        {
            _txtViewNotesshare.text = event_NotesShare;
            _hgtNotesShare.constant = [_txtViewNotesshare sizeThatFits:CGSizeMake(_txtViewNotesshare.frame.size.width, CGFLOAT_MAX)].height;
            
        }

        NSString *event_description = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_NotesNoShare"]];
        [_txtViewNotes_Noshare setFont:[UIFont fontWithName:fontLight size:14]];
        _txtViewNotes_Noshare.tag = 13;
        _txtViewNotes_Noshare.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
        _txtViewNotes_Noshare.returnKeyType = UIReturnKeyDone;
        _txtViewNotes_Noshare.userInteractionEnabled = true;
        _txtViewNotes_Noshare.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        _txtViewNotes_Noshare.autocorrectionType = UITextAutocorrectionTypeNo;
        _txtViewNotes_Noshare.delegate = self;
        _txtViewNotes_Noshare.tag = 21500;
        _txtViewNotes_Noshare.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];

        if (event_description.length == 0)
        {
            _txtViewNotes_Noshare.text = @"If you choose to share this event the notes in this section will not be included";
            _hgtNotes_No_Share.constant = 45;
        }
        else
        {
            _txtViewNotes_Noshare.text = event_description;
            _hgtNotes_No_Share.constant = [_txtViewNotes_Noshare sizeThatFits:CGSizeMake(_txtViewNotes_Noshare.frame.size.width, CGFLOAT_MAX)].height;
        }
    
    
    
    
    
        [_btnTicket setTitleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
        _btnTicket.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
        [_btnTicket setTitle:@"  Tickets" forState:UIControlStateNormal];
    _btnTicket.titleLabel.textAlignment = NSTextAlignmentLeft;
       _btnTicket.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
       [_btnTicket setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
    
    
    
            [_btnMessage setTitleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
           _btnMessage.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
            [_btnMessage setTitle:@"  Messaging" forState:UIControlStateNormal];
    _btnMessage.titleLabel.textAlignment = NSTextAlignmentLeft;
    _btnMessage.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_btnMessage setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
            [_btnMessage addTarget:self action:@selector(messagesButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    
   

    
    [_btnInvite setTitleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
    _btnInvite.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
    [_btnInvite setTitle:@"  Invite Guests" forState:UIControlStateNormal];
    _btnInvite.titleLabel.textAlignment = NSTextAlignmentLeft;
    _btnInvite.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_btnInvite setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
    [_btnInvite addTarget:self action:@selector(openTicketsPopUp:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    NSString *event_save_later = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_save_later"]];
    if([event_save_later isEqualToString:@"1"])
    {
        [_btnSaveLater setSelected:YES];
    }
    else{
        [_btnSaveLater setSelected:NO];
    }
}

#pragma mark
#pragma mark *********************************************** Side Menu Delgates ***********************************************

-(void)menuButtonPressed
{
    NSLog(@"func Side Menu Button");
    [self removePickerView];
    [self dismissKeyboard];
    
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    SideMenu* sidenemuControler = [storyboard instantiateViewControllerWithIdentifier:@"SideMenu"];
    sidenemuControler.sideMenuDelegate=self;
  //  [self.navigationController pushViewController:sidenemuControler animated:YES];
    
    
   
    CATransition *transition = [[CATransition alloc] init];
    transition.duration = 0.5;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [self.view.window.layer addAnimation:transition forKey:kCATransition];
    sidenemuControler.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    sidenemuControler.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:sidenemuControler animated:false completion:nil];
}

-(void)func_SideMenu_RemoveSideMenu
{
    
}
- (void)func_SideMenu_YourDay
{
    [self performSelector:@selector(func_DelayInYourDay) withObject:nil afterDelay:0.5];
}

- (void)func_SideMenu_CreateEvent
{
    
    [self performSelector:@selector(func_DelayInCreateEvent) withObject:nil afterDelay:0.5];
}

- (void)func_SideMenu_MyEvents
{
  
    [self performSelector:@selector(func_DelayInMyEvents) withObject:nil afterDelay:0.5];
}

- (void)func_SideMenu_PastEvents
{
   
    [self performSelector:@selector(func_DelayInPastEvents) withObject:nil afterDelay:0.5];
}

- (void)func_SideMenu_Editevents
{
   
    [self performSelector:@selector(func_DelayInEditEvents) withObject:nil afterDelay:0.5];
}

- (void)func_SideMenu_ChangePassword
{
    
    [self performSelector:@selector(func_DelayInChangePassword) withObject:nil afterDelay:0.5];
}

- (void)func_SideMenu_Logout
{
    
    [self performSelector:@selector(func_DelayInLogout) withObject:nil afterDelay:0.5];
}

-(void)func_DelayInYourDay
{

    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    TodaysEventsViewController* todaysEvents = [storyboard instantiateViewControllerWithIdentifier:@"TodaysEventsViewController"];
    [self.navigationController pushViewController:todaysEvents animated:YES];
}

-(void)func_DelayInCreateEvent
{
    NSLog(@"create event tapped");
}

-(void)func_DelayInMyEvents
{
    NSLog(@"my event tapped");
    
    MyEventsViewController *controller = [[MyEventsViewController alloc]init];
    [self.navigationController pushViewController:controller animated:true];
}

-(void)func_DelayInPastEvents
{
    NSLog(@"past events tapped");
    PastEventsViewController *controller = [[PastEventsViewController alloc] init];
    [self.navigationController pushViewController:controller animated:true];
}

-(void)func_DelayInEditEvents
{
    NSLog(@"edit later events tapped");
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    EditLaterViewController* todaysEvents = [storyboard instantiateViewControllerWithIdentifier:@"EditLaterViewController"];
    [self.navigationController pushViewController:todaysEvents animated:YES];
    
}

-(void)func_DelayInChangePassword
{
    NSLog(@"change password tapped");
    
    ChangePasswordViewController *controller = [[ChangePasswordViewController alloc]init];
    [self.navigationController pushViewController:controller animated:true];
}

-(void)func_DelayInLogout
{
    NSLog(@"logout tapped");
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Logout" message:@"Are you sure, you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSLog(@"logout NOW");
        
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [appDelegate() functionToHandleInvalidAccessToken];
    }];
    
    UIAlertAction *noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertController addAction:yesAction];
    [alertController addAction:noAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:_tblAddresSearch]) {

        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }

    return YES;
}

#pragma mark
#pragma mark Table View Delegates----

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView == _tblTimeSlot){
        return [arrAddRow count];
    }
    else if(tableView == _tblAddresSearch){
        return [arraySearch count];
    }
    return 0;
}

- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    return [arraySearch objectAtIndex:indexPath.row];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if(tableView == _tblTimeSlot){
        
        static NSString *CellIdentifier = @"timeSlotTblCell";
           
           timeSlotTblCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
           
           [cell.btnPlus addTarget:self action:@selector(clickOnPlusBtn) forControlEvents:UIControlEventTouchUpInside];
           [cell.btnMinus addTarget:self action:@selector(clickOnMinusBtn) forControlEvents:UIControlEventTouchUpInside];
           
           
           NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_end_time"]];
            if ([event_end_time isEqualToString:@""] || [event_end_time containsString:@"(null)"] || event_end_time == nil)
            {
                    event_end_time = @"";
             }
                  
             if (event_end_time.length == 0)
             {
                      event_end_time = @"00:00 PM";
             }
            cell.btnStartDate.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
            [cell.btnStartDate setTitleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
           
             
        cell.btnStartDate.titleLabel.textAlignment = NSTextAlignmentLeft;
              cell.btnStartDate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
              [cell.btnStartDate setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
           
           cell.btnStartDate.tag = indexPath.row;
              [cell.btnStartDate addTarget:self action:@selector(selectEventStartTime:) forControlEvents:UIControlEventTouchUpInside];
           
              
            //  NSString *event_start_time = [NSString stringWithFormat:@"%@",[dict valueForKey:@"event_startTime_slot"]];
           
           NSLog(@"arry--%@",arrTimeStartSlotSet);
           
           if ([arrTimeStartSlotSet objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] == nil)
           {
                [cell.btnStartDate setTitle:@"00:00 AM" forState:UIControlStateNormal];
           }
           else{
               NSString *strStartTime = [arrTimeStartSlotSet objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
               [cell.btnStartDate setTitle:strStartTime forState:UIControlStateNormal];
           }

          
           
           cell.btnEndDate.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
           [cell.btnEndDate setTitleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0] forState:UIControlStateNormal];
           [cell.btnEndDate setTitle:event_end_time forState:UIControlStateNormal];
           cell.btnEndDate.titleLabel.textAlignment = NSTextAlignmentLeft;
           cell.btnEndDate.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
           [cell.btnEndDate setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
           [cell.btnEndDate addTarget:self action:@selector(selectEventEndTime) forControlEvents:UIControlEventTouchUpInside];
           
           
           if ([dictNameSlot objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] == nil)
           {
               cell.leftIconwidth.constant = 0;
               cell.rightIconwidth.constant = 24;
           }
           else{
               cell.leftIconwidth.constant = 24;
               cell.rightIconwidth.constant = 0;
           }
           
           cell.btnPopup.tag = indexPath.row;
           [cell.btnPopup addTarget:self action:@selector(ClickOnLeftNotebook:) forControlEvents:UIControlEventTouchUpInside];
            
           return cell;
        
    }
   
    else if(tableView == _tblAddresSearch){
        
        static NSString *cellIdentifier = @"addressTblCell";
            UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
            imageAttachment.image = [UIImage imageNamed:@"location"];
            CGFloat imageOffsetY = -5.0;
            imageAttachment.bounds = CGRectMake(0, imageOffsetY, 18, 18);
            NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
            NSMutableAttributedString *completeText= [[NSMutableAttributedString alloc] initWithString:@""];
            [completeText appendAttributedString:attachmentString];
            
            NSString *strName = [NSString stringWithFormat: @"    %@", [self placeAtIndexPath:indexPath].name];
            NSMutableAttributedString *textAfterIcon= [[NSMutableAttributedString alloc] initWithString:strName];
            [completeText appendAttributedString:textAfterIcon];
            cell.textLabel.attributedText = completeText;
            cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
            cell.textLabel.numberOfLines = 2;
            cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        //}
        
        return cell;
        
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self viewWillLayoutSubviews];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  //  SPGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    
    if(tableView == _tblAddresSearch){
        
         NSString *placeId = [self placeAtIndexPath:indexPath].reference;
        [dictionaryCreateEvent setValue:placeId forKey:@"place_id"];
           
        [[NSUserDefaults standardUserDefaults] setValue:placeId forKey:@"placeId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
           
        [self setDataonMApUSingPlaceId:placeId :indexPath];
           
    }
}

-(void)setDataonMApUSingPlaceId: (NSString *)strPlaceId :(NSIndexPath *)indexPath
{
     // Specify the place data types to return.
        GMSPlaceField fields = (GMSPlaceFieldName | GMSPlaceFieldPlaceID | GMSPlaceFieldAll);
        
        [_placesClient fetchPlaceFromPlaceID:strPlaceId placeFields:fields sessionToken:nil callback:^(GMSPlace * _Nullable place, NSError * _Nullable error) {
            if (error != nil) {
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"QUERY LIMITED" preferredStyle:UIAlertControllerStyleAlert];
                
                NSString *actionTitle = @"OK";
                UIAlertAction *okAction = [UIAlertAction
                                           actionWithTitle:actionTitle
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                           }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                
                
                return;
            }
            if (place != nil) {
                
                NSLog(@"place is: %@", place);
                
                self->_txtFieldPlaceName.text = [place name];
                [self->dictionaryCreateEvent setValue:[place name] forKey:@"event_place"];

                
                
                NSLog(@"The selected place address is: %@", [place formattedAddress]);
                
                
                
                for (int i = 0; i < [place.addressComponents count]; i++)
                {
                    NSLog(@"name %@ = type %@", place.addressComponents[i].name, place.addressComponents[i].types);
                    NSArray *dictTyp = place.addressComponents[i].types;
                     NSLog(@"frst object: %@", dictTyp.firstObject);
                    
                    if([dictTyp.firstObject  isEqual: @"street_number"])
                    {
                        NSLog(@"val %@", place.addressComponents[i].name);
                        
                        self->_txtFieldAddress1.text = place.addressComponents[i].name;
                        [self->dictionaryCreateEvent setValue:place.addressComponents[i].name forKey:@"event_address_first"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_address1"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                    }
                    
                    if([dictTyp.firstObject  isEqual: @"route"])
                    {
                        NSLog(@"val %@", place.addressComponents[i].name);
                        
                        self->_txtFieldAddress2.text = place.addressComponents[i].name;
                        [self->dictionaryCreateEvent setValue:place.addressComponents[i].name forKey:@"event_address_second"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_address2"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                        
                    
                    if([dictTyp.firstObject  isEqual: @"locality"])
                    {
                        NSLog(@"val %@", place.addressComponents[i].name);
                        
                        self->_txtFieldCity.text = place.addressComponents[i].name;
                         self->_txtFieldaddCity.text = place.addressComponents[i].name;
                        [self->dictionaryCreateEvent setValue:place.addressComponents[i].name forKey:@"event_city"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_city"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                    }
                    
                        
                   if([dictTyp.firstObject  isEqual: @"administrative_area_level_1"])
                   {
                       NSLog(@"val %@", place.addressComponents[i].name);
                       
                       
                       self->_txtFieldaddState.text = place.addressComponents[i].shortName;
                       [self->dictionaryCreateEvent setValue:place.addressComponents[i].shortName forKey:@"event_state"];
                       
                       [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].shortName forKey:@"map_state"];
                       [[NSUserDefaults standardUserDefaults] synchronize];
                       
                       
                       
                   }
                    if([dictTyp.firstObject  isEqual: @"postal_code"])
                    {
                        NSLog(@"val %@", place.addressComponents[i].name);
                        self->_txtFieldaddPostalCode.text = place.addressComponents[i].name;
                        [self->dictionaryCreateEvent setValue:place.addressComponents[i].shortName forKey:@"event_zip_code"];
                        
                        [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_zipcode"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    
                }
                
                
                
                
               
                
              
                
             
                
              
              
                
                
                [self.tblAddresSearch deselectRowAtIndexPath:indexPath animated:NO];
                NSArray *newArray = [[NSArray alloc]init];
                self->arraySearch = newArray;
                [self->_tblAddresSearch reloadData];
                [self->_tblAddressHeight setConstant:0];
                self->_tblBottomAdrss.constant = 0;
            }
        }];
        
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(void) clickOnPlusBtn{
    
    if(index<10){
       NSString *str = [NSString stringWithFormat:@"%d",index];
        [arrAddRow addObject:str];
         index++;
        [_tblTimeSlot reloadData];
    }
}




-(void)ClickOnLeftNotebook: (UIButton*) sender{
    
   
    btnPopupTag = (int)sender.tag;
    [_biewMainPopUp setHidden:false];
    [_btnSaveNAme addTarget:self action:@selector(btnClickOnSaveNAmeBtn) forControlEvents:UIControlEventTouchUpInside];
    
}


-(void)btnClickOnSaveNAmeBtn
{
    if ([_txtName.text isEqualToString:@""]  || _txtName.text == nil)
    {
        [self showAlertWithMessage:@"Please enter Time slot name."];
    }
    else
    {
        [dictNameSlot setValue:_txtName.text forKey:[NSString stringWithFormat:@"%d",btnPopupTag]];
        [_biewMainPopUp setHidden:true];
         [_tblTimeSlot reloadData];
    }
     
    
   
}

-(void) clickOnMinusBtn{
    
    if(index == 0){
        
        [arrAddRow removeAllObjects];
        [arrAddRow addObject:@""];
        
    }else{
        NSString *str = [NSString stringWithFormat:@"%d",index];
        [arrAddRow removeObject:str];
        index--;
    }
       
    
    
    
    [_tblTimeSlot reloadData];
}

#pragma mark
#pragma mark Alert Function
- (void) showAlertWithMessage : (NSString*)message
{
    [self.view makeToast:message duration:2.0 position:CSToastPositionBottom];
}


#pragma mark
#pragma mark AlertController Function
- (void) showAlertControllerWithMessage : (NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Event Saved" message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"View Event" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *current_date = [UIFunction getCurrentDateInString];
        NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
        NSArray *arrayMyEvents = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:true];
        
        EventDetailsViewController *controller = [[EventDetailsViewController alloc]init];
        controller.event_id = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:0] valueForKey:@"event_id"]];
        [self.navigationController pushViewController:controller animated:true];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Create Event" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:true completion:nil];
}


#pragma mark
#pragma mark Header Buttons Function
-(void)todaysEventsButtonPressed
{
    NSLog(@"Todays Events Button Pressed");
    
    [self dismissKeyboard];
    [self removePickerView];

    TodaysEventsViewController *todaysEvents = [[TodaysEventsViewController alloc]init];
    todaysEvents.isTodayEvent = true;
    [self.navigationController pushViewController:todaysEvents animated:YES];
}
-(void)notificationsButtonPressed
{
    NSLog(@"Notifications Button Pressed");
    
    [self dismissKeyboard];
    [self removePickerView];
    
    NotificationsViewController *notifications = [[NotificationsViewController alloc]init];
    [self.navigationController pushViewController:notifications animated:YES];
}


#pragma mark
#pragma mark Set Badge Count
-(void)setNotificationAndEventsBadge
{
    @try
    {
        NSString *current_date = [UIFunction getCurrentDateInString];
        NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
        NSArray *array = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:false];
        lblEventsCount.text = [NSString stringWithFormat:@"%zd",array.count];
        lblNotificationsCount.text = @"0";
        
//        if (_viewSideMenu != nil) {
//            
//            [_viewSideMenu func_SideMenu_reloadData];
//        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is 2 %@",exception.description);
    }
}

-(void)createCircleViewWithFrame : (CGRect)frame atView:(UIView*)baseView
{
    UIView *view = [UIFunction createUIViews:frame bckgroundColor:[UIColor whiteColor]];
    view.layer.cornerRadius = view.frame.size.height/2.0;
    view.layer.masksToBounds = true;
    view.clipsToBounds = true;
    [baseView addSubview:view];
}

-(void)createBlackCircleViewWithFrame : (CGRect)frame atView:(UIView*)baseView
{
    UIView *view = [UIFunction createUIViews:frame bckgroundColor:[UIColor blackColor]];
    view.layer.cornerRadius = view.frame.size.height/2.0;
    view.layer.masksToBounds = true;
    view.clipsToBounds = true;
    [baseView addSubview:view];
}


#pragma mark
#pragma mark Done/Save Button Pressed
-(void)doneButtonPressed
{
    NSLog(@"done button pressed %@",dictionaryCreateEvent);
    [self removePickerView];
    [self dismissKeyboard];
    
    NSString *event_city = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_city"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *event_topic = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_topic"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *event_name = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_name"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *event_date = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_date"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSString *event_start_time = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_start_time"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *event_end_time = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_end_time"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
     NSString *event_place = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_place"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
   
    NSString *event_address_first = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_first"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *event_address_second = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_second"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
     NSString *event_state = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_state"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *event_zip_code = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_zip_code"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *event_phone_number = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_phone_number"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *event_website = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_website"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *event_NotesShare = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_NotesShare"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
     NSString *event_NotesNoShare = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_NotesNoShare"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    NSArray *event_images = [dictionaryCreateEvent valueForKey:@"event_images"];

    NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
    
    NSString *latitude = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"latitude"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *longitude = [[NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"longitude"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    NSString *user_id = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_id"]];

    NSString *user_country_code = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"user_country_code"]];
    
     NSString *event_place_id = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"place_id"]];
    
    NSString *event_save_later = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_save_later"]];
    
    
    

    
//    if (event_city.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter city."];
//    }
//    else if (event_topic.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter topic."];
//    }
    if (event_name.length == 0)
    {
        [self showAlertWithMessage:@"Please enter event name."];
    }
//    else if (event_date.length == 0)
//    {
//        [self showAlertWithMessage:@"Please select start date."];
//    }
//    else if (event_start_time.length == 0)
//    {
//        [self showAlertWithMessage:@"Please select start time."];
//    }
//    else if (event_end_time.length == 0)
//    {
//        [self showAlertWithMessage:@"Please select end time."];
//    }
//    else if (event_address_first.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter Address."];
//    }
//    else if (event_address_second.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter Address 2."];
//    }
//    else if (event_zip_code.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter Zip Code."];
//    }
//    else if (event_phone_number.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter Phone Number."];
//    }
//    else if (event_website.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter website."];
//    }
//    else if (event_description.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter description."];
//    }
//    else if (latitude.length == 0 || longitude.length == 0)
//    {
//        [self showAlertWithMessage:@"Please enter correct address."];
//    }
//    else if (user_country_code.length == 0)
//    {
//        [self showAlertWithMessage:@"Please select your country code."];
//    }
    else
    {
        if (isEditEvent == false)
        {
            BOOL isInsertion = [[DataManager getSharedInstance]Insert_EventTableWithCity:event_city topic:event_topic event_name:event_name event_date:event_date event_start_time:event_start_time event_end_time:event_end_time event_place:event_place event_address_first:event_address_first event_address_second:event_address_second event_state:event_state event_zip_code:event_zip_code event_phone_number:event_phone_number event_website:event_website event_notesShare:event_NotesShare event_notesNotShare:event_NotesNoShare event_images:event_images user_email:user_email latitude:latitude longitude:longitude : user_id user_country_code: user_country_code user_place_id:event_place_id event_save_later:event_save_later];
            
            
            if (isInsertion == true)
            {
                [self showAlertControllerWithMessage:@"Event created successfully."];
                [self createDictionaryForEvent];
                [self setNotificationAndEventsBadge];
               // [tableViewCreateEvent reloadData];
                [self.collectionViewForImages reloadData];
                
                
                _txtFieldCity.text = @"";
                 _txtFieldTopic.text = @"";
                 _txtFieldEventName.text = @"";
                 _txtFieldPlaceName.text = @"";
                 _txtFieldAddress1.text = @"";
                 _txtFieldAddress2.text = @"";
                 _txtFieldaddCity.text = @"";
                 _txtFieldaddState.text = @"";
                 _txtFieldaddPostalCode.text = @"";
                 _txtFieldEventPhoneNumber.text = @"";
                 _txtFieldEventWebsite.text = @"";
                
                [_btnEventDate setTitle:@"00-00-00" forState:UIControlStateNormal];
               // [_btnEventStartTime setTitle:@"" forState:UIControlStateNormal];
             //   [_btnEventEndTime setTitle:@"" forState:UIControlStateNormal];
                
                _txtViewNotesshare.text = @"Create a note or describe your event";
                _txtViewNotes_Noshare.text = @"If you choose to share this event the notes in this section will not be included";
                
                _hgtNotesShare.constant = 30;
                _hgtNotes_No_Share.constant = 45;
                
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"place_from_map"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"placeId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
               
                
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_address1"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                   
                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_address2"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                   
                   
                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_city"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                     
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_state"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                   
                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_zipcode"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                
                [_tblTimeSlot reloadData];
                     
               
                
            }
            else
            {
                [self showAlertWithMessage:@"Unable to create event at this moment, please try after some time."];
            }
        }
        else
        {
            BOOL isUpdateSuccess = [[DataManager getSharedInstance]Update_EventTableWithCity:event_city topic:event_topic event_name:event_name event_date:event_date event_start_time:event_start_time event_end_time:event_end_time event_place:event_place event_address_first:event_address_first event_address_second:event_address_second event_state:event_state event_zip_code:event_zip_code event_phone_number:event_phone_number event_website:event_website event_notesShare:event_NotesShare event_notesNotShare:event_NotesNoShare event_images:event_images event_id:edit_event_id user_email:user_email latitude:latitude longitude:longitude :user_id user_country_code: user_country_code user_place_id:event_place_id event_save_later:event_save_later];
            
            if (isUpdateSuccess == true)
            {
                [self setNotificationAndEventsBadge];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"place_from_map"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"placeId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                              
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_address1"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                      
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_address2"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                      
                      
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_city"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                        
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_state"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                      
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_zipcode"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                   
                [_tblTimeSlot reloadData];
                                    
                
                [self.navigationController popViewControllerAnimated:true];
            }
            else
            {
                [self showAlertWithMessage:@"Unable to update event at this moment, please try after some time."];
            }
        }
    }
}



#pragma mark
#pragma mark UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView.tag == 69802140)
    {
        return 1;
    }
    return 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == 69802140)
    {
        return 5;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 69802140)
    {
        return CGSizeMake(widthOfCollectionView/5.0, widthOfCollectionView/5.0);
    }
    return CGSizeMake(0, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 69802140)
    {
        UICollectionViewCell *cell = nil;
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
        cell.backgroundColor=[UIColor clearColor];
        
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
        
        @try
        {
            UICollectionViewLayoutAttributes * theAttributes = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
            CGRect frame = [collectionView convertRect:theAttributes.frame toView:[collectionView superview]];
            
            
           /* if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
            {
                NSString *imageName = [[dictionaryCreateEvent valueForKey:@"event_images"]objectAtIndex:0];
                if (imageName.length > 0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:0]];
                    imageName = [imageName stringByReplacingOccurrencesOfString:@"TOP" withString:@""];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension.length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                [btnAddAttachment setImage:[UIImage imageWithContentsOfFile:filePath] forState:UIControlStateNormal];
                            }
                            else
                            {
                                [btnAddAttachment setImage:[UIImage imageNamed:@"attachment"] forState:UIControlStateNormal];
                            }
                        }
                    }
                }
                else
                {
                    [btnAddAttachment setImage:[UIImage imageNamed:@"upload_image"] forState:UIControlStateNormal];
                }
            }*/
            
            
            
            if (indexPath.row == 0)
            {
                float width = frame.size.width-35.0;
                
                UIView *view = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:view];

                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(view.frame.size.width-width-5, 17.5, width , frame.size.height-35.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];

                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:0]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 1)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(12.5, 12.5, frame.size.width-25.0, frame.size.height-25.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:1]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 2)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(5.0, 5.0, frame.size.width-10.0, frame.size.height-10.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:2]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 3)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(12.5, 12.5, frame.size.width-25.0, frame.size.height-25.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:3]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 4)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(5, 17.5, frame.size.width-35.0, frame.size.height-35.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[dictionaryCreateEvent valueForKey:@"event_images"]count]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryCreateEvent valueForKey:@"event_images"] objectAtIndex:4]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception is 3 %@",exception.description);
        }
        
        cell.userInteractionEnabled = true;
        return cell;
    }
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    indexOfImage = indexPath.row;

    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    
    if ([[arrayImages objectAtIndex:indexOfImage] isEqualToString:@""])
    {
       // [self addEventImages];
    }
    else
    {
        // option to view image or remove image
        [self viewOrRemovePhotoPressed];
    }
}

#pragma mark
#pragma mark //******************** Choose Photo Delegates *************************//
-(void)addEventImages
{
    [self.view endEditing:YES];
    
    [viewToChoosePhoto removeFromSuperview];
    viewToChoosePhoto = nil;
    viewToChoosePhoto = [[ChoosePhotoView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    viewToChoosePhoto.choosePhotoDelegate=self;
    [self.view addSubview:viewToChoosePhoto];
}

-(void)removeChoosePhotoView
{
    [UIView animateWithDuration:.3f animations:^{
        self->viewToChoosePhoto.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        [self->viewToChoosePhoto removeFromSuperview];
        self->viewToChoosePhoto = nil;
    }];
}

-(void)choosePhotoFromCameraButtonPressed
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        // open camera
        [self performSelector:@selector(showCamera) withObject:nil afterDelay:0.3];
    }
    else
    {
        // show alert that device has no camera
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"Device has no camera" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *settingsAction = [UIAlertAction
                                         actionWithTitle:@"OK"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action)
                                         {
                                         }];
        
        [alertController addAction:settingsAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)uploadDocumentButtonPressed
{
    NSLog(@"upload document");
    UIDocumentMenuViewController *importMenu =
    [[UIDocumentMenuViewController alloc] initWithDocumentTypes:[self UTIs] inMode:UIDocumentPickerModeImport];
    importMenu.delegate = self;
    [self presentViewController:importMenu animated:YES completion:nil];
}

-(void)choosePhotoFromGalleryButtonPressed
{
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        
        NSLog(@"status is %zd",status);
        
        switch (status) {
            case PHAuthorizationStatusAuthorized:
            {
                NSLog(@"PHAuthorizationStatusAuthorized");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self openGallery];
                });
            }
                break;
                
            case PHAuthorizationStatusRestricted || status == PHAuthorizationStatusDenied:
            {
                NSLog(@"PHAuthorizationStatusRestricted");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    UIAlertAction *cancelAction = [UIAlertAction
                                                   actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleDestructive
                                                   handler:^(UIAlertAction *action)
                                                   { }];
                    
                    UIAlertAction *settingsAction = [UIAlertAction
                                                     actionWithTitle:@"Settings"
                                                     style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action)
                                                     {
                                                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                     }];
                    
                    
                    [alertController addAction:cancelAction];
                    [alertController addAction:settingsAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                });
            }
                
                break;
                
            case PHAuthorizationStatusDenied:
            {
                NSLog(@"PHAuthorizationStatusDenied");
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
                    
                    
                    UIAlertAction *cancelAction = [UIAlertAction
                                                   actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleDestructive
                                                   handler:^(UIAlertAction *action)
                                                   { }];
                    
                    UIAlertAction *settingsAction = [UIAlertAction
                                                     actionWithTitle:@"Settings"
                                                     style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action)
                                                     {
                                                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                     }];
                    
                    
                    [alertController addAction:cancelAction];
                    [alertController addAction:settingsAction];
                    [self presentViewController:alertController animated:YES completion:nil];
                    
                });
                
            }
                
                break;
                
            case PHAuthorizationStatusNotDetermined:
            {
                NSLog(@"PHAuthorizationStatusNotDetermined");
                
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
                 {
                     if(granted)
                     {
                         NSLog(@"Granted access");
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self openGallery];
                         });
                     }
                     else
                     {
                         NSLog(@"Not granted access");
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
                             
                             
                             UIAlertAction *cancelAction = [UIAlertAction
                                                            actionWithTitle:@"Cancel"
                                                            style:UIAlertActionStyleDestructive
                                                            handler:^(UIAlertAction *action)
                                                            { }];
                             
                             UIAlertAction *settingsAction = [UIAlertAction
                                                              actionWithTitle:@"Settings"
                                                              style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *action)
                                                              {
                                                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                              }];
                             
                             
                             [alertController addAction:cancelAction];
                             [alertController addAction:settingsAction];
                             [self presentViewController:alertController animated:YES completion:nil];
                             
                         });                     }
                 }];
            }
                
                break;
            default:
                break;
        }
    }];
}


-(void)showCamera
{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (status == AVAuthorizationStatusAuthorized)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self openCamera];
        });
    }
    else if (status == AVAuthorizationStatusDenied || status == AVAuthorizationStatusRestricted )
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
        
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Cancel"
                                       style:UIAlertActionStyleDestructive
                                       handler:^(UIAlertAction *action)
                                       { }];
        
        UIAlertAction *settingsAction = [UIAlertAction
                                         actionWithTitle:@"Settings"
                                         style:UIAlertActionStyleDefault
                                         handler:^(UIAlertAction *action)
                                         {
                                             [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                         }];
        
        
        [alertController addAction:cancelAction];
        [alertController addAction:settingsAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (status == AVAuthorizationStatusNotDetermined)
    {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access");
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self openCamera];
                 });
             }
             else
             {
                 NSLog(@"Not granted access");
                 UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"App does not have access to your camera or Photos. To enable access, tap Settings and turn On Camera and Photos." preferredStyle:UIAlertControllerStyleAlert];
                 
                 
                 UIAlertAction *cancelAction = [UIAlertAction
                                                actionWithTitle:@"Cancel"
                                                style:UIAlertActionStyleDestructive
                                                handler:^(UIAlertAction *action)
                                                { }];
                 
                 UIAlertAction *settingsAction = [UIAlertAction
                                                  actionWithTitle:@"Settings"
                                                  style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action)
                                                  {
                                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                                  }];
                 
                 
                 [alertController addAction:cancelAction];
                 [alertController addAction:settingsAction];
                 [self presentViewController:alertController animated:YES completion:nil];
             }
         }];
    }
}


-(void)openCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate  = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:Nil];
}

-(void)openGallery
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate  = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark IMAGE PICKER DELEGATES

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    NSDate *todayDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
    dateFormatter.dateFormat = @"YYYY_MM_dd_hh_mm_ss.SSS";
    NSString *imageName = [NSString stringWithFormat:@"%@.jpg",[dateFormatter stringFromDate:todayDate]];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *pathOfImage = [documentsDirectory stringByAppendingPathComponent:imageName];  // IT IS THE PATH OF CHOOSEN IMAGE
    NSData *imageData= UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerEditedImage"],0.0);
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imageData= UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerOriginalImage"],0.0);
    }
    [imageData writeToFile:pathOfImage atomically:YES];
    
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    
    if (arrayImages.count < 6)
    {
        [arrayImages replaceObjectAtIndex:indexOfImage withObject:imageName];
    }
    else
    {
        [self showAlertWithMessage:@"You can add maximum 5 attachments."];
    }
    
    [dictionaryCreateEvent setValue:arrayImages forKey:@"event_images"];
    NSLog(@"_____ %@",dictionaryCreateEvent);
    [self.collectionViewForImages reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}



#pragma mark
#pragma mark //******************** View or Remove Photo Delegates *************************//
-(void)viewOrRemovePhotoPressed
{
    [self.view endEditing:YES];
    
    [viewFullSizeOrRemovePhoto removeFromSuperview];
    viewFullSizeOrRemovePhoto = nil;
    viewFullSizeOrRemovePhoto = [[RemovePhotoView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    viewFullSizeOrRemovePhoto.removePhotoDelegate=self;
    [self.view addSubview:viewFullSizeOrRemovePhoto];
}

-(void)removePhotoViewFunction
{
    [UIView animateWithDuration:.3f animations:^{
        self->viewFullSizeOrRemovePhoto.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        [self->viewFullSizeOrRemovePhoto removeFromSuperview];
        self->viewFullSizeOrRemovePhoto = nil;
    }];
}

-(void)viewPhotoButtonPressed
{
    NSLog(@"viewPhotoButtonPressed");
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    
    if ([[arrayImages objectAtIndex:indexOfImage]length]>0 && (![[arrayImages objectAtIndex:indexOfImage]hasSuffix:@".jpg"] || [[arrayImages objectAtIndex:indexOfImage]hasSuffix:@".png"])) // attachment is not an image
    {
        NSString *attachmentName = [arrayImages objectAtIndex:indexOfImage];
        NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:attachmentName];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        if (fileExists == true)
        {
            NSLog(@"filePath : %@", filePath);
            NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
            [self loadAttachmentInWebView:fileURL];
        }
    }
    else
    {
        NSArray *filtered = [arrayImages filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"length > 0"]];
        NSMutableArray *photosURL = [[NSMutableArray alloc]init];
        for (int counter = 0; counter < filtered.count; counter++)
        {
            NSString *imageName = [NSString stringWithFormat:@"%@",[filtered objectAtIndex:counter]];
            if ([imageName containsString:@"TOP"])
            {
                imageName = [imageName stringByReplacingOccurrencesOfString:@"TOP" withString:@""];
            }
            
            NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
            if (fileExists == true && ([imageName hasSuffix:@".jpg"] || [imageName hasSuffix:@".png"]))
            {
                [photosURL addObject:filePath];
            }
        }
        
        if (photosURL.count == 0)
        {
            return;
        }
        
        NSArray *photos = [IDMPhoto photosWithFilePaths:photosURL];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:self.view];
        browser.displayActionButton = NO;
        [browser setInitialPageIndex:0];
        browser.displayArrowButton = NO;
        browser.displayCounterLabel = YES;
        browser.usePopAnimation = NO;
        browser.disableVerticalSwipe = YES;
        [browser setInitialPageIndex:indexOfImage];
        [self presentViewController:browser animated:YES completion:nil];
    }
}



-(void)removePhotoButtonPressed
{
    NSLog(@"removePhotoButtonPressed");
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    [arrayImages replaceObjectAtIndex:indexOfImage withObject:@""];
    [dictionaryCreateEvent setObject:arrayImages forKey:@"event_images"];
    [self.collectionViewForImages reloadData];
}


#pragma mark
#pragma mark to show ticket pop up

-(void)openTicketsPopUp: (UIButton *) sender
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    invitePopUpView* controller = [storyboard instantiateViewControllerWithIdentifier:@"invitePopUpView"];
    controller.dictPassDataFrom = dictionaryCreateEvent;
    [self.navigationController pushViewController:controller animated:YES];
}



#pragma mark
#pragma mark Select Event Start Date
-(void)selectEventDate
{
    NSLog(@"select start date of event");
    
    [self.view endEditing:true];
    [self dismissKeyboard];
    
    @try
    {
       // [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];

        [backGroundViewForPicker removeFromSuperview];
        backGroundViewForPicker = nil;
        backGroundViewForPicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor blackColor]];
        [self.view addSubview:backGroundViewForPicker];
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setBarStyle:UIBarStyleBlack];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(removePickerView)];
        
        UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(cancelDatePickerSelect)];
        
        toolBar.items = @[barButtonCancel, flex, barButtonDone];
        barButtonDone.tintColor = [UIColor yellowColor];
        barButtonCancel.tintColor = [UIColor yellowColor];
        [backGroundViewForPicker addSubview:toolBar];
        
        UIDatePicker *datePicker  = [[UIDatePicker alloc]init];
        [datePicker setFrame:CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, self.view.frame.size.width, 216.0)];
        datePicker.datePickerMode = UIDatePickerModeDate;
        [datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
        datePicker.tag = 9542400;
        [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"US"]];
        datePicker.backgroundColor = [UIColor clearColor];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        [datePicker setMinimumDate:[NSDate date]];
        [backGroundViewForPicker addSubview:datePicker];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [self->backGroundViewForPicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self updateDateField : datePicker];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is 4 %@",exception.description);
    }
}

-(void)updateDateField : (UIDatePicker*)datePicker
{
    if (datePicker.tag == 9542400) // Event Start Date
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        [formatter setDateFormat:@"MM-dd-yyyy"];
        NSString *stringFromDate = [formatter stringFromDate:datePicker.date];
        
        [dictionaryCreateEvent setValue:stringFromDate forKey:@"event_date"];
        [_btnEventDate setTitle:stringFromDate forState:UIControlStateNormal];
        //[_tblTimeSlot reloadData];
    }
    
    else if (datePicker.tag == 545454) // Event Start Time
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        [formatter setDateFormat:@"hh:mm a"];
        NSString *stringFromDate = [formatter stringFromDate:datePicker.date];
        tempEventStartTime = stringFromDate;
        
        
      //  NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
      //  [dict setValue:stringFromDate forKey:@"event_startTime_slot"];
        
        //in main dixt will set array Time
      //  [dictionaryCreateEvent setValue:stringFromDate forKey:@"event_start_time"];
        
     //   [_tblTimeSlot reloadData];
       // [_btnEventStartTime setTitle:stringFromDate forState:UIControlStateNormal];
        
    }
    else if (datePicker.tag == 454545) // Event End Time
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        [formatter setDateFormat:@"hh:mm a"];
        NSString *stringFromDate = [formatter stringFromDate:datePicker.date];
        tempEventEndTime = stringFromDate;
        [dictionaryCreateEvent setValue:stringFromDate forKey:@"event_end_time"];
        [_tblTimeSlot reloadData];
      //  [_btnEventEndTime setTitle:stringFromDate forState:UIControlStateNormal];
        
        
    }
}

#pragma mark
#pragma mark Remove Date Picker For Event Start Time and End Time
-(void)removeDatePickerForEventStartTime: (UIButton *) sender
{
    @try
    {
        
        NSLog(@"tag----%d",btnIndex);
        
        NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_date"]];
        NSString *event_start_time = tempEventStartTime;
        NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_end_time"]];
        
        if ([event_date containsString:@"("] || [event_date containsString:@"null"] || event_date == nil)
        {
            event_date = @"";
        }
        
        if ([event_start_time containsString:@"("] || [event_start_time containsString:@"null"] || event_start_time == nil)
        {
            event_start_time = @"";
        }
        
        if ([event_end_time containsString:@"("] || [event_end_time containsString:@"null"] || event_end_time == nil)
        {
            event_end_time = @"";
        }

        if (event_date.length == 0)
        {
            [self showAlertWithMessage:@"Please select start date."];
        }
        else if (event_start_time.length == 0)
        {
            [self showAlertWithMessage:@"Please select start time."];
        }
        else
        {
            NSString *newDateInString = [NSString stringWithFormat:@"%@ %@",event_date, event_start_time];
            BOOL isSelectedDateAllowd = [self isEndDateIsSmallerThanCurrent:newDateInString];

            if (isSelectedDateAllowd == false)
            {
                [self showAlertWithMessage:@"Start time must be greater than current time."];
                tempEventStartTime = @"";
                [dictionaryCreateEvent setValue:tempEventStartTime forKey:@"event_start_time"];
                //set date time
                //[tableViewCreateEvent reloadData];
            }
            else
            {
                [dictionaryCreateEvent setValue:tempEventStartTime forKey:@"event_start_time"];
               // [tableViewCreateEvent reloadData];
                
                
                [arrTimeStartSlotSet setValue:tempEventStartTime forKey:[NSString stringWithFormat:@"%d",btnIndex]];
                
                NSLog(@"dict--%@",arrTimeStartSlotSet);
                
                
               // [arrTimeStartSlotSet addObject:tempEventStartTime];
                 tempEventStartTime = @"";
                [_tblTimeSlot reloadData];
            }
        }
        [self removePickerView];
    }
    @catch (NSException *exception)
    {
        
    }
}

-(void)cancelDatePickerSelect
{
    [self removePickerView];
}

-(void)cancelStartDatePickerSelect
{
    [self removePickerView];
    [dictionaryCreateEvent setValue:@"" forKey:@"event_start_time"];
    [_tblTimeSlot reloadData];
}

-(void)cancelEndDatePickerSelect
{
    [self removePickerView];
    [dictionaryCreateEvent setValue:@"" forKey:@"event_end_time"];
    [_tblTimeSlot reloadData];
}

-(void)removeDatePickerForEventEndTime
{
    @try
    {
        NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_date"]];
        NSString *event_start_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_start_time"]];
        NSString *event_end_time = tempEventEndTime;
        
        
        if ([event_date containsString:@"("] || [event_date containsString:@"null"] || event_date == nil)
        {
            event_date = @"";
        }
        
        if ([event_start_time containsString:@"("] || [event_start_time containsString:@"null"] || event_start_time == nil)
        {
            event_start_time = @"";
        }
        
        if ([event_end_time containsString:@"("] || [event_end_time containsString:@"null"] || event_end_time == nil)
        {
            event_end_time = @"";
        }
        
        
        if (event_date.length == 0)
        {
            [self showAlertWithMessage:@"Please select start date."];
        }
        else if (event_start_time.length == 0)
        {
            [self showAlertWithMessage:@"Please select start time."];
        }
        else if (event_end_time.length == 0)
        {
            [self showAlertWithMessage:@"Please select end time."];
        }

        else
        {
            NSString *newDateForEventStart = [NSString stringWithFormat:@"%@ %@",event_date, event_start_time];
            NSString *newDateForEventEnd = [NSString stringWithFormat:@"%@ %@",event_date, event_end_time];

            BOOL isSelectedDateAllowd = [self isEndDateIsSmallerThanEventStartDate:newDateForEventStart : newDateForEventEnd];
            
            if (isSelectedDateAllowd == false)
            {
                [self showAlertWithMessage:@"End time must be greater than start time."];
                
                tempEventEndTime = @"";
                [dictionaryCreateEvent setValue:tempEventEndTime forKey:@"event_end_time"];
              //  [tableViewCreateEvent reloadData];
            }
            else
            {
                [dictionaryCreateEvent setValue:tempEventEndTime forKey:@"event_end_time"];
            //    [tableViewCreateEvent reloadData];
                tempEventEndTime = @"";
            }
        }
        [self removePickerView];
    }
    @catch (NSException *exception)
    {
        
    }
}


-(void)selectEventEndTime
{
    NSLog(@"select end time of event");
    [self.view endEditing:true];
    [self dismissKeyboard];

    @try
    {
       // [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        [backGroundViewForPicker removeFromSuperview];
        backGroundViewForPicker = nil;
        backGroundViewForPicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor blackColor]];
        [self.view addSubview:backGroundViewForPicker];
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setBarStyle:UIBarStyleBlack];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(removeDatePickerForEventEndTime)];
        
        UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                                 style:UIBarButtonItemStylePlain
                                                                                target:self
                                                                                action:@selector(cancelEndDatePickerSelect)];
        toolBar.items = @[barButtonCancel, flex, barButtonDone];
        barButtonDone.tintColor =  [UIColor yellowColor];
        barButtonCancel.tintColor =  [UIColor yellowColor];
        [backGroundViewForPicker addSubview:toolBar];
        
        UIDatePicker *datePicker  = [[UIDatePicker alloc]init];
        [datePicker setFrame:CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, self.view.frame.size.width, 216.0)];
        datePicker.datePickerMode = UIDatePickerModeTime;
        [datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
        datePicker.tag = 454545;
        [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US"]];
        datePicker.backgroundColor = [UIColor clearColor];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        //[datePicker setMinimumDate:[NSDate date]];
        [backGroundViewForPicker addSubview:datePicker];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [self->backGroundViewForPicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self updateDateField : datePicker];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is 5 %@",exception.description);
    }
}

-(void)selectEventStartTime : (UIButton *)sender
{
    NSLog(@"select event start time");
    
    [self.view endEditing:true];
    [self dismissKeyboard];
    
    @try
    {
    //    [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        NSLog(@"tag----%ld",(long)sender.tag);
        
        btnIndex = (int)sender.tag;
        
        [backGroundViewForPicker removeFromSuperview];
        backGroundViewForPicker = nil;
        backGroundViewForPicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor blackColor]];
        [self.view addSubview:backGroundViewForPicker];
        
        UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        [toolBar setBarStyle:UIBarStyleBlack];
        UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
        
        UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(removeDatePickerForEventStartTime:)];
        
        UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                            style:UIBarButtonItemStylePlain
                                                                           target:self
                                                                           action:@selector(cancelStartDatePickerSelect)];
        
        toolBar.items = @[barButtonCancel, flex, barButtonDone];
        barButtonDone.tintColor =  [UIColor yellowColor];
        barButtonCancel.tintColor =  [UIColor yellowColor];
        [backGroundViewForPicker addSubview:toolBar];
        
        UIDatePicker *datePicker  = [[UIDatePicker alloc]init];
        [datePicker setFrame:CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, self.view.frame.size.width, 216.0)];
        datePicker.datePickerMode = UIDatePickerModeTime;
        datePicker.tag = 545454;
        [datePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
        [datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"en_US"]];
        datePicker.backgroundColor = [UIColor clearColor];
        [datePicker addTarget:self action:@selector(updateDateField:) forControlEvents:UIControlEventValueChanged];
        //[datePicker setMinimumDate:[NSDate date]];
        [backGroundViewForPicker addSubview:datePicker];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [self->backGroundViewForPicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self updateDateField : datePicker];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is 1 %@",exception.description);
    }
}


-(void)dismissKeyboard
{
    [self.view endEditing:YES];
//    [tableViewCreateEvent scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

-(void)removePickerView
{
    [UIView animateWithDuration:.2f animations:^{
        
        [self->backGroundViewForPicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];
        [self->viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];

        
    } completion:^(BOOL finished) {
        
        [self->backGroundViewForPicker removeFromSuperview];
        self->backGroundViewForPicker = nil;
        [self->viewForCountryCodePicker removeFromSuperview];
        self->viewForCountryCodePicker = nil;
    }];
}


#pragma mark
#pragma mark Text View Delegates

- (void)textViewFitToContent:(UITextView *)textView
{
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    textView.scrollEnabled = NO;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    if (textView == _txtViewNotesshare)
    {
        [self textViewFitToContent:_txtViewNotesshare];
        
        _hgtNotesShare.constant = [_txtViewNotesshare sizeThatFits:CGSizeMake(_txtViewNotesshare.frame.size.width, CGFLOAT_MAX)].height;
        
        NSString * stringToRange = [textView.text substringWithRange:NSMakeRange(0,range.location)];
        stringToRange = [stringToRange stringByAppendingString:text];
        NSArray *wordArray       = [stringToRange componentsSeparatedByString:@" "];
        NSString * wordTyped     = [wordArray componentsJoinedByString:@" "];
        [dictionaryCreateEvent setValue:wordTyped forKey:@"event_NotesShare"];
       // [tableViewCreateEvent endUpdates];
        //_eventScrollView.contentSize=CGSizeMake(_eventScrollView.frame.size.width,_eventScrollView.frame.size.height+textView.frame.size.height+10);
        
    }
    
    if (textView == _txtViewNotes_Noshare)   //change key names here and in db
    {
       [self textViewFitToContent:_txtViewNotes_Noshare];
        _hgtNotes_No_Share.constant = [_txtViewNotes_Noshare sizeThatFits:CGSizeMake(_txtViewNotes_Noshare.frame.size.width, CGFLOAT_MAX)].height;
        NSString * stringToRange = [textView.text substringWithRange:NSMakeRange(0,range.location)];
        stringToRange = [stringToRange stringByAppendingString:text];
        NSArray *wordArray       = [stringToRange componentsSeparatedByString:@" "];
        NSString * wordTyped     = [wordArray componentsJoinedByString:@" "];
        [dictionaryCreateEvent setValue:wordTyped forKey:@"event_NotesNoShare"];
        
    }
    
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [self removePickerView];
 //   [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:8 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    
    if(textView.tag == 21000)
    {
        
        NSString *_strNotes = [dictionaryCreateEvent valueForKey:@"event_NotesShare"];
        
        if (_strNotes.length==0)
        {
            textView.text = @"";
        }
        else
        {
            textView.text = _strNotes;
        }
    }
    
    if(textView.tag == 21500)
    {
        
        NSString *_strNotes = [dictionaryCreateEvent valueForKey:@"event_NotesNoShare"];
        
        if (_strNotes.length==0)
        {
            textView.text = @"";
        }
        else
        {
            textView.text = _strNotes;
        }
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    if(textView.tag == 21000)
    {
        NSString *_strNotes = [dictionaryCreateEvent valueForKey:@"event_NotesShare"];
        
        if (_strNotes.length==0)
        {
            textView.text = @"";
        }
        {
            textView.text = _strNotes;
        }
    }
    
    if(textView.tag == 21500)
    {
        NSString *_strNotes = [dictionaryCreateEvent valueForKey:@"event_NotesNoShare"];
        
        if (_strNotes.length==0)
        {
            textView.text = @"";
        }
        {
            textView.text = _strNotes;
        }
    }
}


#pragma mark
#pragma mark TextField Delegate
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField == _txtFieldCity) // city
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_city"];
    }
    else if (textField == _txtFieldTopic) // topic
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_topic"];
    }
    else if (textField == _txtFieldEventName) // name
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_name"];
    }
    else if (textField == _txtFieldPlaceName)
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_place"];
    }
    else if (textField == _txtFieldAddress1) // address1
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_address_first"];
    }
    else if (textField == _txtFieldAddress2) // address2
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_address_second"];
    }
    else if (textField == _txtFieldaddCity)
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_city"];  //infuture add city
    }
    else if (textField == _txtFieldaddState) // state
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_state"];
    }
    else if (textField == _txtFieldaddPostalCode) // zip code
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_zip_code"];
    }
    else if (textField == _txtFieldEventPhoneNumber) // phone number
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_phone_number"];
    }
    else if (textField == _txtFieldEventWebsite) // website
    {
        [dictionaryCreateEvent setValue:@"" forKey:@"event_website"];
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == _txtFieldCity) // topic
    {
        [_txtFieldTopic becomeFirstResponder];
    }
    else if (textField == _txtFieldTopic)  // name
    {
        [_txtFieldEventName becomeFirstResponder];
    }
    else if (textField == _txtFieldEventName)
    {
        [_txtFieldPlaceName becomeFirstResponder];
    }
    else if (textField == _txtFieldPlaceName) // address1
    {
        [_txtFieldAddress1 becomeFirstResponder];
    }
    else if (textField == _txtFieldAddress1) // address2
    {
        [_txtFieldAddress2 becomeFirstResponder];
    }
    if (textField == _txtFieldAddress2) // city
    {
        [_txtFieldaddCity becomeFirstResponder];
    }
    else if (textField == _txtFieldaddCity) // state
    {
        [_txtFieldaddState becomeFirstResponder];
    }
    else if (textField == _txtFieldaddState) // zip code
    {
        [_txtFieldaddPostalCode becomeFirstResponder];
    }
    else if (textField == _txtFieldaddPostalCode) // phone number
    {
        [_txtFieldEventPhoneNumber becomeFirstResponder];
    }
    else if (textField == _txtFieldEventPhoneNumber) // website
    {
        [_txtFieldEventWebsite becomeFirstResponder];
    }
    
    else if (textField == _txtFieldEventWebsite) // website
    {
        [_txtFieldEventWebsite resignFirstResponder];
    }
   
    [textField resignFirstResponder];
    return NO;
}

//-(void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    [self removePickerView];
//
//    if (textField.tag == 74301) // topic
//    {
//        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    else if (textField.tag == 74302) // name
//    {
//        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    else if (textField.tag == 74303) //PLACE
//    {
//        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    else if (textField.tag == 74304) // address1
//    {
//       [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    else if (textField.tag == 74305) // address2
//    {
//       [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:5 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    if (textField.tag == 74306) // city
//    {
//        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    else if (textField.tag == 74307) // state
//    {
//        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    else if (textField.tag == 74308) // zip code
//    {
//        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    else if (textField.tag == 74309) // phone number
//    {
//        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    else if (textField.tag == 74310) // website
//    {
//        [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:8 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//
//
//}

-(void)updatePlaceText: (NSString*)searchText
{
    @try
    {
        
        NSString *trimmedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (trimmedString.length>0)
        {
            searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
            searchQuery.radius = 100.0;
           // shouldBeginEditing = YES;
           // searchQuery.location = self.currentLocation.coordinate;
            searchQuery.input = searchText;
            [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
                if (error) {
                   // SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
                    NSLog(@"Autocomplete error %@", [error localizedDescription]);
                } else {
                   // [arraySearch release];
                    self->arraySearch = [[NSArray alloc]init];
                    NSLog(@"places--- %@", places);
                     self->arraySearch = places;
                    NSLog(@"arraySearch--- %@", self->arraySearch);
                    [self->_tblAddressHeight setConstant:55];
                    self->_tblBottomAdrss.constant = 15;
                   
                    [self->_tblAddresSearch reloadData];
                }
            }];
            
           // [self->_tableViewSearch reloadData];
        }
        else{
            
            
            NSArray *newArray = [[NSArray alloc]init];
            arraySearch = newArray;
            [self->_tblAddresSearch reloadData];
            [self->_tblAddressHeight setConstant:0];
             self->_tblBottomAdrss.constant = 0;
            [_txtFieldPlaceName resignFirstResponder];
            
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void)textFieldDidChange:(UITextField *)textField
{
    NSString *text = _txtFieldPlaceName.text; //[textField.text stringByReplacingCharactersInRange:range withString:string];
    [self updatePlaceText:text];
      
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if (textField == _txtFieldCity || textField == _txtFieldaddCity) // city
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "] invertedSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        
        if (![string isEqualToString:filtered])
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength<=30)
            {
                [dictionaryCreateEvent setObject:text forKey:@"event_city"];
            }
            return (newLength >= 31) ? NO : YES;
        }
        else if (range.length == 1)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setObject:text forKey:@"event_city"];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else if (textField == _txtFieldTopic) // topic
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setValue:text forKey:@"event_topic"];
        }
        return (newLength >= 31) ? NO : YES;
    }
    else if (textField == _txtFieldEventName) // name
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "] invertedSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        
        if (![string isEqualToString:filtered] || [string isEqualToString:filtered])
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
           // NSUInteger newLength = [textField.text length] + [string length] - range.length;
           // if (newLength<=30)
           // {
                [dictionaryCreateEvent setObject:text forKey:@"event_name"];
          //  }
            return YES;
        }
        else if (range.length == 1)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setObject:text forKey:@"event_name"];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else if (textField == _txtFieldAddress1) // address1
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        if (newLength <= 30)
//        {
//
//        }
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [dictionaryCreateEvent setValue:text forKey:@"event_address_first"];
        return YES;
    }
    
    else if (textField == _txtFieldAddress2) // address2
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
     //   NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [dictionaryCreateEvent setValue:text forKey:@"event_address_second"];
        return YES;
    }
    
    else if (textField == _txtFieldaddState) // state
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        //   NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
        [dictionaryCreateEvent setValue:text forKey:@"event_state"];
        return YES;
    }
    
    else if (textField == _txtFieldaddPostalCode) // zip code
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 10)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setValue:text forKey:@"event_zip_code"];
        }
        return (newLength >= 11) ? NO : YES;
    }
    
    else if (textField == _txtFieldEventPhoneNumber) // phone number
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        
        NSString *_strPhoneNumber = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=15)
        {
            [dictionaryCreateEvent setValue:_strPhoneNumber forKey:@"event_phone_number"];
        }
        return (newLength >= 16) ? NO : YES;
    }
    
    else if (textField == _txtFieldEventWebsite) // website
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        //NSUInteger newLength = [textField.text length] + [string length] - range.length;
        //if (newLength <= 30)
       // {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryCreateEvent setValue:text forKey:@"event_website"];
       // }
        return YES;
    }

    return YES;
}

#pragma mark
#pragma maark ----------- Select Address ---------------
//-(void)selectEventAddress : (UIButton*)sender
//{
//    arraySearch = [[NSMutableArray alloc]init];
//
//
//    [self removePickerView];
//    [viewForGoogleSearchAPI removeFromSuperview];
//    viewForGoogleSearchAPI = nil;
//    viewForGoogleSearchAPI = [UIFunction createUIViews:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) bckgroundColor:[UIColor blackColor]];
//    viewForGoogleSearchAPI.alpha = 0.0;
//    [self.view addSubview:viewForGoogleSearchAPI];
//
//    UISearchBar *txtFieldEventAddress = [[UISearchBar alloc] initWithFrame:CGRectMake(20, 250, viewForGoogleSearchAPI.frame.size.width-40, sender.frame.size.height)];
//    txtFieldEventAddress.delegate = self;
//    txtFieldEventAddress.showsCancelButton = YES;
//    txtFieldEventAddress.tintColor = [UIColor blackColor];
//    txtFieldEventAddress.layer.cornerRadius = 5.0;
//    txtFieldEventAddress.clipsToBounds = YES;
//    txtFieldEventAddress.backgroundColor = [UIColor whiteColor];
//    //txtFieldEventAddress.barTintColor = [UIColor whiteColor];
//    //txtFieldEventAddress.searchBarStyle = UISearchBarStyleMinimal;
//    txtFieldEventAddress.userInteractionEnabled = YES;
//    txtFieldEventAddress.placeholder = @"Look up Places";
//    [viewForGoogleSearchAPI addSubview:txtFieldEventAddress];
//
//
//    [UIView animateWithDuration:0.3 animations:^{
//
//        viewForGoogleSearchAPI.alpha = 1.0;
//        txtFieldEventAddress.frame = CGRectMake(txtFieldEventAddress.frame.origin.x, 30, txtFieldEventAddress.frame.size.width, txtFieldEventAddress.frame.size.height);
//
//    } completion:^(BOOL finished) {
//
//        [txtFieldEventAddress becomeFirstResponder];
//
//        [tableViewSearch removeFromSuperview];
//        tableViewSearch = nil;
//        tableViewSearch = [self createTableView:CGRectMake(10, txtFieldEventAddress.frame.size.height+txtFieldEventAddress.frame.origin.y+10, viewForGoogleSearchAPI.frame.size.width-20, viewForGoogleSearchAPI.frame.size.height-txtFieldEventAddress.frame.size.height-txtFieldEventAddress.frame.origin.y-20) backgroundColor:[UIColor clearColor]];
//        [viewForGoogleSearchAPI addSubview:tableViewSearch];
//        tableViewSearch.tag = 67800;
//
//    }];
//
//}
//
//-(void)removeSearchView
//{
//    [UIView animateWithDuration:0.3 animations:^{
//        viewForGoogleSearchAPI.alpha = 0.0;
//    } completion:^(BOOL finished) {
//        [viewForGoogleSearchAPI removeFromSuperview];
//        viewForGoogleSearchAPI = nil;
//        [tableViewSearch removeFromSuperview];
//        tableViewSearch = nil;
//    }];
//}

-(void)openMapScreen {
    
    NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"place_id"]];
    if (([event_address_first length] > 0)) {
        
        MapkitViewController *controller = [[MapkitViewController alloc] initWithNibName:@"MapkitViewController" bundle:nil];
        //controller.isFromMap = YES;
        controller.event_place_id = event_address_first;
        [self.navigationController pushViewController:controller animated:YES];
        
        
    }
    else{
        
        MapkitViewController *controller = [[MapkitViewController alloc] initWithNibName:@"MapkitViewController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
        
    }
}

-(void)onTapDone:(UIBarButtonItem*)item{
    
}

-(void)onTapCancel:(UIBarButtonItem*)item{
    
}


#pragma mark
#pragma mark Add Attachment
-(void)addAttachmentButtonPressed
{
    NSLog(@"addAttachmentButtonPressed");
    
    [self.view endEditing:YES];
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    BOOL isAddImage = false;
    
    for (int counter = 0; counter < arrayImages.count; counter++)
    {
        if ([[arrayImages objectAtIndex:counter]length] == 0)
        {
            indexOfImage = counter;
            isAddImage = true;
            break;
        }
    }
    
    if (isAddImage == true)
    {
        [self addEventImages];
    }
    else
    {
        [self showAlertWithMessage:@"You can add maximum 5 attachments."];
    }
}
     
#pragma mark
#pragma mark Buttons Actions
-(void)readMoreButtonPressed
{
    NSLog(@"read More Button Pressed");
}
-(void)messagesButtonPressed
{
    NSLog(@"messages Button Pressed");
    if(![MFMessageComposeViewController canSendText])
    {
        [self showAlertWithMessage:@"Your device doesn't support SMS!"];
        return;
    }
    
        NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_date"]];
         if ([event_date isEqualToString:@""] || [event_date containsString:@"(null)"] || event_date == nil)
         {
             event_date = @"";
         }
         else{
            
             event_date = [NSString stringWithFormat:@"Date - %@",event_date];
         }
         
         
         NSString *event_start_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_start_time"]];
         NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_end_time"]];
         NSString *time = [NSString stringWithFormat:@"%@ - %@",event_start_time,event_end_time];
         if ([time isEqualToString:@" - "])
         {
             time = @"";
         }
         else{
             time = [NSString stringWithFormat:@"Time - %@",time];
             
         }
         
         
         NSString *event_name = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_name"]];
         if ([event_name isEqualToString:@""] || [event_name containsString:@"(null)"] || event_name == nil)
         {
             event_name = @"";
         }
         else{
             event_name = [NSString stringWithFormat:@"Name - %@",event_name];
         }
         
         NSString *event_city = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_city"]];
         NSString *event_place = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_place"]];
         NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_first"]];
         NSString *event_address_second = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_address_second"]];
         NSString *event_state = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_state"]];
         NSString *event_zip_code = [NSString stringWithFormat:@"%@",[dictionaryCreateEvent valueForKey:@"event_zip_code"]];
      
          NSString *address;
         if([event_city isEqualToString:@""] && [event_place isEqualToString:@""] && [event_address_first isEqualToString:@""] && [event_address_second isEqualToString:@""] && [event_state isEqualToString:@""] && [event_zip_code isEqualToString:@""])
         {
             address = @"";
         }
         else{
              address = [NSString stringWithFormat:@"Address -%@,%@,%@,%@,%@,%@",event_place,event_address_first,event_address_second,event_city,event_state,event_zip_code];
         }
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setSubject:@"Re: AltCal Private Invitation for You"];
    if([event_date isEqualToString:@""] && [time isEqualToString:@""] && [event_name isEqualToString:@""] && [address isEqualToString:@""]){
        [messageController setBody:nil];
    }
    else{
        [messageController setBody: [NSString stringWithFormat:@"Event Details: %@,%@,%@,%@",event_date,time,event_name,address]];
    }
    
    [self presentViewController:messageController animated:YES completion:nil];
    
}
-(void)ticketsButtonPressed
{
    NSLog(@"tickets Button Pressed");
}
   
#pragma Message Composer Delegates
 - (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [self showAlertWithMessage:@"Failed to send SMS!"];
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark
#pragma mark Document Menu Delegates
-(void)documentMenu:(UIDocumentMenuViewController *)documentMenu didPickDocumentPicker:(UIDocumentPickerViewController *)documentPicker
{
    documentPicker.delegate = self;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url
{
    NSLog(@"url is %@",url);
    NSFileCoordinator *coordinator = [[NSFileCoordinator alloc] initWithFilePresenter:nil];
    NSError *error = nil;
    [coordinator coordinateReadingItemAtURL:url options:0 error:&error byAccessor:^(NSURL *newURL) {
        NSData *data = [NSData dataWithContentsOfURL:newURL];
        
        if (data.length == 0)
        {
            [self showAlertWithMessage:@"This document can not be uploaded."];
            return;
        }
        
        NSString *pathExtension = [url pathExtension];
        NSDate *todayDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [NSLocale localeWithLocaleIdentifier:@"en_US"];
        dateFormatter.dateFormat = @"YYYY_MM_dd_hh_mm_ss.SSS";
        NSString *fileName = [NSString stringWithFormat:@"%@.%@",[dateFormatter stringFromDate:todayDate],pathExtension];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *pathOfDocument = [documentsDirectory stringByAppendingPathComponent:fileName];
        [data writeToFile:pathOfDocument atomically:YES];

        
        NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
        arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
        
        if (arrayImages.count < 6)
        {
            [arrayImages replaceObjectAtIndex:indexOfImage withObject:fileName];
        }
        else
        {
            [self showAlertWithMessage:@"You can add maximum 5 attachments."];
        }
        
        [dictionaryCreateEvent setValue:arrayImages forKey:@"event_images"];
        NSLog(@"_____ %@",dictionaryCreateEvent);
        [self.collectionViewForImages reloadData];
    }];
    
    if (error)
    {
        // Do something else
    }
}

-(NSArray*)UTIs
{
    return @[ @"public.content"];
}

#pragma mark
#pragma mark Open Web View For Attachments
-(void)loadAttachmentInWebView : (NSURL*)attachment
{
    [viewForWebView removeFromSuperview];
    viewForWebView = nil;
    viewForWebView = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height) bckgroundColor:[UIColor blackColor]];
    [self.view addSubview:viewForWebView];
    
    [UIView animateWithDuration:0.3 animations:^{
        self->viewForWebView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
        UILabel *label = [UIFunction createLable:CGRectMake(0, 20, self->viewForWebView.frame.size.width, 44) bckgroundColor:[UIColor clearColor] title:@"Attachment" font:[UIFont fontWithName:fontRegular size:14.0] titleColor:[UIColor whiteColor]];
        label.textAlignment = NSTextAlignmentCenter;
        [self->viewForWebView addSubview:label];
        
//        UIButton *btnCancel = [UIFunction createButton:CGRectMake(0, 20, [UIImage imageNamed:@"back_white"].size.width, [UIImage imageNamed:@"back_white"].size.height) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"back_white"] title:nil font:nil titleColor:nil];
//        [btnCancel addTarget:self action:@selector(removeWebView) forControlEvents:UIControlEventTouchUpInside];
//        [viewForWebView addSubview:btnCancel];
        
        UIButton *btnCancel = [UIButton buttonWithType: UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(0, 20, 44, 44);
        btnCancel.backgroundColor = [UIColor clearColor];
        [btnCancel setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(removeWebView) forControlEvents:UIControlEventTouchUpInside];
        [self->viewForWebView addSubview:btnCancel];

        
        
        WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width , self.view.frame.size.height-64)];
        NSURLRequest *request = [NSURLRequest requestWithURL:attachment];
        [webView loadRequest:request];
        webView.navigationDelegate = self;
        //webView.scalesPageToFit = true;
        [self->viewForWebView addSubview:webView];
    }];
}

-(void)removeWebView
{
    [UIView animateWithDuration:0.3 animations:^{
        self->viewForWebView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self->viewForWebView removeFromSuperview];
        self->viewForWebView = nil;
    }];
}


-(void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:true];
}


#pragma mark
#pragma mark func_SelectCountryCode
-(void)func_SelectCountryCode
{
    NSLog(@"func_SelectCountryCode");
    
    [self removeAllPickers];
    [self.view endEditing:YES];
//    [tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:6 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    viewForCountryCodePicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor colorWithRed:214.0/255 green:240.0/255 blue:252.0/255 alpha:1.0]];
    [self.view addSubview:viewForCountryCodePicker];
    
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(doneButtonPressedInPickerView:)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor blackColor];
    [viewForCountryCodePicker addSubview:toolBar];
    
    
    
    
    UIPickerView *pickerCountryCode =  [[UIPickerView alloc]init];
    pickerCountryCode.frame = CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, viewForCountryCodePicker.frame.size.width, 216.0);
    pickerCountryCode.delegate = self;
    pickerCountryCode.dataSource = self;
    pickerCountryCode.showsSelectionIndicator = YES;
    pickerCountryCode.backgroundColor = [UIColor clearColor];
    pickerCountryCode.tag = 480040;
    [viewForCountryCodePicker addSubview:pickerCountryCode];
    
    
    [UIView animateWithDuration:.2f animations:^{
        
        [self->viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
        
    } completion:^(BOOL finished) {
        
        [self->dictionaryCreateEvent setObject:[self->arrayAllCodes objectAtIndex:0] forKey:@"user_country_code"];
      //  [tableViewCreateEvent reloadData];
    }];
}

#pragma mark
#pragma mark Remove All Picker Views
-(void)doneButtonPressedInPickerView :(id)sender
{
    [self. view endEditing:YES];
    
    @try
    {
        //[tableViewCreateEvent scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [self->viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self removeAllPickers];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void)removeAllPickers
{
    [viewForCountryCodePicker removeFromSuperview];
    viewForCountryCodePicker = nil;
}

#pragma mark
#pragma mark Picker View DataSource And Delegates Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        return arrayAllCodes.count;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        return 1;
    }
    return 0;
}

- (UIView*)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (thePickerView.tag == 480040) // select user country and c ountry code
    {
        UIView* viewForPicker = [[UIView alloc]init];
        viewForPicker.frame = CGRectMake(0, 0, self.view.frame.size.width, 45);
        viewForPicker.backgroundColor = [UIColor clearColor];
        
        
        UILabel *countryName = [[UILabel alloc]init];
        countryName.frame = CGRectMake(10, 5, 240, 40);
        countryName.textAlignment = NSTextAlignmentLeft;
        countryName.backgroundColor = [UIColor clearColor];
        countryName.text = [arrayAllCountries objectAtIndex:row];
        countryName.font = [UIFont fontWithName:fontRegular size:16];
        [viewForPicker addSubview:countryName];
        
        UILabel *countryCode = [[UILabel alloc]init];
        countryCode.frame = CGRectMake(self.view.frame.size.width-90, 5, 70, 40);
        countryCode.textAlignment = NSTextAlignmentRight;
        countryCode.font = [UIFont fontWithName:fontRegular size:16];
        countryCode.backgroundColor = [UIColor clearColor];
        countryCode.text = [arrayAllCodes objectAtIndex:row];
        [viewForPicker addSubview:countryCode];
        
        return viewForPicker;
    }
    
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 480040) // select user country and country code
    {
        [dictionaryCreateEvent setObject:[arrayAllCodes objectAtIndex:row] forKey:@"user_country_code"];
      //  [tableViewCreateEvent reloadData];
        _lblCountryCode.text = [arrayAllCodes objectAtIndex:row];
    }
}

#pragma mark
#pragma mark ***************** Multi Options View Delegate *****************
-(void)showMultipleOptionsToAddRemoveAttachment
{
    [self.view endEditing:YES];
    
    [viewMultipleOptions removeFromSuperview];
    viewMultipleOptions = nil;
    viewMultipleOptions = [[MultiOptionView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    viewMultipleOptions.MultiOptionViewDelegate=self;
    [self.view addSubview:viewMultipleOptions];

}
-(void) removeMultiOptionViewFunction
{
    [UIView animateWithDuration:0.3f animations:^{
        self->viewMultipleOptions.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self->viewMultipleOptions removeFromSuperview];
        self->viewMultipleOptions = nil;
    }];
}
     
- (void)choosePhotoFromCameraButtonPressedFromMultiOptionView
{
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    BOOL isImageAdded = false;
    
    for (int counter = 0; counter < arrayImages.count; counter++)
    {
        NSString *imageName = [arrayImages objectAtIndex:counter];
        if (imageName.length == 0)
        {
            indexOfImage = counter;
            isImageAdded = true;
            break;
        }
    }
    
    if (isImageAdded == true)
    {
        isImageAdded = false;
        [self choosePhotoFromCameraButtonPressed];
    }
}
- (void)choosePhotoFromGalleryButtonPressedFromMultiOptionView
{
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    BOOL isImageAdded = false;
    
    for (int counter = 0; counter < arrayImages.count; counter++)
    {
        NSString *imageName = [arrayImages objectAtIndex:counter];
        if (imageName.length == 0)
        {
            indexOfImage = counter;
            isImageAdded = true;
            break;
        }
    }
    
    if (isImageAdded == true)
    {
        isImageAdded = false;
        [self choosePhotoFromGalleryButtonPressed];
    }
}
- (void)uploadDocumentButtonPressedFromMultiOptionView
{
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    BOOL isImageAdded = false;
    
    for (int counter = 0; counter < arrayImages.count; counter++)
    {
        NSString *imageName = [arrayImages objectAtIndex:counter];
        if (imageName.length == 0)
        {
            indexOfImage = counter;
            isImageAdded = true;
            break;
        }
    }
    
    if (isImageAdded == true)
    {
        isImageAdded = false;
        [self uploadDocumentButtonPressed];
    }
}

- (void)viewPhotoButtonPressedFromMultiOptionView
{
    [self viewPhotoButtonPressed];
}

- (void)removePhotoButtonPressedFromMultiOptionView
{
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryCreateEvent valueForKey:@"event_images"]mutableCopy];
    [arrayImages replaceObjectAtIndex:0 withObject:@""];
    [dictionaryCreateEvent setObject:arrayImages forKey:@"event_images"];
    [self.collectionViewForImages reloadData];
}

#pragma mark
#pragma mark Compare Two Dates
- (BOOL)isEndDateIsSmallerThanCurrent:(NSString *)dateOfBirth
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *enddate = [dateFormatter dateFromString:dateOfBirth];
    NSDate *currentdate = [self GetLocalDate];

    
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return NO;
    else
        return YES;
}

- (NSDate *) GetLocalDate
{
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    theCalendar.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    dayComponent = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSDate *localDate = [theCalendar dateFromComponents:dayComponent];
    
    return localDate;
}


-(BOOL)isEndDateIsSmallerThanEventStartDate : (NSString*)eventStartDate : (NSString*)eventEndDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy hh:mm a"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDate *startDate = [dateFormatter dateFromString:eventStartDate];
    NSDate *endDate = [dateFormatter dateFromString:eventEndDate];

    NSTimeInterval distanceBetweenDates = [endDate timeIntervalSinceDate:startDate];
    double secondsInMinute = 60;
    NSInteger secondsBetweenDates = distanceBetweenDates / secondsInMinute;
    
    if (secondsBetweenDates == 0)
        return YES;
    else if (secondsBetweenDates < 0)
        return NO;
    else
        return YES;

}

- (void)showShareExtensionAlert
    {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Put Your Ideas in One Place\n\nSave them all to AltCal\n\nSimply Turn on your Photo Sharing to Insert Screenshots and Images" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [self performSelector:@selector(fetchUserLocation) withObject:nil afterDelay:0.1];
                                       [self showShareExtensionSlideShow];
                                       
                                   }];
        
        UIAlertAction *cancelAction = [UIAlertAction
                                       actionWithTitle:@"Not Now"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                            [self performSelector:@selector(fetchUserLocation) withObject:nil afterDelay:0.1];
                                       }];
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
- (void)showShareExtensionSlideShow
    {
        
        NSMutableArray *photos = [[NSMutableArray alloc] init];
        IDMPhoto *photo = [[IDMPhoto alloc] initWithImage:[UIImage imageNamed:@"SEInstruction"]];
        IDMPhoto *photo2 = [[IDMPhoto alloc] initWithImage:[UIImage imageNamed:@"SEInstruction2"]];
        IDMPhoto *photo3 = [[IDMPhoto alloc] initWithImage:[UIImage imageNamed:@"SEInstruction3"]];
        IDMPhoto *photo4 = [[IDMPhoto alloc] initWithImage:[UIImage imageNamed:@"SEInstruction4"]];
        IDMPhoto *photo5 = [[IDMPhoto alloc] initWithImage:[UIImage imageNamed:@"SEInstruction5"]];
        IDMPhoto *photo6 = [[IDMPhoto alloc] initWithImage:[UIImage imageNamed:@"SEInstruction6"]];
        IDMPhoto *photo7 = [[IDMPhoto alloc] initWithImage:[UIImage imageNamed:@"SEInstruction7"]];
        [photos addObject:photo];
        [photos addObject:photo2];
        [photos addObject:photo3];
        [photos addObject:photo4];
        [photos addObject:photo5];
        [photos addObject:photo6];
        [photos addObject:photo7];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:self.view];
        //    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake([UIScreen mainScreen].bounds.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
        //    [browser.view addSubview:imgViewLogo];
        browser.displayActionButton = NO;
        browser.delegate = self;
        [browser setInitialPageIndex:0];
        browser.displayArrowButton = NO;
        browser.displayCounterLabel = YES;
        browser.usePopAnimation = NO;
        browser.disableVerticalSwipe = YES;
        [self presentViewController:browser animated:YES completion:nil];
    }
    
- (void)photoBrowser:(IDMPhotoBrowser *)photoBrowser didDismissAtPageIndex:(NSUInteger)index
{
}
    
- (void)checkIfImageStored
{
        NSString *current_date = [UIFunction getCurrentDateInString];
        NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
        NSArray *arrayMyEvents = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:true];
        if (arrayMyEvents != nil)
        {
            if (arrayMyEvents.count > 0)
            {
                for (NSDictionary *dictionaryEvent in arrayMyEvents)
                {
                    if ([[dictionaryEvent valueForKey:@"event_images"]count] > 0)
                    {
                        NSString *imageName = [[dictionaryEvent valueForKey:@"event_images"]objectAtIndex:0];
                        if (imageName.length > 0)
                        {
                            return;
                        }
                    }
                }
            }
        }
        [self showShareExtensionAlert];
}

-(IBAction)clickOnEditBtn:(id)sender
{
    if ([sender isSelected]) {
        //del from edit table
        
        [sender setSelected: NO];
        [self->dictionaryCreateEvent setValue:@"" forKey:@"event_save_later"];
        
        
    } else {
        //insert intp edit table
        [sender setSelected: YES];
        [self->dictionaryCreateEvent setValue:@"1" forKey:@"event_save_later"];
    }
}




@end



//let dict = ["0":"time_select","1":"time_select"];
