//
//  invitePopUpView.h
//  Event App
//
//  Created by Reinforce on 24/10/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface invitePopUpView : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblTo;
@property (nonatomic, weak) IBOutlet UILabel *lblMsg;
@property (nonatomic, weak) IBOutlet UILabel *lblInclude;
@property (nonatomic, weak) IBOutlet UILabel *lblsubject;
@property (nonatomic, weak) IBOutlet UIButton *btnSave;
@property (nonatomic, weak) IBOutlet UITextField *txtEmail;
@property (nonatomic, weak) IBOutlet UITextView *txtMessage;
@property (nonatomic, weak) IBOutlet UIView *viewForCountryCodePicker;
@property (nonatomic, weak) IBOutlet UIToolbar *toolBar;
@property (nonatomic, weak) IBOutlet UITableView *pickerCountryCode;
@property (nonatomic, weak) NSMutableDictionary *dictPassDataFrom;

@property (nonatomic, weak) IBOutlet UILabel *lblName;
@property (nonatomic, weak) IBOutlet UILabel *lblDateTime;
@property (nonatomic, weak) IBOutlet UILabel *lblAddress;
@property (nonatomic, weak) IBOutlet UILabel *lblwebsite;

@end

NS_ASSUME_NONNULL_END
