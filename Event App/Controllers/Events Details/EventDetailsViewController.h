//
//  EventDetailsViewController.h
//  Event App
//
//  Created by   on 21/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailsViewController : UIViewController

@property(nonatomic,retain)NSString *event_id;
typedef enum {blank, eventName, eventDateTime, eventPlace,eventAddress, eventAddress2, zipcode, phoneNumber, website, notesShare, notesNotShare,btn} cellTypes;
@end
