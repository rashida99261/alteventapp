//
//  EventDetailsViewController.m
//  Event App
//
//  Created by   on 21/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "TodaysEventsViewController.h"
#import "NotificationsViewController.h"
#import <IDMPhotoBrowser.h>
#import <SafariServices/SafariServices.h>
#import "LandingViewController.h"
#import "RemovePhotoView.h"
#import "invitePopUpView.h"
#import <WebKit/WebKit.h>
#import "MapkitViewController.h"


@interface EventDetailsViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, MFMessageComposeViewControllerDelegate, WKNavigationDelegate , removePhotoMethod>

@property (nonatomic, strong) UICollectionView *collectionViewForImages;

@end

@implementation EventDetailsViewController
{
    UILabel *lblEventsCount;
    UILabel *lblNotificationsCount;
    NSMutableDictionary *dictionaryEvent;
    UITableView *tableEventDetail;
    float widthOfCollectionView;
    NSInteger indexOfImage;
    UIView *viewForWebView;
    UIImageView *btnAddAttachment;
    UILabel *lblCity;
    UILabel *lblTopic;
    NSMutableArray *dataToDisplay;
    NSNumber *locationIconCellType;
    RemovePhotoView *viewFullSizeOrRemovePhoto;
}

@synthesize event_id;

#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self setNotificationAndEventsBadge];
    [self getSelectedEventDetail];
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = true;
    }
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"     ");
    self.view.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    dictionaryEvent = [[NSMutableDictionary alloc]init];
    widthOfCollectionView = 0.0;
    
    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake([UIScreen mainScreen].bounds.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [self.view addSubview:imgViewLogo];
    
    UIButton *btnBack = [UIButton buttonWithType: UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44, 44);
    btnBack.backgroundColor = [UIColor clearColor];
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    

    UIButton *btnTodaysEvents = [UIButton buttonWithType: UIButtonTypeCustom];
    btnTodaysEvents.frame = CGRectMake(self.view.frame.size.width-30-[UIImage imageNamed:@"todays_events"].size.width, 20, 44, 44);
    btnTodaysEvents.backgroundColor = [UIColor clearColor];
    [btnTodaysEvents setContentMode:UIViewContentModeCenter];
    [btnTodaysEvents setImage:[UIImage imageNamed:@"todays_events"] forState:UIControlStateNormal];
    [btnTodaysEvents addTarget:self action:@selector(todaysEventsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnTodaysEvents];
    
    
//    lblEventsCount = [UIFunction createLable:CGRectMake(btnTodaysEvents.frame.size.width-20, btnTodaysEvents.frame.size.height-20, 20, 20) bckgroundColor:[UIColor clearColor] title:nil font:[UIFont fontWithName:fontRegular size:10.0] titleColor:[UIColor whiteColor]];
//    lblEventsCount.textAlignment = NSTextAlignmentCenter;
//    lblEventsCount.layer.cornerRadius = lblEventsCount.frame.size.height/2.0;
//    lblEventsCount.layer.borderColor = [UIColor whiteColor].CGColor;
//    lblEventsCount.layer.borderWidth = 2.0;
//    lblEventsCount.layer.masksToBounds = true;
//    lblEventsCount.userInteractionEnabled = true;
//    [btnTodaysEvents addSubview:lblEventsCount];
    
    UIButton *btnNotifications = [UIButton buttonWithType: UIButtonTypeCustom];
    btnNotifications.frame = CGRectMake(btnTodaysEvents.frame.origin.x-44, btnBack.frame.origin.y, 44, 44);
    btnNotifications.backgroundColor = [UIColor clearColor];
    [btnNotifications setContentMode:UIViewContentModeCenter];
    [btnNotifications setImage:[UIImage imageNamed:@"notifications"] forState:UIControlStateNormal];
    [btnNotifications addTarget:self action:@selector(notificationsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnNotifications];
    
    
    lblNotificationsCount = [UIFunction createLable:CGRectMake(btnNotifications.frame.size.width-20, btnNotifications.frame.size.height-20, 20, 20) bckgroundColor:[UIColor clearColor] title:nil font:[UIFont fontWithName:fontRegular size:10.0] titleColor:[UIColor whiteColor]];
    lblNotificationsCount.textAlignment = NSTextAlignmentCenter;
    lblNotificationsCount.layer.cornerRadius = lblNotificationsCount.frame.size.height/2.0;
    lblNotificationsCount.layer.borderColor = [UIColor whiteColor].CGColor;
    lblNotificationsCount.layer.borderWidth = 2.0;
    lblNotificationsCount.layer.masksToBounds = true;
    lblNotificationsCount.userInteractionEnabled = true;
    [btnNotifications addSubview:lblNotificationsCount];
    
    UIView *viewAddEvent = [UIFunction createUIViews:CGRectMake(0, 70, self.view.frame.size.width, 30) bckgroundColor:[UIColor clearColor]];
    [self.view addSubview:viewAddEvent];
    
    UILabel *lblAddEvent = [UIFunction createLable:CGRectMake(viewAddEvent.frame.size.width/2.0-75.0, 5, 150, viewAddEvent.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:@"YOUR EVENT" font:[UIFont fontWithName:fontLight size:20.0] titleColor:[UIColor whiteColor]];
    lblAddEvent.textAlignment = NSTextAlignmentCenter;
    [viewAddEvent addSubview:lblAddEvent];
    
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-15, viewAddEvent.frame.size.height/2.0-5.0, 10.0, 10.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-35, viewAddEvent.frame.size.height/2.0-3.5, 7.0, 7.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x-50, viewAddEvent.frame.size.height/2.0-2.5, 5.0, 5.0) atView:viewAddEvent];
    
    
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width, viewAddEvent.frame.size.height/2.0-5.0, 10.0, 10.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width+20, viewAddEvent.frame.size.height/2.0-3.5, 7.0, 7.0) atView:viewAddEvent];
    [self createCircleViewWithFrame:CGRectMake(lblAddEvent.frame.origin.x+lblAddEvent.frame.size.width+40, viewAddEvent.frame.size.height/2.0-2.5, 5.0, 5.0) atView:viewAddEvent];
    
    
    UIButton *btnEdit = [UIFunction createButton:CGRectMake(self.view.frame.size.width/2.0-35.0, self.view.frame.size.height-30, 70.0, 30) bckgroundColor:[UIColor clearColor] image:nil title:@"EDIT" font:[UIFont fontWithName:fontRegular size:14.0] titleColor:[UIColor whiteColor]];
    [btnEdit addTarget:self action:@selector(editEventPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnEdit];
    
    
    // collection view
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    self.collectionViewForImages = [[UICollectionView alloc] initWithFrame:CGRectMake(10, btnEdit.frame.origin.y-100, self.view.frame.size.width-20, 100) collectionViewLayout:layout];
    [self.collectionViewForImages setDataSource:self];
    [self.collectionViewForImages setDelegate:self];
    [self.collectionViewForImages registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [self.collectionViewForImages setBackgroundColor:[UIColor clearColor]];
    self.collectionViewForImages.scrollEnabled = YES;
    self.collectionViewForImages.tag = 69802140;
    self.collectionViewForImages.pagingEnabled = YES;
    self.collectionViewForImages.showsHorizontalScrollIndicator = NO;
    self.collectionViewForImages.bounces = NO;
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [self.collectionViewForImages setCollectionViewLayout:layout];
    [self.view addSubview:self.collectionViewForImages];
    widthOfCollectionView = self.collectionViewForImages.frame.size.width; // get widh of collection view
    
    
    UIView *viewCreateEventBackGround = [UIFunction createUIViews:CGRectMake(self.collectionViewForImages.frame.origin.x, viewAddEvent.frame.size.height+viewAddEvent.frame.origin.y+60, self.collectionViewForImages.frame.size.width, self.collectionViewForImages.frame.origin.y-viewAddEvent.frame.size.height-viewAddEvent.frame.origin.y-60) bckgroundColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
    viewCreateEventBackGround.layer.cornerRadius = 5.0;
    [self.view addSubview:viewCreateEventBackGround];


    
    // ****** Add Attachment ********
    btnAddAttachment = [UIFunction createUIImageView:CGRectMake(viewCreateEventBackGround.frame.size.width/2.0-35.0, viewCreateEventBackGround.frame.origin.y-45.0, 90.0, 90.0) backgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0] image:nil isLogo:NO];
    btnAddAttachment.layer.cornerRadius = btnAddAttachment.frame.size.height/2.0;
    btnAddAttachment.clipsToBounds = true;
    btnAddAttachment.contentMode =  UIViewContentModeScaleAspectFill;
    [self.view addSubview:btnAddAttachment];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(addAttachmentTapped)];
    [btnAddAttachment setUserInteractionEnabled:YES];
    [btnAddAttachment addGestureRecognizer:singleFingerTap];
    
    [self createCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x-4, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0), 4.0, 4.0 ) atView:self.view];
    [self createCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x-2, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)-8, 5.0, 5.0 ) atView:self.view];
    [self createCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x+1, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)-16, 5.5, 5.5 ) atView:self.view];
    
    
    [self createBlackCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x+btnAddAttachment.frame.size.width, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)+25, 4.0, 4.0 ) atView:self.view];
    [self createBlackCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x+btnAddAttachment.frame.size.width-2, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)+33, 5.0, 5.0 ) atView:self.view];
    [self createBlackCircleViewWithFrame:CGRectMake(btnAddAttachment.frame.origin.x+btnAddAttachment.frame.size.width-7, btnAddAttachment.frame.size.height+(btnAddAttachment.frame.origin.y/2.0)+41, 5.5, 5.5 ) atView:self.view];
    

    // ****** Event City and Topic ********
    lblCity = [UIFunction createLable:CGRectMake(10, 5, 80, 30) bckgroundColor:[UIColor clearColor] title:nil font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
    lblCity.textAlignment = NSTextAlignmentLeft;
    [viewCreateEventBackGround addSubview:lblCity];

    
    lblTopic = [UIFunction createLable:CGRectMake(viewCreateEventBackGround.frame.size.width-90, 5, 80, 30) bckgroundColor:[UIColor clearColor] title:nil font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
    lblTopic.textAlignment = NSTextAlignmentRight;
    [viewCreateEventBackGround addSubview:lblTopic];
    
    [tableEventDetail removeFromSuperview];
    tableEventDetail = nil;
    tableEventDetail = [self createTableView:CGRectMake(5, lblTopic.frame.size.height+lblTopic.frame.origin.y+10, viewCreateEventBackGround.frame.size.width-10, viewCreateEventBackGround.frame.size.height-lblTopic.frame.size.height-lblTopic.frame.origin.y-10) backgroundColor:[UIColor clearColor]];
    [viewCreateEventBackGround addSubview:tableEventDetail];
    tableEventDetail.tag = 457548;
    locationIconCellType = 0;
}

#pragma mark
#pragma mark dismissViewController
-(void)backButtonPressed
{
    [self.view endEditing:true];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark
#pragma mark Create Table
-(UITableView*) createTableView : (CGRect)frame backgroundColor:(UIColor*)backgroundColor
{
    UITableView *_tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    _tableView.dataSource=self;
    _tableView.delegate = self;
    _tableView.backgroundColor=backgroundColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.alwaysBounceVertical = NO;
    [_tableView setAllowsSelection:YES];
    return _tableView;
}


#pragma mark
#pragma mark Alert Function
- (void) showAlertWithMessage : (NSString*)message
{
    [self.view makeToast:message duration:2.0 position:CSToastPositionBottom];
}


#pragma mark
#pragma mark Header Buttons Function
-(void)todaysEventsButtonPressed
{
    NSLog(@"Todays Events Button Pressed");
    
    [self.view endEditing:true];
    
    TodaysEventsViewController *todaysEvents = [[TodaysEventsViewController alloc]init];
    todaysEvents.isTodayEvent = true;
    [self.navigationController pushViewController:todaysEvents animated:YES];
}
-(void)notificationsButtonPressed
{
    NSLog(@"Notifications Button Pressed");
    
    [self.view endEditing:true];

    NotificationsViewController *notifications = [[NotificationsViewController alloc]init];
    [self.navigationController pushViewController:notifications animated:YES];
}

#pragma mark
#pragma mark Set Badge Count
-(void)setNotificationAndEventsBadge
{
    @try
    {
        NSString *current_date = [UIFunction getCurrentDateInString];
        NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
        NSArray *array = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:false];
        lblEventsCount.text = [NSString stringWithFormat:@"%zd",array.count];
        lblNotificationsCount.text = @"0";
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void)createCircleViewWithFrame : (CGRect)frame atView:(UIView*)baseView
{
    UIView *view = [UIFunction createUIViews:frame bckgroundColor:[UIColor whiteColor]];
    view.layer.cornerRadius = view.frame.size.height/2.0;
    view.layer.masksToBounds = true;
    view.clipsToBounds = true;
    [baseView addSubview:view];
}

-(void)createBlackCircleViewWithFrame : (CGRect)frame atView:(UIView*)baseView
{
    UIView *view = [UIFunction createUIViews:frame bckgroundColor:[UIColor blackColor]];
    view.layer.cornerRadius = view.frame.size.height/2.0;
    view.layer.masksToBounds = true;
    view.clipsToBounds = true;
    [baseView addSubview:view];
}


#pragma mark
#pragma mark Get Selected Event Detail
-(void)getSelectedEventDetail
{
    dictionaryEvent = [[[DataManager getSharedInstance]selectEventWithId:event_id]mutableCopy];
    NSLog(@"____ %@",dictionaryEvent);
    
    NSString *event_city = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_city"]];
    if (event_city.length == 0)
    {
        event_city = @"";
    }
    
    NSString *event_topic = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_topic"]];
    if (event_topic.length == 0)
    {
        event_topic = @"";
    }
    
   // lblCity.text = event_city; 
    lblTopic.text = event_topic;
    
    if ([[dictionaryEvent valueForKey:@"event_images"]count] > 0)
    {
        NSString *imageName = [[dictionaryEvent valueForKey:@"event_images"]objectAtIndex:0];
        if (imageName.length > 0)
        {
            NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
            UIImageView *imgLogo = [btnAddAttachment viewWithTag:555];
            if (fileExists == true)
            {
                NSString *pathExtension = [filePath pathExtension];
                if (pathExtension .length > 0)
                {
                    [btnAddAttachment setUserInteractionEnabled:YES];
                    if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                    {
                        if (imgLogo != nil) {
                            
                            [imgLogo removeFromSuperview];
                        }
                        
                        btnAddAttachment.image = [UIImage imageWithContentsOfFile:filePath];
                    }
                    else
                    {
                        if (imgLogo != nil) {
                            
                            [imgLogo removeFromSuperview];
                        }
                        
                        btnAddAttachment.image = [UIImage imageNamed:@"attachment"];
                    }
                }
            }
        }
        else
        {
            btnAddAttachment.image = nil;
            btnAddAttachment.backgroundColor = [UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0];
            UIImageView *imgLogo = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 0, btnAddAttachment.frame.size.width, btnAddAttachment.frame.size.height)];
            imgLogo.contentMode = UIViewContentModeScaleAspectFit;
            imgLogo.image = [UIImage imageNamed:@"event_icon"];
            imgLogo.tag = 555;
            [btnAddAttachment addSubview:imgLogo];
            [btnAddAttachment setUserInteractionEnabled:NO];
        }
    }
    
    [self setUpDataToDisplay];
    [tableEventDetail reloadData];
    [_collectionViewForImages reloadData];
}



#pragma mark
#pragma mark UICollectionView delegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    if (collectionView.tag == 69802140)
    {
        return 1;
    }
    return 0;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView.tag == 69802140)
    {
        return 5;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 69802140)
    {
        return CGSizeMake(widthOfCollectionView/5.0, widthOfCollectionView/5.0);
    }
    return CGSizeMake(0, 0);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView.tag == 69802140)
    {
        UICollectionViewCell *cell = nil;
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
        cell.backgroundColor=[UIColor clearColor];
        
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
        
        @try
        {
            
            UICollectionViewLayoutAttributes * theAttributes = [collectionView layoutAttributesForItemAtIndexPath:indexPath];
            CGRect frame = [collectionView convertRect:theAttributes.frame toView:[collectionView superview]];
            
            
            if (indexPath.row == 0)
            {
                float width = frame.size.width-35.0;
                
                UIView *view = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
                [cell.contentView addSubview:view];
                
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(view.frame.size.width-width-5, 17.5, width , frame.size.height-35.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[[dictionaryEvent valueForKey:@"event_images"]objectAtIndex:1]length]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryEvent valueForKey:@"event_images"] objectAtIndex:1]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 1)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(12.5, 12.5, frame.size.width-25.0, frame.size.height-25.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[[dictionaryEvent valueForKey:@"event_images"]objectAtIndex:2]length]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryEvent valueForKey:@"event_images"] objectAtIndex:2]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 2)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(5.0, 5.0, frame.size.width-10.0, frame.size.height-10.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[[dictionaryEvent valueForKey:@"event_images"]objectAtIndex:3]length]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryEvent valueForKey:@"event_images"] objectAtIndex:3]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 3)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(12.5, 12.5, frame.size.width-25.0, frame.size.height-25.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                if ([[[dictionaryEvent valueForKey:@"event_images"]objectAtIndex:4]length]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryEvent valueForKey:@"event_images"] objectAtIndex:4]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }
            }
            
            else if (indexPath.row == 4)
            {
                UIImageView *attachmentImage = [UIFunction createUIImageView:CGRectMake(5, 17.5, frame.size.width-35.0, frame.size.height-35.0) backgroundColor:[UIColor clearColor] image:nil isLogo:NO];
                attachmentImage.layer.cornerRadius = attachmentImage.frame.size.height/2.0;
                attachmentImage.layer.masksToBounds = true;
                attachmentImage.clipsToBounds = true;
                attachmentImage.layer.borderColor = [UIColor whiteColor].CGColor;
                attachmentImage.layer.borderWidth = 1.0;
                [cell.contentView addSubview:attachmentImage];
                
                /*if ([[[dictionaryEvent valueForKey:@"event_images"]objectAtIndex:5]length]>0)
                {
                    NSString *imageName = [NSString stringWithFormat:@"%@",[[dictionaryEvent valueForKey:@"event_images"] objectAtIndex:5]];
                    NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
                    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
                    if (fileExists == true)
                    {
                        NSString *pathExtension = [filePath pathExtension];
                        if (pathExtension .length > 0)
                        {
                            if ([pathExtension isEqualToString:@"jpg"] || [pathExtension isEqualToString:@"png"])
                            {
                                attachmentImage.image = [UIImage imageWithContentsOfFile:filePath];
                            }
                            else
                            {
                                attachmentImage.image = [UIImage imageNamed:@"attachment"];
                                
                            }
                        }
                    }
                }*/
            }
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception is %@",exception.description);
        }
        
        cell.userInteractionEnabled = true;
        return cell;
    }
    
    return nil;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:YES];
    indexOfImage = ([[dictionaryEvent valueForKey:@"event_images"] count] > 1) ? indexPath.row + 1 : indexPath.row;
    [self viewOrRemovePhotoPressed];
    //[self viewPhotoButtonPressed];

//    if ([[[dictionaryEvent valueForKey:@"event_images"]objectAtIndex:indexPath.row]length]>0)
//    {
//        [self viewPhotoButtonPressed];
//    }
}

-(void)viewPhotoButtonPressed
{
    NSLog(@"viewPhotoButtonPressed");
    
    NSMutableArray *arrayImages = [[NSMutableArray alloc]init];
    arrayImages = [[dictionaryEvent valueForKey:@"event_images"]mutableCopy];
    
    if ([[arrayImages objectAtIndex:indexOfImage]length]>0 && (![[arrayImages objectAtIndex:indexOfImage]hasSuffix:@".jpg"] || [[arrayImages objectAtIndex:indexOfImage]hasSuffix:@".png"])) // attachment is not an image
    {
        NSString *attachmentName = [arrayImages objectAtIndex:indexOfImage];
        NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:attachmentName];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
        if (fileExists == true)
        {
            NSLog(@"filePath : %@", filePath);
            NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
            [self loadAttachmentInWebView:fileURL];
        }
    }
    else
    {
        NSArray *filtered = [arrayImages filteredArrayUsingPredicate: [NSPredicate predicateWithFormat:@"length > 0"]];
        NSMutableArray *photosURL = [[NSMutableArray alloc]init];
        for (int counter = 0; counter < filtered.count; counter++)
        {
            NSString *imageName = [NSString stringWithFormat:@"%@",[filtered objectAtIndex:counter]];
            NSString* documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
            NSString* filePath = [documentsPath stringByAppendingPathComponent:imageName];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
            if (fileExists == true && ([imageName hasSuffix:@".jpg"] || [imageName hasSuffix:@".png"]))
            {
                [photosURL addObject:filePath];
            }
        }
        
        if (photosURL.count == 0)
        {
            return;
        }
        
        NSArray *photos = [IDMPhoto photosWithFilePaths:photosURL];
        IDMPhotoBrowser *browser = [[IDMPhotoBrowser alloc] initWithPhotos:photos animatedFromView:self.view];
        browser.displayActionButton = NO;
        [browser setInitialPageIndex:0];
        browser.displayArrowButton = NO;
        browser.displayCounterLabel = YES;
        browser.usePopAnimation = NO;
        browser.disableVerticalSwipe = YES;
        [browser setInitialPageIndex:indexOfImage];
        [self presentViewController:browser animated:YES completion:nil];
    }
}

#pragma mark
#pragma mark Table View Data Source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 457548)
    {
        //return 9;
        return dataToDisplay.count;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 457548)
    {
       /* if (indexPath.row == 0)
        {
            return 0;
        }
        else if (indexPath.row == 2)
        {
            return 80;
        }
        else if (indexPath.row == 8)
        {
            return 150;
        }
        else
        {
            return 30;
        }*/
        
        NSNumber *type = [dataToDisplay objectAtIndex:indexPath.row];
        
        if ([type isEqualToNumber:[NSNumber numberWithInteger:blank]]) //city + topic OLD
        {
            return 0;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventName]]) // event name
        {
            return [UIImage imageNamed:@"event_icon"].size.height+5;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventDateTime]]) // event date and time
        {
            
            NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_date"]];
            NSString *event_start_time = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_start_time"]];
            NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_end_time"]];
            
            if ([event_date isEqualToString:@""] || [event_date containsString:@"(null)"] || event_date == nil)
            {
                event_date = @"";
            }
            
            if (event_date.length == 0)
            {
                event_date = @"00-00-00";
            }
            
            if ([event_start_time isEqualToString:@""] || [event_start_time containsString:@"(null)"] || event_start_time == nil)
            {
                event_start_time = @"";
            }
            
            if (event_start_time.length == 0)
            {
                event_start_time = @"";
            }
            
            if ([event_end_time isEqualToString:@""] || [event_end_time containsString:@"(null)"] || event_end_time == nil)
            {
                event_end_time = @"";
            }
            
            if (event_end_time.length == 0)
            {
                event_end_time = @"";
            }
            
            if (event_end_time.length == 0 && event_start_time.length == 0)
            {
                return ([UIImage imageNamed:@"date_icon"].size.height+5);
            }
            
            return 2*([UIImage imageNamed:@"date_icon"].size.height+5);
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventPlace]]) // event place
        {
            return [UIImage imageNamed:@"view_map"].size.height-5;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventAddress]]) // event address1
        {
            return [UIImage imageNamed:@"view_map"].size.height-5;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventAddress2]]) // event address2
        {
            return [UIImage imageNamed:@"view_map"].size.height-5;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:zipcode]]) // event zipcode
        {
            return [UIImage imageNamed:@"view_map"].size.height-5;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:phoneNumber]]) // event phone number
        {
            return [UIImage imageNamed:@"call_icon"].size.height+5;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:website]]) // event website
        {
            return [UIImage imageNamed:@"website_icon"].size.height+5;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:notesShare]]) //description
        {
            NSString *event_NotesShare = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_NotesShare"]];
            CGSize maximumLabelSize = CGSizeMake(250, FLT_MAX);
            CGSize size = [event_NotesShare sizeWithFont:[UIFont fontWithName:fontLight size:14] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
            return size.height+15;
            
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:notesNotShare]]) //description
        {
            NSString *event_NotesNoShare = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_NotesNoShare"]];
            CGSize maximumLabelSize = CGSizeMake(250, FLT_MAX);
            CGSize size = [event_NotesNoShare sizeWithFont:[UIFont fontWithName:fontLight size:14] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByWordWrapping];
            return size.height+15;
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:btn]]) //description
        {
            return 130;
        }
        
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else
    {
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
    }
    
    @try
    {
        CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
        NSNumber *type = [dataToDisplay objectAtIndex:indexPath.row];
        
        if ([type isEqualToNumber:[NSNumber numberWithInteger:blank]]) //city + topic
        {}
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventName]]) //event name
        {
            NSString *event_name = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_name"]];
            
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIImageView *imgViewAddress = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"event_icon"].size.height/2.0, [UIImage imageNamed:@"event_icon"].size.width, [UIImage imageNamed:@"event_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"event_icon"] isLogo:NO];
            [viewBase addSubview:imgViewAddress];

            UILabel *lblEventsName = [UIFunction createLable:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:event_name font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewBase addSubview:lblEventsName];
        }
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventDateTime]]) // event date and time
        {
            NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_date"]];
            NSString *event_start_time = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_start_time"]];
            NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_end_time"]];
            
            if ([event_date isEqualToString:@""] || [event_date containsString:@"(null)"] || event_date == nil)
            {
                event_date = @"";
            }
            
            if (event_date.length == 0)
            {
                event_date = @"00-00-00";
            }
            
            if ([event_start_time isEqualToString:@""] || [event_start_time containsString:@"(null)"] || event_start_time == nil)
            {
                event_start_time = @"";
            }
            
            if (event_start_time.length == 0)
            {
                event_start_time = @"";
            }
            
            if ([event_end_time isEqualToString:@""] || [event_end_time containsString:@"(null)"] || event_end_time == nil)
            {
                event_end_time = @"";
            }
            
            if (event_end_time.length == 0)
            {
                event_end_time = @"";
            }
            
            CGFloat viewBaseForDateHeight = (event_start_time.length == 0 && event_end_time.length == 0) ? frame.size.height : frame.size.height/2;
            
            UIView *viewBaseForDate = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, viewBaseForDateHeight) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBaseForDate];
            
            UIView *viewDate = [UIFunction createUIViews:CGRectMake(0, 5, viewBaseForDate.frame.size.width, viewBaseForDate.frame.size.height-10) bckgroundColor:[UIColor clearColor]];
            [viewBaseForDate addSubview:viewDate];
            
            UIImageView *imgViewDate = [UIFunction createUIImageView:CGRectMake(5, viewDate.frame.size.height/2.0-[UIImage imageNamed:@"date_icon"].size.height/2.0, [UIImage imageNamed:@"date_icon"].size.width, [UIImage imageNamed:@"date_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"date_icon"] isLogo:NO];
            [viewDate addSubview:imgViewDate];
            
            
            UILabel *btnEventDate = [UIFunction createLable:CGRectMake(imgViewDate.frame.size.width+imgViewDate.frame.origin.x+15, 0, viewDate.frame.size.width-imgViewDate.frame.size.width-imgViewDate.frame.origin.x-15, viewDate.frame.size.height) bckgroundColor:[UIColor clearColor] title:event_date font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewDate addSubview:btnEventDate];

            
            // ************* Time ********************* //
            UIView *viewBaseForTime = [UIFunction createUIViews:CGRectMake(0, viewBaseForDate.frame.size.height+viewBaseForDate.frame.origin.y, viewBaseForDate.frame.size.width, viewBaseForDate.frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBaseForTime];
            
            UIView *viewTime = [UIFunction createUIViews:CGRectMake(0, 5, viewBaseForTime.frame.size.width, viewBaseForTime.frame.size.height-10) bckgroundColor:[UIColor clearColor]];
            [viewBaseForTime addSubview:viewTime];
            
            
            UIImageView *imgViewTime = [UIFunction createUIImageView:CGRectMake(5, viewTime.frame.size.height/2.0-[UIImage imageNamed:@"time_icon"].size.height/2.0, [UIImage imageNamed:@"time_icon"].size.width, [UIImage imageNamed:@"time_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"time_icon"] isLogo:NO];
            [viewTime addSubview:imgViewTime];
            
            
            UIButton *btnEventStartTime = [UIFunction createButton:CGRectMake(imgViewTime.frame.size.width+imgViewTime.frame.origin.x+15, 0, 75, viewTime.frame.size.height) bckgroundColor:[UIColor clearColor] image:nil title:event_start_time font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            btnEventStartTime.titleLabel.textAlignment = NSTextAlignmentLeft;
            btnEventStartTime.userInteractionEnabled = false;
            btnEventStartTime.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [viewTime addSubview:btnEventStartTime];
            
            UILabel *lblHiphen = [UIFunction createLable:CGRectMake(btnEventStartTime.frame.origin.x+btnEventStartTime.frame.size.width, 0, 10, viewTime.frame.size.height) bckgroundColor:[UIColor clearColor] title:@"-" font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            lblHiphen.textAlignment = NSTextAlignmentCenter;
            [viewTime addSubview:lblHiphen];
            
            
            UIButton *btnEventEndTime = [UIFunction createButton:CGRectMake(lblHiphen.frame.size.width+lblHiphen.frame.origin.x, 0, btnEventStartTime.frame.size.width, viewTime.frame.size.height) bckgroundColor:[UIColor clearColor] image:nil title:event_end_time font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            btnEventEndTime.titleLabel.textAlignment = NSTextAlignmentLeft;
            btnEventEndTime.userInteractionEnabled = false;
            btnEventEndTime.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [btnEventEndTime setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];//set ur title insects also
            [viewTime addSubview:btnEventEndTime];
            
            if (event_end_time.length == 0)
            {
                [lblHiphen setHidden:YES];
            }
            
            if (event_end_time.length == 0 && event_start_time.length == 0)
            {
                [imgViewTime setHidden:YES];
                [btnEventStartTime setHidden:YES];
                [btnEventEndTime setHidden:YES];
                [lblHiphen setHidden:YES];
            }
        }
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventPlace]]) 
        {
            NSString *event_place = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_place"]];
            if (event_place.length == 0)
            {
                event_place = @"-";
            }
            
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIButton *imgViewAddress = [UIFunction createButton:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"view_map"].size.height/2.0, [UIImage imageNamed:@"view_map"].size.width, [UIImage imageNamed:@"view_map"].size.height) bckgroundColor:[UIColor clearColor] image: [UIImage imageNamed:@"view_map"] title:nil font:nil titleColor:nil];
            [imgViewAddress addTarget:self action:@selector(openMapScreen) forControlEvents:UIControlEventTouchUpInside];
            [viewBase addSubview:imgViewAddress];
            
            
            UILabel *lblAddress1 = [UIFunction createLable:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:event_place font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewBase addSubview:lblAddress1];
        }
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventAddress]]) // event address 1
        {
            NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_address_first"]];
            if (event_address_first.length == 0)
            {
                event_address_first = @"-";
            }
            
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIImageView *imgViewAddress = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"view_map"].size.height/2.0, [UIImage imageNamed:@"view_map"].size.width, [UIImage imageNamed:@"view_map"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"view_map"] isLogo:NO];
            imgViewAddress.hidden = true;
            [viewBase addSubview:imgViewAddress];
            
            
            UILabel *lblAddress1 = [UIFunction createLable:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:event_address_first font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            
            [viewBase addSubview:lblAddress1];
        }
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:eventAddress2]]) // event address 2
        {
            NSString *event_address_second = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_address_second"]];
            if (event_address_second.length == 0)
            {
                event_address_second = @"-";
            }
            
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIImageView *imgViewAddress = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"view_map"].size.height/2.0, [UIImage imageNamed:@"view_map"].size.width, [UIImage imageNamed:@"view_map"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"view_map"] isLogo:NO];
            imgViewAddress.hidden = true;
            [viewBase addSubview:imgViewAddress];
            
            if ([locationIconCellType isEqualToNumber:[NSNumber numberWithInteger:eventAddress2]]) {
                
                imgViewAddress.hidden = false;
            }
            
            UILabel *lblAddress2 = [UIFunction createLable:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:event_address_second font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewBase addSubview:lblAddress2];
        }
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:zipcode]]) // zipcode
        {
            NSString *event_zip_code = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_zip_code"]];
            if (event_zip_code.length == 0)
            {
                event_zip_code = @"";
            }
            else{
                event_zip_code = [NSString stringWithFormat:@"%@",event_zip_code];
            }
            
            NSString *event_city = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_city"]];
            if (event_city.length == 0)
            {
                event_city = @"";
            }
            else{
                 event_city = [NSString stringWithFormat:@"%@ ",event_city];
            }
            
            NSString *event_state = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_state"]];
            if (event_state.length == 0)
            {
                event_state = @"";
            }
            else{
                event_state = [NSString stringWithFormat:@"%@ ",event_state];
            }
            
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIImageView *imgViewAddress = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"view_map"].size.height/2.0, [UIImage imageNamed:@"view_map"].size.width, [UIImage imageNamed:@"view_map"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"view_map"] isLogo:NO];
            imgViewAddress.hidden = true;
            [viewBase addSubview:imgViewAddress];
            
            if ([locationIconCellType isEqualToNumber:[NSNumber numberWithInteger:zipcode]]) {
                
                imgViewAddress.hidden = false;
            }
            
            
             UILabel *lblstackView = [UIFunction createLable:CGRectMake(imgViewAddress.frame.size.width+imgViewAddress.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewAddress.frame.size.width-imgViewAddress.frame.origin.x-15, viewBase.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:[NSString stringWithFormat:@"%@%@%@",event_city,event_state,event_zip_code] font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            
            [viewBase addSubview:lblstackView];
            
//            stackView.axis = UILayoutConstraintAxisHorizontal;
//            stackView.alignment = UIStackViewAlignmentFill; //UIStackViewAlignmentLeading, UIStackViewAlignmentFirstBaseline, UIStackViewAlignmentCenter, UIStackViewAlignmentTrailing, UIStackViewAlignmentLastBaseline
//            stackView.distribution = UIStackViewDistributionFillEqually; //UIStackViewDistributionFillEqually, UIStackViewDistributionFillProportionally, UIStackViewDistributionEqualSpacing, UIStackViewDistributionEqualCentering
            
            
            
            
            
            
            
            
         }
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:phoneNumber]]) //phone number
        {
            NSString *event_phone_number = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_phone_number"]];
            if (event_phone_number.length == 0)
            {
                event_phone_number = @"-";
            }
            NSString *user_country_code = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"user_country_code"]];
            event_phone_number = [NSString stringWithFormat:@"%@%@",user_country_code,event_phone_number];
           
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIImageView *imgViewPhone = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"call_icon"].size.height/2.0, [UIImage imageNamed:@"call_icon"].size.width, [UIImage imageNamed:@"call_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"call_icon"] isLogo:NO];
            [viewBase addSubview:imgViewPhone];
            
            
            UILabel *lblPhoneNumber = [UIFunction createLable:CGRectMake(imgViewPhone.frame.size.width+imgViewPhone.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewPhone.frame.size.width-imgViewPhone.frame.origin.x-15, viewBase.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:event_phone_number font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewBase addSubview:lblPhoneNumber];
        }
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:website]]) //website
        {
            NSString *event_website = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_website"]];
            if (event_website.length == 0)
            {
                event_website = @"-";
            }
            
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIImageView *imgViewWebsite = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"website_icon"].size.height/2.0, [UIImage imageNamed:@"website_icon"].size.width, [UIImage imageNamed:@"website_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"website_icon"] isLogo:NO];
            [viewBase addSubview:imgViewWebsite];


            UILabel *lblWebsite = [UIFunction createLable:CGRectMake(imgViewWebsite.frame.size.width+imgViewWebsite.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewWebsite.frame.size.width-imgViewWebsite.frame.origin.x-15, viewBase.frame.size.height-10) bckgroundColor:[UIColor clearColor] title:event_website font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewBase addSubview:lblWebsite];
        }
        
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:notesShare]]) //description + images
        {
            NSString *event_description = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_NotesShare"]];
            
            if ([event_description isEqualToString:@""] || [event_description containsString:@"(null)"] || event_description == nil)
            {
                event_description = @"";
            }
            
            
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIImageView *imgViewDescription = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"description_icon"].size.height/2.0, [UIImage imageNamed:@"description_icon"].size.width, [UIImage imageNamed:@"description_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"description_icon"] isLogo:NO];
            [viewBase addSubview:imgViewDescription];
            
            
            UILabel *lblState = [[UILabel alloc]initWithFrame:CGRectMake(imgViewDescription.frame.size.width+imgViewDescription.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewDescription.frame.size.width-imgViewDescription.frame.origin.x-15, viewBase.frame.size.height-10)];
            lblState.text = event_description;
            lblState.font = [UIFont fontWithName:fontLight size:14];
            lblState.numberOfLines = 0;
            lblState.lineBreakMode = NSLineBreakByWordWrapping;
            lblState.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
            lblState.backgroundColor = [UIColor clearColor];
            [viewBase addSubview:lblState];
            
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:notesNotShare]]) //description + images
        {
            NSString *event_description = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_NotesNoShare"]];
            
            if ([event_description isEqualToString:@""] || [event_description containsString:@"(null)"] || event_description == nil)
            {
            }
            
            
            UIView *viewBase = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, frame.size.height) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewBase];
            
            UIImageView *imgViewDescription = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"description_icon"].size.height/2.0, [UIImage imageNamed:@"description_icon"].size.width, [UIImage imageNamed:@"description_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"description_icon"] isLogo:NO];
            [viewBase addSubview:imgViewDescription];
            
            
            UILabel *lblState = [[UILabel alloc]initWithFrame:CGRectMake(imgViewDescription.frame.size.width+imgViewDescription.frame.origin.x+15, 5, viewBase.frame.size.width-imgViewDescription.frame.size.width-imgViewDescription.frame.origin.x-15, viewBase.frame.size.height-10)];
            lblState.text = event_description;
            lblState.font = [UIFont fontWithName:fontLight size:14];
            lblState.numberOfLines = 0;
            lblState.lineBreakMode = NSLineBreakByWordWrapping;
            lblState.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
            lblState.backgroundColor = [UIColor clearColor];
            [viewBase addSubview:lblState];
            
//            UIImageView *imgViewWebsite = [UIFunction createUIImageView:CGRectMake(5, viewBase.frame.size.height/2.0-[UIImage imageNamed:@"description_icon"].size.height/2.0, [UIImage imageNamed:@"description_icon"].size.width, [UIImage imageNamed:@"description_icon"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"description_icon"] isLogo:NO];
//            [viewBase addSubview:imgViewWebsite];
//
//
//
//
//            UITextView *txtViewDescription = [[UITextView alloc] init ];
//            txtViewDescription.frame = CGRectMake(imgViewWebsite.frame.size.width+imgViewWebsite.frame.origin.x+15, 0, viewBase.frame.size.width-imgViewWebsite.frame.size.width-imgViewWebsite.frame.origin.x-15, 35);
//            //[txtViewDescription setFont:[UIFont fontWithName:event_description size:14]];
//            txtViewDescription.textColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
//            txtViewDescription.returnKeyType = UIReturnKeyDone;
//            txtViewDescription.userInteractionEnabled = true;
//            txtViewDescription.autocapitalizationType = UITextAutocapitalizationTypeSentences;
//            txtViewDescription.autocorrectionType = NO;
//            txtViewDescription.tag = 21000;
//            txtViewDescription.editable = NO;
//            txtViewDescription.backgroundColor = [UIColor clearColor];
//            [viewBase addSubview:txtViewDescription];
//
//            if (event_description.length == 0)
//            {
//                txtViewDescription.text = @"Description";
//            }
//            else
//            {
//                txtViewDescription.text = event_description;
//            }
            
            //   [viewDescription setHidden:hideDescription];
            
            
        }
        else if ([type isEqualToNumber:[NSNumber numberWithInteger:btn]]) //description + images
        {
            //****** Tickets *******//
            UIView *viewTickets = [UIFunction createUIViews:CGRectMake(10, 0, frame.size.width - 10, 25.0) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewTickets];
            
            BOOL isWebsiteNotAvailable = YES;
            NSString *event_website = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_website"]];
            if ([event_website isEqualToString:@""] || [event_website containsString:@"(null)"] || event_website == nil)
            {
                isWebsiteNotAvailable = NO;
            }
            
            UIImageView *imgViewTickets = [UIFunction createUIImageView:CGRectMake(0, 0, [UIImage imageNamed:@"tickets"].size.width, [UIImage imageNamed:@"tickets"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"tickets"] isLogo:NO];
            [viewTickets addSubview:imgViewTickets];
            
            
            UILabel *lblTickets = [UIFunction createLable:CGRectMake(imgViewTickets.frame.origin.x+imgViewTickets.frame.size.width+20, 0, viewTickets.frame.size.width-imgViewTickets.frame.origin.x-imgViewTickets.frame.size.width-5, viewTickets.frame.size.height) bckgroundColor:[UIColor clearColor] title:@"Tickets" font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewTickets addSubview:lblTickets];
            
            UIButton *btnTickets = [UIFunction createButton:CGRectMake(0, 0, viewTickets.frame.size.width, viewTickets.frame.size.height) bckgroundColor:[UIColor clearColor] image:nil title:nil font:nil titleColor:nil];
            [btnTickets addTarget:self action:@selector(ticketsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [viewTickets addSubview:btnTickets];
            [viewTickets setHidden:isWebsiteNotAvailable];
            
            
            
            if (isWebsiteNotAvailable) {
                
                [viewTickets setFrame:CGRectMake(viewTickets.frame.origin.x, viewTickets.frame.origin.y - 1, viewTickets.frame.size.width, 0)];
            }
            
            //****** Messaging *******//
            UIView *viewMessaging = [UIFunction createUIViews:CGRectMake(viewTickets.frame.origin.x, viewTickets.frame.origin.y + viewTickets.frame.size.height + 5, viewTickets.frame.size.width, 25.0) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewMessaging];
            
            
            UIImageView *imgViewMessaging = [UIFunction createUIImageView:CGRectMake(0, viewMessaging.frame.size.height/2.0-[UIImage imageNamed:@"messaging"].size.height/2.0, [UIImage imageNamed:@"messaging"].size.width, [UIImage imageNamed:@"messaging"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"messaging"] isLogo:NO];
            [viewMessaging addSubview:imgViewMessaging];
            
            
            UILabel *lblMessaging = [UIFunction createLable:CGRectMake(imgViewMessaging.frame.origin.x+imgViewMessaging.frame.size.width+20, 0, viewMessaging.frame.size.width-imgViewMessaging.frame.size.width-imgViewMessaging.frame.origin.x-5, viewMessaging.frame.size.height) bckgroundColor:[UIColor clearColor] title:@"Messaging" font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewMessaging addSubview:lblMessaging];
            
            
            UIButton *btnMessages = [UIFunction createButton:CGRectMake(0, 0, viewMessaging.frame.size.width, viewMessaging.frame.size.height) bckgroundColor:[UIColor clearColor] image:nil title:nil font:nil titleColor:nil];
            [btnMessages addTarget:self action:@selector(messagesButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [viewMessaging addSubview:btnMessages];
            
            //****** Invite Guests *******//
            UIView *viewInviteGuests = [UIFunction createUIViews:CGRectMake(viewMessaging.frame.origin.x, viewMessaging.frame.origin.y + viewMessaging.frame.size.height + 5, viewMessaging.frame.size.width, 25.0) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewInviteGuests];
            
            UIImageView *imgViewInviteGuests = [UIFunction createUIImageView:CGRectMake(0, viewInviteGuests.frame.size.height/2.0-[UIImage imageNamed:@"invite_gusets"].size.height/2.0, [UIImage imageNamed:@"invite_gusets"].size.width, [UIImage imageNamed:@"invite_gusets"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"invite_gusets"] isLogo:NO];
            [viewInviteGuests addSubview:imgViewInviteGuests];
            
            UILabel *lblInviteGuests = [UIFunction createLable:CGRectMake(imgViewInviteGuests.frame.origin.x+imgViewInviteGuests.frame.size.width+20, 0, viewInviteGuests.frame.size.width-imgViewInviteGuests.frame.origin.x-imgViewInviteGuests.frame.size.width-5, viewInviteGuests.frame.size.height) bckgroundColor:[UIColor clearColor] title:@"Invite Guests" font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewInviteGuests addSubview:lblInviteGuests];
            
            UIButton *btnInviteGuests = [UIFunction createButton:CGRectMake(0, 0, viewInviteGuests.frame.size.width, viewInviteGuests.frame.size.height) bckgroundColor:[UIColor clearColor] image:nil title:nil font:nil titleColor:nil];
            [btnInviteGuests addTarget:self action:@selector(inviteGuestsButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [viewInviteGuests addSubview:btnInviteGuests];
            
            //****** Add Image *******//
            UIView *viewAddImage = [UIFunction createUIViews:CGRectMake(viewInviteGuests.frame.origin.x, viewInviteGuests.frame.origin.y + viewInviteGuests.frame.size.height + 5, viewInviteGuests.frame.size.width, 25.0) bckgroundColor:[UIColor clearColor]];
            [cell.contentView addSubview:viewAddImage];
            
            UIImageView *imgViewAddImage = [UIFunction createUIImageView:CGRectMake(0, viewAddImage.frame.size.height/2.0-[UIImage imageNamed:@"add_image"].size.height/2.0, [UIImage imageNamed:@"add_image"].size.width, [UIImage imageNamed:@"add_image"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"add_image"] isLogo:NO];
            [viewAddImage addSubview:imgViewAddImage];
            
            UILabel *lblAddImage = [UIFunction createLable:CGRectMake(imgViewAddImage.frame.origin.x+imgViewAddImage.frame.size.width+20, 0, viewAddImage.frame.size.width-imgViewAddImage.frame.size.width-imgViewAddImage.frame.origin.x, viewAddImage.frame.size.height) bckgroundColor:[UIColor clearColor] title:@"Add Image" font:[UIFont fontWithName:fontLight size:14] titleColor:[UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0]];
            [viewAddImage addSubview:lblAddImage];
            
            UIButton *btnAddImage = [UIFunction createButton:CGRectMake(0, 0, viewAddImage.frame.size.width, viewAddImage.frame.size.height) bckgroundColor:[UIColor clearColor] image:nil title:nil font:nil titleColor:nil];
            [btnAddImage addTarget:self action:@selector(addImageButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [viewAddImage addSubview:btnAddImage];
            
        }
        
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"Expression is %@",exception.description);
    }
    
    cell.userInteractionEnabled = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(void)openMapScreen {
    
    NSString *event_place_id = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"place_id"]];
    if (([event_place_id length] > 0)) {
        
        MapkitViewController *controller = [[MapkitViewController alloc] initWithNibName:@"MapkitViewController" bundle:nil];
        controller.event_place_id = event_place_id;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else{
        MapkitViewController *controller = [[MapkitViewController alloc] initWithNibName:@"MapkitViewController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumber *type = [dataToDisplay objectAtIndex:indexPath.row];
    if ([type isEqualToNumber:[NSNumber numberWithInteger:eventAddress]] || [type isEqualToNumber:[NSNumber numberWithInteger:eventAddress2]] || [type isEqualToNumber:[NSNumber numberWithInteger:zipcode]]) // open map
    {
        //NSString *latitude = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"latitude"]];
        //NSString *longitude = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"longitude"]];
        //NSString *source_address = [NSString stringWithFormat:@"%f,%f",[latitude floatValue],[longitude floatValue]];
//        NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_address_first"]];
//        event_address_first = [event_address_first stringByReplacingOccurrencesOfString:@" " withString:@"+"];
//        NSString *event_address_second = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_address_second"]];
//        event_address_second = [event_address_second stringByReplacingOccurrencesOfString:@" " withString:@"+"];
//        NSString* url = [NSString stringWithFormat: @"http://maps.google.com/?q=%@,%@", event_address_first, event_address_second];
//        NSURL *urlToOpen = [NSURL URLWithString:url];
//        SFSafariViewController *controller = [[SFSafariViewController alloc]initWithURL:urlToOpen];
//        [self presentViewController:controller animated:YES completion:nil];
    }
    else if ([type isEqualToNumber:[NSNumber numberWithInteger:phoneNumber]]) //call
    {
        NSString *event_phone_number = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_phone_number"]];
        if (event_phone_number.length == 0)
        {
            [self showAlertWithMessage:@"No Phone number available"];
        }
        else
        {
            NSString *user_country_code = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"user_country_code"]];
            event_phone_number = [NSString stringWithFormat:@"%@%@",user_country_code,event_phone_number];
            event_phone_number = [@"tel:" stringByAppendingString:event_phone_number];
            //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:]];
            UIApplication *application = [UIApplication sharedApplication];
            NSURL *URL = [NSURL URLWithString:event_phone_number];
            [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                if (success) {
                     NSLog(@"Opened url");
                }
            }];
        }
    }
    
    else if ([type isEqualToNumber:[NSNumber numberWithInteger:website]]) //website
    {
        NSString *event_website = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_website"]];
        if (![[event_website lowercaseString]hasPrefix:@"http"] || ![[event_website lowercaseString]hasPrefix:@"https"])
        {
            event_website = [NSString stringWithFormat:@"http://%@",event_website];
        }
        
        NSURL *urlToOpen = [NSURL URLWithString:event_website];
        SFSafariViewController *controller = [[SFSafariViewController alloc]initWithURL:urlToOpen];
        [self presentViewController:controller animated:YES completion:nil];
    }
}


#pragma mark
#pragma mark Buttons Actions
-(void)readMoreButtonPressed
{
    NSLog(@"read More Button Pressed");
}

-(void)addImageButtonPressed
{
    NSLog(@"add image Button Pressed");
    [self editEventPressed];
}
-(void)inviteGuestsButtonPressed
{
    
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    invitePopUpView* controller = [storyboard instantiateViewControllerWithIdentifier:@"invitePopUpView"];
    controller.dictPassDataFrom = dictionaryEvent;
    [self.navigationController pushViewController:controller animated:YES];
    
}
-(void)messagesButtonPressed
{
    NSLog(@"messages Button Pressed");
    if(![MFMessageComposeViewController canSendText])
    {
        [self showAlertWithMessage:@"Your device doesn't support SMS!"];
        return;
    }
    
        NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_date"]];
         if ([event_date isEqualToString:@""] || [event_date containsString:@"(null)"] || event_date == nil)
         {
             event_date = @"";
         }
         else{
             event_date = [NSString stringWithFormat:@"Date - %@",event_date];
         }
         NSString *event_start_time = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_start_time"]];
         NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_end_time"]];
         NSString *time = [NSString stringWithFormat:@"%@ - %@",event_start_time,event_end_time];
         if ([time isEqualToString:@" - "])
         {
             time = @"";
         }
         else{
             time = [NSString stringWithFormat:@"Time - %@",time];
             
         }
         
         
         NSString *event_name = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_name"]];
         if ([event_name isEqualToString:@""] || [event_name containsString:@"(null)"] || event_name == nil)
         {
             event_name = @"";
         }
         else{
             event_name = [NSString stringWithFormat:@"Name - %@",event_name];
         }
         
         NSString *event_city = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_city"]];
         NSString *event_place = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_place"]];
         NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_address_first"]];
         NSString *event_address_second = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_address_second"]];
         NSString *event_state = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_state"]];
         NSString *event_zip_code = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_zip_code"]];
      
          NSString *address;
         if([event_city isEqualToString:@""] && [event_place isEqualToString:@""] && [event_address_first isEqualToString:@""] && [event_address_second isEqualToString:@""] && [event_state isEqualToString:@""] && [event_zip_code isEqualToString:@""])
         {
             address = @"";
         }
         else{
              address = [NSString stringWithFormat:@"Address -%@,%@,%@,%@,%@,%@",event_place,event_address_first,event_address_second,event_city,event_state,event_zip_code];
         }
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setSubject:@"Re: AltCal Private Invitation for You"];
    if([event_date isEqualToString:@""] && [time isEqualToString:@""] && [event_name isEqualToString:@""] && [address isEqualToString:@""]){
        [messageController setBody:nil];
    }
    else{
        [messageController setBody: [NSString stringWithFormat:@"Event Details: %@,%@,%@,%@",event_date,time,event_name,address]];
    }
    
    [self presentViewController:messageController animated:YES completion:nil];
    
}
-(void)ticketsButtonPressed
{
    NSLog(@"tickets Button Pressed");
    
    NSString *event_website = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_website"]];
    NSString* url = [NSString stringWithFormat: @"https://%@", event_website];
    NSURL *urlToOpen = [NSURL URLWithString:url];
    SFSafariViewController *controller = [[SFSafariViewController alloc]initWithURL:urlToOpen];
    [self presentViewController:controller animated:YES completion:nil];
}

#pragma Message Composer Delegates
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            [self showAlertWithMessage:@"Failed to send SMS!"];
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark
#pragma mark Open Web View For Attachments
-(void)loadAttachmentInWebView : (NSURL*)attachment
{
    [viewForWebView removeFromSuperview];
    viewForWebView = nil;
    viewForWebView = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height) bckgroundColor:[UIColor blackColor]];
    [self.view addSubview:viewForWebView];
    
    [UIView animateWithDuration:0.3 animations:^{
        self->viewForWebView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        
        UILabel *label = [UIFunction createLable:CGRectMake(0, 20, self->viewForWebView.frame.size.width, 44) bckgroundColor:[UIColor clearColor] title:@"Attachment" font:[UIFont fontWithName:fontRegular size:14.0] titleColor:[UIColor whiteColor]];
        label.textAlignment = NSTextAlignmentCenter;
        [self->viewForWebView addSubview:label];
        
//        UIButton *btnCancel = [UIFunction createButton:CGRectMake(0, 20, [UIImage imageNamed:@"back_white"].size.width, [UIImage imageNamed:@"back_white"].size.height) bckgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"back_white"] title:nil font:nil titleColor:nil];
//        [btnCancel addTarget:self action:@selector(removeWebView) forControlEvents:UIControlEventTouchUpInside];
//        [viewForWebView addSubview:btnCancel];
        
        UIButton *btnCancel = [UIButton buttonWithType: UIButtonTypeCustom];
        btnCancel.frame = CGRectMake(0, 20, 44, 44);
        btnCancel.backgroundColor = [UIColor clearColor];
        [btnCancel setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
        [btnCancel addTarget:self action:@selector(removeWebView) forControlEvents:UIControlEventTouchUpInside];
        [self->viewForWebView addSubview:btnCancel];

        
        
        WKWebView *webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 64, self.view.frame.size.width , self.view.frame.size.height-64)];
        NSURLRequest *request = [NSURLRequest requestWithURL:attachment];
        [webView loadRequest:request];
        webView.navigationDelegate = self;
      //  webView.scalesPageToFit = true;
        [self->viewForWebView addSubview:webView];
    }];
}

-(void)removeWebView
{
    [UIView animateWithDuration:0.3 animations:^{
        self->viewForWebView.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
    } completion:^(BOOL finished) {
        [self->viewForWebView removeFromSuperview];
        self->viewForWebView = nil;
    }];
}

#pragma mark
#pragma mark Edit Event
-(void)editEventPressed
{
    NSLog(@"edit event pressed");
    
    
       
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_address1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
       
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_address2"];
    [[NSUserDefaults standardUserDefaults] synchronize];
       
       
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_city"];
    [[NSUserDefaults standardUserDefaults] synchronize];
         
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_state"];
    [[NSUserDefaults standardUserDefaults] synchronize];
       
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"map_zipcode"];
    [[NSUserDefaults standardUserDefaults] synchronize];
         
    
   // LandingViewController *landing = [[LandingViewController alloc]init];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    LandingViewController *landing=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
    landing.isEditEvent = true;
    landing.edit_event_id = event_id;
    [self.navigationController pushViewController:landing animated:true];
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Set Up Data to Display
- (void)setUpDataToDisplay
{
 
    dataToDisplay = [[NSMutableArray alloc] init];
    BOOL displayLocationIconSet = NO;
    [dataToDisplay addObject:[NSNumber numberWithInt:blank]];
    [dataToDisplay addObject:[NSNumber numberWithInt:eventName]];
    
    NSString *event_date = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_date"]];
    NSString *event_start_time = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_start_time"]];
    NSString *event_end_time = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_end_time"]];
    
    if (!(([event_date isEqualToString:@""] || [event_date containsString:@"(null)"] || event_date == nil) && ([event_start_time isEqualToString:@""] || [event_start_time containsString:@"(null)"] || event_start_time == nil) && ([event_end_time isEqualToString:@""] || [event_end_time containsString:@"(null)"] || event_end_time == nil)))
    {
     
        [dataToDisplay addObject:[NSNumber numberWithInt:eventDateTime]];
    }
    
    NSString *event_place = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_place"]];
    if (!([event_place isEqualToString:@""] || [event_place containsString:@"(null)"] || event_place == nil))
    {
        [dataToDisplay addObject:[NSNumber numberWithInt:eventPlace]];
        locationIconCellType = [NSNumber numberWithInt:eventPlace];
        displayLocationIconSet = YES;
    }
    
    NSString *event_address_first = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_address_first"]];
    if (!([event_address_first isEqualToString:@""] || [event_address_first containsString:@"(null)"] || event_address_first == nil))
    {
        [dataToDisplay addObject:[NSNumber numberWithInt:eventAddress]];
        locationIconCellType = [NSNumber numberWithInt:eventAddress];
        displayLocationIconSet = YES;
    }

    NSString *event_address_second = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_address_second"]];
    if (!([event_address_second isEqualToString:@""] || [event_address_second containsString:@"(null)"] || event_address_second == nil))
    {
        [dataToDisplay addObject:[NSNumber numberWithInt:eventAddress2]];
        if (!displayLocationIconSet) {
            
            locationIconCellType = [NSNumber numberWithInt:eventAddress2];
            displayLocationIconSet = YES;
        }
    }
    
    NSString *event_zip_code = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_zip_code"]];
    if (!([event_zip_code isEqualToString:@""] || [event_zip_code containsString:@"(null)"] || event_zip_code == nil))
    {
        [dataToDisplay addObject:[NSNumber numberWithInt:zipcode]];
        if (!displayLocationIconSet) {
            
            locationIconCellType = [NSNumber numberWithInt:zipcode];
            displayLocationIconSet = YES;
        }
    }
    
    NSString *event_phone_number = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_phone_number"]];
    if (!([event_phone_number isEqualToString:@""] || [event_phone_number containsString:@"(null)"] || event_phone_number == nil))
    {
        [dataToDisplay addObject:[NSNumber numberWithInt:phoneNumber]];
    }
    
    NSString *event_website = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_website"]];
    if (!([event_website isEqualToString:@""] || [event_website containsString:@"(null)"] || event_website == nil))
    {
        [dataToDisplay addObject:[NSNumber numberWithInt:website]];
    }
    
    NSString *event_NotesShare = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_NotesShare"]];
    if (!([event_NotesShare isEqualToString:@""] || [event_NotesShare containsString:@"(null)"] || event_NotesShare == nil))
    {
        [dataToDisplay addObject:[NSNumber numberWithInt:notesShare]];
    }
    
    NSString *event_NotesNoShare = [NSString stringWithFormat:@"%@",[dictionaryEvent valueForKey:@"event_NotesNoShare"]];
    if (!([event_NotesNoShare isEqualToString:@""] || [event_NotesNoShare containsString:@"(null)"] || event_NotesNoShare == nil))
    {
        [dataToDisplay addObject:[NSNumber numberWithInt:notesNotShare]];
    }
    
    [dataToDisplay addObject:[NSNumber numberWithInt:btn]];
    //[dataToDisplay addObject:[NSNumber numberWithInt:notesShare]];
   // [dataToDisplay addObject:[NSNumber numberWithInt:notesNotShare]];
    
//    NSNumber *type = [dataToDisplay objectAtIndex:0];
//    NSNumber *typeValue = [NSNumber numberWithInt:blank];
//
//    if ([type isEqualToNumber:typeValue]) {
//
//        [self showAlertWithMessage:@"New Method!!!!"];
//    }
}
    
#pragma mark
#pragma mark //******************** View or Remove Photo Delegates *************************//
-(void)viewOrRemovePhotoPressed
    {
        [self.view endEditing:YES];
        
        [viewFullSizeOrRemovePhoto removeFromSuperview];
        viewFullSizeOrRemovePhoto = nil;
        viewFullSizeOrRemovePhoto = [[RemovePhotoView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        viewFullSizeOrRemovePhoto.removePhotoDelegate=self;
        [self.view addSubview:viewFullSizeOrRemovePhoto];
    }
    
-(void)removePhotoViewFunction
{
    [UIView animateWithDuration:.3f animations:^{
        self->viewFullSizeOrRemovePhoto.frame = CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height);
            
        } completion:^(BOOL finished) {
            [self->viewFullSizeOrRemovePhoto removeFromSuperview];
            self->viewFullSizeOrRemovePhoto = nil;
        }];
}

-(void)removePhotoButtonPressed
{
    NSLog(@"removePhotoButtonPressed");
    //LandingViewController *landing = [[LandingViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    LandingViewController *landing=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
    landing.isEditEvent = true;
    landing.edit_event_id = event_id;
    [self.navigationController pushViewController:landing animated:true];
}
    
- (void)addAttachmentTapped
{
    [self.view endEditing:YES];
    indexOfImage = 0;
    [self viewOrRemovePhotoPressed];
}


@end
