//
//  ViewController.h
//  Event App
//
//  Created by   on 17/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation* currentLocation;

@end

