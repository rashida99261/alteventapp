//
//  ViewController.m
//  Event App
//
//  Created by   on 17/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import "ViewController.h"
#import "EnterOTPViewController.h"
#import "LandingViewController.h"
#import "GetMobilePasswordViewController.h"
#import "IDMPhotoBrowser.h"
#import <CoreLocation/CoreLocation.h>

@interface ViewController () <UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate,CLLocationManagerDelegate>

@end

@implementation ViewController
{
    UIView *viewLogIn;
    UIView *viewSignUp;
    UITableView *tableViewLogIn;
    UITableView *tableViewSignUp;
    BOOL isLogInVisible;
    NSMutableDictionary *dictionaryLogIn;
    NSMutableDictionary *dictionarySignUp;
    UIView *viewForCountryCodePicker;
    NSArray *arrayAllCodes;
    NSArray *arrayAllCountries;
    NSInteger forgotPasswordClickCount;
}

#pragma mark
#pragma mark View Will Appear and Dosapper
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager = [[CLLocationManager alloc]init];
          self.locationManager.delegate = self;
          self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
          self.locationManager.distanceFilter = kCLDistanceFilterNone;
          [self.locationManager startUpdatingLocation];
    }
  
   
    
//    if ([CLLocationManager locationServicesEnabled] )
//    {
//        if (self.locationManager == nil )
//        {
//            self.locationManager = [[CLLocationManager alloc] init];
//            self.locationManager.delegate = self;
//            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//            self.locationManager.distanceFilter = 50; //kCLDistanceFilterNone// kDistanceFilter;
//        }
//        [self.locationManager startUpdatingLocation];
//    }
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *docsDir;
    NSArray *dirPaths;
    dirPaths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0]; 
    NSString *databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"EventApp.sqlite"]];
    NSLog(@"%@",databasePath);
    
   
    self.view.backgroundColor = [UIColor blackColor];
    
    UIImageView *bgImgView = [UIFunction createUIImageView:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) backgroundColor:[UIColor blackColor] image:[UIImage imageNamed:@"background_image"] isLogo:NO];
    bgImgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:bgImgView];
    
    arrayAllCodes = [[UIFunction func_GetAllCountries_Code]mutableCopy];
    arrayAllCountries = [[UIFunction func_GetAllCountries_Name]mutableCopy];

    
    NSMutableDictionary *user_data = [[NSMutableDictionary alloc]init];
    user_data = [[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData]mutableCopy];
    
    NSString *isLogin = [[NSUserDefaults standardUserDefaults] valueForKey:@"isLogin"];
    if([isLogin isEqualToString:@"yes"]){
        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
        LandingViewController *controller=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
        controller.isFromLogin = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        isLogInVisible = YES;
        
        dictionaryLogIn = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                           @"",@"user_email",
                           @"",@"user_password",
                           @"0",@"remember_me",
                           @"",@"user_login_date",
                           nil];
        
        
        dictionarySignUp = [[NSMutableDictionary alloc]initWithObjectsAndKeys:
                            @"",@"user_name",
                            @"",@"user_email",
                            @"",@"user_password",
                            @"",@"user_mobile_number",
                            @"",@"user_country_code",
                            @"",@"user_sign_up_date",
                            @"",@"user_login_date",
                            @"",@"user_city",
                            @"",@"user_latitude",
                            @"",@"user_longitude",
                            
                            nil];
        
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSString *currntDate =  [dateFormatter stringFromDate:[NSDate date]];
        
        [dictionarySignUp setValue:currntDate forKey:@"user_sign_up_date"];
        [dictionarySignUp setValue:currntDate forKey:@"user_login_date"];
        
        if ([UIFunction autoDetectCountryCode].length>0)
        {
            [dictionarySignUp setObject:[NSString stringWithFormat:@"+%@",[UIFunction autoDetectCountryCode]] forKey:@"user_country_code"];
        }
        else
        {
            [dictionarySignUp setObject:@"+1" forKey:@"user_country_code"];
        }

        

       // check if remember me was tapped or not
        NSMutableDictionary *rememberMeDictionary = [[NSMutableDictionary alloc]init];
        rememberMeDictionary = [[DataManager getSharedInstance]Select_RememberUserTable];
        NSLog(@"rememberMeDictionary is %@",rememberMeDictionary);
        
        if ([rememberMeDictionary valueForKey:@"user_email"] != nil)
        {
            NSString *email = [NSString stringWithFormat:@"%@",[rememberMeDictionary valueForKey:@"user_email"]];
            NSString *password = [NSString stringWithFormat:@"%@",[rememberMeDictionary valueForKey:@"user_password"]];
            
            [dictionaryLogIn setValue:email forKey:@"user_email"];
            [dictionaryLogIn setValue:password forKey:@"user_password"];
            [dictionaryLogIn setValue:@"1" forKey:@"remember_me"];
            [dictionaryLogIn setValue:currntDate forKey:@"user_login_date"];
        }
        
        
        UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake(self.view.frame.size.width/2.0-[UIImage imageNamed:@"logo_image"].size.width/2.0, 35, [UIImage imageNamed:@"logo_image"].size.width, [UIImage imageNamed:@"logo_image"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"logo_image"] isLogo:NO];
        [self.view addSubview:imgViewLogo];
        UITapGestureRecognizer *singleFingerTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self
                                                action:@selector(openAltCalWebsite)];
        [imgViewLogo setUserInteractionEnabled:YES];
        [imgViewLogo addGestureRecognizer:singleFingerTap];        
        
        viewLogIn = [UIFunction createUIViews:CGRectMake(0, imgViewLogo.frame.size.height+imgViewLogo.frame.origin.y+10, self.view.frame.size.width, self.view.frame.size.height-imgViewLogo.frame.size.height-imgViewLogo.frame.origin.y-10) bckgroundColor:[UIColor clearColor]];
        [self.view addSubview:viewLogIn];
        
        [tableViewLogIn removeFromSuperview];
        tableViewLogIn = nil;
        tableViewLogIn = [self createTableView:CGRectMake(15, 10, viewLogIn.frame.size.width-30, 150) backgroundColor:[UIColor clearColor]];
        [viewLogIn addSubview:tableViewLogIn];
        tableViewLogIn.tag = 457548;
        tableViewLogIn.contentSize = [tableViewLogIn sizeThatFits:CGSizeMake(CGRectGetWidth(tableViewLogIn.bounds), CGFLOAT_MAX)];
        
        
        UIButton *btnLogIn_LogIn = [UIFunction createButton:CGRectMake(viewLogIn.frame.size.width/2.0-95, tableViewLogIn.frame.size.height+tableViewLogIn.frame.origin.y+20, 90, 30) bckgroundColor:[UIColor blackColor] image:nil title:@"LOG IN" font:[UIFont fontWithName:fontBold size:14.0] titleColor:[UIColor whiteColor]];
        btnLogIn_LogIn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        btnLogIn_LogIn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnLogIn_LogIn addTarget:self action:@selector(logInButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        btnLogIn_LogIn.layer.cornerRadius = 5;
        btnLogIn_LogIn.layer.borderWidth = 1.0;
        btnLogIn_LogIn.layer.borderColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0].CGColor;
        [viewLogIn addSubview:btnLogIn_LogIn];
        
        
        UIButton *btnRegister_LogIn = [UIFunction createButton:CGRectMake(viewLogIn.frame.size.width/2.0+5, btnLogIn_LogIn.frame.origin.y, btnLogIn_LogIn.frame.size.width, btnLogIn_LogIn.frame.size.height) bckgroundColor:[UIColor clearColor] image:nil title:@"SIGN UP" font:[UIFont fontWithName:fontBold size:14.0] titleColor:[UIColor whiteColor]];
        btnRegister_LogIn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        btnRegister_LogIn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnRegister_LogIn addTarget:self action:@selector(signUpButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        btnRegister_LogIn.layer.cornerRadius = 5;
        btnRegister_LogIn.layer.borderWidth = 1.0;
        btnRegister_LogIn.layer.borderColor = [UIColor colorWithRed:115.0/255 green:115.0/255 blue:115.0/255 alpha:1.0].CGColor;
        [viewLogIn addSubview:btnRegister_LogIn];
        
        
        
        //MARK: ----------- SIGN UP -----------------------
        
        viewSignUp = [UIFunction createUIViews:CGRectMake(0, imgViewLogo.frame.size.height+imgViewLogo.frame.origin.y+10, self.view.frame.size.width, self.view.frame.size.height-imgViewLogo.frame.size.height-imgViewLogo.frame.origin.y-10) bckgroundColor:[UIColor clearColor]];
        //viewSignUp.hidden = YES;
        viewSignUp.alpha = 0.0;
        [self.view addSubview:viewSignUp];
        
        [tableViewSignUp removeFromSuperview];
        tableViewSignUp = nil;
        tableViewSignUp = [self createTableView:CGRectMake(15, 10, viewSignUp.frame.size.width-30, 200) backgroundColor:[UIColor clearColor]];
        [viewSignUp addSubview:tableViewSignUp];
        tableViewSignUp.tag = 587800;
        tableViewSignUp.contentSize = [tableViewSignUp sizeThatFits:CGSizeMake(CGRectGetWidth(tableViewSignUp.bounds), CGFLOAT_MAX)];
        if (self.view.frame.size.height == 568.0)
        {
            [tableViewSignUp setContentInset:UIEdgeInsetsMake(0, 0, 130, 0)];
        }
        
        UIButton *btnLogIn_SignUp = [UIFunction createButton:CGRectMake(viewSignUp.frame.size.width/2.0-95, tableViewSignUp.frame.size.height+tableViewSignUp.frame.origin.y+20, 90, 30) bckgroundColor:[UIColor clearColor] image:nil title:@"LOG IN" font:[UIFont fontWithName:fontBold size:14.0] titleColor:[UIColor whiteColor]];
        btnLogIn_SignUp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        btnLogIn_SignUp.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnLogIn_SignUp addTarget:self action:@selector(logInButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        btnLogIn_SignUp.layer.cornerRadius = 5;
        btnLogIn_SignUp.layer.borderWidth = 1.0;
        btnLogIn_SignUp.layer.borderColor = [UIColor colorWithRed:115.0/255 green:115.0/255 blue:115.0/255 alpha:1.0].CGColor;
        [viewSignUp addSubview:btnLogIn_SignUp];
        
        
        UIButton *btnRegister_SignUp = [UIFunction createButton:CGRectMake(viewSignUp.frame.size.width/2.0+5, btnLogIn_SignUp.frame.origin.y, btnLogIn_SignUp.frame.size.width, btnLogIn_SignUp.frame.size.height) bckgroundColor:[UIColor blackColor] image:nil title:@"SIGN UP" font:[UIFont fontWithName:fontBold size:14.0] titleColor:[UIColor whiteColor]];
        btnRegister_SignUp.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        btnRegister_SignUp.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnRegister_SignUp addTarget:self action:@selector(signUpButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        btnRegister_SignUp.layer.cornerRadius = 5;
        btnRegister_SignUp.layer.borderWidth = 1.0;
        btnRegister_SignUp.layer.borderColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0].CGColor;
        [viewSignUp addSubview:btnRegister_SignUp];
    }
    
    forgotPasswordClickCount = 0;
}

#pragma mark
#pragma mark Create Table
-(UITableView*) createTableView : (CGRect)frame backgroundColor:(UIColor*)backgroundColor
{
    UITableView *_tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    _tableView.dataSource=self;
    _tableView.delegate = self;
    _tableView.backgroundColor=backgroundColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.alwaysBounceVertical = NO;
    [_tableView setAllowsSelection:YES];
    return _tableView;
}

#pragma mark
#pragma mark ************** Location current **************




-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSString *currentLatitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString *currentLongitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    if (currentLatitude.length==0)
    {
        currentLatitude = @"";
    }
    
    if (currentLongitude.length==0)
    {
        currentLongitude = @"";
    }
    
    [self.locationManager stopUpdatingLocation];
    
    [dictionarySignUp setValue:currentLatitude forKey:@"user_latitude"];
    [dictionarySignUp setValue:currentLongitude forKey:@"user_longitude"];
    
    
}

#pragma mark
#pragma mark ************** Buttons Actions **************
-(void)logInButtonPressed
{
    NSLog(@"log in button pressed");
   
    [self dismissKeyboard];
    [self removeAllPickers];

    if (isLogInVisible == NO)
    {
        isLogInVisible = YES;
        viewLogIn.alpha = 0.0;
        viewSignUp.alpha = 1.0;
        [UIView animateWithDuration:0.3 animations:^{
            self->viewLogIn.alpha = 1.0;
            self->viewSignUp.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self->tableViewLogIn reloadData];
            [self->tableViewSignUp reloadData];
        }];
    }
    else
    {
        NSLog(@"please enter login data %@", dictionaryLogIn);
        
        NSString *email = [[NSString stringWithFormat:@"%@",[dictionaryLogIn valueForKey:@"user_email"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *password = [[NSString stringWithFormat:@"%@",[dictionaryLogIn valueForKey:@"user_password"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *remember_me = [[NSString stringWithFormat:@"%@",[dictionaryLogIn valueForKey:@"remember_me"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *user_login_date = [[NSString stringWithFormat:@"%@",[dictionaryLogIn valueForKey:@"user_login_date"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        
        

        if (email.length == 0)
        {
            [self showAlertWithMessage:@"Please enter email."];
        }
        else if ([UIFunction validateEmail:email] == 0)
        {
            [self showAlertWithMessage:@"Please enter a valid email."];
        }
        else if (password.length == 0)
        {
            [self showAlertWithMessage:@"Please enter a password."];
        }
        else if (password.length < 6)
        {
            [self showAlertWithMessage:@"Password must be atleast 6 digits long."];
        }
        else
        {
            NSLog(@"Login user from database");
            
            // firstly check, if the email id is already exists or not, if exists, show success alert else ask user to sign up.
            NSMutableDictionary *dicionaryUserData = [[NSMutableDictionary alloc]init];
            dicionaryUserData = [[[DataManager getSharedInstance]Select_UserDataBaseTable:email]mutableCopy];
            if ([dicionaryUserData valueForKey:@"user_email"] != nil)
            {
                NSString *user_stored_password = [NSString stringWithFormat:@"%@",[dicionaryUserData valueForKey:@"user_password"]];
                NSString *isPhoneNumberVerified = [NSString stringWithFormat:@"%@",[dicionaryUserData valueForKey:@"isPhoneNumberVerified"]];

                if ([user_stored_password isEqualToString:password])
                {
                    [[NSUserDefaults standardUserDefaults]setObject:dicionaryUserData forKey:kLoginData];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:kAppGroup];
                    [sharedDefaults setObject:dicionaryUserData forKey:kLoginData];
                    [sharedDefaults synchronize];
                    
                    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    NSString *currntDate =  [dateFormatter stringFromDate:[NSDate date]];
                    [dictionaryLogIn setValue:currntDate forKey:@"user_login_date"];
                    
                    if ([remember_me integerValue] == 1 || [remember_me isEqualToString:@"1"])
                    {
                        NSMutableDictionary *rememberMeDictionary = [[NSMutableDictionary alloc]init];
                        rememberMeDictionary = [[DataManager getSharedInstance]Select_RememberUserTable];
                        NSLog(@"rememberMeDictionary is %@",rememberMeDictionary);
                        if ([rememberMeDictionary valueForKey:@"user_email"] != nil)
                        {
                            // update
                            [[DataManager getSharedInstance]Update_RememberMeTable:email :password :user_login_date];
                        }
                        else
                        {
                            [[DataManager getSharedInstance]Insert_RememberMeTable:email :password :user_login_date];
                        }
                    }
                    
                    if ([isPhoneNumberVerified isEqualToString:@"1"] || [isPhoneNumberVerified integerValue] == 1)
                    {
                        [self pushToNextController:@"1"];
                        
                    }
                    else
                    {
                        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Finish Your Account Set Up" message:@"Please Confirm Your Recovery Number" preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *cancelAction = [UIAlertAction
                                                       actionWithTitle:@"Not Now"
                                                       style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction *action)
                                                       {
                                                            [self pushToNextController:@"1"];
                                                       }];
                        UIAlertAction *sendAction = [UIAlertAction
                                                     actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action)
                                                     {
                                                         NSString *user_country_code = [NSString stringWithFormat:@"%@",[dicionaryUserData valueForKey:@"user_country_code"]];
                                                         NSString *user_mobile_number = [NSString stringWithFormat:@"%@",[dicionaryUserData valueForKey:@"user_mobile_number"]];
                                                         NSString *phNumberWithCountryCode = [NSString stringWithFormat:@"%@%@",user_country_code,user_mobile_number];
                                                         [self requestForOTPAtNumber:phNumberWithCountryCode andCountryCode:user_country_code];
                                                     }];
                        
                        [alertController addAction:cancelAction];
                        [alertController addAction:sendAction];
                        [self presentViewController:alertController animated:YES completion:nil];
                        
                    }
                    
                    
                }
                else
                {
                    [self showAlertWithMessage:@"Password does not match."];
                }
            }
            else
            {
                [self showAlertWithMessage:@"User does not exists. Please Sign Up."];
            }
        }
    }
}

-(void)signUpButtonPressed
{
    NSLog(@"sign up button pressed");
    
    [self dismissKeyboard];
    [self removeAllPickers];
    
    if (isLogInVisible == YES)
    {
        isLogInVisible = NO;
        viewSignUp.alpha = 0.0;
        viewLogIn.alpha = 1.0;
        [UIView animateWithDuration:0.3 animations:^{
            self->viewSignUp.alpha = 1.0;
            self->viewLogIn.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self->tableViewLogIn reloadData];
            [self->tableViewSignUp reloadData];
        }];
    }
    else
    {
        NSLog(@"please enter sign up data %@",dictionarySignUp);
        
        NSString *name = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_name"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *email = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_email"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *password = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_password"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *mobile_number = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_mobile_number"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *user_country_code = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_country_code"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        NSString *user_sign_up_date = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_sign_up_date"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *user_login_date = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_login_date"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *user_city = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_city"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *user_latitude = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_latitude"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *user_longitude = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_longitude"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        
        

        
        if (name.length == 0)
        {
            [self showAlertWithMessage:@"Please enter your name."];
        }
        else if ([UIFunction stringContainsEmoji:name] == 1)
        {
            [self showAlertWithMessage:@"Name should contains only alphabets."];
        }
        else if (name.length < 3)
        {
            [self showAlertWithMessage:@"Name should be atleast 3 digits long."];
        }
        else if (email.length == 0)
        {
            [self showAlertWithMessage:@"Please enter email."];
        }
        else if ([UIFunction validateEmail:email] == 0)
        {
            [self showAlertWithMessage:@"Please enter a valid email."];
        }
        else if (password.length == 0)
        {
            [self showAlertWithMessage:@"Please enter a password."];
        }
        else if (password.length < 6)
        {
            [self showAlertWithMessage:@"Password must be atleast 6 digits long."];
        }
        else if (user_country_code.length == 0)
        {
            [self showAlertWithMessage:@"Please select your country code."];
        }
        else if (mobile_number.length == 0)
        {
            [self showAlertWithMessage:@"Please enter a valid mobile number."];
        }
        else if ([self isPasswordValid:password] == false)
        {
            [self showAlertWithMessage:@"Please use combination of lowercase, uppercase, and numbers."];
        }
        else
        {
            NSLog(@"Register user into database");
            
            // firstly check, if the email id is already exists or not, if exists, show alert else insert into database.
            NSMutableDictionary *dicionaryUserData = [[NSMutableDictionary alloc]init];
            dicionaryUserData = [[[DataManager getSharedInstance]Select_UserDataBaseTable:email]mutableCopy];
            if ([dicionaryUserData valueForKey:@"user_email"] != nil)
            {
                [self showAlertWithMessage:@"User already exists. Please Log In"];
            }
            else
            {
                NSMutableDictionary *dictionaryLastUser = [[[DataManager getSharedInstance]Select_Last_UserFrom_DataBaseTable]mutableCopy];
                NSInteger last_user_id = [[dictionaryLastUser valueForKey:@"user_id"]integerValue];
                last_user_id++;
                NSString *new_id = [NSString stringWithFormat:@"%zd",last_user_id];
                
               
                BOOL isInsertionSuccess =  [[DataManager getSharedInstance]Insert_UserDataBaseTable:new_id :name :email :password :mobile_number :@"0" :user_country_code :user_sign_up_date :user_login_date :user_city :user_latitude :user_longitude];
                                            
                if (isInsertionSuccess == NO)
                {
                    [self showAlertWithMessage:@"Unable to insert into database. Please try again later."];
                }
                else
                {
                    [dictionarySignUp setObject:new_id forKey:@"user_id"];
                    [[NSUserDefaults standardUserDefaults]setObject:dictionarySignUp forKey:kLoginData];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    
                    NSString *phNumberWithCountryCode = [NSString stringWithFormat:@"%@%@",user_country_code,mobile_number];
                    [self requestForOTPAtNumber:phNumberWithCountryCode andCountryCode:user_country_code];
                }
            }
        }
    }
}

-(void)forgotPasswordButtonPressed
{
    [self.view endEditing:YES];
    
    
    [self askForMobileNumberRecovery];
    
//    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Can't remember your password?" message:@"Don't worry worst things happen :)" preferredStyle:UIAlertControllerStyleAlert];
//
//    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
//     {
//         textField.placeholder = @"Please enter your mobile number.";
//         textField.keyboardType = UIKeyboardTypeEmailAddress;
//     }];
//
//    UIAlertAction *cancelAction = [UIAlertAction
//                                   actionWithTitle:@"Cancel"
//                                   style:UIAlertActionStyleDefault
//                                   handler:^(UIAlertAction *action)
//                                   { }];
//
//    UIAlertAction *sendAction = [UIAlertAction
//                                 actionWithTitle:@"OK"
//                                 style:UIAlertActionStyleDefault
//                                 handler:^(UIAlertAction *action)
//                                 {
//                                     UITextField *emailTextField = alertController.textFields.lastObject;
//
//
//
//
//                                     BOOL isEmailIdCorrect = [UIFunction validateEmail:emailTextField.text];
//                                     if (isEmailIdCorrect == 1)
//                                     {
//                                         NSString *email = [emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//
//                                         // firstly check, if the email id is already exists or not, if exists, show success alert else ask user to sign up.
//                                         NSMutableDictionary *dicionaryUserData = [[NSMutableDictionary alloc]init];
//                                         dicionaryUserData = [[[DataManager getSharedInstance]Select_UserDataBaseTable:email]mutableCopy];
//                                         if ([dicionaryUserData valueForKey:@"user_email"] != nil)
//                                         {
//                                             NSString *user_stored_password = [NSString stringWithFormat:@"%@",[dicionaryUserData valueForKey:@"user_password"]];
//
//                                             [self showPassword:[NSString stringWithFormat:@"Your password is\n%@", user_stored_password]];
//                                         }
//                                         else
//                                         {
//                                             [self showAlertWithMessage:@"User does not exists. "];
//                                             self->forgotPasswordClickCount = self->forgotPasswordClickCount + 1;
//
//                                             if(self->forgotPasswordClickCount > 1)
//                                             {
//                                                 [self askForMobileNumberRecovery];
//                                             }
//                                         }
//                                     }
//                                     else
//                                     {
//                                         [self showAlertWithMessage:@"Please enter a valid email."];
//                                     }
//                                 }];
//
//    [alertController addAction:cancelAction];
//    [alertController addAction:sendAction];
//    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)mobileNumberInfoButtonPressed
{
    [self showAlertWithMessage:@"We'll use this if you lose your password."];
}

#pragma mark
#pragma mark Table View Data Source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 457548) // login table
    {
        return 3;
    }
    else // register table
    {
        return 4;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 457548) // login table
    {
        return 50;
    }
    else // register table
    {
        return 50;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else
    {
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
    }
    
    @try
    {
        CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
        
        if (tableView.tag == 457548) // login table
        {
            if (indexPath.row == 0) // Email Text Field
            {
                NSString *email = [NSString stringWithFormat:@"%@",[dictionaryLogIn valueForKey:@"user_email"]];
                
                UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, frame.size.width-20, frame.size.height)];
                txtField.placeholder = @"E-mail";
                txtField.tag = 74201;
                txtField.font = [UIFont fontWithName:fontRegular size:16];
                txtField.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
                txtField.delegate = self;
                txtField.text = email;
                txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtField.textAlignment = NSTextAlignmentLeft;
                txtField.returnKeyType = UIReturnKeyNext;
                txtField.keyboardType = UIKeyboardTypeEmailAddress;
                txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                txtField.autocorrectionType = UITextAutocorrectionTypeNo;
                [cell.contentView addSubview:txtField];
                if ([txtField respondsToSelector:@selector(setAttributedPlaceholder:)]) {
                    UIColor *color = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
                    txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"E-mail" attributes:@{NSForegroundColorAttributeName: color}];
                }
            }
            else if (indexPath.row == 1) // Password Text Field
            {
                NSString *password = [NSString stringWithFormat:@"%@",[dictionaryLogIn valueForKey:@"user_password"]];

                UIButton *btnForgotPassword = [UIFunction createButton:CGRectMake(frame.size.width-70, 5, 70, frame.size.height-10) bckgroundColor:[UIColor clearColor] image:nil title:@"Forgot?" font:[UIFont fontWithName:fontRegular size:16] titleColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
                [btnForgotPassword addTarget:self action:@selector(forgotPasswordButtonPressed) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:btnForgotPassword];

                
                UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, btnForgotPassword.frame.origin.x-10, frame.size.height)];
                txtField.placeholder = @"Password";
                txtField.tag = 74202;
                txtField.font = [UIFont fontWithName:fontRegular size:14];
                txtField.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
                txtField.delegate = self;
                txtField.text = password;
                txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtField.textAlignment = NSTextAlignmentLeft;
                txtField.returnKeyType = UIReturnKeyDone;
                txtField.secureTextEntry = true;
                txtField.keyboardType = UIKeyboardTypeDefault;
                txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                txtField.autocorrectionType = UITextAutocorrectionTypeNo;
                [cell.contentView addSubview:txtField];
                txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]}];
            }
            else if (indexPath.row == 2) // Remember Me
            {
                NSString *remember_me = [NSString stringWithFormat:@"%@",[dictionaryLogIn valueForKey:@"remember_me"]];

                UIImageView *imgView = [UIFunction createUIImageView:CGRectMake(10, frame.size.height/2.0-[UIImage imageNamed:@"uncheck"].size.height/2.0, [UIImage imageNamed:@"uncheck"].size.width, [UIImage imageNamed:@"uncheck"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"uncheck"] isLogo:NO];
                [cell.contentView addSubview:imgView];
                
                UILabel *lblRememberMe = [UIFunction createLable:CGRectMake(imgView.frame.size.width+imgView.frame.origin.x+10, 5, frame.size.width-imgView.frame.size.width-imgView.frame.origin.x-10 , frame.size.height-10) bckgroundColor:[UIColor clearColor] title:@"Remember Me" font:[UIFont fontWithName:fontRegular size:16.0] titleColor:[UIColor whiteColor]];
                [cell.contentView addSubview:lblRememberMe];
                
                if ([remember_me isEqualToString:@"0"] || [remember_me integerValue] == 0)
                {
                    imgView.image = [UIImage imageNamed:@"uncheck"];
                }
                else
                {
                    imgView.image = [UIImage imageNamed:@"check"];
                }
            }
            
            if (indexPath.row != 2)
            {
                UIView *viewDivider = [UIFunction createUIViews:CGRectMake(0, frame.size.height-1, frame.size.width, 1) bckgroundColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:0.6]];
                [cell.contentView addSubview:viewDivider];
            }
        }
        else // register table
        {
            if (indexPath.row == 0) // Name Text Field
            {
                NSString *name = [NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_name"]];
                
                UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, frame.size.width-20, frame.size.height)];
                txtField.placeholder = @"Your Name";
                txtField.tag = 74101;
                txtField.font = [UIFont fontWithName:fontRegular size:16];
                txtField.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
                txtField.delegate = self;
                txtField.text = name;
                txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtField.textAlignment = NSTextAlignmentLeft;
                txtField.returnKeyType = UIReturnKeyNext;
                txtField.keyboardType = UIKeyboardTypeDefault;
                txtField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                txtField.autocorrectionType = UITextAutocorrectionTypeNo;
                [cell.contentView addSubview:txtField];
                txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]}];
            }
            else if (indexPath.row == 1) // Email Text Field
            {
                NSString *email = [NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_email"]];
                
                UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, frame.size.width-20, frame.size.height)];
                txtField.placeholder = @"E-mail";
                txtField.tag = 74102;
                txtField.font = [UIFont fontWithName:fontRegular size:16];
                txtField.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
                txtField.delegate = self;
                txtField.text = email;
                txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtField.textAlignment = NSTextAlignmentLeft;
                txtField.returnKeyType = UIReturnKeyNext;
                txtField.keyboardType = UIKeyboardTypeEmailAddress;
                txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                txtField.autocorrectionType = UITextAutocorrectionTypeNo;
                [cell.contentView addSubview:txtField];
                txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]}];
            }
            else if (indexPath.row == 2) // Password Text Field
            {
                NSString *password = [NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_password"]];
                
                UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(10, 5, frame.size.width-20, frame.size.height)];
                txtField.placeholder = @"Password";
                txtField.tag = 74103;
                txtField.font = [UIFont fontWithName:fontRegular size:16];
                txtField.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
                txtField.delegate = self;
                txtField.text = password;
                txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtField.textAlignment = NSTextAlignmentLeft;
                txtField.returnKeyType = UIReturnKeyNext;
                txtField.secureTextEntry = true;
                txtField.keyboardType = UIKeyboardTypeDefault;
                txtField.autocapitalizationType = UITextAutocapitalizationTypeNone;
                txtField.autocorrectionType = UITextAutocorrectionTypeNo;
                [cell.contentView addSubview:txtField];
                txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]}];
            }
            else if (indexPath.row == 3) // Mobile Number Text Field
            {
                NSString *mobile_number = [NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_mobile_number"]];
                NSString *user_country_code = [[NSString stringWithFormat:@"%@",[dictionarySignUp valueForKey:@"user_country_code"]]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

                UILabel *lblCountryCode = [UIFunction createLable:CGRectMake(10, 5, 50, frame.size.height) bckgroundColor:[UIColor clearColor] title:user_country_code font:[UIFont fontWithName:fontRegular size:14.0] titleColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
                lblCountryCode.userInteractionEnabled = true;
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(func_SelectCountryCode)];
                [tap setNumberOfTouchesRequired:1];
                [lblCountryCode addGestureRecognizer:tap];
                [cell.contentView addSubview:lblCountryCode];

                
                UITextField *txtField = [[UITextField alloc]initWithFrame:CGRectMake(lblCountryCode.frame.size.width+lblCountryCode.frame.origin.x, 5, frame.size.width-20-lblCountryCode.frame.size.width-lblCountryCode.frame.origin.x, frame.size.height)];
                txtField.placeholder = @"Mobile Phone";
                txtField.tag = 74104;
                txtField.font = [UIFont fontWithName:fontRegular size:16];
                txtField.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
                txtField.delegate = self;
                txtField.text = mobile_number;
                txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
                txtField.textAlignment = NSTextAlignmentLeft;
                txtField.returnKeyType = UIReturnKeyNext;
                txtField.keyboardType = UIKeyboardTypeNumberPad;
                txtField.autocapitalizationType = UITextAutocapitalizationTypeWords;
                txtField.autocorrectionType = UITextAutocorrectionTypeNo;
                [cell.contentView addSubview:txtField];
                txtField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:txtField.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]}];
                
                UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
                [toolBar setBarStyle:UIBarStyleDefault];
                UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
                
                UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                                  style:UIBarButtonItemStylePlain
                                                                                 target:self
                                                                                 action:@selector(dismissKeyboard)];
                toolBar.items = @[flex, barButtonDone];
                barButtonDone.tintColor = [UIColor blackColor];
                txtField.inputAccessoryView = toolBar;
                
                UIButton *btnMobile = [UIFunction createButton:CGRectMake(frame.size.width-15, 5, 10, frame.size.height-10) bckgroundColor:[UIColor clearColor] image:nil title:@"?" font:[UIFont fontWithName:fontRegular size:16] titleColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
                [btnMobile addTarget:self action:@selector(mobileNumberInfoButtonPressed) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:btnMobile];
            }
            
            UIView *viewDivider = [UIFunction createUIViews:CGRectMake(5, frame.size.height-1, frame.size.width-10, 1) bckgroundColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:0.6]];
            [cell.contentView addSubview:viewDivider];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Expression is %@",exception.description);
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.userInteractionEnabled = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 457548 && indexPath.row == 2) // login table
    {
        NSString *remember_me = [NSString stringWithFormat:@"%@",[dictionaryLogIn valueForKey:@"remember_me"]];
        if ([remember_me isEqualToString:@"0"] || [remember_me integerValue] == 0)
        {
            [dictionaryLogIn setValue:@"1" forKey:@"remember_me"];
        }
        else
        {
            [dictionaryLogIn setValue:@"0" forKey:@"remember_me"];
        }
        
        [tableViewLogIn reloadData];
    }
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [tableViewLogIn scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [tableViewSignUp scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}


#pragma mark
#pragma mark TextField Delegate
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [tableViewSignUp scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    [tableViewLogIn scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    
}


-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField.tag == 74101) // full name
    {
        [dictionarySignUp setValue:@"" forKey:@"user_name"];
    }
    else if (textField.tag == 74102) // sign up email
    {
        [dictionarySignUp setValue:@"" forKey:@"user_email"];
    }
    else if (textField.tag == 74103) // sign up password
    {
        [dictionarySignUp setObject:@"" forKey:@"user_password"];
    }
    else if (textField.tag == 74104) // sign up mobile number
    {
        [dictionarySignUp setObject:@"" forKey:@"user_mobile_number"];
    }
    else if (textField.tag == 74201) // sign in email
    {
        [dictionaryLogIn setValue:@"" forKey:@"user_email"];
    }
    else if (textField.tag == 74202) // sign in password
    {
        [dictionaryLogIn setObject:@"" forKey:@"user_password"];
    }
    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField.tag == 74101) // full name
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74102];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74102) // sign up email
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74103];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74103) // sign up password
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74104];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74104) // sign up mobile number
    {
        [textField resignFirstResponder];
        [tableViewLogIn scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        [tableViewSignUp scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
    else if (textField.tag == 74201) // sign in email
    {
        UITextField *_txtField = (UITextField*)[self.view viewWithTag:74202];
        [_txtField becomeFirstResponder];
    }
    else if (textField.tag == 74202) // sign in password
    {
        [textField resignFirstResponder];
        [tableViewLogIn scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
        [tableViewSignUp scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
    }
    
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if (textField.tag == 74101) // full name
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSCharacterSet *invalidCharSet = [[[NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "] invertedSet] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
        
        if (![string isEqualToString:filtered] || [string isEqualToString:filtered])
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            if (newLength<=30)
            {
                [dictionarySignUp setObject:text forKey:@"user_name"];
            }
            return (newLength >= 31) ? NO : YES;
        }
        else if (range.length == 1)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionarySignUp setObject:text forKey:@"user_name"];
            return YES;
        }
        else
        {
            return NO;
        }
    }
    
    else if (textField.tag == 74102) // sign up email
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=50)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionarySignUp setValue:text forKey:@"user_email"];
        }
        return (newLength >= 51) ? NO : YES;
    }
    
    else if (textField.tag == 74103) // sign up password
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=50)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionarySignUp setValue:text forKey:@"user_password"];
        }
        return (newLength >= 51) ? NO : YES;
    }
    
    else if (textField.tag == 74104) // sign up mobile number
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        
        NSString *_strPhoneNumber = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=15)
        {
            [dictionarySignUp setValue:_strPhoneNumber forKey:@"user_mobile_number"];
        }
        return (newLength >= 16) ? NO : YES;
    }
    
    else if (textField.tag == 74201) // sign in email
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=50)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryLogIn setValue:text forKey:@"user_email"];
        }
        return (newLength >= 51) ? NO : YES;
    }
    
    else if (textField.tag == 74202) // sign in password
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=50)
        {
            NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [dictionaryLogIn setValue:text forKey:@"user_password"];
        }
        return (newLength >= 51) ? NO : YES;
    }

    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self removeAllPickers];

    if (textField.tag == 74101) // full name
    {
        [tableViewSignUp scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74102) // sign up email
    {
        [tableViewSignUp scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74103) // sign up password
    {
        [tableViewSignUp scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74104) // sign up mobile number
    {
        [tableViewSignUp scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74201) // sign in email
    {
        [tableViewLogIn scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else if (textField.tag == 74202) // sign in password
    {
        [tableViewLogIn scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
}

#pragma mark
#pragma mark Alert Function
- (void) showAlertWithMessage : (NSString*)message
{
    [self.view makeToast:message duration:2.0 position:CSToastPositionBottom];
}


#pragma mark
#pragma mark Push To Next Controller
-(void)pushToNextController : (NSString*)isPhoneNumberVerified
{
    if ([isPhoneNumberVerified isEqualToString:@"1"] || [isPhoneNumberVerified integerValue] == 1)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
        LandingViewController *controller=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
        controller.isFromLogin = YES;
        
        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isLogin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        EnterOTPViewController *controller = [[EnterOTPViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(BOOL) isPasswordValid:(NSString *)pwd
{
    NSCharacterSet *upperCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyz"];
    
    if ( [pwd length]<6 )
        return NO;  // too long or too short
    NSRange rang;
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if ( !rang.length )
        return NO;  // no letter
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]];
    if ( !rang.length )
        return NO;  // no number;
    rang = [pwd rangeOfCharacterFromSet:upperCaseChars];
    if ( !rang.length )
        return NO;  // no uppercase letter;
    rang = [pwd rangeOfCharacterFromSet:lowerCaseChars];
    if ( !rang.length )
        return NO;  // no lowerCase Chars;
    return YES;
}


#pragma mark
#pragma mark func_SelectCountryCode
-(void)func_SelectCountryCode
{
    NSLog(@"func_SelectCountryCode");
    
    [self removeAllPickers];
    [self.view endEditing:YES];
    [tableViewSignUp scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    
    viewForCountryCodePicker = [UIFunction createUIViews:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0) bckgroundColor:[UIColor colorWithRed:214.0/255 green:240.0/255 blue:252.0/255 alpha:1.0]];
    [self.view addSubview:viewForCountryCodePicker];
    
    
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(doneButtonPressedInPickerView:)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor blackColor];
    [viewForCountryCodePicker addSubview:toolBar];

    
    
    
    UIPickerView *pickerCountryCode =  [[UIPickerView alloc]init];
    pickerCountryCode.frame = CGRectMake(0, toolBar.frame.size.height+toolBar.frame.origin.y, viewForCountryCodePicker.frame.size.width, 216.0);
    pickerCountryCode.delegate = self;
    pickerCountryCode.dataSource = self;
    pickerCountryCode.showsSelectionIndicator = YES;
    pickerCountryCode.backgroundColor = [UIColor clearColor];
    pickerCountryCode.tag = 480040;
    [viewForCountryCodePicker addSubview:pickerCountryCode];

    
    [UIView animateWithDuration:.2f animations:^{
        
        [self->viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height-260.0, self.view.frame.size.width, 260.0)];
        
    } completion:^(BOOL finished) {
        
        [self->dictionarySignUp setObject:[self->arrayAllCodes objectAtIndex:0] forKey:@"user_country_code"];
        
        [self->tableViewSignUp reloadData];
    }];
}

#pragma mark
#pragma mark Remove All Picker Views
-(void)doneButtonPressedInPickerView :(id)sender
{
    [self. view endEditing:YES];
    
    @try
    {
        [tableViewSignUp scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        
        [UIView animateWithDuration:.2f animations:^{
            
            [self->viewForCountryCodePicker setFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, 260.0)];
            
        } completion:^(BOOL finished) {
            
            [self removeAllPickers];
        }];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

-(void)removeAllPickers
{
    [viewForCountryCodePicker removeFromSuperview];
    viewForCountryCodePicker = nil;
}

#pragma mark
#pragma mark Picker View DataSource And Delegates Methods

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        return arrayAllCodes.count;
    }
    return 0;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        return 1;
    }
    return 0;
}

- (UIView*)pickerView:(UIPickerView *)thePickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (thePickerView.tag == 480040) // select user country and country code
    {
        UIView* viewForPicker = [[UIView alloc]init];
        viewForPicker.frame = CGRectMake(0, 0, self.view.frame.size.width, 45);
        viewForPicker.backgroundColor = [UIColor clearColor];
        
        UILabel *countryName = [[UILabel alloc]init];
        countryName.frame = CGRectMake(10, 5, 240, 40);
        countryName.textAlignment = NSTextAlignmentLeft;
        countryName.backgroundColor = [UIColor clearColor];
        countryName.text = [arrayAllCountries objectAtIndex:row];
        countryName.font = [UIFont fontWithName:fontRegular size:16];
        [viewForPicker addSubview:countryName];
        
        UILabel *countryCode = [[UILabel alloc]init];
        countryCode.frame = CGRectMake(self.view.frame.size.width-90, 5, 70, 40);
        countryCode.textAlignment = NSTextAlignmentRight;
        countryCode.font = [UIFont fontWithName:fontRegular size:16];
        countryCode.backgroundColor = [UIColor clearColor];
        countryCode.text = [arrayAllCodes objectAtIndex:row];
        [viewForPicker addSubview:countryCode];
        
        return viewForPicker;
    }
    
    return nil;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView.tag == 480040) // select user country and c ountry code
    {
        [dictionarySignUp setObject:[arrayAllCodes objectAtIndex:row] forKey:@"user_country_code"];
        [tableViewSignUp reloadData];
    }
}

#pragma mark
#pragma mark Request For OTP
- (void)requestForOTPAtNumber : (NSString*)phNumberWithCountryCode andCountryCode : (NSString*)countryCode
{
    [appDelegate() showActivityIndicatorWithColor:[UIColor whiteColor]];

    NSError *error = nil;
    id<SINPhoneNumber> phoneNumber = [SINPhoneNumberUtil() parse:phNumberWithCountryCode defaultRegion:countryCode error:&error];
    
    if (!phoneNumber)
    {
        [appDelegate() hideActivityIndicator];

        NSLog(@"error is %@",error.description);
        return;
    }
    
    NSString *phoneNumberInE164 = [SINPhoneNumberUtil() formatNumber:phoneNumber format:SINPhoneNumberFormatE164];
    id<SINVerification> verification = [SINVerification SMSVerificationWithApplicationKey:kSinchKey phoneNumber:phoneNumberInE164];
    
    [verification initiateWithCompletionHandler:^(id<SINInitiationResult> result, NSError *error)
     {
         [appDelegate() hideActivityIndicator];

         if (result.success)
         {
             [self pushToNextController:@"0"];
         }
         else
         {
             NSLog(@"error is %@",error.description);
             [self showAlertWithMessage:error.localizedDescription];
             [self pushToNextController:@"0"];
         }
     }];
}


- (void)askForMobileNumberRecovery
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Forgot your Email?\n\nEnter your Saved Mobile Phone to Access Your Account" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
        UIAlertAction *sendAction = [UIAlertAction
                                     actionWithTitle:@"OK"
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction *action)
                                     {
                                         GetMobilePasswordViewController *controller = [[GetMobilePasswordViewController alloc] init];
                                         controller.isFromForgotPassword = YES;
                                         [self.navigationController pushViewController:controller animated:YES];
                                     }];
    
        [alertController addAction:sendAction];
        [self presentViewController:alertController animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showPassword: (NSString*)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:message message:@"" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"OK"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   
                               }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}
    
- (void)openAltCalWebsite
{
    NSURL *url = [[NSURL alloc] initWithString:@"http://www.alt-cal.com/"];
   // [[UIApplication sharedApplication] openURL:url];
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];

}

@end
