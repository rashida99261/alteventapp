//
//  EditLaterViewController.h
//  Event App
//
//  Created by Reinforce on 02/12/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditLaterViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITableView *tableMyEvents;
@property (nonatomic, weak) IBOutlet UIView *lblStatus;

@end

NS_ASSUME_NONNULL_END
