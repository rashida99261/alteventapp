//
//  EditLaterViewController.m
//  Event App
//
//  Created by Reinforce on 02/12/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import "EditLaterViewController.h"
#import "editLaterTblCell.h"
#import "EventDetailsViewController.h"

@interface EditLaterViewController () <UITableViewDelegate, UITableViewDataSource>



@end

@implementation EditLaterViewController
{
    NSMutableArray *arraySaveLAter;
}

#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self getAllMyEvents];
}

-(void)getAllMyEvents
{
    NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
    NSMutableArray *arrayMyEvents = [[DataManager getSharedInstance]Select_MyEventsWithSaveLater:user_email];
    NSLog(@"______ %@",arrayMyEvents);
    
    
    arraySaveLAter = [[NSMutableArray alloc]init];
    for (int i = 0; i <= arrayMyEvents.count - 1 ; i++)
    {
            NSDictionary *dict = [arrayMyEvents objectAtIndex:i];
            NSString *event_save_later = [dict valueForKey:@"event_save_later"];
            
            if([event_save_later isEqualToString:@"1"]){
                [arraySaveLAter addObject:dict];
            }
    }
        
        if(arraySaveLAter.count > 0){
        
            [_lblStatus setHidden:true];
            [_tableMyEvents setHidden:false];
            
            [self sortEventsArray];
        }
        else{
            [_lblStatus setHidden:false];
            [_tableMyEvents setHidden:true];
        }
    
    
   
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}


-(void)sortEventsArray {
    
    NSMutableArray *arrDate = [[NSMutableArray alloc]init];
    NSMutableArray *arrTime = [[NSMutableArray alloc]init];
    NSMutableArray *arrNames = [[NSMutableArray alloc]init];

    if(arraySaveLAter.count > 0){
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM-dd-yyyy hh:mm a"];
        for (int i = 0; i <= arraySaveLAter.count - 1 ; i++)
        {
            NSDictionary *dict = [arraySaveLAter objectAtIndex:i];
            NSString *strDate = [dict valueForKey:@"event_date"];
            if([strDate length ]== 0){
                [arrNames addObject:dict];
            }
            else{
                [arrDate addObject:dict];
            }
        }
        
        NSArray *sortedDateArray = [arrDate sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
            NSString *strDate = [obj1 valueForKey:@"event_date"];
            NSString *strDate2 = [obj2 valueForKey:@"event_date"];
            
            NSString *strTime1 = [obj1 valueForKey:@"event_start_time"];
            NSString *strTime2 = [obj2 valueForKey:@"event_start_time"];
            
            NSString *strWholeDate = [NSString stringWithFormat:@"%@,%@", strDate,strTime1];
            NSString *strWholeDate2 = [NSString stringWithFormat:@"%@,%@", strDate2,strTime2];
            if([strDate isEqualToString:strDate2]){
                NSComparisonResult result = [strWholeDate caseInsensitiveCompare:strWholeDate2];
                return result == NSOrderedDescending;
                
            }
            NSComparisonResult result = [strWholeDate caseInsensitiveCompare:strWholeDate2];
            return result == NSOrderedDescending;
        }];
        
        arrDate = [sortedDateArray mutableCopy];
        NSArray *sortedNameArray = [arrNames sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
            
            NSString *event_name1 = [obj1 valueForKey:@"event_name"];
            NSString *event_name2 = [obj2 valueForKey:@"event_name"];
            NSComparisonResult result = [event_name1 caseInsensitiveCompare:event_name2];
            return result == NSOrderedDescending;
        }];
        
        arrNames = [sortedNameArray mutableCopy];
        NSArray *newArray=[arrDate arrayByAddingObjectsFromArray:arrNames];
        arraySaveLAter = [newArray mutableCopy];
        NSLog(@"main arr-- %@",arraySaveLAter);
        
         [_tableMyEvents reloadData];
    }
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    _tableMyEvents.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    
}
 
-(NSString*)changeDateFormatter : (NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    dateFormatter.dateFormat = @"MMM dd, yyyy";
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    NSString *convertedate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    return convertedate;
}


#pragma mark
#pragma mark dismissViewController
-(IBAction)backButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark
#pragma mark Table View Data Source and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arraySaveLAter.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"editLaterTblCell";
    
    editLaterTblCell *cell= [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    NSString *event_name = [NSString stringWithFormat:@"%@",[[arraySaveLAter objectAtIndex:indexPath.row] valueForKey:@"event_name"]];
    cell.lblEventName.text = event_name;
    
    NSString *event_start_time = [NSString stringWithFormat:@"%@",[[arraySaveLAter objectAtIndex:indexPath.row] valueForKey:@"event_start_time"]];
    if (event_start_time.length == 0)
    {
                    event_start_time = @"";
    }
    NSString *event_date = [NSString stringWithFormat:@"%@",[[arraySaveLAter objectAtIndex:indexPath.row] valueForKey:@"event_date"]];
    if (event_date.length == 0)
    {
        event_date = @"";
    }
     else
     {
         if (event_start_time.length > 0) {
    
             event_start_time = [NSString stringWithFormat:@"%@, ", event_start_time];
         }
    
         event_date = [self changeDateFormatter:event_date];
     }
    
    cell.lblEventDateTime.text = [NSString stringWithFormat:@"%@%@",event_start_time, event_date];
    
//                UIImageView *imgViewNext = [UIFunction createUIImageView:CGRectMake(frame.size.width-10-[UIImage imageNamed:@"next_black"].size.width, frame.size.height/2.0-[UIImage imageNamed:@"next_black"].size.height/2.0, [UIImage imageNamed:@"next_black"].size.width, [UIImage imageNamed:@"next_black"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"next_black"] isLogo:NO];
//                [cell.contentView addSubview:imgViewNext];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        EventDetailsViewController *controller = [[EventDetailsViewController alloc]init];
        controller.event_id = [NSString stringWithFormat:@"%@",[[arraySaveLAter objectAtIndex:indexPath.row] valueForKey:@"event_id"]];
        [self.navigationController pushViewController:controller animated:true];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        BOOL isDelete = [[DataManager getSharedInstance]deleteEventWithId:[NSString stringWithFormat:@"%@",[[arraySaveLAter objectAtIndex:indexPath.row] valueForKey:@"event_id"]]];
        if (isDelete == true)
        {
            [self getAllMyEvents];
        }
    }
}

@end
