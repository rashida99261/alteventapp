//
//  MyEventsViewController.m
//  Event App
//
//  Created by   on 21/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import "MyEventsViewController.h"
#import "EventDetailsViewController.h"
#import "LandingViewController.h"


@interface MyEventsViewController () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation MyEventsViewController
{
    UITableView *tableMyEvents;
    NSMutableArray *arrayMyEvents;
    
    NSMutableArray *arrDate;
    NSMutableArray *arrTime;
    NSMutableArray *arrNames;
}

#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self getAllMyEvents];
}

-(void)getAllMyEvents
{
    NSString *current_date = [UIFunction getCurrentDateInString];
    NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
    arrayMyEvents = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:true];
    NSLog(@"______ %@",arrayMyEvents);
    [self removePastEvents];
    [self sortEventsArray];
    [tableMyEvents reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    arrayMyEvents = [[NSMutableArray alloc]init];
    arrDate = [[NSMutableArray alloc]init];
    arrTime = [[NSMutableArray alloc]init];
    arrNames = [[NSMutableArray alloc]init];
    
    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake([UIScreen mainScreen].bounds.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [self.view addSubview:imgViewLogo];
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44,  44);
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    
    UILabel *lblHeader = [UIFunction createLable:CGRectMake(btnBack.frame.size.width+btnBack.frame.origin.x, 80, self.view.frame.size.width-2*(btnBack.frame.size.width+btnBack.frame.origin.x), 44) bckgroundColor:[UIColor clearColor] title:@"Your Events" font:[UIFont fontWithName:fontRegular size:18.0] titleColor:[UIColor whiteColor]];
    lblHeader.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblHeader];
    
    UIView *blackHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, lblHeader.frame.origin.y + lblHeader.frame.size.height + 10)];
    [blackHeader setBackgroundColor:[UIColor blackColor]];
    [self.view insertSubview:blackHeader atIndex:0];
    
    [tableMyEvents removeFromSuperview];
    tableMyEvents = nil;
    tableMyEvents = [self createTableView:CGRectMake(0, blackHeader.frame.size.height+blackHeader.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height-blackHeader.frame.size.height-blackHeader.frame.origin.y-10) backgroundColor:[UIColor clearColor]];
    [self.view addSubview:tableMyEvents];
    tableMyEvents.tag = 457548;
}


#pragma mark
#pragma mark dismissViewController
-(void)backButtonPressed
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createEventButtonPressed
{
    [self.view endEditing:YES];
    //LandingViewController *controller = [[LandingViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    LandingViewController *controller=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark
#pragma mark Create Table
-(UITableView*) createTableView : (CGRect)frame backgroundColor:(UIColor*)backgroundColor
{
    UITableView *_tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    _tableView.dataSource=self;
    _tableView.delegate = self;
    _tableView.backgroundColor=backgroundColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.alwaysBounceVertical = NO;
    [_tableView setAllowsSelection:YES];
    return _tableView;
}

#pragma mark
#pragma mark Table View Data Source and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (arrayMyEvents.count == 0)
    {
        return 1;
    }
    return arrayMyEvents.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (arrayMyEvents.count == 0)
    {
        return tableView.frame.size.height;
    }
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else
    {
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
    }
    CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
    
    @try
    {
        if (arrayMyEvents.count == 0)
        {
            UILabel *lblNoEvents = [UIFunction createLable:CGRectMake(10, frame.size.height/2.0-20.0, frame.size.width-20, 40) bckgroundColor:[UIColor clearColor] title:@"No Events yet!" font:[UIFont fontWithName:fontLight size:18.0] titleColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];
            lblNoEvents.textAlignment = NSTextAlignmentCenter;
            [self.view addSubview:lblNoEvents];
            
            UIButton *btnCreateEvent = [UIButton buttonWithType:UIButtonTypeCustom];
            btnCreateEvent.frame = CGRectMake((frame.size.width / 2) - 22, lblNoEvents.frame.origin.y + lblNoEvents.frame.size.height, 44,  44);
            [btnCreateEvent setImage:[UIImage imageNamed:@"create_event"] forState:UIControlStateNormal];
            [btnCreateEvent addTarget:self action:@selector(createEventButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:btnCreateEvent];
        }
        else
        {
            NSString *event_name = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_name"]];
            NSString *event_start_time = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_start_time"]];
            if (event_start_time.length == 0)
            {
                event_start_time = @"";
            }
            NSString *event_date = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_date"]];
            if (event_date.length == 0)
            {
                event_date = @"";
            }
            else
            {
                if (event_start_time.length > 0) {
                    
                    event_start_time = [NSString stringWithFormat:@"%@, ", event_start_time];
                }
                
                event_date = [self changeDateFormatter:event_date];
            }
           
            NSString *event_date_time = [NSString stringWithFormat:@"%@%@",event_start_time, event_date];
            
            
            UIView *viewDivider = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, 1) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
            [cell.contentView addSubview:viewDivider];
            
            
            UIImageView *imgViewNext = [UIFunction createUIImageView:CGRectMake(frame.size.width-10-[UIImage imageNamed:@"next_black"].size.width, frame.size.height/2.0-[UIImage imageNamed:@"next_black"].size.height/2.0, [UIImage imageNamed:@"next_black"].size.width, [UIImage imageNamed:@"next_black"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"next_black"] isLogo:NO];
            [cell.contentView addSubview:imgViewNext];
            
            
            if (arrayMyEvents.count-1 == indexPath.row)
            {
                UIView *viewDividerBottom = [UIFunction createUIViews:CGRectMake(0, frame.size.height-1, frame.size.width, 1) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
                [cell.contentView addSubview:viewDividerBottom];
            }
            
            CGFloat eventNameY = event_date_time.length == 0 ? 25 : 10;
            
            UILabel *lblEventName = [UIFunction createLable:CGRectMake(10, eventNameY, imgViewNext.frame.origin.x-20, 20) bckgroundColor:[UIColor clearColor] title:event_name font:[UIFont fontWithName:fontBold size:16.0] titleColor:[UIColor blackColor]];
            [cell.contentView addSubview:lblEventName];
            
            UILabel *lblEventDateTime = [UIFunction createLable:CGRectMake(lblEventName.frame.origin.x, lblEventName.frame.size.height+lblEventName.frame.origin.y, lblEventName.frame.size.width, lblEventName.frame.size.height) bckgroundColor:[UIColor clearColor] title:event_date_time font:[UIFont fontWithName:fontLight size:14.0] titleColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];
            [cell.contentView addSubview:lblEventDateTime];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.userInteractionEnabled = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (arrayMyEvents.count > 0)
    {
        EventDetailsViewController *controller = [[EventDetailsViewController alloc]init];
        controller.event_id = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_id"]];
        [self.navigationController pushViewController:controller animated:true];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        BOOL isDelete = [[DataManager getSharedInstance]deleteEventWithId:[NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_id"]]];
        if (isDelete == true)
        {
            [self getAllMyEvents];
        }
    }
}

-(NSString*)changeDateFormatter : (NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    dateFormatter.dateFormat = @"MMM dd, yyyy";
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    NSString *convertedate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    return convertedate;
}

- (void)removePastEvents
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSMutableArray *pastEvents = [[NSMutableArray alloc] init];
    pastEvents = arrayMyEvents;
    NSDate *today = [[NSDate alloc] init];
    NSString *todayString = [dateFormatter stringFromDate:today];
    NSDate *formattedTodayDate = [dateFormatter dateFromString:todayString];
    [arrayMyEvents enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        
        NSString *event_date = [NSString stringWithFormat:@"%@",[obj valueForKey:@"event_date"]];
        
        if ([event_date length] > 0) {
            
            NSDate *eventDate = [dateFormatter dateFromString:event_date];
            if (eventDate != nil) {
                
                if ([formattedTodayDate compare:eventDate] == NSOrderedDescending) {
                    [pastEvents removeObjectAtIndex:idx];
                }
            }
        }
    }];
    
    arrayMyEvents = pastEvents;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sortEventsArray {
    
    arrDate = [[NSMutableArray alloc]init];
    arrTime = [[NSMutableArray alloc]init];
    arrNames = [[NSMutableArray alloc]init];

    if(arrayMyEvents.count > 0){
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MM-dd-yyyy hh:mm a"];
        for (int i = 0; i <= arrayMyEvents.count - 1 ; i++)
        {
            NSDictionary *dict = [arrayMyEvents objectAtIndex:i];
            NSString *strDate = [dict valueForKey:@"event_date"];
            if([strDate length ]== 0){
                [arrNames addObject:dict];
            }
            else{
                [arrDate addObject:dict];
            }
        }
        
        NSArray *sortedDateArray = [arrDate sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
            NSString *strDate = [obj1 valueForKey:@"event_date"];
            NSString *strDate2 = [obj2 valueForKey:@"event_date"];
            
            NSString *strTime1 = [obj1 valueForKey:@"event_start_time"];
            NSString *strTime2 = [obj2 valueForKey:@"event_start_time"];
            
            NSString *strWholeDate = [NSString stringWithFormat:@"%@,%@", strDate,strTime1];
            NSString *strWholeDate2 = [NSString stringWithFormat:@"%@,%@", strDate2,strTime2];
            if([strDate isEqualToString:strDate2]){
                NSComparisonResult result = [strWholeDate caseInsensitiveCompare:strWholeDate2];
                return result == NSOrderedDescending;
                
            }
            NSComparisonResult result = [strWholeDate caseInsensitiveCompare:strWholeDate2];
            return result == NSOrderedDescending;
        }];
        
        arrDate = [sortedDateArray mutableCopy];
        NSArray *sortedNameArray = [arrNames sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
            
            NSString *event_name1 = [obj1 valueForKey:@"event_name"];
            NSString *event_name2 = [obj2 valueForKey:@"event_name"];
            NSComparisonResult result = [event_name1 caseInsensitiveCompare:event_name2];
            return result == NSOrderedDescending;
        }];
        
        arrNames = [sortedNameArray mutableCopy];
        NSArray *newArray=[arrDate arrayByAddingObjectsFromArray:arrNames];
        arrayMyEvents = [newArray mutableCopy];
        NSLog(@"main arr-- %@",arrayMyEvents);
    }
}
@end
