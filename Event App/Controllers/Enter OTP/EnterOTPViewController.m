//
//  EnterOTPViewController.m
//  Event App
//
//  Created by   on 17/03/2017.
//  Copyright © 2017. All rights reserved.
//

#import "EnterOTPViewController.h"
#import "LandingViewController.h"

@interface EnterOTPViewController () <UITextFieldDelegate>

@end

@implementation EnterOTPViewController
{
    NSString *enteredOTP;
    UITextField *txtField;
}
#pragma mark
#pragma mark View Will Appear and Disapper
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
    
    [txtField becomeFirstResponder];
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"     ");
    self.view.backgroundColor = [UIColor colorWithRed:0.0/255 green:0.0/255 blue:0.0/255 alpha:1.0];
    enteredOTP = @"";
    
//    UIButton *btnBack = [UIFunction createButton:CGRectMake(20, 20, [UIImage imageNamed:@"back_white"].size.width,  [UIImage imageNamed:@"back_white"].size.height) bckgroundColor:[UIColor clearColor] image: [UIImage imageNamed:@"back_white"] title:nil font:nil titleColor:nil];
//    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnBack];
    
    UIButton *btnBack = [UIButton buttonWithType: UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44, 44);
    btnBack.backgroundColor = [UIColor clearColor];
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];


    UILabel *lblEnterOTP = [UIFunction createLable:CGRectMake(10, 100, self.view.frame.size.width - 20, 60) bckgroundColor:[UIColor clearColor] title:@"Enter Your AltCal\nVerification" font:[UIFont fontWithName:fontBold size:16.0] titleColor:[UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0]];
    lblEnterOTP.numberOfLines = 2;
    lblEnterOTP.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblEnterOTP];

    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    
    UIView *viewEnterOTP = [UIFunction createUIViews:CGRectMake(lblEnterOTP.frame.origin.x, lblEnterOTP.frame.size.height+lblEnterOTP.frame.origin.y+5, 100, 35) bckgroundColor:[UIColor clearColor]];
    viewEnterOTP.layer.cornerRadius = 5.0;
    viewEnterOTP.layer.borderWidth = 1.0;
    viewEnterOTP.layer.borderColor = [UIColor whiteColor].CGColor;
    viewEnterOTP.center = CGPointMake(screenWidth/2, viewEnterOTP.center.y);
    [self.view addSubview:viewEnterOTP];

    
    txtField = [[UITextField alloc]initWithFrame:CGRectMake(5, 0, viewEnterOTP.frame.size.width-10, viewEnterOTP.frame.size.height)];
    txtField.tag = 74300;
    txtField.font = [UIFont fontWithName:fontRegular size:16];
    txtField.textColor = [UIColor colorWithRed:255.0/255 green:255.0/255 blue:255.0/255 alpha:1.0];
    txtField.delegate = self;
    txtField.text = enteredOTP;
    txtField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    txtField.textAlignment = NSTextAlignmentCenter;
    txtField.returnKeyType = UIReturnKeyNext;
    txtField.keyboardType = UIKeyboardTypeNumberPad;
    txtField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    txtField.autocorrectionType = UITextAutocorrectionTypeNo;
    [viewEnterOTP addSubview:txtField];
    UIToolbar *toolBar= [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    [toolBar setBarStyle:UIBarStyleDefault];
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    UIBarButtonItem *barButtonDone = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                      style:UIBarButtonItemStylePlain
                                                                     target:self
                                                                     action:@selector(dismissKeyboard)];
    toolBar.items = @[flex, barButtonDone];
    barButtonDone.tintColor = [UIColor blackColor];
    txtField.inputAccessoryView = toolBar;
    
    UIButton *btnResendCode = [UIFunction createButton:CGRectMake(viewEnterOTP.frame.origin.x, viewEnterOTP.frame.origin.y+viewEnterOTP.frame.size.height+10, viewEnterOTP.frame.size.width, viewEnterOTP.frame.size.height) bckgroundColor:[UIColor clearColor] image: nil title:@"Send Again" font: [UIFont fontWithName:fontRegular size:16.0] titleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255 alpha:1.0]];
    [btnResendCode addTarget:self action:@selector(resendButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    btnResendCode.center = CGPointMake(screenWidth/2, btnResendCode.center.y);
    [self.view addSubview:btnResendCode];
    
    UIButton *btnDone = [UIFunction createButton:CGRectMake(btnResendCode.frame.origin.x+10, btnResendCode.frame.origin.y+btnResendCode.frame.size.height, btnResendCode.frame.size.width-20, 30) bckgroundColor:[UIColor whiteColor] image: nil title:@"LOGIN" font: [UIFont fontWithName:fontRegular size:16.0] titleColor:[UIColor colorWithRed:0.0/255.0 green:0.0/255.0 blue:0.0/255 alpha:1.0]];
    [btnDone addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    btnDone.layer.cornerRadius = 5.0;
    [self.view addSubview:btnDone];

}

#pragma mark
#pragma mark dismissViewController
-(void)backButtonPressed
{
    [self dismissKeyboard];
    [appDelegate() functionToHandleInvalidAccessToken];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

-(void)resendButtonPressed
{
    NSLog(@"resend Button Pressed");
    NSMutableDictionary *user_data = [[NSMutableDictionary alloc]init];
    user_data = [[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData]mutableCopy];
    
    NSString *user_mobile_number = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_mobile_number"]];
    NSString *user_country_code = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_country_code"]];
    NSString *phNumberWithCountryCode = [NSString stringWithFormat:@"%@%@",user_country_code,user_mobile_number];
    [self requestForOTPAtNumber:phNumberWithCountryCode andCountryCode:user_country_code];
}

-(void)doneButtonPressed
{
    NSLog(@"done button pressed");
    
    [self.view endEditing:YES];
    
    enteredOTP = [enteredOTP stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if (enteredOTP.length < 4)
    {
        [self showAlertWithMessage:@"Please enter a valid code."];
    }
//    else if ([enteredOTP isEqualToString:@"1111"])
//    {
//        [self successBlockOfROP];
//    }
    else
    {
        NSMutableDictionary *user_data = [[NSMutableDictionary alloc]init];
        user_data = [[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData]mutableCopy];
        
        NSString *user_mobile_number = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_mobile_number"]];
        NSString *user_country_code = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_country_code"]];
        NSString *phNumberWithCountryCode = [NSString stringWithFormat:@"%@%@",user_country_code,user_mobile_number];
        [self verifyOTPForPhoneNumber:phNumberWithCountryCode andCountryCode:user_country_code andOTP:enteredOTP];
    }
}


#pragma mark
#pragma mark TextField Delegate
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    if (textField.tag == 74300)
    {
        enteredOTP = @"";
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{
    if (textField.tag == 74300)
    {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        if ([string rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location != NSNotFound)
        {
            return NO;
        }
        
        NSString *code = [textField.text stringByReplacingCharactersInRange:range withString:string];
        NSInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength<=4)
        {
            enteredOTP = code;
        }
        return (newLength >= 5) ? NO : YES;
    }
    
    return YES;
}


#pragma mark
#pragma mark Alert Function
- (void) showAlertWithMessage : (NSString*)message
{
    [self.view makeToast:message duration:2.0 position:CSToastPositionBottom];
}



#pragma mark
#pragma mark Sinch Verification
- (void)requestForOTPAtNumber : (NSString*)phNumberWithCountryCode andCountryCode : (NSString*)countryCode
{
    [appDelegate() showActivityIndicatorWithColor:[UIColor whiteColor]];
    
    NSError *error = nil;
    id<SINPhoneNumber> phoneNumber = [SINPhoneNumberUtil() parse:phNumberWithCountryCode defaultRegion:countryCode error:&error];
    
    if (!phoneNumber)
    {
        [appDelegate() hideActivityIndicator];
        
        NSLog(@"error is %@",error.description);
        return;
    }
    
    NSString *phoneNumberInE164 = [SINPhoneNumberUtil() formatNumber:phoneNumber format:SINPhoneNumberFormatE164];
    id<SINVerification> verification = [SINVerification SMSVerificationWithApplicationKey:kSinchKey phoneNumber:phoneNumberInE164];
    
    [verification initiateWithCompletionHandler:^(id<SINInitiationResult> result, NSError *error)
     {
         [appDelegate() hideActivityIndicator];
         
         if (result.success)
         {
             [self showAlertWithMessage:@"We have sent you a code. Please check your phone number."];
         }
         else
         {
             NSLog(@"error is %@",error.description);
             [self showAlertWithMessage:error.localizedDescription];
         }
     }];
}


-(void)verifyOTPForPhoneNumber : (NSString*)phNumberWithCountryCode andCountryCode : (NSString*)countryCode andOTP:(NSString*)otpCode
{
    [appDelegate() showActivityIndicatorWithColor:[UIColor whiteColor]];
    NSError *error = nil;
    id<SINPhoneNumber> phoneNumber = [SINPhoneNumberUtil() parse:phNumberWithCountryCode defaultRegion:countryCode error:&error];
    
    if (!phoneNumber)
    {
        [appDelegate() hideActivityIndicator];
        NSLog(@"error is %@",error.description);
        return;
    }
    
    NSString *phoneNumberInE164 = [SINPhoneNumberUtil() formatNumber:phoneNumber format:SINPhoneNumberFormatE164];
    id<SINVerification> verification = [SINVerification SMSVerificationWithApplicationKey:kSinchKey phoneNumber:phoneNumberInE164];
    [verification verifyCode:otpCode completionHandler:^(BOOL success, NSError* error)
     {
         [appDelegate() hideActivityIndicator];
         
         if (success)
         {
             NSLog(@"Verification Successful");
             [self successBlockOfROP];
         }
         else
         {
             [self showAlertWithMessage:@"Please enter a valid code."];
         }
     }];
}


-(void)successBlockOfROP
{
    @try
    {
        NSMutableDictionary *user_data = [[NSMutableDictionary alloc]init];
        user_data = [[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData]mutableCopy];
        [user_data setValue:@"1" forKey:@"isPhoneNumberVerified"];
        
        NSString *user_name = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_name"]];
        NSString *user_email = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_email"]];
        NSString *user_password = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_password"]];
        NSString *user_mobile_number = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_mobile_number"]];
        NSString *user_id = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_id"]];
        NSString *user_country_code = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_country_code"]];
        
        NSString *user_sign_up_date = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_sign_up_date"]];
        NSString *user_login_date = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_login_date"]];
        NSString *user_city = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_city"]];
        NSString *user_latitude = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_latitude"]];
        NSString *user_longitude = [NSString stringWithFormat:@"%@",[user_data valueForKey:@"user_longitude"]];
        
        BOOL isUpdateSuccess = [[DataManager getSharedInstance]Update_UserDataBaseTable:user_id :user_name :user_email :user_password :user_mobile_number :@"1" :user_country_code :user_sign_up_date :user_login_date :user_city :user_latitude :user_longitude];
                                
         if (isUpdateSuccess == YES)
        {
            [[NSUserDefaults standardUserDefaults]setObject:user_data forKey:kLoginData];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isLogin"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
            LandingViewController *controller=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
            controller.isFromLogin = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else
        {
            [self showAlertWithMessage:@"Unable to update data. Please try again later."];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@", exception.description);
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
