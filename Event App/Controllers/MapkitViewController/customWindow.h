//
//  customWindow.h
//  Event App
//
//  Created by Rashida on 08/11/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface customWindow : UIView

@property (nonatomic, weak) IBOutlet UIView *viewInner;

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;
@property (nonatomic, weak) IBOutlet UILabel *lblSubtitle;
@property (nonatomic, weak) IBOutlet UIImageView *imgMain;

@property (nonatomic, weak) IBOutlet UIButton *btnFrwd;
@property (nonatomic, weak) IBOutlet UIButton *btnBack;
@property (nonatomic, weak) IBOutlet UIButton *btnWebsite;
@property (nonatomic, weak) IBOutlet UIButton *btnCall;
@property (nonatomic, weak) IBOutlet UIButton *btnInsta;
@property (nonatomic, weak) IBOutlet UIButton *btnMore;
@property (nonatomic, weak) IBOutlet UIButton *btnCross;


@end

NS_ASSUME_NONNULL_END
