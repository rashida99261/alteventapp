//
//  MapkitViewController.h
//  Event App
//
//  Created by Reinforce on 03/10/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SBSliderView.h"

NS_ASSUME_NONNULL_BEGIN

@interface MapkitViewController : UIViewController<SBSliderDelegate>


@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation* currentLocation;
@property (nonatomic, strong) NSString *event_place_id;

@property (strong, nonatomic) NSMutableArray *matchingItems;

@property (nonatomic, weak) IBOutlet GMSMapView *mapView;
@property (nonatomic, weak) IBOutlet UISearchBar *searchAddress;
@property (nonatomic, weak) IBOutlet UITableView *tableViewSearch;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *hgtTopView;

@property (nonatomic, weak) IBOutlet UIView *viewPhotos;

@property BOOL markerTapped;
@property BOOL cameraMoving;
@property BOOL idleAfterMovement;
@property (strong, nonatomic) GMSMarker *currentlyTappedMarker;

@end

NS_ASSUME_NONNULL_END
