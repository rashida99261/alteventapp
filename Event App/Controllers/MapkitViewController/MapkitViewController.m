//
//  MapkitViewController.m
//  Event App
//
//  Created by Reinforce on 03/10/19.
//  Copyright © 2019 Applify Tech Pvt Ltd. All rights reserved.
//

#import "MapkitViewController.h"
#import "LandingViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
//#import "addressTblCell.h"

#import "SPGooglePlacesAutocompleteQuery.h"
#import "SPGooglePlacesAutocompletePlace.h"
#import <SDWebImage/UIImageView+WebCache.h>
//#import <HCSStarRatingView/HCSStarRatingView.h>

#import "customWindow.h"



@interface MapkitViewController ()<MKMapViewDelegate, UITextFieldDelegate,MKLocalSearchCompleterDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,UITableViewDelegate, UITableViewDataSource>



@end

//MKMapView *mapView;

NSMutableArray *arrRf;
NSArray *arraySearch;
BOOL _firstLocationUpdate;
GMSPlacesClient *_placesClient;
int count;

SPGooglePlacesAutocompleteQuery *searchQuery;

GMSPlace *markerPlace;
customWindow *customWindowInfo;






@implementation MapkitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
//    [self getLocationFromAddressString:@""];
    
    self.markerTapped = NO;
    self.cameraMoving = NO;
    self.idleAfterMovement = NO;
    
    count = 0;
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    _searchAddress.returnKeyType = UIKeyboardTypeDefault;
    _searchAddress.barTintColor = [UIColor blackColor];
    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setTextColor:[UIColor whiteColor]];


    [_tableViewSearch setHidden:TRUE];
   // mapView = [[MKMapView alloc]
                      // initWithFrame:CGRectMake(0,64,self.view.bounds.size.width,self.view.bounds.size.height)];
  //  mapView.showsUserLocation = YES;
  //  mapView.mapType = MKMapTypeStandard;
  //  mapView.delegate = self;
    
    
    // [navbar setBarTintColor:[UIColor lightGrayColor]];
    // [view bringSubviewToFront:mapView];
    
   
   
    
   // mapView = [GMSMapView mapWithFrame:CGRectMake(0, 65, self.view.frame.size.width, self.view.frame.size.height) camera:camera];
    _mapView.delegate = self;
    _mapView.settings.compassButton = YES;
    _mapView.settings.myLocationButton = YES;
    
    
    
    
     [_viewPhotos setHidden:YES];
    
    
    
    // Listen to the myLocation property of GMSMapView.
//    self.view = mapView;
    
    [_mapView addObserver:self
              forKeyPath:@"myLocation"
                 options:NSKeyValueObservingOptionNew
                 context:NULL];
    
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        self->_mapView.myLocationEnabled = YES;
    });
    
    
    
   // NSString *placeId = [[NSUserDefaults standardUserDefaults] valueForKey:@"placeId"];
    if ([_event_place_id length] > 0){
        [self setDataonMApUSingPlaceId:_event_place_id:0];
     }
     else{
         if ([CLLocationManager locationServicesEnabled] )
         {
             if (self.locationManager == nil )
             {
                 self.locationManager = [[CLLocationManager alloc] init];
                 self.locationManager.delegate = self;
                 self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
                 self.locationManager.distanceFilter = 50; //kCLDistanceFilterNone// kDistanceFilter;
             }
             [self.locationManager startUpdatingLocation];
         }
     }
    
    
    _placesClient = [[GMSPlacesClient alloc] init];
    //UIBarButtonItem* doneBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(onTapDone:)];
    //navItem.rightBarButtonItem = doneBtn;
    
}

-(void)onTapCancel:(UIBarButtonItem*)item{
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    self.currentLocation = [locations lastObject];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:self.currentLocation.coordinate.latitude
                                                            longitude:self.currentLocation.coordinate.longitude
                                                                 zoom:15];
    [_mapView animateToCameraPosition:camera];
    
    _mapView.myLocationEnabled = YES;

    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *styleUrl = [mainBundle URLForResource:@"style" withExtension:@"json"];
    NSError *error;

        // Set the map style by passing the URL for style.json.
    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];

    if (!style) {
          NSLog(@"The style definition could not be loaded: %@", error);
    }

    _mapView.mapStyle = style;
    
    
    [self.locationManager stopUpdatingLocation];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(self.currentLocation.coordinate.latitude, self.currentLocation.coordinate.longitude);
    marker.icon = [UIImage imageNamed:@"search_location"];
    marker.map = _mapView;
    
    
}


#pragma mark - KVO updates

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!_firstLocationUpdate) {
        // If the first location update has not yet been received, then jump to that
        // location.
        _firstLocationUpdate = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:12];
    }
}


#pragma mark
#pragma mark Search Delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar1
{
    [searchBar1 resignFirstResponder];
}

//- (void)searchBarTextDidEndEditing:(UISearchBar *)aSearchBar
//{
//        [aSearchBar resignFirstResponder];
//        NSArray *newArray = [[NSArray alloc]init];
//        arraySearch = newArray;
//        [self->_hgtTopView setConstant:54];
//        [self->_tableViewSearch reloadData];
//}


-(void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText
{
    
    @try
    {
        
        NSString *trimmedString = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if (trimmedString.length>0)
        {
            searchQuery = [[SPGooglePlacesAutocompleteQuery alloc] init];
            searchQuery.radius = 100.0;
           // shouldBeginEditing = YES;
            searchQuery.location = self.currentLocation.coordinate;
            searchQuery.input = searchText;
            [searchQuery fetchPlaces:^(NSArray *places, NSError *error) {
                if (error) {
                   // SPPresentAlertViewWithErrorAndTitle(error, @"Could not fetch Places");
                    NSLog(@"Autocomplete error %@", [error localizedDescription]);
                } else {
                   // [arraySearch release];
                    arraySearch = [[NSArray alloc]init];
                    NSLog(@"places--- %@", places);
                    arraySearch = places;
                     NSLog(@"arraySearch--- %@", arraySearch);
                    [self->_hgtTopView setConstant:200];
                    self->_tableViewSearch.delegate = self;
                    self->_tableViewSearch.dataSource = self;
                    [self->_tableViewSearch setHidden:FALSE];
                    [self->_tableViewSearch reloadData];
                }
            }];
            
           // [self->_tableViewSearch reloadData];
        }
        else{
            
            
            NSArray *newArray = [[NSArray alloc]init];
            arraySearch = newArray;
            
            [self->_tableViewSearch reloadData];
            [self->_hgtTopView setConstant:54];
            [_tableViewSearch setHidden:TRUE];
            [_mapView clear];
            [_searchAddress resignFirstResponder];
            
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"searchBarCancelButtonClicked");
   // [self removeSearchView];
}

#pragma mark
#pragma mark Table View Data Source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"arraySearch.count--- %lu", (unsigned long)arraySearch.count);
    return arraySearch.count;
}

- (SPGooglePlacesAutocompletePlace *)placeAtIndexPath:(NSIndexPath *)indexPath {
    return [arraySearch objectAtIndex:indexPath.row];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"addressTblCell";
        UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        NSTextAttachment *imageAttachment = [[NSTextAttachment alloc] init];
        imageAttachment.image = [UIImage imageNamed:@"location"];
        CGFloat imageOffsetY = -5.0;
        imageAttachment.bounds = CGRectMake(0, imageOffsetY, 18, 18);
        NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:imageAttachment];
        NSMutableAttributedString *completeText= [[NSMutableAttributedString alloc] initWithString:@""];
        [completeText appendAttributedString:attachmentString];
        
        NSString *strName = [NSString stringWithFormat: @"    %@", [self placeAtIndexPath:indexPath].name];
        NSMutableAttributedString *textAfterIcon= [[NSMutableAttributedString alloc] initWithString:strName];
        [completeText appendAttributedString:textAfterIcon];
        cell.textLabel.attributedText = completeText;
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:13];
        cell.textLabel.numberOfLines = 2;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    //}
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  //  SPGooglePlacesAutocompletePlace *place = [self placeAtIndexPath:indexPath];
    
    _searchAddress.text = [self placeAtIndexPath:indexPath].name;
    NSString *placeId = [self placeAtIndexPath:indexPath].reference;
    
    [[NSUserDefaults standardUserDefaults] setValue:placeId forKey:@"placeId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [self setDataonMApUSingPlaceId:placeId :indexPath];
    
    
   
  
}

-(void)setDataonMApUSingPlaceId: (NSString *)strPlaceId :(NSIndexPath *)indexPath
{
     // Specify the place data types to return.
        GMSPlaceField fields = (GMSPlaceFieldName | GMSPlaceFieldPlaceID | GMSPlaceFieldAll);
        
        [_placesClient fetchPlaceFromPlaceID:strPlaceId placeFields:fields sessionToken:nil callback:^(GMSPlace * _Nullable place, NSError * _Nullable error) {
            if (error != nil) {
                
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error" message:@"QUERY LIMITED" preferredStyle:UIAlertControllerStyleAlert];
                
                NSString *actionTitle = @"OK";
                UIAlertAction *okAction = [UIAlertAction
                                           actionWithTitle:actionTitle
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                           }];
                
                [alertController addAction:okAction];
                [self presentViewController:alertController animated:YES completion:nil];
                
                
                return;
            }
            if (place != nil) {
                
                NSLog(@"place is: %@", place);
                NSLog(@"The selected place is: %@", [place name]);
                
                
                NSLog(@"The selected place address is: %@", [place formattedAddress]);
                
                
                
                for (int i = 0; i < [place.addressComponents count]; i++)
                {
                    NSLog(@"name %@ = type %@", place.addressComponents[i].name, place.addressComponents[i].types);
                    NSArray *dictTyp = place.addressComponents[i].types;
                     NSLog(@"frst object: %@", dictTyp.firstObject);
                    
                    if([dictTyp.firstObject  isEqual: @"street_number"])
                    {
                        NSLog(@"val %@", place.addressComponents[i].name);
                        
                        [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_address1"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                    }
                    
                    if([dictTyp.firstObject  isEqual: @"route"])
                    {
                        NSLog(@"val %@", place.addressComponents[i].name);
                        [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_address2"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                        
    //                if([dictTyp.firstObject  isEqual: @"sublocality_level_1"])
    //                {
    //                    NSLog(@"val %@", place.addressComponents[i].name);
    //                }
    //
    //                if([dictTyp.firstObject  isEqual: @"locality"])
    //                {
    //                    NSLog(@"val %@", place.addressComponents[i].name);
    //                }
                    
                    if([dictTyp.firstObject  isEqual: @"locality"])
                    {
                        NSLog(@"val %@", place.addressComponents[i].name);
                        [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_city"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                        
                   if([dictTyp.firstObject  isEqual: @"administrative_area_level_1"])
                   {
                       NSLog(@"val %@", place.addressComponents[i].name);
                       [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].shortName forKey:@"map_state"];
                       [[NSUserDefaults standardUserDefaults] synchronize];
                       
                       
                   }
                    
    //                if([dictTyp.firstObject  isEqual: @"country"])
    //                 {
    //                     NSLog(@"val %@", place.addressComponents[i].name);
    //
    //                    [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_zipcode"];
    //                    [[NSUserDefaults standardUserDefaults] synchronize];
    //                 }
                         
                    if([dictTyp.firstObject  isEqual: @"postal_code"])
                    {
                        NSLog(@"val %@", place.addressComponents[i].name);
                        [[NSUserDefaults standardUserDefaults] setValue:place.addressComponents[i].name forKey:@"map_zipcode"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                                
                    }
                    
                }
                
                
                
                
                CLLocationCoordinate2D coord = [place coordinate];
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:coord.latitude
                                                                        longitude:coord.longitude
                                                                             zoom:15];
                [self->_mapView animateToCameraPosition:camera];
                
                   
                   self->_mapView.myLocationEnabled = YES;

                   NSBundle *mainBundle = [NSBundle mainBundle];
                   NSURL *styleUrl = [mainBundle URLForResource:@"style" withExtension:@"json"];
                   NSError *error;

                       // Set the map style by passing the URL for style.json.
                   GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];

                   if (!style) {
                         NSLog(@"The style definition could not be loaded: %@", error);
                   }

                   self->_mapView.mapStyle = style;
                
                
                GMSMarker *marker = [[GMSMarker alloc] init];
                marker.position = CLLocationCoordinate2DMake(coord.latitude, coord.longitude);
                marker.map = self->_mapView;
                marker.infoWindowAnchor = CGPointMake(0.5, 0);
                marker.icon = [UIImage imageNamed:@"search_location"];
                
                self->_mapView.delegate = self;
                markerPlace = place;
                [self.tableViewSearch deselectRowAtIndexPath:indexPath animated:NO];
                arraySearch = nil;
                [self->_hgtTopView setConstant:54];
                [self->_tableViewSearch setHidden:TRUE];
                [self->_tableViewSearch reloadData];
            }
        }];
        
}


- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    UIView *view = [[UIView alloc] init];
    view.frame = CGRectMake(0, 0, 280, 40);
    view.backgroundColor = [UIColor colorWithRed:0.5 green:0.8 blue:0.4 alpha:1.0];
    
    return view;
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    
    // A marker has been tapped, so set that state flag
    self.markerTapped = YES;
    
    // If a marker has previously been tapped and stored in currentlyTappedMarker, then nil it out
    if(self.currentlyTappedMarker) {
        self.currentlyTappedMarker = nil;
    }
    
    // make this marker our currently tapped marker
    self.currentlyTappedMarker = marker;
    
    // if our custom info window is already being displayed, remove it and nil the object out
    if([customWindowInfo isDescendantOfView:self.view]) {
        [customWindowInfo removeFromSuperview];
        customWindowInfo = nil;
    }
    
    /* animate the camera to center on the currently tapped marker, which causes
     mapView:didChangeCameraPosition: to be called */
    GMSCameraUpdate *cameraUpdate = [GMSCameraUpdate setTarget:marker.position];
    [self.mapView animateWithCameraUpdate:cameraUpdate];
    
    return YES;
}


- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    /* if we got here after we've previously been idle and displayed our custom info window,
     then remove that custom info window and nil out the object */
    if(self.idleAfterMovement) {
        if([customWindowInfo isDescendantOfView:self.view]) {
            [customWindowInfo removeFromSuperview];
            customWindowInfo = nil;
        }
    }
    
    // if we got here after a marker was tapped, then set the cameraMoving state flag to YES
    if(self.markerTapped) {
        self.cameraMoving = YES;
    }
}


- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    
    /* if we got here and a marker was tapped and our animate method was called, then it means we're ready
     to show our custom info window */
    if(self.markerTapped && self.cameraMoving) {
        
        // reset our state first
        self.cameraMoving = NO;
        self.markerTapped = NO;
        self.idleAfterMovement = YES;
        
        // create our custom info window UIView and set the color to blueish
        
        customWindowInfo = [[[NSBundle mainBundle] loadNibNamed:@"customWindow" owner:self options:nil] firstObject];
        customWindowInfo.viewInner.layer.borderColor = [UIColor blackColor].CGColor;
        customWindowInfo.viewInner.layer.borderWidth = 1.5;
        
        CGPoint markerPoint = [self.mapView.projection pointForCoordinate:self.currentlyTappedMarker.position];
        customWindowInfo.frame = CGRectMake(20, markerPoint.y-120, customWindowInfo.frame.size.width, customWindowInfo.frame.size.height);

        
        customWindowInfo.lblTitle.textAlignment = NSTextAlignmentLeft;
        customWindowInfo.lblTitle.font = [UIFont fontWithName:fontBold size:16.0];
        // lblTitle.numberOfLines = 0;
        //  lblTitle.lineBreakMode = NSLineBreakByWordWrapping;
        customWindowInfo.lblTitle.text = [markerPlace name];
        // customWindowInfo newFrame = lblTitle.frame;
        // newFrame.size.height = [self getLabelHeight:lblTitle];
        // lblTitle.frame = newFrame;
        
        
        customWindowInfo.lblSubtitle.textAlignment = NSTextAlignmentLeft;
        customWindowInfo.lblSubtitle.font = [UIFont fontWithName:fontBold size:14.0];
        customWindowInfo.lblSubtitle.text = [markerPlace formattedAddress];
        //    CGRect newFrameT = lblSubTitle.frame;
        //    newFrameT.size.height = [self getLabelHeight:lblSubTitle];
        //    lblSubTitle.frame = newFrameT;
        //    [viewBase addSubview:lblSubTitle];
        
        arrRf = [[NSMutableArray alloc]init];
        for(NSDictionary *dict in [markerPlace photos])
        {
            NSString *ref = [dict valueForKey:@"reference"];
            [arrRf addObject:ref];
        }
        
        
        if([[markerPlace phoneNumber] length] > 0)
        {
            [customWindowInfo.btnCall setHidden:NO];
            [customWindowInfo.btnCall addTarget:self action:@selector(clickOnCall:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        
        if([[markerPlace website].absoluteString length] > 0)
        {
            [customWindowInfo.btnWebsite setHidden:NO];
            [customWindowInfo.btnWebsite addTarget:self action:@selector(clickOnUrl:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        if(arrRf.count > 0){
            NSString *refFrst = [arrRf objectAtIndex:0];
            NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=%@&key=%@",refFrst,kGoogleAPIKey];
            [customWindowInfo.imgMain setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@""] options:nil usingActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
            
            [customWindowInfo.btnFrwd addTarget:self action:@selector(clickOnFrwd:) forControlEvents:UIControlEventTouchUpInside];
            [customWindowInfo.btnBack addTarget:self action:@selector(clickOnBack:) forControlEvents:UIControlEventTouchUpInside];
            
        }

        
        
        
      [customWindowInfo.btnCross addTarget:self action:@selector(clickOnCrossBtn:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:customWindowInfo];
    }
}

-(void)clickOnCrossBtn: (UIButton*) sender
{
    [customWindowInfo removeFromSuperview];
     customWindowInfo = nil;
}



-(void)clickOnUrl: (UIButton*) sender
{
    [[UIApplication sharedApplication] openURL:[markerPlace website] options:@{} completionHandler:nil];

}


-(void)clickOnCall: (UIButton*) sender
{
    NSString *strPnoneno = [NSString stringWithFormat:@"tel:%@",[markerPlace phoneNumber]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:strPnoneno]];
    
    
}


-(void)clickOnFrwd: (UIButton *) sender
{
    if(count < arrRf.count - 1)
     {
         count += 1;
          NSString *ref = [arrRf objectAtIndex:count];
          //NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=%@&key=AIzaSyAQEHGmGiw2luwH3VHcbx_CME0MbHCBHGQ",ref];
         NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=%@&key=%@",ref,kGoogleAPIKey];
        // customWindowInfo.imgMain.backgroundColor = [UIColor redColor];
         [customWindowInfo.imgMain setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@""] options:nil usingActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
         
     }
}


-(void)clickOnBack: (UIButton *) sender
{
    if(count > 0)
    {
        count = count-1;
        if(count == 0){
        }
        else{
            NSString *ref = [arrRf objectAtIndex:count];
            //NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=%@&key=AIzaSyAQEHGmGiw2luwH3VHcbx_CME0MbHCBHGQ",ref];
            NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?maxwidth=200&photoreference=%@&key=%@",ref,kGoogleAPIKey];
         //   customWindowInfo.imgMain.backgroundColor = [UIColor yellowColor];
            [customWindowInfo.imgMain setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:@""] options:nil usingActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
        }
        
    }
}


- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    return size.height;
}

-(IBAction)clickOnSaveBtn:(id)sender
{
    if(_searchAddress.text.length > 0)
    {
        
        [[NSUserDefaults standardUserDefaults] setValue:[markerPlace name] forKey:@"place_from_map"];
        [[NSUserDefaults standardUserDefaults] synchronize];
                
        [self.navigationController popViewControllerAnimated:TRUE];
    }
    else{
        [self.navigationController popViewControllerAnimated:TRUE];
    }
    
}


-(IBAction)clickOnBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}

@end


/*
 
 Issues list
 1 OTP is coming login - Unable to get Please explain
 2 Invite pop up data integration and UI design - Done
 3 City from map set on top  - Done
 4 Keyboard do not hide unable to click on save button - Done
 5 On edit event alarm and notification button do not show - Done
 6 on return to edit event to add more images - save isnt working - Not getting issue please elaborate
 7 Fix city state,postal code spacing - done
 8 Message from view event and pass data - Done
 9 Vertical Line spacing of address in view event - Done
 10 Hide Tickets on websites - Done
 11 Location pop up after slide show pop up - Done
 12 Proper data set on map on View map and Create Map - Done
 11 Add cross button on map marker view and chnage marker icon and map style- Done
 13 replace Shareview controller files in code - Done
 14 When address again select from map do not set proper - Done
 
 */










