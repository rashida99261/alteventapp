//
//  PastEventsViewController.h
//  Event App
//
//  Created by Zaeem Khatib on 06/11/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PastEventsViewController : UIViewController

@end

NS_ASSUME_NONNULL_END
