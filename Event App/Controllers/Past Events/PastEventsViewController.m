//
//  PastEventsViewController.m
//  Event App
//
//  Created by Zaeem Khatib on 06/11/18.
//  Copyright © 2018 Applify Tech Pvt Ltd. All rights reserved.
//

#import "PastEventsViewController.h"
#import "EventDetailsViewController.h"
#import "LandingViewController.h"

@interface PastEventsViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@end

@implementation PastEventsViewController

{
    UITableView *tableMyEvents;
    NSMutableArray *arrayMyEvents;
    UISearchBar *searchBar;
    NSDateFormatter *dateFormatter;
    NSMutableArray *arrayAllPastEvents;
}

#pragma mark
#pragma mark View Will Appear and Disappear
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self getAllMyEvents];
}

-(void)getAllMyEvents
{
    NSString *current_date = [UIFunction getCurrentDateInString];
    NSString *user_email = [NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults]valueForKey:kLoginData] valueForKey:@"user_email"]];
    arrayMyEvents = [[DataManager getSharedInstance]Select_MyEventsWithEmail:user_email andDate:current_date forMyEvents:true];
    NSLog(@"______ %@",arrayMyEvents);
    arrayAllPastEvents = [[NSMutableArray alloc] init];
    [self filterPastEvents];
    [self sortEventsArray];
    [tableMyEvents reloadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

#pragma mark
#pragma mark View Did Load
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    arrayMyEvents = [[NSMutableArray alloc]init];
    
    UIImageView *imgViewLogo = [UIFunction createUIImageView:CGRectMake([UIScreen mainScreen].bounds.size.width/2.0-[UIImage imageNamed:@"landing_logo"].size.width/2.0, 20, [UIImage imageNamed:@"landing_logo"].size.width, [UIImage imageNamed:@"landing_logo"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"landing_logo"] isLogo:false];
    [self.view addSubview:imgViewLogo];
    
    UIButton *btnBack = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBack.frame = CGRectMake(0, 20, 44,  44);
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];
    
    
    UILabel *lblHeader = [UIFunction createLable:CGRectMake(btnBack.frame.size.width+btnBack.frame.origin.x, 80, self.view.frame.size.width-2*(btnBack.frame.size.width+btnBack.frame.origin.x), 44) bckgroundColor:[UIColor clearColor] title:@"Past Events" font:[UIFont fontWithName:fontRegular size:18.0] titleColor:[UIColor whiteColor]];
    lblHeader.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:lblHeader];
    
    UIView *blackHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, lblHeader.frame.origin.y + lblHeader.frame.size.height + 10)];
    [blackHeader setBackgroundColor:[UIColor blackColor]];
    [self.view insertSubview:blackHeader atIndex:0];
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, blackHeader.frame.size.height+blackHeader.frame.origin.y, self.view.frame.size.width, 44)];
    [searchBar setPlaceholder:@"Search Events"];
    [searchBar setShowsCancelButton:YES animated:YES];
    searchBar.delegate = self;
    [self.view addSubview:searchBar];
    
    [tableMyEvents removeFromSuperview];
    tableMyEvents = nil;
    tableMyEvents = [self createTableView:CGRectMake(0, searchBar.frame.size.height+searchBar.frame.origin.y+10, self.view.frame.size.width, self.view.frame.size.height-searchBar.frame.size.height-searchBar.frame.origin.y-10) backgroundColor:[UIColor clearColor]];
    [self.view addSubview:tableMyEvents];
    tableMyEvents.tag = 457548;
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
}


#pragma mark
#pragma mark dismissViewController
-(void)backButtonPressed
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)createEventButtonPressed
{
    [self.view endEditing:YES];
    //LandingViewController * = [[LandingViewController alloc]init];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Event" bundle:nil];
    LandingViewController *controller=[storyboard instantiateViewControllerWithIdentifier:@"LandingViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark
#pragma mark Create Table
-(UITableView*) createTableView : (CGRect)frame backgroundColor:(UIColor*)backgroundColor
{
    UITableView *_tableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStylePlain];
    _tableView.dataSource=self;
    _tableView.delegate = self;
    _tableView.backgroundColor=backgroundColor;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.alwaysBounceVertical = NO;
    [_tableView setAllowsSelection:YES];
    return _tableView;
}

#pragma mark
#pragma mark Table View Data Source and Delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (arrayMyEvents.count == 0)
    {
        return 1;
    }
    return arrayMyEvents.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (arrayMyEvents.count == 0)
    {
        return tableView.frame.size.height;
    }
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell= (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    else
    {
        UIView *subview;
        while ((subview= [[[cell contentView]subviews]lastObject])!=nil)
        {
            [subview removeFromSuperview];
        }
    }
    CGRect frame = [tableView rectForRowAtIndexPath:indexPath];
    
    @try
    {
        if (arrayMyEvents.count == 0)
        {
            NSString *title = ([searchBar.text length] > 0) ? @"No matches found." : @"No Events yet!";
            UILabel *lblNoEvents = [self.view viewWithTag:9999];
            
            if (lblNoEvents == nil) {
                
                lblNoEvents = [UIFunction createLable:CGRectMake(10, frame.size.height/2.0-20.0, frame.size.width-20, 40) bckgroundColor:[UIColor clearColor] title:title font:[UIFont fontWithName:fontLight size:18.0] titleColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];
                lblNoEvents.textAlignment = NSTextAlignmentCenter;
                lblNoEvents.tag = 9999;
                [self.view addSubview:lblNoEvents];
                
                UIButton *btnCreateEvent = [UIButton buttonWithType:UIButtonTypeCustom];
                btnCreateEvent.frame = CGRectMake((frame.size.width / 2) - 22, lblNoEvents.frame.origin.y + lblNoEvents.frame.size.height, 44,  44);
                [btnCreateEvent setImage:[UIImage imageNamed:@"create_event"] forState:UIControlStateNormal];
                [btnCreateEvent addTarget:self action:@selector(createEventButtonPressed) forControlEvents:UIControlEventTouchUpInside];
                [self.view addSubview:btnCreateEvent];
            }
            
            [lblNoEvents setText:title];
        }
        else
        {
            UILabel *lblNoEvents = [self.view viewWithTag:9999];
            if (lblNoEvents != nil) {
                
                [lblNoEvents removeFromSuperview];
            }
            NSString *event_name = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_name"]];
            NSString *event_start_time = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_start_time"]];
            if (event_start_time.length == 0)
            {
                event_start_time = @"";
            }
            NSString *event_date = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_date"]];
            if (event_date.length == 0)
            {
                event_date = @"";
            }
            else
            {
                if (event_start_time.length > 0) {
                    
                    event_start_time = [NSString stringWithFormat:@"%@, ", event_start_time];
                }
                
                event_date = [self changeDateFormatter:event_date];
            }
            
            NSString *event_date_time = [NSString stringWithFormat:@"%@%@",event_start_time, event_date];
            
            
            UIView *viewDivider = [UIFunction createUIViews:CGRectMake(0, 0, frame.size.width, 1) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
            [cell.contentView addSubview:viewDivider];
            
            
            UIImageView *imgViewNext = [UIFunction createUIImageView:CGRectMake(frame.size.width-10-[UIImage imageNamed:@"next_black"].size.width, frame.size.height/2.0-[UIImage imageNamed:@"next_black"].size.height/2.0, [UIImage imageNamed:@"next_black"].size.width, [UIImage imageNamed:@"next_black"].size.height) backgroundColor:[UIColor clearColor] image:[UIImage imageNamed:@"next_black"] isLogo:NO];
            [cell.contentView addSubview:imgViewNext];
            
            
            if (arrayMyEvents.count-1 == indexPath.row)
            {
                UIView *viewDividerBottom = [UIFunction createUIViews:CGRectMake(0, frame.size.height-1, frame.size.width, 1) bckgroundColor:[UIColor colorWithRed:242.0/255 green:242.0/255 blue:242.0/255 alpha:1.0]];
                [cell.contentView addSubview:viewDividerBottom];
            }
            
            CGFloat eventNameY = event_date_time.length == 0 ? 25 : 10;
            
            UILabel *lblEventName = [UIFunction createLable:CGRectMake(10, eventNameY, imgViewNext.frame.origin.x-20, 20) bckgroundColor:[UIColor clearColor] title:event_name font:[UIFont fontWithName:fontBold size:16.0] titleColor:[UIColor blackColor]];
            [cell.contentView addSubview:lblEventName];
            
            UILabel *lblEventDateTime = [UIFunction createLable:CGRectMake(lblEventName.frame.origin.x, lblEventName.frame.size.height+lblEventName.frame.origin.y, lblEventName.frame.size.width, lblEventName.frame.size.height) bckgroundColor:[UIColor clearColor] title:event_date_time font:[UIFont fontWithName:fontLight size:14.0] titleColor:[UIColor colorWithRed:136.0/255 green:136.0/255 blue:136.0/255 alpha:1.0]];
            [cell.contentView addSubview:lblEventDateTime];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception is %@",exception.description);
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.userInteractionEnabled = true;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (arrayMyEvents.count > 0)
    {
        EventDetailsViewController *controller = [[EventDetailsViewController alloc]init];
        controller.event_id = [NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_id"]];
        [self.navigationController pushViewController:controller animated:true];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        BOOL isDelete = [[DataManager getSharedInstance]deleteEventWithId:[NSString stringWithFormat:@"%@",[[arrayMyEvents objectAtIndex:indexPath.row] valueForKey:@"event_id"]]];
        if (isDelete == true)
        {
            [self getAllMyEvents];
        }
    }
}

-(NSString*)changeDateFormatter : (NSString*)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM-dd-yyyy"];
    NSDate *date = [dateFormatter dateFromString:dateString];
    dateFormatter.dateFormat = @"MMM dd, yyyy";
    NSLocale *locale = [[NSLocale alloc]initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    NSString *convertedate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:date]];
    return convertedate;
}

- (void)filterPastEvents
{
    NSMutableArray *pastEvents = [[NSMutableArray alloc] init];
    NSDate *today = [[NSDate alloc] init];
    NSString *todayString = [dateFormatter stringFromDate:today];
    NSDate *formattedTodayDate = [dateFormatter dateFromString:todayString];
    [arrayMyEvents enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
       
        NSString *event_date = [NSString stringWithFormat:@"%@",[obj valueForKey:@"event_date"]];
        
        if ([event_date length] > 0) {
            
            NSDate *eventDate = [self->dateFormatter dateFromString:event_date];
            if (eventDate != nil) {
                
                if ([formattedTodayDate compare:eventDate] == NSOrderedDescending) {
                    
                    [pastEvents addObject:obj];
                }
            }
        }
    }];
    
    arrayMyEvents = pastEvents;
    arrayAllPastEvents = arrayMyEvents;
}

#pragma mark
#pragma mark Search Bar Delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self performSearch:searchText];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar setText:@""];
    [searchBar endEditing:YES];
    [self performSearch:searchBar.text];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self performSearch:searchBar.text];
    [searchBar endEditing:YES];
}

- (void)performSearch: (NSString *)searchText
{
    if ([searchText length] > 0) {
        
        NSArray *filteredarray = [arrayAllPastEvents filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(event_name CONTAINS[cd] %@)", searchText]];
        arrayMyEvents = [filteredarray mutableCopy];
    }
    else {
        
        arrayMyEvents = arrayAllPastEvents;
    }
    
    [tableMyEvents reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)sortEventsArray {
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"MM-dd-yyyy hh:mm a"];
    NSArray *sortedArray = [arrayMyEvents sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
        
        NSString *strDate = [obj1 valueForKey:@"event_date"];
        NSString *strTime = [obj1 valueForKey:@"event_start_time"];
        NSString *strTime2 = [obj2 valueForKey:@"event_start_time"];
        NSString *strDate2 = [obj2 valueForKey:@"event_date"];
        NSString *eventName = [obj1 valueForKey:@"event_name"];
        NSString *eventName2 = [obj2 valueForKey:@"event_name"];
        
        if ([strTime length] == 0 && [strTime2 length] > 0) {
            
            return YES;
        }
        
        else if ([strTime2 length] == 0 && [strTime length] > 0) {
            
            return NO;
        }
        
        else if ([strTime length] == 0 && [strTime2 length] == 0) {
            
            NSComparisonResult result = [eventName caseInsensitiveCompare:eventName2];
            return result == NSOrderedDescending;
        }
        
        NSString *strWholeDate = [NSString stringWithFormat:@"%@ %@", strDate, strTime];
        NSString *strWholeDate2 = [NSString stringWithFormat:@"%@ %@", strDate2, strTime2];
        NSDate *date = [df dateFromString:strWholeDate];
        NSDate *date2 = [df dateFromString:strWholeDate2];
        return [date compare:date2];
    }];
    
    arrayMyEvents = [sortedArray mutableCopy];
}

@end
